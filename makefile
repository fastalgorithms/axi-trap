PROJECT = int2

FC = gfortran
FFLAGS = -w -fopenmp -std=legacy -O2 -mcmodel=medium
FLINK = gfortran -w -llapack -lblas -fopenmp -std=legacy

export OMP_NUM_THREADS=32
export OMP_STACKSIZE=2048M


.PHONY: all clean list


#
# use only the file part of the filename, then manually specify
# the build location
#

# SOURCES =  ../src/axikernels.f90 \
#   ../src/kernels.f \
#   ../src/axideco.f \
#   ../src/axideco_new.f90 \
#   ../src/axiplot.f90 \
#   ../src/axi_geometries.f90 \
#   ../src/dalpert.f90 \
#   ../src/zalpert.f90 \
#   ../src/dalpert_modes.f90 \
#   ../src/zalpert_modes.f90 \
#   ../src/mfie.f \
#   ../src/mfie_new.f90 \
#   ../src/debye.f \
#   ../src/debye_new.f90 \
#   ../src/jh2bcompute.f \
#   ../utils/lapack_wrap.f90 \
#   ../utils/prini.f \
#   ../utils/adapgaus_quad.f \
#   ../utils/cadapgaus_quad.f \
#   ../utils/cadapgau_k16.f \
#   ../utils/legeexps.f \
#   ../utils/chebexps.f \
#   ../utils/dfft.f \
#   ../utils/qcorrand.f90 \
#   ../utils/corrand.f \
#   ../utils/next235.f \
#   ../utils/elliptic.f \
#   ../utils/elliptic_ke.f \
#   ../utils/q2lege.f90 \
#   ../utils/pplot.f \
#   ../utils/cjbvals.f \
#   ../utils/gammanew_eval.f \
#   ../utils/troisplot.f \
#   ../utils/quaplot.f \
#   ../utils/alpert.f \
#   ../utils/csvdpiv.f \
# 	../utils/cqrsolve.f \
#   ../utils/dotcross3d.f \
#   ../utils/anaresa.f \
#   ../hk/fdiffc.f \
#   ../hk/surfdivaxi.f \
#   ../hk/surflapaxi.f \
#   ../hk/surfgradaxi.f \
#   ../hk/surfcurlaxi.f \
#   ../hk/zgecoall.f \
#   ../hk/ode_periodic_surflap.f \
#   ../hk/surflapinteq.f \
#   ../hk/rotviarecur3.f \
#   ../hk/rotprojvar.f \
#   ../hk/yrecursion.f \
#   ../hk/hjfuns3d.f \
#   ../hk/emdyadic.f \
#   ../hk/projections.f \
#   ../hk/emrouts3.f \
#   ../hk/emabrot3.f \
#   ../hk/xrecursion.f \
#   ../hk/helmrouts2.f






# the below is needed for maxwell/muller.f, but needs to be pulled in from other directories
#
#            ../helmrot/test65.f \
#            ../aximfie/test51.f \
#            ../alpertmat/test15.f \
#            ../axidebye/test112.f \
#            ../hellskitchen/Greengard/Trapquads2d/formsysmatkrc2.f \
#            ../hellskitchen/Greengard/Trapquads2d/slpgen_hybrid.f \
#            ../hellskitchen/Greengard/Trapquads2d/slpgenc_hybrid.f \
#            ../hellskitchen/Greengard/Trapquads2d/slpgenc2.f \
#            ../hellskitchen/Greengard/Trapquads2d/dlpgenc2.f \
#            ../hellskitchen/Greengard/Trapquads2d/fdiffc.f \
#            ../hellskitchen/Greengard/Trapquads2d/surfdivaxi.f \
#            ../hellskitchen/Greengard/Trapquads2d/surflapaxi.f \
#            ../hellskitchen/Greengard/Trapquads2d/surfgradaxi.f \
#            ../hellskitchen/Greengard/Trapquads2d/surfcurlaxi.f \
#            ../hellskitchen/Greengard/Trapquads2d/edebye.f \



OBJECTS = $(patsubst %.f,%.o,$(patsubst %.f90,%.o,$(SOURCES)))


.PHONY: usage
default: usage

usage:
	@echo "------------------------------------------------------------------------"
	@echo "Makefile for axi-trap exampls. Specify what to make:"
	@echo ""
	@echo "  make testing/"
	@echo "         axikernels   test the modal green's function eval"
	@echo ""
	@echo ""
	@echo "  make clean              clean out all the *.o and executable files"
	@echo ""
	@echo "For faster (multicore) making, append the flag -j"
	@echo "------------------------------------------------------------------------"



%.o : %.f
	$(FC) -c $(FFLAGS) $< -o $@

%.o : %.f90
	$(FC) -c $(FFLAGS) $< -o $@






#
# Below is a list of all the various examples
#

FINAL = ../final-codes/src

#### list of options ####

# test the modal helmholtz kernel evaluators
testing/axikernels: src/axikernels.o utils/prini.o utils/dfft.o utils/next235.o \
    utils/adapgaus_quad.o utils/cadapgaus_quad.o utils/elliptic.o utils/cadapgau_k16.o
	mkdir -p build
	$(FLINK) testing/test_axikernels.f90 -o build/test_axikernels $^
	(cd build; ./test_axikernels)


# #### Laplace ####

# # testing the Laplace Green's identity
# laplace/lap_greens: laplace/lap_greens.o $(OBJECTS)
# 	rm -f lap_greens
# 	$(FLINK) -o lap_greens laplace/lap_greens.o $(OBJECTS)
# 	./lap_greens

# # solve the exterior Laplace Dirichlet problem
# laplace/lap_g1_ext_dir: laplace/lap_g1_ext_dir.o $(OBJECTS)
# 	rm -f lap_g1_ext_dir
# 	$(FLINK) -o lap_g1_ext_dir laplace/lap_g1_ext_dir.o $(OBJECTS)
# 	./lap_g1_ext_dir

# # solve the interior Laplace Dirichlet problem
# laplace/lap_g1_int_dir: laplace/lap_g1_int_dir.o $(OBJECTS)
# 	rm -f lap_g1_int_dir
# 	$(FLINK) -o lap_g1_int_dir laplace/lap_g1_int_dir.o $(OBJECTS)
# 	./lap_g1_int_dir





# #### Helmholtz ####

# # test the modal helmholtz kernel evaluators
# helmholtz/helm_kernels: helmholtz/helm_kernels.o $(OBJECTS)
# 	rm -f helmkernels
# 	$(FLINK) -o helm_kernels helmholtz/helm_kernels.o $(OBJECTS)
# 	./helm_kernels

# # interior genus 1 helmholtz dirichlet problem
# helmholtz/helm_g1_int_dir: helmholtz/helm_g1_int_dir.o $(OBJECTS)
# 	rm -f helm_g1_int_dir
# 	$(FLINK) -o helm_g1_int_dir helmholtz/helm_g1_int_dir.o $(OBJECTS)
# 	./helm_g1_int_dir

# # testing the Helmholtz Green's identity
# helmholtz/helm_greens: helmholtz/helm_greens.o $(OBJECTS)
# 	rm -f helm_greens
# 	$(FLINK) -o helm_greens helmholtz/helm_greens.o $(OBJECTS)
# 	./helm_greens





# #### Maxwell ####


# maxwell/mfie_g1_ext: maxwell/mfie_g1_ext.o $(OBJECTS)
# 	rm -f mfie_g1_ext
# 	$(FLINK) -o mfie_g1_ext maxwell/mfie_g1_ext.o $(OBJECTS)
# 	./mfie_g1_ext


# #maxwell/debye_pec_pipe: maxwell/debye_pec_pipe.o $(OBJECTS)
# #	rm -f debye_pec_pipe
# #	$(FLINK) -o debye_pec_pipe maxwell/debye_pec_pipe.o $(OBJECTS)
# #	./debye_pec_pipe

# maxwell/debye_pec_test: maxwell/debye_pec_test.o $(OBJECTS)
# 	rm -f debye_pec_test
# 	$(FLINK) -o debye_pec_test maxwell/debye_pec_test.o $(OBJECTS)
# 	./debye_pec_test

# maxwell/debye_pec_test1: maxwell/debye_pec_test1.o $(OBJECTS)
# 	rm -f debye_pec_test1
# 	$(FLINK) -o debye_pec_test1 maxwell/debye_pec_test1.o $(OBJECTS)
# 	./debye_pec_test1



# #### Beltrami ####




clean:
	rm -f testing/*.o
	rm -f src/*.o
	rm -f build/*



