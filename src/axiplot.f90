subroutine axi_platonic(itype, nover, maxtri, &
    npatches, triainfo, isides)
  implicit real *8 (a-h,o-z)
  integer :: isides(*)
  real *8 :: triainfo(3,3,maxtri)

  real *8 :: verts(3,100), ifaces(3,100), verts1(3,3)
  real *8 :: verts2(3,3), verts3(3,3), verts4(3,3)

  !
  ! this routine returns minimal (but possibly oversampled)
  ! triangulations of the platonic solids (i.e. a cube that isn't
  ! oversampled has 12 triangles)
  !
  !      Input:
  ! itype - determine the platonic solid to return
  !      1 - tetrahedron
  !      2 - cube
  !      3 - octahedron
  !      4 - icosahedron
  ! nover - determines how many times to oversample the skeleton
  !    triangulation (every oversampling generates 4 time as many
  !    triangles)
  ! maxtri - maximum number of allowable triangles
  !
  !      Output:
  ! npatches - number of triangles
  ! triainfo - triangle info
  ! isides - the side that the (possibly oversampled) triangle
  !     belongs to (relative to original triangulation), arbitrarily
  !     ordered.
  !
  !
  call axi_rsolid(itype, verts, nverts, ifaces, nfaces)
  call axi_gentriainfo(verts, nverts, ifaces, nfaces, triainfo)
  npatches = nfaces

  do i = 1,nfaces
    isides(i) = i
  end do

  if (nover .eq. 0) then
    return
  end if

  !
  ! go about oversampling the triangles
  !
  do ijk = 1,nover

    call prinf('ijk = *', ijk, 1)

    nnn = npatches
    if (nnn*4 .gt. maxtri) then
      call prinf('maxtri exceeded, maxtri = *', maxtri, 1)
      stop
    end if

    do i = 1,nnn

      call axi_refine4_flat(triainfo(1,1,i), verts1, &
          verts2, verts3, verts4)
      do j = 1,3
        do k = 1,3
          triainfo(k,j,i) = verts1(k,j)
          triainfo(k,j,npatches+1) = verts2(k,j)
          triainfo(k,j,npatches+2) = verts3(k,j)
          triainfo(k,j,npatches+3) = verts4(k,j)
        end do
        isides(npatches+1) = isides(i)
        isides(npatches+2) = isides(i)
        isides(npatches+3) = isides(i)
      end do

      npatches = npatches +3
    end do
  end do

  return
end subroutine axi_platonic





subroutine axi_rsolid(itype, verts, nverts, ifaces, nfaces)
  implicit real *8 (a-h,o-z)
  dimension verts(3,*),ifaces(3,*)
  !
  ! This subroutine returns the vertices and faces of regular (and
  ! not so regular) polyhedra.
  !
  !         Input:
  ! itype - determine the platonic solid to return
  !      1 - tetrahedron
  !      2 - cube
  !      3 - octahedron
  !      4 - icosahedron
  !

  if( itype .eq. 1 ) then
    !c
    !c       ... tetrahedron
    !c
    nverts=4
    nfaces=4

    ! ... vertices
    kk=1
    verts(1,kk)=-1
    verts(2,kk)=-1/sqrt(3.0d0)
    verts(3,kk)=-1/sqrt(6.0d0)

    kk=kk+1
    verts(1,kk)=+1
    verts(2,kk)=-1/sqrt(3.0d0)
    verts(3,kk)=-1/sqrt(6.0d0)

    kk=kk+1
    verts(1,kk)=0
    verts(2,kk)=+2/sqrt(3.0d0)
    verts(3,kk)=-1/sqrt(6.0d0)

    kk=kk+1
    verts(1,kk)=0
    verts(2,kk)=0
    verts(3,kk)=+3/sqrt(6.0d0)

    !... faces
    kk=1
    ifaces(1,kk)=2
    ifaces(2,kk)=1
    ifaces(3,kk)=3

    kk=kk+1
    ifaces(1,kk)=1
    ifaces(2,kk)=2
    ifaces(3,kk)=4

    kk=kk+1
    ifaces(1,kk)=2
    ifaces(2,kk)=3
    ifaces(3,kk)=4

    kk=kk+1
    ifaces(1,kk)=3
    ifaces(2,kk)=1
    ifaces(3,kk)=4

  else if (itype .eq. 2) then

    !
    ! a cube
    !
    nverts=8
    nfaces=12

    ! ... vertices
    kk=1
    verts(1,kk)=-1
    verts(2,kk)=-1
    verts(3,kk)=-1

    kk=kk+1
    verts(1,kk)=+1
    verts(2,kk)=-1
    verts(3,kk)=-1

    kk=kk+1
    verts(1,kk)=+1
    verts(2,kk)=+1
    verts(3,kk)=-1

    kk=kk+1
    verts(1,kk)=-1
    verts(2,kk)=+1
    verts(3,kk)=-1

    kk=kk+1
    verts(1,kk)=-1
    verts(2,kk)=-1
    verts(3,kk)=+1

    kk=kk+1
    verts(1,kk)=+1
    verts(2,kk)=-1
    verts(3,kk)=+1

    kk=kk+1
    verts(1,kk)=+1
    verts(2,kk)=+1
    verts(3,kk)=+1

    kk=kk+1
    verts(1,kk)=-1
    verts(2,kk)=+1
    verts(3,kk)=+1

    ! ... faces
    kk=1
    ifaces(1,kk)=2
    ifaces(2,kk)=1
    ifaces(3,kk)=3

    kk=kk+1
    ifaces(1,kk)=4
    ifaces(2,kk)=3
    ifaces(3,kk)=1

    kk=kk+1
    ifaces(1,kk)=1
    ifaces(2,kk)=2
    ifaces(3,kk)=6

    kk=kk+1
    ifaces(1,kk)=6
    ifaces(2,kk)=5
    ifaces(3,kk)=1

    kk=kk+1
    ifaces(1,kk)=2
    ifaces(2,kk)=3
    ifaces(3,kk)=7

    kk=kk+1
    ifaces(1,kk)=7
    ifaces(2,kk)=6
    ifaces(3,kk)=2

    kk=kk+1
    ifaces(1,kk)=3
    ifaces(2,kk)=4
    ifaces(3,kk)=8

    kk=kk+1
    ifaces(1,kk)=8
    ifaces(2,kk)=7
    ifaces(3,kk)=3

    kk=kk+1
    ifaces(1,kk)=4
    ifaces(2,kk)=1
    ifaces(3,kk)=5

    kk=kk+1
    ifaces(1,kk)=5
    ifaces(2,kk)=8
    ifaces(3,kk)=4

    kk=kk+1
    ifaces(1,kk)=5
    ifaces(2,kk)=6
    ifaces(3,kk)=7

    kk=kk+1
    ifaces(1,kk)=7
    ifaces(2,kk)=8
    ifaces(3,kk)=5

  else if (itype .eq. 3) then

    !
    ! an octahedron
    !
    nverts=6
    nfaces=8

    ! ... vertices
    kk=1
    verts(1,kk)=-1
    verts(2,kk)=0
    verts(3,kk)=0

    kk=kk+1
    verts(1,kk)=0
    verts(2,kk)=-1
    verts(3,kk)=0

    kk=kk+1
    verts(1,kk)=+1
    verts(2,kk)=0
    verts(3,kk)=0

    kk=kk+1
    verts(1,kk)=0
    verts(2,kk)=+1
    verts(3,kk)=0

    kk=kk+1
    verts(1,kk)=0
    verts(2,kk)=0
    verts(3,kk)=+1

    kk=kk+1
    verts(1,kk)=0
    verts(2,kk)=0
    verts(3,kk)=-1

    ! ... faces
    kk=1
    ifaces(1,kk)=1
    ifaces(2,kk)=2
    ifaces(3,kk)=5

    kk=kk+1
    ifaces(1,kk)=2
    ifaces(2,kk)=3
    ifaces(3,kk)=5

    kk=kk+1
    ifaces(1,kk)=3
    ifaces(2,kk)=4
    ifaces(3,kk)=5

    kk=kk+1
    ifaces(1,kk)=4
    ifaces(2,kk)=1
    ifaces(3,kk)=5

    kk=kk+1
    ifaces(1,kk)=2
    ifaces(2,kk)=1
    ifaces(3,kk)=6

    kk=kk+1
    ifaces(1,kk)=3
    ifaces(2,kk)=2
    ifaces(3,kk)=6

    kk=kk+1
    ifaces(1,kk)=4
    ifaces(2,kk)=3
    ifaces(3,kk)=6

    kk=kk+1
    ifaces(1,kk)=1
    ifaces(2,kk)=4
    ifaces(3,kk)=6

  else if (itype .eq. 4) then

    !
    ! an icosahedron
    !
    nverts=12
    nfaces=20

    ! . . . vertices
    !
    done=1
    pi=4*atan(done)

    do i=1,5
      verts(1,i)=cos(2*pi*i/5.0d0)
      verts(2,i)=sin(2*pi*i/5.0d0)
      verts(3,i)=0
    end do

    !
    ! find the top vertex
    !
    s=sqrt((verts(1,2)-verts(1,1))**2+(verts(2,2)-verts(2,1))**2)
    d=sqrt(s**2-1)

    verts(1,6)=0
    verts(2,6)=0
    verts(3,6)=d

    x=(1-d**2)/(2*d)
    do i=1,6
      verts(3,i)=verts(3,i)+x
    end do

    do i=1,6
      verts(1,i+6)=-verts(1,i)
      verts(2,i+6)=-verts(2,i)
      verts(3,i+6)=-verts(3,i)
    end do

    !
    ! ... faces
    kk=1
    ifaces(1,kk)=1
    ifaces(2,kk)=2
    ifaces(3,kk)=6

    kk=kk+1
    ifaces(1,kk)=2
    ifaces(2,kk)=3
    ifaces(3,kk)=6

    kk=kk+1
    ifaces(1,kk)=3
    ifaces(2,kk)=4
    ifaces(3,kk)=6

    kk=kk+1
    ifaces(1,kk)=4
    ifaces(2,kk)=5
    ifaces(3,kk)=6

    kk=kk+1
    ifaces(1,kk)=5
    ifaces(2,kk)=1
    ifaces(3,kk)=6

    kk=kk+1
    ifaces(1,kk)=8
    ifaces(2,kk)=7
    ifaces(3,kk)=12

    kk=kk+1
    ifaces(1,kk)=9
    ifaces(2,kk)=8
    ifaces(3,kk)=12

    kk=kk+1
    ifaces(1,kk)=10
    ifaces(2,kk)=9
    ifaces(3,kk)=12

    kk=kk+1
    ifaces(1,kk)=11
    ifaces(2,kk)=10
    ifaces(3,kk)=12

    kk=kk+1
    ifaces(1,kk)=7
    ifaces(2,kk)=11
    ifaces(3,kk)=12

    kk=kk+1
    ifaces(1,kk)=2
    ifaces(2,kk)=1
    ifaces(3,kk)=10

    kk=kk+1
    ifaces(1,kk)=3
    ifaces(2,kk)=2
    ifaces(3,kk)=11

    kk=kk+1
    ifaces(1,kk)=4
    ifaces(2,kk)=3
    ifaces(3,kk)=7

    kk=kk+1
    ifaces(1,kk)=5
    ifaces(2,kk)=4
    ifaces(3,kk)=8

    kk=kk+1
    ifaces(1,kk)=1
    ifaces(2,kk)=5
    ifaces(3,kk)=9

    kk=kk+1
    ifaces(1,kk)=8
    ifaces(2,kk)=9
    ifaces(3,kk)=5

    kk=kk+1
    ifaces(1,kk)=9
    ifaces(2,kk)=10
    ifaces(3,kk)=1

    kk=kk+1
    ifaces(1,kk)=10
    ifaces(2,kk)=11
    ifaces(3,kk)=2

    kk=kk+1
    ifaces(1,kk)=11
    ifaces(2,kk)=7
    ifaces(3,kk)=3

    kk=kk+1
    ifaces(1,kk)=7
    ifaces(2,kk)=8
    ifaces(3,kk)=4

  endif

  !
  ! scale the thing so that each edge has length 1
  !
  r=sqrt(verts(1,1)**2+verts(2,1)**2+verts(3,1)**2)

  do i=1,nverts
    verts(1,i)=verts(1,i)/r
    verts(2,i)=verts(2,i)/r
    verts(3,i)=verts(3,i)/r
  end do

  return
end subroutine axi_rsolid





subroutine axi_gentriainfo(verts,nverts,ifaces,nfaces,triainfo)
  implicit real *8 (a-h,o-z)
  dimension verts(3,1),ifaces(3,1),triainfo(3,3,1)

  do i=1,nfaces

    triainfo(1,1,i)=verts(1,ifaces(1,i))
    triainfo(2,1,i)=verts(2,ifaces(1,i))
    triainfo(3,1,i)=verts(3,ifaces(1,i))

    triainfo(1,2,i)=verts(1,ifaces(2,i))
    triainfo(2,2,i)=verts(2,ifaces(2,i))
    triainfo(3,2,i)=verts(3,ifaces(2,i))

    triainfo(1,3,i)=verts(1,ifaces(3,i))
    triainfo(2,3,i)=verts(2,ifaces(3,i))
    triainfo(3,3,i)=verts(3,ifaces(3,i))

  end do

  return
end subroutine axi_gentriainfo





subroutine axi_vtk_flat_scalars(iw, ntri, xtri1s, m, sigma, title)
  implicit real *8 (a-h,o-z)
  real *8 :: xtri1s(3,3,ntri), sigma(m,3,ntri)
  character(len=*) :: title

  character(len=1024) :: filename, dataname, valsname, imgname
  character(len=1024) :: trisname, vecsname, centname
  character(len=12) :: fmt, fmt3, fmt4
  character(len=25) :: fmt2

  !
  ! This routine plots a sequence of FLAT TRIANGLES with surface
  ! color vals.
  !
  ! Input:
  !   iw - plot number, controls the filenames
  !   ntri - number of flat triangles
  !   xtri1s - full triangle information
  !   sigma - function to plot, tabulated at corners of triangles
  !
  ! Output:
  !   a vtk file plotIW.vtk that can be plotted in paraview
  !
  !

  if (iw .lt. 10) then
    fmt = "(A4,I1,A4)"
  elseif (iw .lt. 100) then
    fmt = "(A4,I2,A4)"
  elseif (iw .lt. 1000) then
    fmt = "(A4,I3,A4)"
  elseif (iw .lt. 10000) then
    fmt = "(A4,I4,A4)"
  end if

  write(filename, fmt) 'plot', iw, '.vtk'

  !
  ! write the vtk plotting script
  !
  iunit1 = 877
  open(unit = iunit1, file=trim(filename), status='replace')

  write(iunit1,'(a)') "# vtk DataFile Version 3.0"
  write(iunit1,'(a)') trim(title)
  write(iunit1,'(a)') "ASCII"
  write(iunit1,'(a)') "DATASET UNSTRUCTURED_GRID"
  write(iunit1,'(a,i8,a)') "POINTS ", ntri*3, " float"

  fmt2 = "(E11.5,2X,E11.5,2X,E11.5)"
  do i = 1,ntri
    write(iunit1,fmt2) xtri1s(1,1,i), xtri1s(2,1,i), xtri1s(3,1,i)
    write(iunit1,fmt2) xtri1s(1,2,i), xtri1s(2,2,i), xtri1s(3,2,i)
    write(iunit1,fmt2) xtri1s(1,3,i), xtri1s(2,3,i), xtri1s(3,3,i)
  end do


  write(iunit1,'(a,i8,i8)') "CELLS ", ntri, ntri*4

  do i = 1,ntri
    i1 = 3*(i-1) + 1
    write(iunit1,'(a,i8,i8,i8)') "3 ", i1-1, i1, i1+1
  end do

  write(iunit1,'(a,i8)') "CELL_TYPES ", ntri
  do i = 1,ntri
    write(iunit1,'(a)') "5"
  end do

  write(iunit1,'(a)') ""
  write(iunit1,'(a,i8)') "POINT_DATA ", ntri*3
  write(iunit1,'(a,i4)') "SCALARS scalar_function float ", m
  write(iunit1,'(a)') "LOOKUP_TABLE default"
  do i = 1,ntri
    do j = 1,3
      do k = 1,m
        write(iunit1,'(E11.5)') sigma(k,j,i)
      end do
    end do
  end do

  close(iunit1)

  return
end subroutine axi_vtk_flat_scalars





subroutine axi_vtk_flat(iw, ntri, xtri1s, title)
  implicit real *8 (a-h,o-z)
  real *8 :: xtri1s(3,3,ntri)
  character(len=*) :: title

  character(len=1024) :: filename, dataname, valsname, imgname
  character(len=1024) :: trisname, vecsname, centname
  character(len=12) :: fmt, fmt3, fmt4
  character(len=25) :: fmt2

  !
  ! This routine plots a sequence of FLAT TRIANGLES with surface
  ! color vals.
  !
  ! Input:
  !   iw - plot number, controls the filenames
  !   ntri - number of flat triangles
  !   xtri1s - full triangle information
  !
  ! Output:
  !   files which can be executed in matlab to plot the surface
  !
  !

  if (iw .lt. 10) then
    fmt = "(A4,I1,A4)"
    fmt3 = "(A8,I1,A4)"
    fmt4 = "(A5,I1,A4)"
  elseif (iw .lt. 100) then
    fmt = "(A4,I2,A4)"
    fmt3 = "(A8,I2,A4)"
    fmt4 = "(A5,I2,A4)"
  elseif (iw .lt. 1000) then
    fmt = "(A4,I3,A4)"
    fmt3 = "(A8,I3,A4)"
    fmt4 = "(A5,I3,A4)"
  elseif (iw .lt. 10000) then
    fmt = "(A4,I4,A4)"
    fmt3 = "(A8,I4,A4)"
    fmt4 = "(A5,I4,A4)"
  end if

  write(filename, fmt) 'plot', iw, '.vtk'

  !
  ! write the vtk plotting script
  !
  iunit1 = 877
  open(unit = iunit1, file=trim(filename), status='replace')

  write(iunit1,'(a)') "# vtk DataFile Version 3.0"
  write(iunit1,'(a)') "vtk output"
  write(iunit1,'(a)') "ASCII"
  !write(iunit1,'(a)') "DATASET POLYDATA"
  write(iunit1,'(a)') "DATASET UNSTRUCTURED_GRID"
  write(iunit1,'(a,i8,a)') "POINTS ", ntri*3, " float"

  fmt2 = "(E11.5,2X,E11.5,2X,E11.5)"
  do i = 1,ntri
    write(iunit1,fmt2) xtri1s(1,1,i), xtri1s(2,1,i), xtri1s(3,1,i)
    write(iunit1,fmt2) xtri1s(1,2,i), xtri1s(2,2,i), xtri1s(3,2,i)
    write(iunit1,fmt2) xtri1s(1,3,i), xtri1s(2,3,i), xtri1s(3,3,i)
  end do


  write(iunit1,'(a,i8,i8)') "CELLS ", ntri, ntri*4

  do i = 1,ntri
    i1 = 3*(i-1) + 1
    write(iunit1,'(a,i8,i8,i8)') "3 ", i1-1, i1, i1+1
  end do

  write(iunit1,'(a,i8)') "CELL_TYPES ", ntri
  do i = 1,ntri
    write(iunit1,'(a)') "5"
  end do

  write(iunit1,'(a,i8)') "POINT_DATA ", ntri*3
  write(iunit1,'(a)') "SCALARS scalars float 1"
  write(iunit1,'(a)') "LOOKUP_TABLE default"
  do i = 1,ntri
    do j = 1,3
      write(iunit1,'(E11.5)') xtri1s(3,j,i)
    end do
  end do



  write(iunit1,'(a)') ""
  write(iunit1,'(a)') ""
  write(iunit1,'(a,i8)') "CELL_DATA ", ntri
  write(iunit1,'(a)') "SCALARS scalars float 1"
  write(iunit1,'(a)') "LOOKUP_TABLE default"
  do i = 1,ntri
    write(iunit1,'(E13.5)') (xtri1s(3,1,i) + &
        xtri1s(3,2,i) + xtri1s(3,3,i))/3
  end do







  close(iunit1)




  return
end subroutine axi_vtk_flat





subroutine axi_rectmesh(vert1, vert2, vert3, vert4, nover, maxtri, &
    ntri, triaskel)
  implicit real *8 (a-h,o-z)
  real *8 :: triaskel(3,3,maxtri)
  real *8 :: vert1(3), vert2(3), vert3(3), vert4(3)

  real *8 :: verts(3,100), ifaces(3,100), verts1(3,3)
  real *8 :: verts2(3,3), verts3(3,3), verts4(3,3)

  !
  ! this routine returns a (possibly oversampled) triangular mesh of
  ! the rectangle defined by the four vertices, ordered in counter-clockwise
  ! fashion  -- keep in mind the
  ! triangles are stored with u,v,w components.
  !
  !      Input:
  ! nover - determines how many times to oversample the skeleton
  !    triangulation (every oversampling generates 4 time as many
  !    triangles)
  ! maxtri - maximum number of allowable triangles
  !
  !      Output:
  ! ntri - number of triangles created
  ! triaskel - skeleton mesh info, basically just vertices of each
  !     triangle
  !
  !

  width = umax-umin
  height = vmax-vmin

  !
  ! generate the two original triangles
  !
  ntri = 1
  do i = 1,3
    triaskel(i,1,ntri) = vert1(i)
    triaskel(i,2,ntri) = vert2(i)
    triaskel(i,3,ntri) = vert4(i)
  end do

  ntri = 2
  do i = 1,3
    triaskel(i,1,ntri) = vert3(i)
    triaskel(i,2,ntri) = vert4(i)
    triaskel(i,3,ntri) = vert2(i)
  end do


  !
  ! go about oversampling the triangles
  !
  do ijk = 1,nover

    nnn = ntri
    if (nnn*4 .gt. maxtri) then
      call prinf('maxtri exceeded, maxtri = *', maxtri, 1)
      stop
    end if

    do i = 1,nnn

      call axi_refine4_flat(triaskel(1,1,i), verts1, &
          verts2, verts3, verts4)
      do j = 1,3
        do k = 1,3
          triaskel(k,j,i) = verts1(k,j)
          triaskel(k,j,ntri+1) = verts2(k,j)
          triaskel(k,j,ntri+2) = verts3(k,j)
          triaskel(k,j,ntri+3) = verts4(k,j)
        end do
      end do

      ntri = ntri +3
    end do
  end do

  return
end subroutine axi_rectmesh


subroutine axi_rectmesh0(umin, umax, vmin, vmax, triaskel)
  implicit real *8 (a-h,o-z)
  real *8 :: triaskel(3,3,2)

  !
  ! this routine returns a skeleton mesh with two triangles in
  ! the rectangle [umin,umax] \times [umin,vmax] -- keep in mind the
  ! triangles are stored with u,v,w components, with w=0 (in order to
  ! be consistent with other skeleton meshes)
  !
  !      Input:
  ! umin, umax, vmin, vmax - sets the dimensions of the rectangle
  !
  !      Output:
  ! triaskel - skeleton mesh info, basically just vertices of each
  !     triangle
  !
  !

  triaskel(1,1,1) = umin
  triaskel(2,1,1) = vmin
  triaskel(1,2,1) = umax
  triaskel(2,2,1) = vmin
  triaskel(1,3,1) = umin
  triaskel(2,3,1) = vmax

  triaskel(1,1,2) = umax
  triaskel(2,1,2) = vmax
  triaskel(1,2,2) = umin
  triaskel(2,2,2) = vmax
  triaskel(1,3,2) = umax
  triaskel(2,3,2) = vmin

  ntri = 2
  do n = 1,ntri
    do i = 1,3
      triaskel(3,i,n) = 0
    end do
  end do

  return
end subroutine axi_rectmesh0




subroutine axi_refine4_flat(verts, verts1, verts2, verts3, verts4)
  implicit real *8 (a-h,o-z)
  real *8 :: verts(3,3), verts1(3,3), verts2(3,3), verts3(3,3)
  real *8 :: verts4(3,3)

  real *8 :: xyz12(3), xyz23(3), xyz13(3)

  !
  ! perform a refinement of a flat triangle into four other flat
  ! triangles
  !
  xyz12(1) = (verts(1,1) + verts(1,2))/2
  xyz12(2) = (verts(2,1) + verts(2,2))/2
  xyz12(3) = (verts(3,1) + verts(3,2))/2

  xyz23(1) = (verts(1,2) + verts(1,3))/2
  xyz23(2) = (verts(2,2) + verts(2,3))/2
  xyz23(3) = (verts(3,2) + verts(3,3))/2

  xyz13(1) = (verts(1,1) + verts(1,3))/2
  xyz13(2) = (verts(2,1) + verts(2,3))/2
  xyz13(3) = (verts(3,1) + verts(3,3))/2

  !
  ! first subdivision
  !
  verts1(1,1) = verts(1,1)
  verts1(2,1) = verts(2,1)
  verts1(3,1) = verts(3,1)

  verts1(1,2) = xyz12(1)
  verts1(2,2) = xyz12(2)
  verts1(3,2) = xyz12(3)

  verts1(1,3) = xyz13(1)
  verts1(2,3) = xyz13(2)
  verts1(3,3) = xyz13(3)

  !
  ! second subdivision
  !
  verts2(1,1) = xyz23(1)
  verts2(2,1) = xyz23(2)
  verts2(3,1) = xyz23(3)

  verts2(1,2) = xyz13(1)
  verts2(2,2) = xyz13(2)
  verts2(3,2) = xyz13(3)

  verts2(1,3) = xyz12(1)
  verts2(2,3) = xyz12(2)
  verts2(3,3) = xyz12(3)

  !
  ! third subdivision
  !
  verts3(1,1) = xyz12(1)
  verts3(2,1) = xyz12(2)
  verts3(3,1) = xyz12(3)

  verts3(1,2) = verts(1,2)
  verts3(2,2) = verts(2,2)
  verts3(3,2) = verts(3,2)

  verts3(1,3) = xyz23(1)
  verts3(2,3) = xyz23(2)
  verts3(3,3) = xyz23(3)

  !
  ! fourth subdivision
  !
  verts4(1,1) = xyz13(1)
  verts4(2,1) = xyz13(2)
  verts4(3,1) = xyz13(3)

  verts4(1,2) = xyz23(1)
  verts4(2,2) = xyz23(2)
  verts4(3,2) = xyz23(3)

  verts4(1,3) = verts(1,3)
  verts4(2,3) = verts(2,3)
  verts4(3,3) = verts(3,3)

  return
end subroutine axi_refine4_flat





subroutine axi_vtk_vector(iw, n, xys, nphi, field, title)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,n), field(3,nphi,n)
  character(len=*) :: title

  real *8, allocatable :: triaskel(:,:,:), dvals(:,:,:)

  character(len=1024) :: filename, dataname, valsname, imgname
  character(len=1024) :: trisname, vecsname, centname
  character(len=12) :: fmt, fmt3, fmt4
  character(len=25) :: fmt2

  !
  !
  !

  if (iw .lt. 10) then
    fmt = "(A4,I1,A4)"
  elseif (iw .lt. 100) then
    fmt = "(A4,I2,A4)"
  elseif (iw .lt. 1000) then
    fmt = "(A4,I3,A4)"
  elseif (iw .lt. 10000) then
    fmt = "(A4,I4,A4)"
  end if

  write(filename, fmt) 'plot', iw, '.vtk'


  !
  ! construct the triangles to plot
  !
  done = 1
  pi = 4*atan(done)
  ntri = 2*nphi*n
  allocate(triaskel(3,3,ntri))
  hp = 2*pi/nphi
  itri = 0
  do j = 1,nphi
    phi = (j-1)*hp
    do i = 1,n
      itri = itri + 1
      triaskel(1,1,itri) = xys(1,i)*cos(phi)
      triaskel(2,1,itri) = xys(1,i)*sin(phi)
      triaskel(3,1,itri) = xys(2,i)
      triaskel(1,2,itri) = xys(1,i)*cos(phi+hp)
      triaskel(2,2,itri) = xys(1,i)*sin(phi+hp)
      triaskel(3,2,itri) = xys(2,i)
      if (i .le. n-1) then
        triaskel(1,3,itri) = xys(1,i+1)*cos(phi)
        triaskel(2,3,itri) = xys(1,i+1)*sin(phi)
        triaskel(3,3,itri) = xys(2,i+1)
      elseif (i .eq. n) then
        triaskel(1,3,itri) = xys(1,1)*cos(phi)
        triaskel(2,3,itri) = xys(1,1)*sin(phi)
        triaskel(3,3,itri) = xys(2,1)
      end if

      itri = itri + 1
      if (i .le. n-1) then
        triaskel(1,1,itri) = xys(1,i+1)*cos(phi+hp)
        triaskel(2,1,itri) = xys(1,i+1)*sin(phi+hp)
        triaskel(3,1,itri) = xys(2,i+1)
        triaskel(1,2,itri) = xys(1,i+1)*cos(phi)
        triaskel(2,2,itri) = xys(1,i+1)*sin(phi)
        triaskel(3,2,itri) = xys(2,i+1)
      elseif (i .eq. n) then
        triaskel(1,1,itri) = xys(1,1)*cos(phi+hp)
        triaskel(2,1,itri) = xys(1,1)*sin(phi+hp)
        triaskel(3,1,itri) = xys(2,1)
        triaskel(1,2,itri) = xys(1,1)*cos(phi)
        triaskel(2,2,itri) = xys(1,1)*sin(phi)
        triaskel(3,2,itri) = xys(2,1)
      end if

      triaskel(1,3,itri) = xys(1,i)*cos(phi+hp)
      triaskel(2,3,itri) = xys(1,i)*sin(phi+hp)
      triaskel(3,3,itri) = xys(2,i)
    end do
  end do


  !
  ! generate the data to plot
  !
  m = 3
  allocate(dvals(m,3,ntri))
  itri = 0
  do j = 1,nphi
    phi = (j-1)*hp
    do i = 1,n

      iii1 = i+1
      jjj1 = j+1
      if (i .eq. n) iii1 = 1
      if (j .eq. nphi) jjj1 = 1

      itri = itri + 1
      do k = 1,m
        dvals(k,1,itri) = field(k,j,i)
        dvals(k,2,itri) = field(k,jjj1,i)
        dvals(k,3,itri) = field(k,j,iii1)
      end do

      itri = itri + 1
      do k = 1,m
        dvals(k,1,itri) = field(k,jjj1,iii1)
        dvals(k,2,itri) = field(k,j,iii1)
        dvals(k,3,itri) = field(k,jjj1,i)
      end do
    end do
  end do


  !
  ! write the vtk plotting script
  !
  iunit1 = 877
  open(unit = iunit1, file=trim(filename), status='replace')

  write(iunit1,'(a)') "# vtk DataFile Version 3.0"
  write(iunit1,'(a)') trim(title)
  write(iunit1,'(a)') "ASCII"
  write(iunit1,'(a)') "DATASET UNSTRUCTURED_GRID"
  write(iunit1,'(a,i8,a)') "POINTS ", ntri*3, " float"

  fmt2 = "(E11.5,2X,E11.5,2X,E11.5)"
  do i = 1,ntri
    write(iunit1,fmt2) triaskel(1,1,i),triaskel(2,1,i),triaskel(3,1,i)
    write(iunit1,fmt2) triaskel(1,2,i),triaskel(2,2,i),triaskel(3,2,i)
    write(iunit1,fmt2) triaskel(1,3,i),triaskel(2,3,i),triaskel(3,3,i)
  end do


  write(iunit1,'(a,i8,i8)') "CELLS ", ntri, ntri*4

  do i = 1,ntri
    i1 = 3*(i-1) + 1
    write(iunit1,'(a,i8,i8,i8)') "3 ", i1-1, i1, i1+1
  end do

  write(iunit1,'(a,i8)') "CELL_TYPES ", ntri
  do i = 1,ntri
    write(iunit1,'(a)') "5"
  end do

  write(iunit1,'(a)') ""
  write(iunit1,'(a,i8)') "POINT_DATA ", ntri*3
  write(iunit1,'(a)') "SCALARS vector_function float 3"
  !!!!write(iunit1,'(a,i4)') "SCALARS scalar_function float ", m
  write(iunit1,'(a)') "LOOKUP_TABLE default"
  do i = 1,ntri
    do j = 1,3
      write(iunit1,fmt2) dvals(1,j,i), dvals(2,j,i), dvals(3,j,i)
      !write(iunit1,fmt2) 1.0, 0.0, 0.0
    end do
  end do

  close(iunit1)

  return
end subroutine axi_vtk_vector





subroutine axi_vtk_scalar(iw, n, xys, nphi, m, sigma, title)
  implicit real *8 (a-h,o-z)
  integer :: iw, n, nphi, m
  real *8 :: xys(2,n), sigma(m,nphi,n)
  character(len=*) :: title

  ! this routine plots a scalar field along a surface of revolution defined by
  ! the generating curve xys
  !
  ! Input:
  !   iw - the output file number
  !   n - number of points on the generating curve
  !   xys - xy-coordinates (i.e. rz-coords) on the generating curve
  !   nphi - number of points in the azimuthal direction
  !   m - number of scalar functions to plot
  !   sigma - the scalar functions to plot
  !   title - the title of the plot


  real *8, allocatable :: triaskel(:,:,:), dvals(:,:,:)

  character(len=1024) :: filename, dataname, valsname, imgname
  character(len=1024) :: trisname, vecsname, centname
  character(len=12) :: fmt, fmt3, fmt4
  character(len=25) :: fmt2

  !
  !
  !

  if (iw .lt. 10) then
    fmt = "(A4,I1,A4)"
  elseif (iw .lt. 100) then
    fmt = "(A4,I2,A4)"
  elseif (iw .lt. 1000) then
    fmt = "(A4,I3,A4)"
  elseif (iw .lt. 10000) then
    fmt = "(A4,I4,A4)"
  end if

  write(filename, fmt) 'plot', iw, '.vtk'


  !
  ! construct the triangles to plot
  !
  done = 1
  pi = 4*atan(done)
  ntri = 2*nphi*n
  allocate(triaskel(3,3,ntri))
  hp = 2*pi/nphi
  itri = 0
  do j = 1,nphi
    phi = (j-1)*hp
    do i = 1,n
      itri = itri + 1
      triaskel(1,1,itri) = xys(1,i)*cos(phi)
      triaskel(2,1,itri) = xys(1,i)*sin(phi)
      triaskel(3,1,itri) = xys(2,i)
      triaskel(1,2,itri) = xys(1,i)*cos(phi+hp)
      triaskel(2,2,itri) = xys(1,i)*sin(phi+hp)
      triaskel(3,2,itri) = xys(2,i)
      if (i .le. n-1) then
        triaskel(1,3,itri) = xys(1,i+1)*cos(phi)
        triaskel(2,3,itri) = xys(1,i+1)*sin(phi)
        triaskel(3,3,itri) = xys(2,i+1)
      elseif (i .eq. n) then
        triaskel(1,3,itri) = xys(1,1)*cos(phi)
        triaskel(2,3,itri) = xys(1,1)*sin(phi)
        triaskel(3,3,itri) = xys(2,1)
      end if

      itri = itri + 1
      if (i .le. n-1) then
        triaskel(1,1,itri) = xys(1,i+1)*cos(phi+hp)
        triaskel(2,1,itri) = xys(1,i+1)*sin(phi+hp)
        triaskel(3,1,itri) = xys(2,i+1)
        triaskel(1,2,itri) = xys(1,i+1)*cos(phi)
        triaskel(2,2,itri) = xys(1,i+1)*sin(phi)
        triaskel(3,2,itri) = xys(2,i+1)
      elseif (i .eq. n) then
        triaskel(1,1,itri) = xys(1,1)*cos(phi+hp)
        triaskel(2,1,itri) = xys(1,1)*sin(phi+hp)
        triaskel(3,1,itri) = xys(2,1)
        triaskel(1,2,itri) = xys(1,1)*cos(phi)
        triaskel(2,2,itri) = xys(1,1)*sin(phi)
        triaskel(3,2,itri) = xys(2,1)
      end if

      triaskel(1,3,itri) = xys(1,i)*cos(phi+hp)
      triaskel(2,3,itri) = xys(1,i)*sin(phi+hp)
      triaskel(3,3,itri) = xys(2,i)
    end do
  end do


  !
  ! generate the data to plot
  !
  allocate(dvals(m,3,ntri))
  itri = 0
  do j = 1,nphi
    phi = (j-1)*hp
    do i = 1,n

      iii1 = i+1
      jjj1 = j+1
      if (i .eq. n) iii1 = 1
      if (j .eq. nphi) jjj1 = 1

      itri = itri + 1
      do k = 1,m
        dvals(k,1,itri) = sigma(k,j,i)
        dvals(k,2,itri) = sigma(k,jjj1,i)
        dvals(k,3,itri) = sigma(k,j,iii1)
      end do

      itri = itri + 1
      do k = 1,m
        dvals(k,1,itri) = sigma(k,jjj1,iii1)
        dvals(k,2,itri) = sigma(k,j,iii1)
        dvals(k,3,itri) = sigma(k,jjj1,i)
      end do
    end do
  end do


  !
  ! write the vtk plotting script
  !
  iunit1 = 877
  open(unit = iunit1, file=trim(filename), status='replace')

  write(iunit1,'(a)') "# vtk DataFile Version 3.0"
  write(iunit1,'(a)') trim(title)
  write(iunit1,'(a)') "ASCII"
  write(iunit1,'(a)') "DATASET UNSTRUCTURED_GRID"
  write(iunit1,'(a,i8,a)') "POINTS ", ntri*3, " float"

  fmt2 = "(E11.5,2X,E11.5,2X,E11.5)"
  do i = 1,ntri
    write(iunit1,fmt2) triaskel(1,1,i),triaskel(2,1,i),triaskel(3,1,i)
    write(iunit1,fmt2) triaskel(1,2,i),triaskel(2,2,i),triaskel(3,2,i)
    write(iunit1,fmt2) triaskel(1,3,i),triaskel(2,3,i),triaskel(3,3,i)
  end do


  write(iunit1,'(a,i8,i8)') "CELLS ", ntri, ntri*4

  do i = 1,ntri
    i1 = 3*(i-1) + 1
    write(iunit1,'(a,i8,i8,i8)') "3 ", i1-1, i1, i1+1
  end do

  write(iunit1,'(a,i8)') "CELL_TYPES ", ntri
  do i = 1,ntri
    write(iunit1,'(a)') "5"
  end do

  write(iunit1,'(a)') ""
  write(iunit1,'(a,i8)') "POINT_DATA ", ntri*3
  write(iunit1,'(a,i4)') "SCALARS scalar_function float ", m
  write(iunit1,'(a)') "LOOKUP_TABLE default"
  do i = 1,ntri
    do j = 1,3
      do k = 1,m
        write(iunit1,'(E11.5)') dvals(k,j,i)
      end do
    end do
  end do

  close(iunit1)

  return
end subroutine axi_vtk_scalar




subroutine axi_vtk_surface(iw, n, xys, nphi, title)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,n)
  character(len=*) :: title

  real *8, allocatable :: triaskel(:,:,:)

  character(len=1024) :: filename, dataname, valsname, imgname
  character(len=1024) :: trisname, vecsname, centname
  character(len=12) :: fmt, fmt3, fmt4
  character(len=25) :: fmt2

  !
  ! This routine plots a surface of revolution with n points on its
  ! generating curve as a sequence of triangles, nphi of them in the
  ! azimuthal direction.
  !
  ! Input:
  !   iw - plot number, controls the filenames
  !   n - number of flat triangles
  !   xys - points on the generating curve
  !
  ! Output:
  !   plotiw.vtk - file which can be loaded in paraview
  !
  !

  if (iw .lt. 10) then
    fmt = "(A4,I1,A4)"
  elseif (iw .lt. 100) then
    fmt = "(A4,I2,A4)"
  elseif (iw .lt. 1000) then
    fmt = "(A4,I3,A4)"
  elseif (iw .lt. 10000) then
    fmt = "(A4,I4,A4)"
  end if

  write(filename, fmt) 'plot', iw, '.vtk'

  !
  ! construct data to plot
  !
  done = 1
  pi = 4*atan(done)
  ntri = 2*nphi*n
  allocate(triaskel(3,3,ntri))
  hp = 2*pi/nphi
  itri = 0
  do j = 1,nphi
    phi = (j-1)*hp
    do i = 1,n
      itri = itri + 1
      triaskel(1,1,itri) = xys(1,i)*cos(phi)
      triaskel(2,1,itri) = xys(1,i)*sin(phi)
      triaskel(3,1,itri) = xys(2,i)
      triaskel(1,2,itri) = xys(1,i)*cos(phi+hp)
      triaskel(2,2,itri) = xys(1,i)*sin(phi+hp)
      triaskel(3,2,itri) = xys(2,i)
      if (i .le. n-1) then
        triaskel(1,3,itri) = xys(1,i+1)*cos(phi)
        triaskel(2,3,itri) = xys(1,i+1)*sin(phi)
        triaskel(3,3,itri) = xys(2,i+1)
      elseif (i .eq. n) then
        triaskel(1,3,itri) = xys(1,1)*cos(phi)
        triaskel(2,3,itri) = xys(1,1)*sin(phi)
        triaskel(3,3,itri) = xys(2,1)
      end if

      itri = itri + 1
      if (i .le. n-1) then
        triaskel(1,1,itri) = xys(1,i+1)*cos(phi+hp)
        triaskel(2,1,itri) = xys(1,i+1)*sin(phi+hp)
        triaskel(3,1,itri) = xys(2,i+1)
        triaskel(1,2,itri) = xys(1,i+1)*cos(phi)
        triaskel(2,2,itri) = xys(1,i+1)*sin(phi)
        triaskel(3,2,itri) = xys(2,i+1)
      elseif (i .eq. n) then
        triaskel(1,1,itri) = xys(1,1)*cos(phi+hp)
        triaskel(2,1,itri) = xys(1,1)*sin(phi+hp)
        triaskel(3,1,itri) = xys(2,1)
        triaskel(1,2,itri) = xys(1,1)*cos(phi)
        triaskel(2,2,itri) = xys(1,1)*sin(phi)
        triaskel(3,2,itri) = xys(2,1)
      end if

      triaskel(1,3,itri) = xys(1,i)*cos(phi+hp)
      triaskel(2,3,itri) = xys(1,i)*sin(phi+hp)
      triaskel(3,3,itri) = xys(2,i)
    end do
  end do

  !
  ! write the vtk plotting script
  !
  iunit1 = 877
  open(unit = iunit1, file=trim(filename), status='replace')

  write(iunit1,'(a)') "# vtk DataFile Version 3.0"
  write(iunit1,'(a)') trim(title)
  write(iunit1,'(a)') "ASCII"
  write(iunit1,'(a)') "DATASET UNSTRUCTURED_GRID"
  write(iunit1,'(a,i8,a)') "POINTS ", ntri*3, " float"

  fmt2 = "(E11.5,2X,E11.5,2X,E11.5)"
  do i = 1,ntri
    write(iunit1,fmt2) triaskel(1,1,i),triaskel(2,1,i),triaskel(3,1,i)
    write(iunit1,fmt2) triaskel(1,2,i),triaskel(2,2,i),triaskel(3,2,i)
    write(iunit1,fmt2) triaskel(1,3,i),triaskel(2,3,i),triaskel(3,3,i)
  end do


  write(iunit1,'(a,i8,i8)') "CELLS ", ntri, ntri*4

  do i = 1,ntri
    i1 = 3*(i-1) + 1
    write(iunit1,'(a,i8,i8,i8)') "3 ", i1-1, i1, i1+1
  end do

  write(iunit1,'(a,i8)') "CELL_TYPES ", ntri
  do i = 1,ntri
    write(iunit1,'(a)') "5"
  end do

  write(iunit1,'(a,i8)') "POINT_DATA ", ntri*3
  write(iunit1,'(a)') "SCALARS scalars float 1"
  write(iunit1,'(a)') "LOOKUP_TABLE default"
  do i = 1,ntri
    do j = 1,3
      write(iunit1,'(E11.5)') triaskel(3,j,i)
    end do
  end do

  close(iunit1)

  return
end subroutine axi_vtk_surface





subroutine axi_plot_surface(nphi, n, xyzs, iw, title)
  implicit real *8 (a-h,o-z)
  real *8 :: xyzs(3,nphi,n)
  character (len=*) :: title

  real *8, allocatable :: xmesh(:,:), ymesh(:,:), zmesh(:,:)
  character(len=30) :: rowfmt
  character(len=8) :: fmt
  character(len=4) :: fname

  fmt = '(i4.4)'
  write (fname,fmt) iw ! converting integer to string using a 'internal file'

  !
  ! just the surface of revolution defined by xyzs
  !
  mm = n+1
  nn = nphi+1
  allocate(xmesh(mm,nn))
  allocate(ymesh(mm,nn))
  allocate(zmesh(mm,nn))

  do i = 1,n
    do j = 1,nphi
      xmesh(i,j) = xyzs(1,j,i)
      ymesh(i,j) = xyzs(2,j,i)
      zmesh(i,j) = xyzs(3,j,i)
    end do
  end do

  do i = 1,n
    xmesh(i,nphi+1) = xmesh(i,1)
    ymesh(i,nphi+1) = ymesh(i,1)
    zmesh(i,nphi+1) = zmesh(i,1)
  end do

  do j = 1,nphi+1
    xmesh(n+1,j) = xmesh(1,j)
    ymesh(n+1,j) = ymesh(1,j)
    zmesh(n+1,j) = zmesh(1,j)
  end do

  call axi_writematrix(mm, nn, xmesh, 'xmesh' // fname // '.dat')
  call axi_writematrix(mm, nn, ymesh, 'ymesh' // fname // '.dat')
  call axi_writematrix(mm, nn, zmesh, 'zmesh' // fname // '.dat')

  call axi_write_plotsurf(fname, title)

  return
end subroutine axi_plot_surface





subroutine axi_write_plotsurf(fname, title)
  implicit real *8 (a-h,o-z)
  character (len=*) :: title
  character (len=4) :: fname

  iunit = 873
  open(unit = iunit, file='plotsurf' // fname // '.m', status='replace')
  write(iunit,*) 'X = load("xmesh' // fname // '.dat");'
  write(iunit,*) 'Y = load("ymesh' // fname // '.dat");'
  write(iunit,*) 'Z = load("zmesh' // fname // '.dat");'
  write(iunit,*) 'h = surf(X,Y,Z);'
  write(iunit,*) "set(h, 'edgealpha', .1);"
  write(iunit,*) "title('" // trim(title) // "', 'fontsize', 16);"
  write(iunit,*) "xlabel('x', 'fontsize', 16);"
  write(iunit,*) "ylabel('y', 'fontsize', 16);"
  write(iunit,*) "zlabel('z', 'fontsize', 16);"
  write(iunit,*) 'axis equal;'
  close(iunit)

  return
end subroutine axi_write_plotsurf





subroutine axi_plot_scalar(nphi, n, xyzs, vals, iw, title)
  implicit real *8 (a-h,o-z)
  real *8 :: xyzs(3,nphi,n), vals(nphi,n)
  character (len=*) :: title

  real *8, allocatable :: xmesh(:,:), ymesh(:,:), zmesh(:,:)
  real *8, allocatable :: colors(:,:)
  character(len=30) :: rowfmt

  !
  ! just the surface of revolution defined by xyzs
  !
  mm = n+1
  nn = nphi+1
  allocate(xmesh(mm,nn))
  allocate(ymesh(mm,nn))
  allocate(zmesh(mm,nn))
  allocate(colors(mm,nn))

  do i = 1,n
    do j = 1,nphi
      xmesh(i,j) = xyzs(1,j,i)
      ymesh(i,j) = xyzs(2,j,i)
      zmesh(i,j) = xyzs(3,j,i)
      colors(i,j) = vals(j,i)
    end do
  end do

  do i = 1,n
    xmesh(i,nphi+1) = xmesh(i,1)
    ymesh(i,nphi+1) = ymesh(i,1)
    zmesh(i,nphi+1) = zmesh(i,1)
    colors(i,nphi+1) = colors(i,1)
  end do

  do j = 1,nphi+1
    xmesh(n+1,j) = xmesh(1,j)
    ymesh(n+1,j) = ymesh(1,j)
    zmesh(n+1,j) = zmesh(1,j)
    colors(n+1,j) = colors(1,j)
  end do

  call axi_writematrix(mm, nn, xmesh, 'xmesh.dat')
  call axi_writematrix(mm, nn, ymesh, 'ymesh.dat')
  call axi_writematrix(mm, nn, zmesh, 'zmesh.dat')
  call axi_writematrix(mm, nn, colors, 'colors.dat')

  return
end subroutine axi_plot_scalar





subroutine axi_plot_zscalar(nphi, n, xyzs, zvals, iw, title)
  implicit real *8 (a-h,o-z)
  real *8 :: xyzs(3,nphi,n), zvals(nphi,n)
  character (len=*) :: title

  real *8, allocatable :: xmesh(:,:), ymesh(:,:), zmesh(:,:)
  real *8, allocatable :: rcolors(:,:), rvals(:,:), ivals(:,:)
  real *8, allocatable :: icolors(:,:)
  character(len=30) :: rowfmt

  !
  ! just the surface of revolution defined by xyzs
  !
  mm = n+1
  nn = nphi+1
  allocate(xmesh(mm,nn))
  allocate(ymesh(mm,nn))
  allocate(zmesh(mm,nn))
  allocate(rcolors(mm,nn))
  allocate(icolors(mm,nn))
  allocate(rvals(nphi,n))
  allocate(ivals(nphi,n))

  call axi_meshgrid3(nphi, n, xyzs, xmesh, &
    ymesh, zmesh)

  call axi_realpart(nphi*n, zvals, rvals)
  call axi_imagpart(nphi*n, zvals, ivals)

  call axi_meshgrid1(nphi, n, rvals, rcolors)
  call axi_meshgrid1(nphi, n, ivals, icolors)

  call axi_writematrix(mm, nn, xmesh, 'xmesh.dat')
  call axi_writematrix(mm, nn, ymesh, 'ymesh.dat')
  call axi_writematrix(mm, nn, zmesh, 'zmesh.dat')
  call axi_writematrix(mm, nn, rcolors, 'rcolors.dat')
  call axi_writematrix(mm, nn, icolors, 'icolors.dat')

  return
end subroutine axi_plot_zscalar





subroutine axi_plot_zvector(nphi, n, xyzs, zvecs, iw, title)
  implicit real *8 (a-h,o-z)
  real *8 :: xyzs(3,nphi,n), zvecs(nphi,n)
  character (len=*) :: title

  real *8, allocatable :: xmesh(:,:), ymesh(:,:), zmesh(:,:)
  real *8, allocatable :: rmesh(:,:), rvals(:,:), ivals(:,:)
  real *8, allocatable :: imesh(:,:), rvecs(:,:,:), ivecs(:,:,:)

  !
  ! plot a vectorfield along the surface of revolution xyzs
  !
  mm = n+1
  nn = nphi+1
  allocate(xmesh(mm,nn))
  allocate(ymesh(mm,nn))
  allocate(zmesh(mm,nn))
  allocate(rmesh(mm,nn))
  allocate(imesh(mm,nn))
  allocate(rvals(nphi,n))
  allocate(ivals(nphi,n))
  allocate(rvecs(3,nphi,n))
  allocate(ivecs(3,nphi,n))

  call axi_meshgrid3(nphi, n, xyzs, xmesh, &
    ymesh, zmesh)

  call axi_realpart(3*nphi*n, zvecs, rvecs)
  call axi_imagpart(3*nphi*n, zvecs, ivecs)

  do j = 1,n
    do i = 1,nphi
      rvals(i,j) = sqrt(rvecs(1,i,j)**2 + rvecs(2,i,j)**2 &
          + rvecs(3,i,j)**2)
      ivals(i,j) = sqrt(ivecs(1,i,j)**2 + ivecs(2,i,j)**2 &
          + ivecs(3,i,j)**2)
    end do
  end do

  call axi_meshgrid1(nphi, n, rvals, rmesh)
  call axi_meshgrid1(nphi, n, ivals, imesh)

  !
  ! dump out the magnitude plot
  !
  call axi_writematrix(mm, nn, xmesh, 'xmesh.dat')
  call axi_writematrix(mm, nn, ymesh, 'ymesh.dat')
  call axi_writematrix(mm, nn, zmesh, 'zmesh.dat')
  call axi_writematrix(mm, nn, rmesh, 'rcolors.dat')
  call axi_writematrix(mm, nn, imesh, 'icolors.dat')

  !
  ! and the data for the quiver3d
  !
  call axi_writevectors(nphi*n, xyzs, rvecs, 'real_vectors.dat')
  call axi_writevectors(nphi*n, xyzs, ivecs, 'imag_vectors.dat')

  return
end subroutine axi_plot_zvector





subroutine axi_writevectors(n, xyzs, vecs, filename)
  implicit real *8 (a-h,o-z)
  real *8 :: xyzs(3,n), vecs(3,n)
  character (len=*) :: filename
  character (len=30) :: rowfmt

  !
  !
  !

  iunit = 873
  write(rowfmt,'(a,i5,a)') '(6(1X,e12.5))'

  open(unit = iunit, file=trim(filename), status='replace')
  do i = 1,n
    write(iunit,fmt=rowfmt) xyzs(1,i), xyzs(2,i), xyzs(3,i), &
        vecs(1,i), vecs(2,i), vecs(3,i)
  end do
  close(iunit)

  return
end subroutine axi_writevectors





subroutine axi_meshgrid3(nphi, n, xyzs, xmesh, ymesh, zmesh)
  implicit real *8 (a-h,o-z)
  real *8 :: xyzs(3,nphi,n), xmesh(n+1,nphi+1)
  real *8 :: ymesh(n+1,nphi+1), zmesh(n+1,nphi+1)

  do i = 1,n
    do j = 1,nphi
      xmesh(i,j) = xyzs(1,j,i)
      ymesh(i,j) = xyzs(2,j,i)
      zmesh(i,j) = xyzs(3,j,i)
    end do
  end do

  do i = 1,n
    xmesh(i,nphi+1) = xmesh(i,1)
    ymesh(i,nphi+1) = ymesh(i,1)
    zmesh(i,nphi+1) = zmesh(i,1)
  end do

  do j = 1,nphi+1
    xmesh(n+1,j) = xmesh(1,j)
    ymesh(n+1,j) = ymesh(1,j)
    zmesh(n+1,j) = zmesh(1,j)
  end do

  return
end subroutine axi_meshgrid3



subroutine axi_meshgrid1(nphi, n, vals, mesh)
  implicit real *8 (a-h,o-z)
  real *8 :: vals(nphi,n), mesh(n+1,nphi+1)

  do i = 1,n
    do j = 1,nphi
      mesh(i,j) = vals(j,i)
    end do
  end do

  do i = 1,n
    mesh(i,nphi+1) = mesh(i,1)
  end do

  do j = 1,nphi+1
    mesh(n+1,j) = mesh(1,j)
  end do

  return
end subroutine axi_meshgrid1



subroutine axi_realpart(n, z, x)
  implicit real *8 (a-h,o-z)
  real *8 :: x(n)
  complex *16 :: z(n)

  do i = 1,n
    x(i) = real(z(i))
  end do

  return
end subroutine axi_realpart





subroutine axi_imagpart(n, z, x)
  implicit real *8 (a-h,o-z)
  real *8 :: x(n)
  complex *16 :: z(n)

  do i = 1,n
    x(i) = aimag(z(i))
  end do

  return
end subroutine axi_imagpart





subroutine axi_writematrix(m, n, a, filename)
  implicit real *8 (a-h,o-z)
  real *8 :: a(m,n)
  character (len=*) :: filename
  character (len=30) :: rowfmt

  iunit = 873
  write(rowfmt,'(a,i5,a)') '(',n,'(1X,e12.5))'

  open(unit = iunit, file=trim(filename), status='replace')
  do i = 1,m
    write(iunit,fmt=rowfmt) (a(i,j), j=1,n)
  end do
  close(iunit)

  return
end subroutine axi_writematrix





subroutine axi_writemat(filename, m, n, a)
  implicit real *8 (a-h,o-z)
  real *8 :: a(m,n)
  character (len=*) :: filename

  !
  ! write the m \times n matrix a out to a file
  !

  return
end subroutine axi_writemat
