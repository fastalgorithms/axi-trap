



subroutine zalpertmat_dlp(ier, norder, ns, xys, dxys, &
    h, gfun, par0, par1, par2, diag, ifscale, amat)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,ns), dxys(2,ns)
  real *8 :: par1(*), par2(*)
  complex *16 :: amat(ns,ns), diag, par0(*)
  
  integer :: its(100),its2(100)
  real *8 :: src(10), targ(10)
  real *8 :: tpts(100), xpts(100), ypts(100), spts(100)
  real *8 :: rnxpts(100), rnypts(100), txtra(100), coefs(100)
  real *8 :: extranodes(30), extraweights(30)

  complex *16 :: zval, zgrad(10), zgrad0(10), cd
  
  real *8, allocatable :: dnorms(:,:), dsdt(:)

  ! 
  ! this routine builds the matrix which applies the double
  ! layer potential gfun to a vector using alpert quadrature
  ! 
  ! input:
  !     diag - the diagonal to add to the matrix
  !     ifscale - if set to ifscale=1, then scale for surface of
  !           revolution quadrature, i.e. aij = aij*2*pi*xys(1,j)
  ! 

  done=1
  pi=4*atan(done)
  
  call getalpert(norder, nskip, nextra, extranodes, &
      extraweights)

  ier = 1
  if (norder .eq. 0) ier = 0
  if (norder .eq. 2) ier = 0
  if (norder .eq. 4) ier = 0
  if (norder .eq. 8) ier = 0
  if (norder .eq. 16) ier = 0
  if (ier .ne. 0) then
    call prinf('wrong quad order, norder=*', norder, 1)
    stop
  end if
        
  !
  ! carry out "punctured" trapezoidal rule and fill in matrix
  ! entries, skipping entries within nskip of the diagonal
  !
  do j=1,ns
    do i=1,ns
      amat(i,j)=0
    end do
  end do

  n=ns-2*nskip+1

  !
  ! compute norms and dsdt
  !
  allocate(dnorms(2,ns), dsdt(ns))
  do i = 1,ns
    dsdt(i) = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
    dnorms(1,i) = dxys(2,i)/dsdt(i)
    dnorms(2,i) = -dxys(1,i)/dsdt(i)
  end do
  
  do i = 1,ns
    iii = i-1+nskip
    do k=0,n-1
      iii = iii+1
      if (iii .gt. ns) iii = iii-ns
      call gfun(par0, xys(1,iii), xys(1,i), par1, par2, &
          zval, zgrad, zgrad0)
      cd = dnorms(1,iii)*zgrad0(1) + dnorms(2,iii)*zgrad0(2)
      amat(i,iii)=cd*dsdt(iii)*h
    end do
  end do
  
  if (norder .eq. 0) then

    if (ifscale .eq. 1) then
      do j = 1,ns
        do i = 1,ns
          amat(i,j) = 2*pi*xys(1,j)*amat(i,j)
        end do
      end do
    end if

    do i = 1,ns
      amat(i,i) = amat(i,i) + diag
    end do

    return
  end if

  !
  ! now add in corrections and interpolated stuff for alpert
  ! first determine all the interpolation coefficients
  !
  ninterp=norder+2

  do ipt=1,ns

    do i=1,nextra
      txtra(i)=h*(ipt-1)+h*extranodes(i)
    end do


    do i=1,nextra

      !
      ! find the closest ninterp points to each of the txtra
      !
      
      n1=txtra(i)/h
      if (txtra(i) .lt. 0) n1=n1-1
      n2=n1+1
      nnn=n1-(ninterp-2)/2
      
      do j=1,ninterp
        its(j)=nnn+j-1
        its2(j)=its(j)+1
        if (its2(j) .le. 0) its2(j)=its2(j)+ns
        if (its2(j) .gt. ns) its2(j)=its2(j)-ns
      end do
      
      !
      ! fill interpolation nodes and function values
      !
      do j=1,ninterp
        tpts(j) = its(j)*h
        xpts(j) = xys(1,its2(j))
        ypts(j) = xys(2,its2(j))
        spts(j) = dsdt(its2(j))
        rnxpts(j) = dnorms(1,its2(j))
        rnypts(j) = dnorms(2,its2(j))
      end do
      
      !
      ! now compute the values of xs, ys, dsdt at ttt using barycentric
      ! interpolation
      !
      ttt=txtra(i)
      call bary1_coefs_new(ninterp,tpts,ttt,coefs)

      xxx=0
      yyy=0
      sss=0
      rnxxx=0
      rnyyy=0

      do j=1,ninterp
        xxx = xxx+xpts(j)*coefs(j)
        yyy = yyy+ypts(j)*coefs(j)
        sss = sss+spts(j)*coefs(j)
        rnxxx = rnxxx+rnxpts(j)*coefs(j)
        rnyyy = rnyyy+rnypts(j)*coefs(j)
      end do

      !
      ! evaluate the kernel at the new quadrature point xxx,yyy and
      ! add its contribution to the matrix at its interpolation points
      !
      src(1) = xxx
      src(2) = yyy
      call gfun(par0, src, xys(1,ipt), par1, par2, &
          zval, zgrad, zgrad0)
      cd = rnxxx*zgrad0(1) + rnyyy*zgrad0(2)

      do j = 1,ninterp
        jjj = its2(j)
        amat(ipt,jjj) = amat(ipt,jjj) &
            + cd*sss*h*extraweights(i)*coefs(j)
      end do

    end do
  end do

  !
  ! and scale if necessary
  !
  if (ifscale .eq. 1) then
    do j = 1,ns
      do i = 1,ns
        amat(i,j) = 2*pi*xys(1,j)*amat(i,j)
      end do
    end do
  end if

  do i = 1,ns
    amat(i,i) = amat(i,i) + diag
  end do

  
  return
end subroutine zalpertmat_dlp







