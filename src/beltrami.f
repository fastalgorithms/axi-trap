c
c
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c       this is the end of the debugging code, and the beginning
c       of the routines that will be used to solve the
c       axisymmetric mfie
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c       a few user-callable routines are available in this file,
c       they are:
c
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c
c
        subroutine taylorstate1(eps, zk, mode, n, rl, ps, zs,
     1    dpdt, dzdt, dpdt2, dzdt2, norder, ind, rhs, flux_tor,
     2    rhom, zms)
        implicit real *8 (a-h,o-z)
c
        real *8 ps(1), zs(1), dpdt(1), dzdt(1), dpdt2(1), dzdt2(1)
c
        complex *16 zk, ima, rhs(1), flux_tor
        complex *16 rhom(1), zms(2,1), alpha
c
c       this routine calculates the interior taylor state (beltrami
c       field corresponding to the data rhs (usually =0) and the 
c       toroidal flux. it returns a charge and current which can be 
c       used to evaluate the field
c


c
        return
        end
c
c
c
c
c
        subroutine taylorstate2(eps, zk, mode, n, rl, ps_in, zs_in,
     1    dpdt_in, dzdt_in, dpdt2_in, dzdt2_in, ps_out, zs_out,
     2    dpdt_out, dzdt_out, dpdt2_out, dzdt2_out,
     3    norder, ind, rhs, flux_tor, flux_pol, rhom_in, zms_in,
     4    rhom_out, zms_out)
        implicit double precision (a-h, o-z)
c
        real *8 ps_in(1), zs_in(1), dpdt_in(1), dzdt_in(1)
        real *8 dpdt2_in(1), dzdt2_in(1)
        real *8 ps_out(1), zs_out(1), dpdt_out(1), dzdt_out(1)
        real *8 dpdt2_out(1), dzdt2_out(1)
c
        complex *16 zk, ima, rhs(1), flux_tor, flux_pol
        complex *16 rhom_in(1), rhom_out(1)
        complex *16 zms_in(2,1), zms_out(2,1)
c

c
        return
        end
c
c
c
c
c
c$$$        subroutine creabeltrami_g2(eps, zk, m, n, rl, ps_in, zs_in,
c$$$     1      dpdt_in, dzdt_in, dpdt2_in, dzdt2_in, ps_out, zs_out,
c$$$     2      dpdt_out, dzdt_out, dpdt2_out, dzdt2_out, 
c$$$     3      norder, ind, cmat)
c$$$        implicit double precision (a-h, o-z)
c$$$c
c$$$        double precision ps_in(1), zs_in(1), dpdt_in(1), dzdt_in(1)
c$$$        double precision dpdt2_in(1), dzdt2_in(1), dsdt_in(10000)
c$$$        double precision ps_out(1), zs_out(1), dpdt_out(1)
c$$$        double precision dzdt_out(1)
c$$$        double precision dpdt2_out(1), dzdt2_out(1), dsdt_out(10000)
c$$$c
c$$$        double complex ima, zk, cmat(2*n+2, 2*n+2), zk0, val
c$$$        double complex ch1_in(2,10000), ch2_in(2,10000), x, y
c$$$        double complex ch1_out(2,10000), ch2_out(2,10000)
c$$$        double complex cd, cd0, cd1, cd2, cda, cdb, cda2, cdb2, cdb0
c$$$        double complex jh_in(2,10000), mh_in(2,10000)
c$$$        double complex jh_out(2,10000), mh_out(2,10000)
c$$$c
c$$$        external ghfun2, ghfun4n1
c$$$c
c$$$        double precision wsave(1000000)
c$$$        complex *16 ctemp(1000000), ctemp1(1000000), 
c$$$     3      ctemp3(1000000), ctemp4(1000000), 
c$$$     4      work(10000000), work2(10000000), ctemp2(1000000),
c$$$     5      work3(10000000),
c$$$     6      uz(10000000),vz(10000000),sz(1000000)
c$$$        complex *16, allocatable :: wsprime(:,:), wgradrep(:,:)
c$$$        complex *16, allocatable :: wa2(:,:)
c$$$        complex *16, allocatable :: wa12(:,:), wa21(:,:)
c$$$        complex *16, allocatable :: wa2_11(:,:), wa2_12(:,:)
c$$$        complex *16, allocatable :: wa2_21(:,:), wa2_22(:,:)
c$$$        complex *16, allocatable :: w(:,:),wcc(:,:),wss(:,:)
c$$$        complex *16, allocatable :: wp(:,:),wccp(:,:),wssp(:,:)
c$$$        complex *16, allocatable :: wz(:,:),wccz(:,:),wssz(:,:)
c$$$        complex *16, allocatable :: wnxcurl(:,:)
c$$$        complex *16, allocatable :: wnxcurl11(:,:), wnxcurl22(:,:)
c$$$        complex *16, allocatable :: wnxcurl12(:,:), wnxcurl21(:,:)
c$$$        complex *16, allocatable :: wndotcurl12(:,:),wndotcurl21(:,:)
c$$$        complex *16, allocatable :: wndot(:,:),wnxt(:,:)
c$$$        complex *16, allocatable :: a1(:,:),a2(:,:),a3(:,:)
c$$$        complex *16, allocatable :: e1(:,:),e2(:,:),e3(:,:)
c$$$        complex *16, allocatable :: etop(:,:),ebottom(:,:)
c$$$        complex *16, allocatable :: wbeltrami_ext(:,:)
c$$$        complex *16, allocatable :: wbeltrami_int(:,:)
c$$$        complex *16, allocatable :: cmat_in(:,:), cmat_ext(:,:)
c$$$        complex *16, allocatable :: wsprime11(:,:), wsprime12(:,:)
c$$$        complex *16, allocatable :: wsprime21(:,:), wsprime22(:,:)
c$$$c
c$$$c       this subroutine constructs the genus 2 beltrami matrix
c$$$c
c$$$        allocate(wsprime(n,n))
c$$$        allocate(wgradrep(2*n,n))
c$$$        allocate(wbeltrami_ext(2*n,n))
c$$$        allocate(wbeltrami_int(2*n,n))
c$$$c
c$$$        allocate(wa2(2*n,2*n))
c$$$c
c$$$        allocate(wa12(3*n,2*n))
c$$$        allocate(wa21(3*n,2*n))
c$$$c
c$$$        allocate(wa2_11(2*n,2*n))
c$$$        allocate(wa2_12(2*n,2*n))
c$$$        allocate(wa2_21(2*n,2*n))
c$$$        allocate(wa2_22(2*n,2*n))
c$$$c
c$$$        allocate(w(n,n))
c$$$        allocate(wcc(n,n))
c$$$        allocate(wss(n,n))
c$$$c
c$$$        allocate(wp(n,n))
c$$$        allocate(wccp(n,n))
c$$$        allocate(wssp(n,n))
c$$$c
c$$$        allocate(wz(n,n))
c$$$        allocate(wccz(n,n))
c$$$        allocate(wssz(n,n))
c$$$c
c$$$        allocate(wnxcurl(2*n,2*n))
c$$$        allocate(wnxcurl22(2*n,2*n))
c$$$        allocate(wnxcurl11(2*n,2*n))
c$$$        allocate(wnxcurl12(2*n,2*n))
c$$$        allocate(wnxcurl21(2*n,2*n))
c$$$        allocate(wndotcurl21(n,2*n))
c$$$        allocate(wndotcurl12(n,2*n))
c$$$        allocate(wndot(n,3*n))
c$$$        allocate(wnxt(2*n,2*n))
c$$$c
c$$$        allocate(a1(n,n), a2(n,n), a3(n,n))
c$$$        allocate(e1(2*n,n),e2(2*n,n),e3(2*n,n))
c$$$        allocate(etop(2*n,n),ebottom(2*n,n))
c$$$c
c$$$        allocate(wsprime11(n,n))
c$$$        allocate(wsprime12(n,n))
c$$$        allocate(wsprime21(n,n))
c$$$        allocate(wsprime22(n,n))
c$$$
c$$$c
c$$$c       begin building the matrix
c$$$c
c$$$        ima=(0,1)
c$$$        done=1.0d0
c$$$        pi=4*atan(done)
c$$$c
c$$$c       compute dsdt around the two curves
c$$$c
c$$$        do i=1,n
c$$$          dd = sqrt(dpdt_in(i)**2 + dzdt_in(i)**2)
c$$$          dsdt_in(i) = dd
c$$$          dd = sqrt(dpdt_out(i)**2 + dzdt_out(i)**2)
c$$$          dsdt_out(i) = dd
c$$$        enddo
c$$$
c$$$c
c$$$        if (1 .eq. 0) then
c$$$c
c$$$        call prin2('ps_in = *', ps_in, n)
c$$$        call prin2('zs_in = *', zs_in, n)
c$$$        call prin2('dpdt_in = *', dpdt_in, n)
c$$$        call prin2('dzdt_in = *', dzdt_in, n)
c$$$        call prin2('dpdt2_in = *', dpdt2_in, n)
c$$$        call prin2('dzdt2_in = *', dzdt2_in, n)
c$$$        call prin2('dsdt_in = *', dsdt_in, n)
c$$$c
c$$$        print *
c$$$        print *
c$$$        print *
c$$$
c$$$c
c$$$c       print out the resolution of the geometry
c$$$c
c$$$        call dffti(n, wsave)
c$$$c
c$$$c        do i = 1,n
c$$$c          ps_in(i) = ps_in(i)*dsdt_in(i)
c$$$c          zs_in(i) = zs_in(i)*dsdt_in(i)
c$$$c          dpdt_in(i) = dpdt_in(i)*dsdt_in(i)
c$$$c          dzdt_in(i) = dzdt_in(i)*dsdt_in(i)
c$$$c          dpdt2_in(i) = dpdt2_in(i)*dsdt_in(i)
c$$$c          dzdt2_in(i) = dzdt2_in(i)*dsdt_in(i)
c$$$c        enddo
c$$$c
c$$$        call prin2('dsdt_in = *', dsdt_in, n)
c$$$        call dfftf(n, ps_in, wsave)
c$$$        call prin2('fft of ps_in = *', ps_in, n)
c$$$c 
c$$$        call prin2('dsdt_in = *', dsdt_in, n)
c$$$        call dfftf(n, zs_in, wsave)
c$$$        call prin2('fft of zs_in = *', zs_in, n)
c$$$c 
c$$$        call prin2('dsdt_in = *', dsdt_in, n)
c$$$        call dfftf(n, dpdt_in, wsave)
c$$$        call prin2('fft of dpdt_in = *', dpdt_in, n)
c$$$c 
c$$$        call prin2('dsdt_in = *', dsdt_in, n)
c$$$        call dfftf(n, dzdt_in, wsave)
c$$$        call prin2('fft of dzdt_in = *', dzdt_in, n)
c$$$c 
c$$$        call prin2('dsdt_in = *', dsdt_in, n)
c$$$        call dfftf(n, dpdt2_in, wsave)
c$$$        call prin2('fft of dpdt2_in = *', dpdt2_in, n)
c$$$c 
c$$$        call prin2('dsdt_in = *', dsdt_in, n)
c$$$        call dfftf(n, dzdt2_in, wsave)
c$$$        call prin2('fft of dzdt2_in = *', dzdt2_in, n)
c$$$c
c$$$        call prin2('dsdt_in = *', dsdt_in, n)
c$$$        call dfftf(n, dsdt_in, wsave)
c$$$        call prin2('fft of dsdt_in = *', dsdt_in, n)
c$$$c
c$$$
c$$$        print *
c$$$        print *
c$$$        print *
c$$$c
c$$$        call dfftf(n, ps_out, wsave)
c$$$        call prin2('fft of ps_out = *', ps_out, n)
c$$$c 
c$$$        call dfftf(n, zs_out, wsave)
c$$$        call prin2('fft of zs_out = *', zs_out, n)
c$$$c 
c$$$        call dfftf(n, dpdt_out, wsave)
c$$$        call prin2('fft of dpdt_out = *', dpdt_out, n)
c$$$c 
c$$$        call dfftf(n, dzdt_out, wsave)
c$$$        call prin2('fft of dzdt_out = *', dzdt_out, n)
c$$$c 
c$$$        call dfftf(n, dpdt2_out, wsave)
c$$$        call prin2('fft of dpdt2_out = *', dpdt2_out, n)
c$$$c 
c$$$        call dfftf(n, dzdt2_out, wsave)
c$$$        call prin2('fft of dzdt2_out = *', dzdt2_out, n)
c$$$c
c$$$        call dfftf(n, dsdt_out, wsave)
c$$$        call prin2('fft of dsdt_out = *', dsdt_out, n)
c$$$c
c$$$        endif
c$$$
c$$$cccc        stop
c$$$c
c$$$c       . . . first the (1,1) block, interior to interior
c$$$c
c$$$        allocate( cmat_in(n+1,n+1) )
c$$$c
c$$$        tau = 1
c$$$        call creabeltrami_ext(eps, zk, m, n, rl, ps_in, zs_in,
c$$$     1      dpdt_in, dzdt_in, dpdt2_in, dzdt2_in, norder,
c$$$     1      ind, tau, cmat_in)
c$$$c
c$$$        do i = 1,n
c$$$          do j = 1,n
c$$$            cmat(i,j) = cmat_in(i,j)
c$$$          enddo
c$$$        enddo
c$$$
c$$$c
c$$$c       . . . next the (2,1) block, interior sources to exterior
c$$$c
c$$$        call surfgradinvlapintmat(ps_in, zs_in, dpdt_in,
c$$$     1      dzdt_in, dpdt2_in, dzdt2_in, n, m, rl, wgradrep)
c$$$c
c$$$        h=rl/n
c$$$        call creapieces_st(eps, zk, m, n, rl, ps_in, zs_in,
c$$$     1      dpdt_in, dzdt_in, ps_out, zs_out, dpdt_out, 
c$$$     2      dzdt_out, w, wp, wz, wcc, wccp, wccz, wss, wssp,
c$$$     3      wssz)
c$$$c
c$$$        do i=1,n
c$$$          dpds = dpdt_out(i)/dsdt_out(i)
c$$$          dzds = dzdt_out(i)/dsdt_out(i)
c$$$          do j=1,n
c$$$            wsprime(i,j) = dzds*wp(i,j) - dpds*wz(i,j)
c$$$          enddo
c$$$        enddo
c$$$c
c$$$        iftan = 0
c$$$        call creavecpotmat7(eps, zk, m, n, rl, ps_in, zs_in,
c$$$     1      dpdt_in, dzdt_in, norder, iftan, wa21, wcc, wss, w)
c$$$c
c$$$        call creavecpotmat2x2(eps, zk, m, n, rl, ps_in, zs_in, 
c$$$     1      dpdt_in, dzdt_in, norder, wa2_21, wcc, wss, w)
c$$$c
c$$$        diag = 0
c$$$        call creanxcurlmat2_st(eps, zk, m, n, rl, ps_in, zs_in,
c$$$     1      dpdt_in, dzdt_in, ps_out, zs_out, dpdt_out, dzdt_out,
c$$$     2      norder, diag, wnxcurl21, w, wcc, wss, wp, wccp, wssp, 
c$$$     3      wccz, wssz)
c$$$c
c$$$        call creandotcurlmat_st(eps, zk, m, n, rl, ps_in, zs_in,
c$$$     1      dpdt_in, dzdt_in, ps_out, zs_out, dpdt_out, dzdt_out,
c$$$     2      wndotcurl21, w, wcc, wss, wccp, wssp, wccz, wssz)
c$$$c
c$$$        call creandotmat(n, dpdt_out, dzdt_out, wndot)
c$$$        call creanxtmat(n, wnxt)
c$$$c
c$$$c       and assemble
c$$$c
c$$$        x = 1
c$$$        y = ima
c$$$        call cmatmul601(2*n, 2*n, wnxt, n, wgradrep, work)
c$$$        call cmatadd601(2*n, n, x, wgradrep, y, work, wbeltrami_int)
c$$$        call cmatmul601(n, 2*n, wndotcurl21, n, wbeltrami_int, a3)
c$$$c
c$$$        call cmatmul601(n, 3*n, wndot, 2*n, wa21, work)
c$$$        call cmatmul601(n, 2*n, work, n, wbeltrami_int, a1)
c$$$c
c$$$        do i = 1, n
c$$$          do j = 1, n
c$$$            cmat(n+i,j) = -zk*zk*a1(i,j) - wsprime(i,j) - zk*a3(i,j)
c$$$          enddo
c$$$        enddo
c$$$
c$$$c
c$$$c       . . . next the (1,2) block, exterior sources to interior
c$$$c         first construct the source-target matrices
c$$$c
c$$$        call surfgradinvlapintmat(ps_out, zs_out, dpdt_out,
c$$$     1      dzdt_out, dpdt2_out, dzdt2_out, n, m, rl, wgradrep)
c$$$c
c$$$        h=rl/n
c$$$        call creapieces_st(eps, zk, m, n, rl, ps_out, zs_out,
c$$$     1      dpdt_out, dzdt_out, ps_in, zs_in, dpdt_in, 
c$$$     2      dzdt_in, w, wp, wz, wcc, wccp, wccz, wss, wssp,
c$$$     3      wssz)
c$$$c
c$$$        do i=1,n
c$$$          dpds = dpdt_in(i)/dsdt_in(i)
c$$$          dzds = dzdt_in(i)/dsdt_in(i)
c$$$          do j=1,n
c$$$            wsprime(i,j) = dzds*wp(i,j) - dpds*wz(i,j)
c$$$          enddo
c$$$        enddo
c$$$c
c$$$        iftan = 0
c$$$        call creavecpotmat7(eps, zk, m, n, rl, ps_out, zs_out,
c$$$     1      dpdt_out, dzdt_out, norder, iftan, wa12, wcc, wss, w)
c$$$c
c$$$        call creavecpotmat2x2(eps, zk, m, n, rl, ps_out, zs_out, 
c$$$     1      dpdt_out, dzdt_out, norder, wa2_12, wcc, wss, w)
c$$$c
c$$$        diag = 0
c$$$        call creanxcurlmat2_st(eps, zk, m, n, rl, ps_out, zs_out,
c$$$     1      dpdt_out, dzdt_out, ps_in, zs_in, dpdt_in, dzdt_in,
c$$$     2      norder, diag, wnxcurl12, w, wcc, wss, wp, wccp, wssp, 
c$$$     3      wccz, wssz)
c$$$c
c$$$        call creandotcurlmat_st(eps, zk, m, n, rl, ps_out, zs_out,
c$$$     1      dpdt_out, dzdt_out, ps_in, zs_in, dpdt_in, dzdt_in,
c$$$     2      wndotcurl12, w, wcc, wss, wccp, wssp, wccz, wssz)
c$$$c
c$$$        call creandotmat(n, dpdt_in, dzdt_in, wndot)
c$$$        call creanxtmat(n, wnxt)
c$$$c
c$$$c       and assemble
c$$$c
c$$$        x = 1
c$$$        y = -ima
c$$$        call cmatmul601(2*n, 2*n, wnxt, n, wgradrep, work)
c$$$        call cmatadd601(2*n, n, x, wgradrep, y, work, wbeltrami_ext)
c$$$c
c$$$        call cmatmul601(n, 3*n, wndot, 2*n, wa12, work)
c$$$        call cmatmul601(n, 2*n, work, n, wbeltrami_ext, a1)
c$$$c
c$$$        call cmatmul601(n, 2*n, wndotcurl12, n, wbeltrami_ext, a3)
c$$$c
c$$$        do i = 1, n
c$$$          do j = 1, n
c$$$            cmat(i,n+j) = -zk*zk*a1(i,j) - wsprime(i,j) - zk*a3(i,j)
c$$$          enddo
c$$$        enddo
c$$$
c$$$
c$$$c
c$$$c       . . . next the (2,2) block, exterior to exterior, but the
c$$$c       interior of the domain
c$$$c
c$$$        allocate( cmat_ext(n+1, n+1) )
c$$$c
c$$$        call creabeltrami(eps, zk, m, n, rl, ps_out, zs_out,
c$$$     1      dpdt_out, dzdt_out, dpdt2_out, dzdt2_out, norder,
c$$$     1      ind, tau, cmat_ext)
c$$$c
c$$$        do i = 1,n
c$$$          do j = 1,n
c$$$            cmat(n+i,n+j) = cmat_ext(i,j)
c$$$          enddo
c$$$        enddo
c$$$
c$$$c
c$$$c       only test non-zero mode right now
c$$$c
c$$$        if (m .ne. 0) then
c$$$c
c$$$          do i = 1,2*n+2
c$$$            cmat(2*n+1,i) = 0
c$$$            cmat(2*n+2,i) = 0
c$$$            cmat(i,2*n+1) = 0
c$$$            cmat(i,2*n+2) = 0
c$$$          enddo
c$$$c
c$$$          cmat(2*n+1,2*n+1) = 1
c$$$          cmat(2*n+2,2*n+2) = 1
c$$$          return
c$$$c
c$$$        endif
c$$$
c$$$c
c$$$c       . . . now for the augmented conditions
c$$$c
c$$$        call get2harmvecs(n, ps_in, zs_in, ch1_in, ch2_in)
c$$$        call get2harmvecs(n, ps_out, zs_out, ch1_out, ch2_out)
c$$$c
c$$$c       . . . make inner and outer harmonic vector fields
c$$$c
c$$$        do i = 1, n
c$$$          jh_out(1,i) = ch1_out(1,i) - ima*ch2_out(1,i)
c$$$          jh_out(2,i) = ch1_out(2,i) - ima*ch2_out(2,i)
c$$$          mh_out(1,i) = -ima*jh_out(1,i)
c$$$          mh_out(2,i) = -ima*jh_out(2,i)
c$$$        enddo
c$$$c
c$$$        do i = 1, n
c$$$          mh_in(1,i) = ch1_in(1,i) + ima*ch2_in(1,i)
c$$$          mh_in(2,i) = ch1_in(2,i) + ima*ch2_in(2,i)
c$$$          jh_in(1,i) = ima*mh_in(1,i)
c$$$          jh_in(2,i) = ima*mh_in(2,i)
c$$$        enddo
c$$$c
c$$$c       add in the contribution of the harmonic vector fields to
c$$$c       the normal components of the field
c$$$c
c$$$c       interior-to-interior
c$$$c
c$$$        do i = 1,n
c$$$          cmat(i,2*n+1) = cmat_in(i,n+1)
c$$$        enddo
c$$$c
c$$$c       interior sources, exterior targets
c$$$c
c$$$        call creandotmat(n, dpdt_out, dzdt_out, wndot)
c$$$        call cmatmul601(n, 3*n, wndot, 2*n, wa21, work2)
c$$$        call cmatvec601(n, 2*n, work2, mh_in, work)
c$$$c
c$$$        call cmatvec601(n, 2*n, wndotcurl21, mh_in, work2)
c$$$c
c$$$        do i = 1, n
c$$$          cmat(n+i,2*n+1) = ima*zk*work(i) + ima*work2(i)
c$$$        enddo
c$$$
c$$$c
c$$$c       exterior sources, interior targets
c$$$c
c$$$        call creandotmat(n, dpdt_in, dzdt_in, wndot)
c$$$        call cmatmul601(n, 3*n, wndot, 2*n, wa12, work2)
c$$$        call cmatvec601(n, 2*n, work2, mh_out, work)
c$$$c
c$$$        call cmatvec601(n, 2*n, wndotcurl12, jh_out, work2)
c$$$c
c$$$        do i = 1, n
c$$$          cmat(i,2*n+2) = ima*zk*work(i) + work2(i)
c$$$        enddo
c$$$
c$$$c
c$$$c       exterior-exterior
c$$$c
c$$$        do i = 1,n
c$$$          cmat(n+i,2*n+2) = cmat_ext(i,n+1)
c$$$        enddo
c$$$c
c$$$        do i = 1,2*n+2
c$$$          cmat(2*n+1,i) = 0
c$$$          cmat(2*n+2,i) = 0
c$$$        enddo
c$$$
c$$$c
c$$$c       . . . and now the flux conditions, first the contribution
c$$$c       of the charge densities
c$$$c
c$$$c       interior charge, interior fluxes
c$$$c
c$$$        call creapiecesa(eps, zk, m, n, rl, ps_in, zs_in,
c$$$     1      dpdt_in, dzdt_in, norder, w, wp, wz, wcc, wccp, wccz,
c$$$     2      wss, wssp, wssz)
c$$$c
c$$$        call creavecpotmat2x2(eps, zk, m, n, rl, ps_in, zs_in,
c$$$     1      dpdt_in, dzdt_in, norder, wa2_11, wcc, wss, w)
c$$$c
c$$$        diag = .5d0
c$$$        call creanxcurlmat2(eps, zk, m, n, rl, ps_in, zs_in,
c$$$     1      dpdt_in, dzdt_in, norder, diag, wnxcurl11, w, wcc, wss,
c$$$     2      wp, wccp, wssp, wccz, wssz)
c$$$c
c$$$        call cmatmul601(2*n, 2*n, wa2_11, n, wbeltrami_int, e1)
c$$$c
c$$$        call cmatmul601(2*n, 2*n, wnxt, 2*n, wnxcurl11, work)
c$$$        call cmatmul601(2*n, 2*n, work, n, wbeltrami_int, e2)
c$$$c
c$$$        do i = 1, 2*n
c$$$          do j = 1, n
c$$$            etop(i,j) = -zk*zk*e1(i,j) + zk*e2(i,j)
c$$$          enddo
c$$$        enddo
c$$$c
c$$$        do i = 1, n
c$$$          ctemp1(i) = h*dsdt_in(i)
c$$$          i1 = 2*(i-1)+1
c$$$            do j = 1, n
c$$$              a1(i,j) = etop(i1,j)
c$$$            enddo
c$$$        enddo
c$$$c
c$$$        ione = 1
c$$$        call cmatmul601(ione, n, ctemp1, n, a1, work)
c$$$c
c$$$        iii = 2*ind
c$$$        scl = 2*pi*ps_in(ind)
c$$$        do j = 1, n
c$$$          cmat(2*n+1,j) = -work(j)
c$$$          cmat(2*n+2,j) = -scl*etop(iii,j)
c$$$        enddo
c$$$
c$$$c
c$$$c       interior charge, exterior fluxes
c$$$c
c$$$        call cmatmul601(2*n, 2*n, wa2_21, n, wbeltrami_int, e1)
c$$$c
c$$$        call cmatmul601(2*n, 2*n, wnxt, 2*n, wnxcurl21, work)
c$$$        call cmatmul601(2*n, 2*n, work, n, wbeltrami_int, e2)
c$$$c
c$$$        do i = 1, 2*n
c$$$          do j = 1, n
c$$$            etop(i,j) = -zk*zk*e1(i,j) + zk*e2(i,j)
c$$$          enddo
c$$$        enddo
c$$$c
c$$$        do i = 1, n
c$$$          ctemp1(i) = h*dsdt_out(i)
c$$$          i1 = 2*(i-1)+1
c$$$            do j = 1, n
c$$$              a1(i,j) = etop(i1,j)
c$$$            enddo
c$$$        enddo
c$$$c
c$$$        ione = 1
c$$$        call cmatmul601(ione, n, ctemp1, n, a1, work)
c$$$c
c$$$        iii = 2*ind
c$$$        scl = 2*pi*ps_out(ind)
c$$$        do j = 1, n
c$$$          cmat(2*n+1,j) = cmat(2*n+1,j) + work(j)
c$$$          cmat(2*n+2,j) = cmat(2*n+2,j) + scl*etop(iii,j)
c$$$        enddo
c$$$
c$$$
c$$$
c$$$c
c$$$c       exterior charge, exterior fluxes
c$$$c
c$$$        call creapiecesa(eps, zk, m, n, rl, ps_out, zs_out,
c$$$     1      dpdt_out, dzdt_out, norder, w, wp, wz, wcc, wccp, wccz,
c$$$     2      wss, wssp, wssz)
c$$$c
c$$$        call creavecpotmat2x2(eps, zk, m, n, rl, ps_out, zs_out,
c$$$     1      dpdt_out, dzdt_out, norder, wa2_22, wcc, wss, w)
c$$$c
c$$$        diag = -.5d0
c$$$        call creanxcurlmat2(eps, zk, m, n, rl, ps_out, zs_out,
c$$$     1      dpdt_out, dzdt_out, norder, diag, wnxcurl22, w, wcc, wss,
c$$$     2      wp, wccp, wssp, wccz, wssz)
c$$$c
c$$$        call cmatmul601(2*n, 2*n, wa2_22, n, wbeltrami_ext, e1)
c$$$c
c$$$        call cmatmul601(2*n, 2*n, wnxt, 2*n, wnxcurl22, work)
c$$$        call cmatmul601(2*n, 2*n, work, n, wbeltrami_ext, e2)
c$$$c
c$$$        do i = 1, 2*n
c$$$          do j = 1, n
c$$$            etop(i,j) = -zk*zk*e1(i,j) + zk*e2(i,j)
c$$$          enddo
c$$$        enddo
c$$$c
c$$$        do i = 1, n
c$$$          ctemp1(i) = h*dsdt_out(i)
c$$$          i1 = 2*(i-1)+1
c$$$            do j = 1, n
c$$$              a1(i,j) = etop(i1,j)
c$$$          enddo
c$$$        enddo
c$$$c
c$$$        call cmatmul601(ione, n, ctemp1, n, a1, work)
c$$$c
c$$$        iii = 2*ind
c$$$        scl = 2*pi*ps_out(ind)
c$$$        do j = 1, n
c$$$          cmat(2*n+1,n+j) = work(j)
c$$$          cmat(2*n+2,n+j) = scl*etop(iii,j)
c$$$        enddo
c$$$c
c$$$c       exterior charge, interior fluxes
c$$$c
c$$$        call cmatmul601(2*n, 2*n, wa2_12, n, wbeltrami_ext, e1)
c$$$c
c$$$        call cmatmul601(2*n, 2*n, wnxt, 2*n, wnxcurl12, work)
c$$$        call cmatmul601(2*n, 2*n, work, n, wbeltrami_ext, e2)
c$$$c
c$$$        do i = 1, 2*n
c$$$          do j = 1, n
c$$$            etop(i,j) = -zk*zk*e1(i,j) + zk*e2(i,j)
c$$$          enddo
c$$$        enddo
c$$$c
c$$$        do i = 1, n
c$$$          ctemp1(i) = h*dsdt_in(i)
c$$$          i1 = 2*(i-1)+1
c$$$            do j = 1, n
c$$$              a1(i,j) = etop(i1,j)
c$$$          enddo
c$$$        enddo
c$$$c
c$$$        call cmatmul601(ione, n, ctemp1, n, a1, work)
c$$$c
c$$$        iii = 2*ind
c$$$        scl = 2*pi*ps_in(ind)
c$$$        do j = 1, n
c$$$          cmat(2*n+1,n+j) = cmat(2*n+1,n+j) - work(j)
c$$$          cmat(2*n+2,n+j) = cmat(2*n+2,n+j) - scl*etop(iii,j)
c$$$        enddo
c$$$
c$$$
c$$$
c$$$c       . . . and now the contribution of harmonic vector fields
c$$$c       to the fluxes
c$$$c
c$$$c       interior vecs, interior fluxes
c$$$c
c$$$        call cmatvec601(2*n, 2*n, wa2_11, mh_in, work)
c$$$c
c$$$        call cmatmul601(2*n, 2*n, wnxt, 2*n, wnxcurl11, work3)
c$$$        call cmatvec601(2*n, 2*n, work3, jh_in, work2)
c$$$c
c$$$        cda = 0
c$$$        do i = 1,n
c$$$          i2 = 2*i-1
c$$$          cd = ima*zk*work(i2) - work2(i2)
c$$$          cda = cda + h*dsdt_in(i)*cd
c$$$        enddo
c$$$c
c$$$        iii = 2*ind 
c$$$        cdb = ima*zk*work(iii) - work2(iii)
c$$$c
c$$$        cmat(2*n+1,2*n+1) = -cda
c$$$        cmat(2*n+2,2*n+1) = -2*pi*ps_in(ind)*cdb
c$$$
c$$$c
c$$$c       interior vecs, exterior fluxes
c$$$c
c$$$        call cmatvec601(2*n, 2*n, wa2_21, mh_in, work)
c$$$c
c$$$        call cmatmul601(2*n, 2*n, wnxt, 2*n, wnxcurl21, work3)
c$$$        call cmatvec601(2*n, 2*n, work3, jh_in, work2)
c$$$c
c$$$        cda = 0
c$$$        do i = 1,n
c$$$          i2 = 2*i-1
c$$$          cd = ima*zk*work(i2) - work2(i2)
c$$$          cda = cda + h*dsdt_out(i)*cd
c$$$        enddo
c$$$c
c$$$        iii = 2*ind 
c$$$        cdb = 2*pi*ps_out(ind)*(ima*zk*work(iii) - work2(iii))
c$$$c
c$$$        cmat(2*n+1,2*n+1) = cmat(2*n+1,2*n+1) + cda
c$$$        cmat(2*n+2,2*n+1) = cmat(2*n+2,2*n+1) + cdb
c$$$
c$$$c
c$$$c       exterior vecs, exterior fluxes
c$$$c
c$$$        call cmatvec601(2*n, 2*n, wa2_22, mh_out, work)
c$$$c
c$$$        call cmatmul601(2*n, 2*n, wnxt, 2*n, wnxcurl22, work3)
c$$$        call cmatvec601(2*n, 2*n, work3, jh_out, work2)
c$$$c
c$$$        cda = 0
c$$$        do i = 1,n
c$$$          i2 = 2*i-1
c$$$          cd = ima*zk*work(i2) - work2(i2)
c$$$          cda = cda + h*dsdt_out(i)*cd
c$$$        enddo
c$$$c
c$$$        iii = 2*ind 
c$$$        cdb = ima*zk*work(iii) - work2(iii)
c$$$c
c$$$        cmat(2*n+1,2*n+2) = cda
c$$$        cmat(2*n+2,2*n+2) = 2*pi*ps_out(ind)*cdb
c$$$
c$$$c
c$$$c       exterior vecs, interior fluxes
c$$$c
c$$$        call cmatvec601(2*n, 2*n, wa2_12, mh_out, work)
c$$$c
c$$$        call cmatmul601(2*n, 2*n, wnxt, 2*n, wnxcurl12, work3)
c$$$        call cmatvec601(2*n, 2*n, work3, jh_out, work2)
c$$$c
c$$$        cda = 0
c$$$        do i = 1,n
c$$$          i2 = 2*i-1
c$$$          cd = ima*zk*work(i2) - work2(i2)
c$$$          cda = cda + h*dsdt_in(i)*cd
c$$$        enddo
c$$$c
c$$$        iii = 2*ind 
c$$$        cdb = 2*pi*ps_in(ind)*(ima*zk*work(iii) - work2(iii))
c$$$c
c$$$        cmat(2*n+1,2*n+2) = cmat(2*n+1,2*n+2) - cda
c$$$        cmat(2*n+2,2*n+2) = cmat(2*n+2,2*n+2) - cdb
c$$$c
c$$$c
c$$$        return
c$$$        end
c
c
c
c
c
        subroutine creapieces_st(eps, zk, m, n, rl, ps, zs,
     1      dpdt, dzdt, ps1, zs1, dpdt1, dzdt1,
     1      w, wp, wz, wcc, wccp, wccz, wss, wssp, wssz)
         implicit real *8 (a-h,o-z)
        real *8 ps(1), zs(1),dpdt(1),dzdt(1),dsdt(10000),
     1      ps1(1),zs1(1),dpdt1(1),dzdt1(1),
     1      rnp1(10000),rnz1(10000)
        complex *16 zk,ima,w(1),wp(1),wz(1),wcc(1),wccp(1),
     1      wccz(1),wss(1),wssp(1),wssz(1)
        external ghfun2,ghfun2cc,ghfun2ss,ghfun4n1,ghfun2ccn1,
     1      ghfun2ssn1
c
c       this routine constructs all possible kernels in matrix form
c       with parameter zk for separate source-target arrangements. the
c       targets are given by ps1, zs1, etc.
c
c       output:
c         w - n by n matrix with axisymmetric kernel
c         wp - n by n matrix with target p derivative kernel
c         wz - n by n matrix with target z derivative kernel
c         wcc - n by n matrix with axisymmetric cos cos kernel
c         wccp - n by n matrix with target p derivative cos cos kernel
c         wccz - n by n matrix with target p derivative cos cos kernel
c         wss - n by n matrix with axisymmetric sin sin kernel
c         wssp - n by n matrix with target p derivative sin sin kernel
c         wssz - n by n matrix with target p derivative sin sin kernel
c       
c
        done=1.0d0
        pi=4*atan(done)
        ima=(0,1)
c
        h=rl/n
        do 1400 i=1,n
          sc=dpdt(i)**2+dzdt(i)**2
          sc=sqrt(sc)
          dsdt(i)=sc
 1400   continue
c
c       next the single layer matrices...
c
        call formslpmat(ier, w, ps, zs, dsdt, ps1, zs1,
     1      h, n, ghfun2, eps, zk, m)
c
        call formslpmat(ier, wcc, ps, zs, dsdt,ps1,zs1,h,n,
     1      ghfun2cc,eps,zk,m)
c
        call formslpmat(ier,wss,ps,zs,dsdt,ps1, zs1,h,n,
     1      ghfun2ss,eps,zk,m)
c
c       ...and the d/dp matrices...
c
        do i=1,n
          rnp1(i)=1
          rnz1(i)=0
        enddo
c
        call formsprimemat(ier, wp, ps, zs, dsdt, ps1, zs1,
     1      rnp1, rnz1, h, n, ghfun4n1, eps, zk, m)
c
        call formsprimemat(ier, wccp, ps, zs, dsdt, ps1, zs1,
     1      rnp1, rnz1, h, n, ghfun2ccn1, eps, zk, m)
c
        call formsprimemat(ier, wssp, ps, zs, dsdt, ps1, zs1,
     1      rnp1, rnz1, h, n, ghfun2ssn1, eps, zk, m)

c
c       ...and the d/dz matrices
c
        do i=1,n
          rnp1(i)=0
          rnz1(i)=1
        enddo
c
        call formsprimemat(ier, wz, ps, zs, dsdt, ps1, zs1,
     1      rnp1, rnz1, h, n, ghfun4n1, eps, zk, m)
c
        call formsprimemat(ier, wccz, ps, zs, dsdt, ps1, zs1,
     1      rnp1, rnz1, h, n, ghfun2ccn1, eps, zk, m)
c
        call formsprimemat(ier, wssz, ps, zs, dsdt, ps1, zs1,
     1      rnp1, rnz1, h, n, ghfun2ssn1, eps, zk, m)
c
        return
        end
c
c
c
c
c
        subroutine formslpmat(ier, amat, xs, ys, dsdt, xs1, ys1,
     1      h, ns, gfun, eps, zk, mode)
        implicit real *8 (a-h,o-z)
        real *8 xs(1), ys(1), dsdt(1), xs1(1), ys1(1)
        complex *16 ima,amat(ns,ns),zk,u
c
c       construct a source-target single layer matrix
c
c       input:
c         xs, ys - source points
c         xs1, ys1 - target points
c
ccccc$omp parallel do default(shared)
ccccc$omp$private(i,j,u) 
c
        do i = 1,ns
        do j = 1,ns
          call gfun(eps,zk,mode,xs1(i),ys1(i),xs(j),ys(j),u)
          amat(i,j)=u*dsdt(j)*h
        enddo
        enddo
c
ccccc$omp end parallel do
c
        return
        end
c
c
c
c
c
        subroutine formsprimemat(ier, amat, xs, ys, dsdt, xs1, ys1,
     1      rnx1, rny1, h, ns, gfun, eps, zk, mode)
        implicit real *8 (a-h,o-z)
        real *8 xs(1), ys(1), dsdt(1), xs1(1), ys1(1), rnx1(1)
        real *8 rny1(1)
        complex *16 ima, amat(ns,ns), zk, u
c
c       construct a source-target single layer matrix
c
c       input:
c         xs, ys - source points
c         xs1, ys1 - target points
c         rnx1, rny1 - normals at target points
c
c$omp parallel do default(shared)
c$omp$private(i,j,u) 
c
        do i = 1,ns
        do j = 1,ns
          call gfun(eps, zk, mode, xs1(i), ys1(i), xs(j), ys(j),
     1        rnx1(i), rny1(i), u)
          amat(i,j) = u*dsdt(j)*h
        enddo
        enddo
c
c$omp end parallel do
c
        return
        end
c
c
c
c
c
        subroutine creabeltrami_exterior(eps, zk, m, n, rl, ps, zs,
     1      dpdt, dzdt, dpdt2, dzdt2, norder, ind, tau, amat)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1),dpdt2(1),dzdt2(1),
     1      dsdt(10000)
        complex *16 ima, zk, amat(n+1,n+1), zk0, val,
     1      ch1(2,10000), ch2(2,10000), x, y,
     2      ctemp(1000000), ctemp1(1000000), ctemp2(1000000),
     3      ctemp3(1000000), ctemp4(1000000), harmj(2,10000),
     4      work(10000000), work2(10000000), cd, cd0, cd1, cd2,
     5      cda, cdb, cdb2, work3(10000000), harmm(2,10000), cdb0,
     6      uz(10000000),vz(10000000),sz(1000000)
        complex *16, allocatable :: wsurfdiv(:,:),wsurfgrad(:,:)
        complex *16, allocatable :: wsurflap(:,:),winvsl(:,:)
        complex *16, allocatable :: ws0(:,:),wsk(:,:),wsprime(:,:)
        complex *16, allocatable :: ws0div(:,:)
        complex *16, allocatable :: wgradsk(:,:),wgradrep(:,:)
        complex *16, allocatable :: wgradrepj(:,:),wgradrepm(:,:)
        complex *16, allocatable :: wa(:,:),wa2(:,:)
        complex *16, allocatable :: w(:,:),wcc(:,:),wss(:,:)
        complex *16, allocatable :: wp(:,:),wccp(:,:),wssp(:,:)
        complex *16, allocatable :: wz(:,:),wccz(:,:),wssz(:,:)
        complex *16, allocatable :: wnxcurl(:,:),wndotcurl(:,:)
        complex *16, allocatable :: wnxcurl0(:,:)
        complex *16, allocatable :: wndot(:,:),wnxt(:,:)
        complex *16, allocatable :: w1(:,:),w2(:,:),w3(:,:)
        complex *16, allocatable :: a1(:,:),a2(:,:),a3(:,:),a4(:,:)
        complex *16, allocatable :: b1(:,:),b2(:,:)
        complex *16, allocatable :: e1(:,:),e2(:,:),e3(:,:)
        complex *16, allocatable :: etop(:,:),ebottom(:,:)
        complex *16, allocatable :: wones(:,:)
        complex *16, allocatable :: wbeltrami(:,:)
        external ghfun2, ghfun4n1
c
c       this subroutine constructs the matrix that is inverted to
c       solve the full debye formulation of maxwell for a surface
c       of revolution
c
c       input:
c         eps - precision with which to calculate kernels
c         zk - the complex helmholtz parameter
c         m - fourier mode to compute
c         n - number of points in curve discretization ps,zs
c         rl - length of parameterization interval
c         ps,zs - parameterization of curve
c         dpdt,dzdt - first derivatives of parameterization
c         dpdt2,dzdt2 - second derivatives of parameterization
c         norder - the order of alpert quadratures to use
c         ind - the index of ps,zs to use for the b-cycle integration
c
c       output:
c         amat - a 2n+2 by 2n+2 matrix mapping rho, rhom, alpha, beta
c             to s0 surf div e and n \dot h, along with two extra rows
c             for integral conditions on line integrals of e
c
c
        allocate(wsurfdiv(n,2*n))
        allocate(wsurfgrad(2*n,n))
        allocate(wsurflap(n,n))
        allocate(winvsl(n,n))
c
        allocate(ws0(n,n))
        allocate(wsk(n,n))
        allocate(wsprime(n,n))
        allocate(wgradsk(2*n,n))
        allocate(wgradrep(2*n,n))
        allocate(wbeltrami(2*n,n))
        allocate(wgradrepj(2*n,n))
        allocate(wgradrepm(2*n,n))
        allocate(ws0div(n,2*n))
c
        allocate(wa(3*n,2*n))
        allocate(wa2(2*n,2*n))
        allocate(w(n,n))
        allocate(wcc(n,n))
        allocate(wss(n,n))
c
        allocate(wp(n,n))
        allocate(wccp(n,n))
        allocate(wssp(n,n))
c
        allocate(wz(n,n))
        allocate(wccz(n,n))
        allocate(wssz(n,n))
c
        allocate(wnxcurl(2*n,2*n))
        allocate(wnxcurl0(2*n,2*n))
        allocate(wndotcurl(n,2*n))
        allocate(wndot(n,3*n))
        allocate(wnxt(2*n,2*n))
c
        allocate(w1(2*n,2*n))
        allocate(w2(2*n,2*n))
        allocate(w3(2*n,2*n))
        allocate(a1(n,n),a2(n,n),a3(n,n),a4(n,n))
        allocate(b1(n,2*n),b2(n,2*n))
        allocate(e1(2*n,n),e2(2*n,n),e3(2*n,n))
        allocate(etop(2*n,n),ebottom(2*n,n))
c
        allocate(wones(n,n))
c
        ima=(0,1)
        done=1.0d0
        pi=4*atan(done)
c
c       compute dsdt around the generating curve
c
        do 1200 i=1,n
        dd=dpdt(i)**2+dzdt(i)**2
        dd=sqrt(dd)
        dsdt(i)=dd
 1200   continue

c
c       build a bunch of matrices that we'll need, first make
c       the surface div, grad, lap, and invlap
c
        call surfdivaximat(ps,zs,dpdt,dzdt,n,m,rl,wsurfdiv)
c
        call surfgradaximat(ps,zs,dpdt,dzdt,n,m,rl,wsurfgrad)
c
        ifinv=0
        call surflapaximat(ps,zs,dpdt,dzdt,dpdt2,dzdt2,n,m,rl,
     1      ifinv,wsurflap)
c
        call creainvsurflapmat(ps,zs,dpdt,dzdt,dpdt2,dzdt2,
     1      n,m,rl,winvsl)

c
c       ...some layer potential matrices
c
        h=rl/n
        call creapiecesa(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      norder,w,wp,wz,wcc,wccp,wccz,wss,wssp,wssz)
c
        zk0=0
        call formslpmatbac(ier,ws0,norder,ps,zs,dsdt,h,n,
     1      ghfun2,eps,zk0,m)
c
        call ccopy601(n*n,w,wsk)
c
        do 2400 i=1,n
        dpds=dpdt(i)/dsdt(i)
        dzds=dzdt(i)/dsdt(i)
        do 2200 j=1,n
            wsprime(i,j) = dzds*wp(i,j) - dpds*wz(i,j)
            if (i .eq. j) wsprime(i,j) = -.5d0 + wsprime(i,j)
 2200   continue
 2400   continue
c
cccc        call creagradsk(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
cccc     1      norder,wgradsk,w,wp,wz)

        call cmatmul601(2*n,n,wsurfgrad,n,wsk,wgradsk)

c
c       ...and some vector operators
c
        iftan=0
        call creavecpotmat7(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      norder,iftan,wa,wcc,wss,w)
c
        call creavecpotmat2x2(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      norder,wa2,wcc,wss,w)
c
        diag = .5d0
        call creanxcurlmat2(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      norder,diag,wnxcurl,w,wcc,wss,wp,wccp,wssp,wccz,wssz)
c
        call creandotcurlmat(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      norder,wndotcurl,w,wcc,wss,wccp,wssp,wccz,wssz)
c
        call creandotmat(n,dpdt,dzdt,wndot)
c
        call creanxtmat(n,wnxt)

c
c       update wgradrep depending on the actual representation
c       of current...
c
cccc        call cmatmul601(2*n,n,wsurfgrad,n,winvsl,wgradrep)

        call surfgradinvlapintmat(ps,zs,dpdt,dzdt,dpdt2,dzdt2,
     1      n,m,rl,wgradrep)

c
c       form the 'mean zero preserving' 'ones' matrix which is
c       used in the 0th fourier mode
c
        surfarea=0
        do i = 1, n
            surfarea = surfarea + 2*pi*ps(i)*dsdt(i)*h
        enddo
c
        h=rl/n
        do i = 1,n
        do j = 1,n
            wones(i,j) = ps(j)*h*dsdt(j)*2*pi/surfarea/1.5d0
        enddo
        enddo

c
c       construct the charge to current operators for beltrami
c       fields
c
        x = 1
        y = ima
        call cmatmul601(2*n, 2*n, wnxt, n, wgradrep, work)
        call cmatadd601(2*n, n, x, wgradrep, y, work, wbeltrami)
        call cmatmul601(n, 2*n, wndotcurl, n, wbeltrami, a3)
c
        call cmatmul601(n, 3*n, wndot, 2*n, wa, work)
        call cmatmul601(n, 2*n, work, n, wbeltrami, a1)
c
        do i = 1, n
        do j = 1, n
            amat(i,j) = -zk*zk*a1(i,j) - wsprime(i,j) 
     1          - zk*a3(i,j)
        enddo
        enddo

c
c       compute the n+1 column and row
c
        if (m .ne. 0) then
            do k = 1, n+1
                amat(n+1,k) = 0
                amat(k,n+1) = 0
            enddo
            amat(n+1,n+1)=1
            return
        endif

c
c       if here, m=0 - create contribution of harmonic fields to
c       n \cdot H
c       first compute the two harmonic vector fields
c
        call get2harmvecs(n, ps, zs, ch1, ch2)

        do i = 1, n
          harmm(1,i) = ch1(1,i) + ima*ch2(1,i)
          harmm(2,i) = ch1(2,i) + ima*ch2(2,i)
        enddo
c
c       . . . their contribution to the normal component
c
        ione = 1
        call cmatmul601(n, 3*n, wndot, 2*n, wa, work2)
        call cmatvec601(n, 2*n, work2, harmm, work)
c
        call cmatvec601(n, 2*n, wndotcurl, harmm, work2)
c
        do i = 1, n
            amat(i,n+1) = ima*zk*work(i) + ima*work2(i)
        enddo
        
c
c       . . . rhom's contribution to the line integral(s)
c       one around the A cycle and one around the B cycle...
c
c
        call cmatmul601(2*n, 2*n, wa2, n, wbeltrami, e1)
c
        call cmatmul601(2*n, 2*n, wnxt, 2*n, wnxcurl, work)
        call cmatmul601(2*n, 2*n, work, n, wbeltrami, e2)
c
        do i = 1, 2*n
        do j = 1, n
            etop(i,j) = -zk*zk*e1(i,j) + zk*e2(i,j)
        enddo
        enddo
c
        do i = 1, n
            ctemp1(i) = h*dsdt(i)
            i1 = 2*(i-1)+1
            do j = 1, n
                a1(i,j) = etop(i1,j)
            enddo
        enddo
c
        ione = 1
        call cmatmul601(ione, n, ctemp1, n, a1, work)
c
        iii = 2*ind
        scl = 2*pi*ps(ind)
        do j = 1, n
            amat(n+1,j) = tau*work(j) + (1-tau)*etop(iii,j)*scl
        enddo
c
c       and now the contribution of j_h and m_h
c       . . . first the vector potential
c
        call cmatvec601(2*n, 2*n, wa2, harmm, work)
c
        cda = 0
        do i = 1, n
            i2 = 2*(i-1) + 1
            cda = cda + h*dsdt(i)*ima*zk*work(i2)
        enddo
c
        cda = tau*cda + (1-tau)*ima*zk*work(iii)*scl
c
c       . . . then the curl of the vector potential
c
        call cmatmul601(2*n, 2*n, wnxt, 2*n, wnxcurl, w3)
        call cmatvec601(2*n, 2*n, w3, harmm, work)
c
        cdb0 = 0
        do i = 1, n
            i2 = 2*(i-1) + 1
            cdb0 = cdb0 - ima*h*dsdt(i)*work(i2)
        enddo
c
        cdb0 = tau*cdb0 - ima*(1-tau)*work(iii)*scl
        amat(n+1,n+1) = (cda + cdb0)
        
c
        return
        end
c
c
c
c
c
        subroutine creabeltrami_interior(eps, zk, m, n, rl, ps, zs,
     1      dpdt, dzdt, dpdt2, dzdt2, norder, ind, tau, amat)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1),dpdt2(1),dzdt2(1),
     1      dsdt(10000)
        complex *16 ima, zk, amat(n+1,n+1), zk0, val,
     1      ch1(2,10000), ch2(2,10000), x, y,
     2      ctemp(1000000), ctemp1(1000000), ctemp2(1000000),
     3      ctemp3(1000000), ctemp4(1000000), harmj(2,10000),
     4      work(10000000), work2(10000000), cd, cd0, cd1, cd2,
     5      cda, cdb, cdb2, work3(10000000), harmm(2,10000), cdb0,
     6      uz(10000000),vz(10000000),sz(1000000)
        complex *16, allocatable :: wsurfdiv(:,:),wsurfgrad(:,:)
        complex *16, allocatable :: wsurflap(:,:),winvsl(:,:)
        complex *16, allocatable :: ws0(:,:),wsk(:,:),wsprime(:,:)
        complex *16, allocatable :: ws0div(:,:)
        complex *16, allocatable :: wgradsk(:,:),wgradrep(:,:)
        complex *16, allocatable :: wgradrepj(:,:),wgradrepm(:,:)
        complex *16, allocatable :: wa(:,:),wa2(:,:)
        complex *16, allocatable :: w(:,:),wcc(:,:),wss(:,:)
        complex *16, allocatable :: wp(:,:),wccp(:,:),wssp(:,:)
        complex *16, allocatable :: wz(:,:),wccz(:,:),wssz(:,:)
        complex *16, allocatable :: wnxcurl(:,:),wndotcurl(:,:)
        complex *16, allocatable :: wnxcurl0(:,:)
        complex *16, allocatable :: wndot(:,:),wnxt(:,:)
        complex *16, allocatable :: w1(:,:),w2(:,:),w3(:,:)
        complex *16, allocatable :: a1(:,:),a2(:,:),a3(:,:),a4(:,:)
        complex *16, allocatable :: b1(:,:),b2(:,:)
        complex *16, allocatable :: e1(:,:),e2(:,:),e3(:,:)
        complex *16, allocatable :: etop(:,:),ebottom(:,:)
        complex *16, allocatable :: wones(:,:)
        complex *16, allocatable :: wbeltrami(:,:)
        external ghfun2, ghfun4n1
c
c       this subroutine constructs the matrix that is inverted to
c       solve the full debye formulation of maxwell for a surface
c       of revolution
c
c       input:
c         eps - precision with which to calculate kernels
c         zk - the complex helmholtz parameter
c         m - fourier mode to compute
c         n - number of points in curve discretization ps,zs
c         rl - length of parameterization interval
c         ps,zs - parameterization of curve
c         dpdt,dzdt - first derivatives of parameterization
c         dpdt2,dzdt2 - second derivatives of parameterization
c         norder - the order of kapur-rokhlin quadratures to 
c             use, 2, 6, or 10
c         ind - the index of ps,zs to use for the b-cycle integration
c
c       output:
c         amat - a 2n+2 by 2n+2 matrix mapping rho, rhom, alpha, beta
c             to s0 surf div e and n \dot h, along with two extra rows
c             for integral conditions on line integrals of e
c
c
c        allocate(wsurfdiv(n,2*n))
        allocate(wsurfgrad(2*n,n))
c        allocate(wsurflap(n,n))
        allocate(winvsl(n,n))
c
        allocate(ws0(n,n))
        allocate(wsk(n,n))
        allocate(wsprime(n,n))
cccc        allocate(wgradsk(2*n,n))
        allocate(wgradrep(2*n,n))
        allocate(wbeltrami(2*n,n))
        allocate(wgradrepj(2*n,n))
        allocate(wgradrepm(2*n,n))
        allocate(ws0div(n,2*n))
c
        allocate(wa(3*n,2*n))
        allocate(wa2(2*n,2*n))
        allocate(w(n,n))
        allocate(wcc(n,n))
        allocate(wss(n,n))
c
        allocate(wp(n,n))
        allocate(wccp(n,n))
        allocate(wssp(n,n))
c
        allocate(wz(n,n))
        allocate(wccz(n,n))
        allocate(wssz(n,n))
c
        allocate(wnxcurl(2*n,2*n))
        allocate(wnxcurl0(2*n,2*n))
        allocate(wndotcurl(n,2*n))
        allocate(wndot(n,3*n))
        allocate(wnxt(2*n,2*n))
c
        allocate(w1(2*n,2*n))
        allocate(w2(2*n,2*n))
        allocate(w3(2*n,2*n))
        allocate(a1(n,n),a2(n,n),a3(n,n),a4(n,n))
        allocate(b1(n,2*n),b2(n,2*n))
        allocate(e1(2*n,n),e2(2*n,n),e3(2*n,n))
        allocate(etop(2*n,n),ebottom(2*n,n))
c
        allocate(wones(n,n))
c
        ima=(0,1)
        done=1.0d0
        pi=4*atan(done)
c
c       compute dsdt around the generating curve
c
        do 1200 i=1,n
        dd=dpdt(i)**2+dzdt(i)**2
        dd=sqrt(dd)
        dsdt(i)=dd
 1200   continue

c
c       build a bunch of matrices that we'll need, first make
c       the surface div, grad, lap, and invlap
c
c        call surfdivaximat(ps,zs,dpdt,dzdt,n,m,rl,wsurfdiv)
c
        call surfgradaximat(ps,zs,dpdt,dzdt,n,m,rl,wsurfgrad)
c
c        ifinv=0
c        call surflapaximat(ps,zs,dpdt,dzdt,dpdt2,dzdt2,n,m,rl,
c     1      ifinv,wsurflap)
c
        call creainvsurflapmat(ps,zs,dpdt,dzdt,dpdt2,dzdt2,
     1      n,m,rl,winvsl)

c
c       ...some layer potential matrices
c
        h=rl/n
        call creapiecesa(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      norder,w,wp,wz,wcc,wccp,wccz,wss,wssp,wssz)
c
        zk0=0
        call formslpmatbac(ier,ws0,norder,ps,zs,dsdt,h,n,
     1      ghfun2,eps,zk0,m)
c
        call ccopy601(n*n,w,wsk)
c
        do 2400 i=1,n
        dpds=dpdt(i)/dsdt(i)
        dzds=dzdt(i)/dsdt(i)
        do 2200 j=1,n
            wsprime(i,j) = dzds*wp(i,j) - dpds*wz(i,j)
            if (i .eq. j) wsprime(i,j) = .5d0 + wsprime(i,j)
 2200   continue
 2400   continue
c
cccc        call creagradsk(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
cccc     1      norder,wgradsk,w,wp,wz)

cccc        call cmatmul601(2*n,n,wsurfgrad,n,wsk,wgradsk)

c
c       ...and some vector operators
c
        iftan=0
        call creavecpotmat7(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      norder,iftan,wa,wcc,wss,w)
c
        call creavecpotmat2x2(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      norder,wa2,wcc,wss,w)
c
c       this is most likely the correct sign
c
        diag = -.5d0
        call creanxcurlmat2(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      norder,diag,wnxcurl,w,wcc,wss,wp,wccp,wssp,wccz,wssz)
c
        call creandotcurlmat(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      norder,wndotcurl,w,wcc,wss,wccp,wssp,wccz,wssz)
c
        call creandotmat(n,dpdt,dzdt,wndot)
c
        call creanxtmat(n,wnxt)

c
c       update wgradrep depending on the actual representation
c       of current...
c
        call cmatmul601(2*n,n,wsurfgrad,n,winvsl,wgradrep)

cccc        call surfgradinvlapintmat(ps,zs,dpdt,dzdt,dpdt2,dzdt2,
cccc     1      n,m,rl,wgradrep)

c
c       form the 'mean zero preserving' 'ones' matrix which is
c       used in the 0th fourier mode
c
c        surfarea=0
c        do i = 1, n
c            surfarea = surfarea + 2*pi*ps(i)*dsdt(i)*h
c        enddo
c
        h=rl/n
c        do i = 1,n
c        do j = 1,n
c            wones(i,j) = ps(j)*h*dsdt(j)*2*pi/surfarea/1.5d0
c        enddo
c        enddo

c
c       construct the charge to current operators for beltrami
c       fields
c
        x = 1
        y = -ima
        call cmatmul601(2*n, 2*n, wnxt, n, wgradrep, work)
        call cmatadd601(2*n, n, x, wgradrep, y, work, wbeltrami)
        call cmatmul601(n, 2*n, wndotcurl, n, wbeltrami, a3)
c
        call cmatmul601(n, 3*n, wndot, 2*n, wa, work)
        call cmatmul601(n, 2*n, work, n, wbeltrami, a1)
c
        do i = 1, n
        do j = 1, n
            amat(i,j) = -zk*zk*a1(i,j) - wsprime(i,j) 
     1          - zk*a3(i,j)
cccc            if (m .eq. 0) amat(i,j) = amat(i,j) + wones(i,j)
        enddo
        enddo

c
c       compute the n+1 column and row
c
        if (m .ne. 0) then
          do k = 1, n
            amat(n+1,k) = 0
            amat(k,n+1) = 0
          enddo
          amat(n+1,n+1)=1
          return
        endif

c
c       if here, m=0 - create contribution of harmonic fields to
c       n \cdot H
c       first compute the two harmonic vector fields
c
        call get2harmvecs(n, ps, zs, ch1, ch2)
c
        do i = 1, n
            harmj(1,i) = ch1(1,i) - ima*ch2(1,i)
            harmj(2,i) = ch1(2,i) - ima*ch2(2,i)
            harmm(1,i) = -ima*harmj(1,i)
            harmm(2,i) = -ima*harmj(2,i)
        enddo

c       . . . their contribution to the normal component
c
        ione = 1
        call cmatmul601(n, 3*n, wndot, 2*n, wa, work2)
        call cmatvec601(n, 2*n, work2, harmm, work)
c
        call cmatvec601(n, 2*n, wndotcurl, harmj, work2)
c
        do i = 1, n
            amat(i,n+1) = (ima*zk*work(i) + work2(i))
        enddo

c
c       . . . rhom's contribution to the line integral(s)
c       one around the A cycle and one around the B cycle...
c
c
        call cmatmul601(2*n, 2*n, wa2, n, wbeltrami, e1)
c
        call cmatmul601(2*n, 2*n, wnxt, 2*n, wnxcurl, work)
        call cmatmul601(2*n, 2*n, work, n, wbeltrami, e2)
c
        do i = 1, 2*n
        do j = 1, n
            etop(i,j) = -zk*zk*e1(i,j) + zk*e2(i,j)
        enddo
        enddo
c
        do i = 1, n
            ctemp1(i) = h*dsdt(i)
            i1 = 2*(i-1)+1
            do j = 1, n
                a1(i,j) = etop(i1,j)
            enddo
        enddo
c
        call cmatmul601(ione, n, ctemp1, n, a1, work)
c
        iii = 2*ind
        scl = 2*pi*ps(ind)
        do j = 1, n
            amat(n+1,j) = tau*work(j) + (1-tau)*etop(iii,j)*scl
cccc            amat(n+1,j) = work(j)
        enddo

c
c       and now the contribution of j_h and m_h
c       . . . first m_h
c
        call cmatvec601(2*n, 2*n, wa2, harmm, work)
c
        cda = 0
        do i = 1, n
            i2 = 2*(i-1) + 1
            cda = cda + h*dsdt(i)*ima*zk*work(i2)
        enddo
c
        cda = tau*cda + (1-tau)*ima*zk*work(iii)*scl
c
c       . . . then j_h
c
        call cmatmul601(2*n, 2*n, wnxt, 2*n, wnxcurl, w3)
        call cmatvec601(2*n, 2*n, w3, harmj, work)
c
        cdb0 = 0
        do i = 1, n
            i2 = 2*(i-1) + 1
            cdb0 = cdb0 - h*dsdt(i)*work(i2)
        enddo
c
        cdb0 = tau*cdb0 - (1-tau)*work(iii)*scl
        amat(n+1,n+1) = (cda+  cdb0)
        
c
c       scale the last row by zk
c
        do j = 1, n+1
cccc            amat(n+1,j) = amat(n+1,j)/zk
        enddo
c
        return
        end
c
c
c
c
c
        subroutine jh1diff(eps, zk, n, rl, ps, zs, dpdt, 
     1      dzdt, norder, cdb)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1),dsdt(10000),
     1      zeros(10000), ones(10000)
        complex *16 zk,ima,cdb,c1,c2,c3,zk0,cdb0,cdb5,sigma(10000),
     1      cd, vec1(10000), vec2(10000), vec3(10000), y(10000)
        external gccdiff, gccn1diff
c
        complex *16, allocatable :: wcc(:,:), wccp(:,:), wccz(:,:)
c
        allocate(wcc(n,n))
        allocate(wccp(n,n))
        allocate(wccz(n,n))
c
c       this subroutine carefully computes the difference integral of the 
c       H field due to j_h1 + ima*j_h2 along an a cycle
c
        done=1.0d0
        pi=4*atan(done)
        ima=(0,1)
        m=0
c
        do 1400 i=1,n
        dd=dpdt(i)**2+dzdt(i)**2
        dsdt(i)=sqrt(dd)
 1400   continue

c
c       evaluate each of the three terms carefully
c
        do i = 1, n
            sigma(i) = 1
            ones(i) = 1
            zeros(i) = 0
        enddo
c
        h=rl/n
c
        call formslpmatbac(ier, wcc, norder, ps, zs, dsdt, h, n,
     1      gccdiff, eps, zk, m)
c
        call formsprimematbac(ier, wccp, norder, ps, zs, ones,
     1      zeros, dsdt, h, n, gccn1diff, eps, zk, m)
c
        call formsprimematbac(ier, wccp, norder, ps, zs, zeros,
     1      ones, dsdt, h, n, gccn1diff, eps, zk, m)
c
        call cmatvec601(n, n, wccz, sigma, vec1)
        call cmatvec601(n, n, wcc, sigma, vec2)
        call cmatvec601(n, n, wccp, sigma, vec3)
c
        do i = 1, n
            y(i) = dpdt(i)/dsdt(i)*vec1(i)
     1          - dzdt(i)/dsdt(i)/ps(i)*vec2(i)
     2          - dzdt(i)/dsdt(i)*vec3(i)
            y(i) = ima*y(i)
        enddo
c
        cdb = 0
        do i = 1, n
            cdb = cdb + h*dsdt(i)*y(i)
        enddo
c
        return
        end
c
c
c
c
c
        subroutine gccdiff(eps, zk, m, p, z, p0, z0, val)
        implicit real *8 (a-h,o-z)
        complex *16 ima,val,rint,funcc,zk
        real *8 p1(10),p2(10)
        external funcc
c
        ima=(0,1)
        pi=4*atan(1.0d0)
c
        a=(p-p0)**2+(z-z0)**2+4*p*p0
        b=4*p*p0/a
c
        p1(1)=0
        p1(2)=a
        p1(3)=b
        p1(4)=real(zk)
        p1(5)=imag(zk)
c
        p2(1)=m+.1
        if (m .lt. 0) p2(1)=m-.1
        p2(2)=p
        p2(3)=z
        p2(4)=p0
        p2(5)=z0
c
        a4=0
        b4=pi/2
        k4=16
c
        call cadapgau(ier,a4,b4,funcc,p1,p2,k4,eps,
     1      rint,maxrec,numint)
c
        if (ier .ne. 0) then
          call prinf('in cadapgau, ier=*',ier,1)
          stop
        endif
c
        rk=sqrt(b)

        d=((p-p0)**2+(z-z0)**2)/a
        rkp=sqrt(d)

        call ke_eva(rk,rkp,fk,fe)
cccc        call elliptic_ke(rk,fk,fe)


cccc        call prin2('zk=*',zk,2)
cccc        call prin2('rint=*',rint,2)


        val=rint
        val=val+fk*4/sqrt(a)
        val=val/4/pi
        val=val*p0
c
        return
        end
c
c
c
        function fcc(t,par1,par2)
        implicit real *8 (a-h,o-z)
        complex *16 fcc,val,ima,zz,zz2,z3,zk
        real *8 par1(1),par2(1)
c
        ima=(0,1)
        pi=4*atan(1.0d0)
        a=par1(2)
        b=par1(3)
        zk=par1(4)+ima*par1(5)
c
        m=par2(1)
        m = 0
c
        p=par2(2)
        z=par2(3)
        p0=par2(4)
        z0=par2(5)
c
        d=(p**2+p0**2-2*p*p0*cos(t)+(z-z0)**2)
        d=sqrt(d)
        zikr=ima*zk*d
        sc=abs(zikr)
c
        thresh=1.0d-3
        if (sc .ge. thresh) z3 = (exp(zikr) - 1)/zikr
        if (sc .lt. thresh) then
          z3=1+zikr/2+zikr**2/3/2+zikr**3/4/3/2+zikr**4/5/4/3/2
          z3=z3+zikr**5/6/5/4/3/2+zikr**6/7/6/5/4/3/2
          z3=z3+zikr**7/8/7/6/5/4/3/2
        endif
c
        zz = cos(t)*z3*ima/4/pi
        fcc = zz
c

        return
        end
c
c
c
c
c
        subroutine gccn1diff(eps,zk,m,p,z,p0,z0,vnp,vnz,val)
        implicit real *8 (a-h,o-z)
        complex *16 zk,val,ima,dp,dz
c
c       returns the directional derivative at the target of ghfun2cc,
c       see ghfun2cc for description
c
        ima=(0,1)
        call gccdiffgrad(eps,zk,m,p,z,p0,z0,dp,dz)
        val=vnp*dp+vnz*dz
c
        return
        end
c
c
c
c
c
        subroutine gccdiffgrad(eps,zk,m,p,z,p0,z0,dp,dz)
        implicit real *8 (a-h,o-z)
        real *8 p1(10),p2(10)
        complex *16 zk,val,ima,fccp,fccz,rint,rint2,dp,dz
        external fccp, fccz
c
c       this subroutine calculates the partial derivatives of
c       ghfun2cc with respect to p and z
c
c       input:
c           eps - the precision to pass to the adaptive gaussian
c                 quadrature routine
c           zk - the complex helmholtz parameter
c           m - the fourier mode to evaluate
c           p,z - cylindrical coordinates for target point
c           p0,z0 - cylindrical coordinates for source point
c
c       output:
c           dp - the p partial derivative of ghfun2cc
c           dz - the z partial derivative of ghfun2cc
c
c
        ima=(0,1)
        pi=4*atan(1.0d0)
c
        a=(p-p0)**2+(z-z0)**2+4*p*p0
        b=4*p*p0/a
c
        p1(1)=0
        p1(2)=a
        p1(3)=b
        p1(4)=real(zk)
        p1(5)=imag(zk)
c
        p2(1)=m+.1
        if (m .lt. 0) p2(1)=m-.1
        p2(2)=p
        p2(3)=z
        p2(4)=p0
        p2(5)=z0
c
        a4=0
        b4=pi/2
        k4=16
c
        call cadapgau(ier,a4,b4,fccp,p1,p2,k4,eps,
     1      dp,maxrec,numint)
        dp=dp*p0/4/pi
c
        if (ier .ne. 0) then
          call prinf('in cadapgau, ier=*',ier,1)
          stop
        endif
c
        call cadapgau(ier,a4,b4,fccz,p1,p2,k4,eps,
     1      dz,maxrec,numint)
        dz=dz*p0/4/pi
c
        if (ier .ne. 0) then
          call prinf('in cadapgau, ier=*',ier,1)
          stop
        endif
c
        return
        end
c
c
c
        function fccp(t,par1,par2)
        implicit real *8 (a-h,o-z)
        complex *16 fccp,val,ima,zz,zz2,z3,zk
        real *8 par1(1),par2(1)
c
        ima=(0,1)
        pi=4*atan(1.0d0)
        a=par1(2)
        b=par1(3)
        zk=par1(4)+ima*par1(5)
c
        m=par2(1)
c
        p=par2(2)
        z=par2(3)
        p0=par2(4)
        z0=par2(5)
c
        d=(p-p0)**2+4*p*p0*sin(t)**2+(z-z0)**2
        d=sqrt(d)
c
        zz=(1-2*sin(m*t)**2)*(1-2*sin(t)**2)
        zz=zz*4*exp(ima*zk*d)*(p-p0+2*p0*sin(t)**2)
        zz=zz*(ima*zk-1/d)/d/d
c
        fccp=zz
c
        return
        end
c
c
c
        function fccz(t,par1,par2)
        implicit real *8 (a-h,o-z)
        complex *16 fccz,val,ima,zz,zz2,z3,zk
        real *8 par1(1),par2(1)
c
        ima=(0,1)
        pi=4*atan(1.0d0)
        a=par1(2)
        b=par1(3)
        zk=par1(4)+ima*par1(5)
c
        m=par2(1)
c
        p=par2(2)
        z=par2(3)
        p0=par2(4)
        z0=par2(5)
c
        d=(p-p0)**2+4*p*p0*sin(t)**2+(z-z0)**2
        d=sqrt(d)
c
        zz=(1-2*sin(m*t)**2)*(1-2*sin(t)**2)
        zz=zz*4*exp(ima*zk*d)*(z-z0)
        zz=zz*(ima*zk-1/d)/d/d
c
        fccz=zz
c
        return
        end
c
c
c
c
c
        subroutine ndotdirect(n,dpdt,dzdt,field,vals)
        implicit real *8 (a-h,o-z)
        real *8 dpdt(1),dzdt(1)
        complex *16 field(3,1),vals(1)
c
c       apply n \cdot directly to the vector field
c
        do 1600 i=1,n
        dsdt=sqrt(dpdt(i)**2+dzdt(i)**2)
        dpds=dpdt(i)/dsdt
        dzds=dzdt(i)/dsdt
        vals(i)=dzds*field(1,i)-dpds*field(3,i)
 1600   continue
c
        return
        end
c
c
c
c
c
        subroutine nxdirect(n, field, fieldout)
        implicit real *8 (a-h,o-z)
        complex *16 field(2,1), fieldout(2,1)
c
c       apply n \times directly to a tangential vector field
c
        do i = 1, n
            fieldout(1,i) = field(2,i)
            fieldout(2,i) = -field(1,i)
        enddo
c
        return
        end
c
c
c
c
c
        subroutine get2harmvecs(n,ps,zs,ch1,ch2)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1)
        complex *16 ch1(2,1),ch2(2,1)
c
c       this subroutine fills ch1 and ch2 with 2 vectors
c       who span the space of harmonic vector fields, following
c       the convention that ch2 = n \times ch1
c
c       input:
c         n - length of ps,zs
c         ps,zs - points describing the generating curve for the 
c             surface of revolution
c
c       output:
c         ch1,ch2 - two harmonic vector fields on a surface of rotation
c             with 1 hole, constructed such that ch2 = n \times ch1
c
c
        do 1200 i=1,n
        ch1(1,i)=1/ps(i)
        ch1(2,i)=0
        ch2(1,i)=0
        ch2(2,i)=-1/ps(i)
 1200   continue
c
        return
        end
c
c
c
c
c
c$$$        subroutine adebyeeva(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
c$$$     1      dpdt2,dzdt2,norder,ifj,rho,rhom,zjs,zms,alpha,beta,
c$$$     2      p,theta,z,avec)
c$$$        implicit real *8 (a-h,o-z)
c$$$        real *8 ps(1),zs(1),dpdt(1),dzdt(1),dpdt2(1),dzdt2(1)
c$$$        complex *16 ima,zk,rho(1),rhom(1),alpha,beta,avec(1),
c$$$     1      zjs(2,1),zms(2,1),sc,
c$$$     2      vals1(100),vals2(100),vals3(100),
c$$$     3      zjs2(2,10000),zms2(2,10000)
c$$$c
c$$$c       evaluate the e field generated by rho, rhom, alpha, beta
c$$$c       (or zjs, zms) on the surface ps,zs at the point p,theta,z 
c$$$c       which is OFF the surface
c$$$c
c$$$c       input:
c$$$c         eps - the precision to whicih kernels will be evaluated
c$$$c         zk - the complex helmholz parameter
c$$$c         m - the fourier mode to compute
c$$$c         n - number of points in the generator curve discritization
c$$$c         rl - length of parameterizing interval
c$$$c         ps,zs - points on the generating curve
c$$$c         dpdt,dzdt - derivative of ps,zs with respect to parameterizing
c$$$c             variable, no necessarily arclength
c$$$c         dpdt2,dzdt2 - 2nd derivative of ps,zs with respect to 
c$$$c             parameterizing variable, no necessarily arclength
c$$$c         ifj - if zjs and zms have been created by a previous
c$$$c             call to charge2current, edebyeeva, or hdebyeeva set
c$$$c             this to 1 to skip that computation, 0 otherwise
c$$$c         rho,rhom - electric and magnetic charges
c$$$c         zjs,zms - current constructed from rho, rhom, alpha, beta.
c$$$c             only an input variable if ifj=1, otherwise, an output.
c$$$c         alpha,beta - coefficients of the harmonic forms
c$$$c         p,theta,z - point in cylindrical coordinates at which to
c$$$c             evaluate the e field
c$$$c
c$$$c       output:
c$$$c         avec - the "vector potential" at the point p,theta,z - note that
c$$$c             this is the classical vector potential used in the
c$$$c             representation
c$$$c
c$$$c                       E = ik A - grad phi
c$$$c                       H = curl A
c$$$c
c$$$c
c$$$        ima=(0,1)
c$$$        done=1
c$$$        pi=4*atan(done)
c$$$c
c$$$c       create the electric and magnetic currents if necessary
c$$$c
c$$$        if (ifj .eq. 0) then
c$$$        irep=0
c$$$        call charge2current(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
c$$$     1      dpdt2,dzdt2,norder,rho,rhom,alpha,beta,irep,zjs,zms)
c$$$        endif
c$$$
c$$$
c$$$c
c$$$c       just scale them things for now
c$$$c
c$$$        sc=zk
c$$$        do 1600 i=1,n
c$$$        zjs2(1,i)=zjs(1,i)/sc
c$$$        zjs2(2,i)=zjs(2,i)/sc
c$$$        zms2(1,i)=zms(1,i)/sc
c$$$        zms2(2,i)=zms(2,i)/sc
c$$$ 1600 continue
c$$$
c$$$
c$$$c
c$$$c       ...calculate the vector potential A
c$$$c
c$$$        call vecpoteval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
c$$$     1      zjs2,p,theta,z,vals1)
c$$$
c$$$c
c$$$c       ...calculate the curl of the vector potential Q
c$$$c
c$$$        call curlaeval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
c$$$     1      zms2,p,theta,z,vals3)
c$$$
c$$$c
c$$$c       ...and combine them
c$$$c
c$$$        i1=1
c$$$        i2=-1
c$$$        i3=-1
c$$$        avec(1)=i1*ima*zk*vals1(1)+i3*vals3(1)
c$$$        avec(2)=i1*ima*zk*vals1(2)+i3*vals3(2)
c$$$        avec(3)=i1*ima*zk*vals1(3)+i3*vals3(3)
c$$$c
c$$$        return
c$$$        end
c$$$c
c$$$c
c$$$c
c$$$c
c$$$c 
        subroutine beltramicurrent(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      dpdt2,dzdt2,norder,rhom, alpha, rho, zjs, zms)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1),dpdt2(1),dzdt2(1),
     1      dsdt(10000)
        complex *16 ima,zk,rho(1),rhom(1),alpha,beta,zk0,
     1      zjs(2,1),zms(2,1),ch1(2,10000),ch2(2,10000),
     2      zjr(2,10000),zjrm(2,10000),zjrm2(2,10000),
     3      wmat(2000000),work(1000000),work2(10000),work3(10000)
        external ghfun2
c
c       this subroutine constructs electric current and magnetic
c       current from the electric and magnetic charges, rho, rhom
c       and the coefficients of the harmonic vector fields, alpha,
c       beta
c
c       input:
c         eps - precision with which to calculate kernels
c         zk - the complex helmholtz parameter
c         m - fourier mode to compute
c         n - number of points in curve discretization ps,zs
c         rl - length of parameterization interval
c         ps,zs - parameterization of curve
c         dpdt,dzdt - first derivatives of parameterization
c         dpdt2,dzdt2 - second derivatives of parameterization
c         rhom - the magnetic charge
c         alpha - coefficient of first harmonic vector form
c
c       output:
c           rho - 
c         zjs - tangential electric current
c         zms - tangential magnetic current
c
c
        ima=(0,1)
        done=1
        pi=4*atan(done)
c
        do i = 1, n
            dsdt(i)=sqrt(dpdt(i)**2+dzdt(i)**2)
        enddo
c
c       construct the harmonic vector fields if mode=0
c
        if (m .eq. 0) call get2harmvecs(n,ps,zs,ch1,ch2)
c
        do i = 1, n
            ch1(1,i) = ch1(1,i) - ima*ch2(1,i)
            ch1(2,i) = ch1(2,i) - ima*ch2(2,i)
        enddo

c
c       use the rho and rhom representation...
c
        do i = 1, n
            rho(i) = ima*rhom(i)
        enddo
c
        call surfgradinvlapintmat(ps,zs,dpdt,dzdt,dpdt2,dzdt2,
     1      n,m,rl,wmat)
c
        call cmatvec601(2*n, n, wmat, rho, zjr)
        call nxdirect(n, zjr, zjrm)

c
c       and assemble
c
        do i = 1, n
            zjs(1,i)=ima*zk*(zjr(1,i) - ima*zjrm(1,i))
            zjs(2,i)=ima*zk*(zjr(2,i) - ima*zjrm(2,i))
        enddo
c
        if (m .eq. 0) then
            do i = 1, n
                zjs(1,i) = zjs(1,i) + alpha*ch1(1,i)
                zjs(2,i) = zjs(2,i) + alpha*ch1(2,i)
            enddo
        endif
c
        do i = 1, n
            zms(1,i) = -ima*zjs(1,i)
            zms(2,i) = -ima*zjs(2,i)
        enddo
c
        return
        end
c
c
c
c
c
        subroutine beltramieva(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      dpdt2,dzdt2,sigma,alpha,zvs,
     2      p,theta,z,field)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1),dpdt2(1),dzdt2(1)
        complex *16 ima,zk,sigma(1),alpha,field(1),
     2      vals1(100),vals2(100),vals3(100)
c
c       evaluate the beltrami field generated by sigma and current zvs
c       on the surface ps,zs at the point p,theta,z which is OFF
c       the surface
c
c       input:
c         eps - the precision to whicih kernels will be evaluated
c         zk - the complex helmholz parameter
c         m - the fourier mode to compute
c         n - number of points in the generator curve discritization
c         rl - length of parameterizing interval
c         ps,zs - points on the generating curve
c         dpdt,dzdt - derivative of ps,zs with respect to parameterizing
c             variable, no necessarily arclength
c         dpdt2,dzdt2 - 2nd derivative of ps,zs with respect to 
c             parameterizing variable, no necessarily arclength
c         sigma - beltrami charge
c         zvs - beltrami current
c         alpha - beltrami harm vec coefficient
c         p,theta,z - point in cylindrical coordinates at which to
c             evaluate the e field
c
c       output:
c         hfield - the beltrami field in cylindrical coordinates 
c             at the point p,theta,z
c
c
        ima=(0,1)
        done=1
        pi=4*atan(done)
c
c       ...calculate the vector potential Q
c
        call vecpoteval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      zvs,p,theta,z,vals1)
c
c       ...calculate the gradient of psi
c
        call ghfun2grad(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      sigma,p,theta,z,vals2)
c
c       ...calculate the curl of the vector potential Q
c
        call curlaeval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      zvs,p,theta,z,vals3)

c
c       ...and combine them
c
        i1 = 1
        i2 = -1
        i3 = 1
c
        field(1)=i1*ima*zk*vals1(1)+i2*vals2(1)+i3*ima*vals3(1)
        field(2)=i1*ima*zk*vals1(2)+i2*vals2(2)+i3*ima*vals3(2)
        field(3)=i1*ima*zk*vals1(3)+i2*vals2(3)+i3*ima*vals3(3)
c
        return
        end
c
c
c
c
c
        subroutine formbcurr1(eps,zk,m,n,rl,ps,zs,dpdt,
     1      dzdt, dpdt2, dzdt2, sigma, alpha, zvs)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1),dpdt2(1),dzdt2(1)
        complex *16 ima, zk, sigma(1), alpha, zvs(2,1),
     1      ch1(2,10000), ch2(2,10000), zvtemp(2,10000),
     2      zvtemp2(2,10000),
     3      wmat(2000000),work(1000000)
c
c       this subroutine constructs beltrami current on the surface
c       for the exterior field representation
c
c       input:
c         eps - precision with which to calculate kernels
c         zk - the complex helmholtz parameter
c         m - fourier mode to compute
c         n - number of points in curve discretization ps,zs
c         rl - length of parameterization interval
c         ps,zs - parameterization of curve
c         dpdt,dzdt - first derivatives of parameterization
c         dpdt2,dzdt2 - second derivatives of parameterization
c         sigma - the beltrami charge
c         alpha - coefficient of tangential harmonic vector field
c
c       output:
c         zvs - tangential beltrami current
c
c
        ima=(0,1)
        done=1
        pi=4*atan(done)
c
c
c       build the current
c
        call surfgradinvlapintmat(ps,zs,dpdt,dzdt,dpdt2,dzdt2,
     1      n,m,rl,wmat)
c
        call cmatvec601(2*n, n, wmat, sigma, zvtemp)
        call nxdirect(n, zvtemp, zvtemp2)

c
c       and assemble
c
        do i = 1, n
            zvs(1,i)=ima*zk*(zvtemp(1,i) + ima*zvtemp2(1,i))
            zvs(2,i)=ima*zk*(zvtemp(2,i) + ima*zvtemp2(2,i))
        enddo
c
        if (m .ne. 0) return

c
c       construct the harmonic vector fields if mode=0
c
        call get2harmvecs(n,ps,zs,ch1,ch2)
        do i = 1, n
            ch1(1,i) = ch1(1,i) + ima*ch2(1,i)
            ch1(2,i) = ch1(2,i) + ima*ch2(2,i)
        enddo
c
        do i = 1, n
            zvs(1,i) = zvs(1,i) + alpha*ch1(1,i)
            zvs(2,i) = zvs(2,i) + alpha*ch1(2,i)
        enddo
c
        return
        end
c
c
c
c
c
        subroutine beltramicurrent_ext(eps,zk,m,n,rl,ps,zs,dpdt,
     1      dzdt,dpdt2,dzdt2,norder,sigma, alpha, rho, zjs, zms)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1),dpdt2(1),dzdt2(1),
     1      dsdt(10000)
        complex *16 ima,zk,rho(1),sigma(1),alpha,beta,zk0,
     1      zjs(2,1),zms(2,1),ch1(2,10000),ch2(2,10000),
     2      zjr(2,10000),zjrm(2,10000),zjrm2(2,10000),
     3      wmat(2000000),work(1000000),work2(10000),work3(10000)
        external ghfun2
c
c       this subroutine constructs electric current and magnetic
c       current from the electric and magnetic charges, rho, rhom
c       and the coefficients of the harmonic vector fields, alpha,
c       beta
c
c       input:
c         eps - precision with which to calculate kernels
c         zk - the complex helmholtz parameter
c         m - fourier mode to compute
c         n - number of points in curve discretization ps,zs
c         rl - length of parameterization interval
c         ps,zs - parameterization of curve
c         dpdt,dzdt - first derivatives of parameterization
c         dpdt2,dzdt2 - second derivatives of parameterization
c         rhom - the magnetic charge
c         alpha - coefficient of first harmonic vector form
c
c       output:
c           rho - 
c         zjs - tangential electric current
c         zms - tangential magnetic current
c
c
        ima=(0,1)
        done=1
        pi=4*atan(done)
c
        do i = 1, n
            dsdt(i)=sqrt(dpdt(i)**2+dzdt(i)**2)
        enddo
c
c       construct the harmonic vector fields if mode=0
c
        if (m .eq. 0) call get2harmvecs(n,ps,zs,ch1,ch2)
c
        do i = 1, n
            ch1(1,i) = ch1(1,i) - ima*ch2(1,i)
            ch1(2,i) = ch1(2,i) - ima*ch2(2,i)
        enddo

c
c       use the rho and rhom representation...
c
        do i = 1, n
            rho(i) = ima*sigma(i)
        enddo
c
        call surfgradinvlapintmat(ps,zs,dpdt,dzdt,dpdt2,dzdt2,
     1      n,m,rl,wmat)
c
        call cmatvec601(2*n, n, wmat, rho, zjr)
c
        call cmatvec601(2*n, n, wmat, sigma, work)
        call nxdirect(n, work, zjrm)

c
c       and assemble
c
        do i = 1, n
            zjs(1,i)=ima*zk*(zjr(1,i) - zjrm(1,i))
            zjs(2,i)=ima*zk*(zjr(2,i) - zjrm(2,i))
        enddo
c
        if (m .eq. 0) then
            do i = 1, n
                zjs(1,i) = zjs(1,i) + alpha*ch1(1,i)
                zjs(2,i) = zjs(2,i) + alpha*ch1(2,i)
            enddo
        endif
c
        do i = 1, n
            zms(1,i) = -ima*zjs(1,i)
            zms(2,i) = -ima*zjs(2,i)
        enddo
c
        return
        end
c
c
c
c
c
        subroutine curlaeval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      zjs,p,theta,z,vals)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1)
        complex *16 ima,zk,vals(1),zjs(2,1),ee,ss,cc,eep,ssp,ccp,
     1      ssz,ccz,dp,ds,dz,zint1,zint2,zint3,zint4,zint5,zint6,
     2      zint7,zint8,zint9,zint10,zint11,zint12
c
c       this routine evaluates the curl of the vector potetial of a
c       surface current at an arbitrary point.  the surface current
c       can be arbitrarily specified in t-hat and theta-hat components.
c
c       input:
c         eps - the precision with which to evaluate the kernels
c         zk - complex helmholtz parameter
c         m - the fourier mode we are computing
c         n - the number of points sampled on the curve ps,zs
c         rl - the length of the curve parameterizing interval, not
c             necessarily arclength
c         ps,zs - the generating curve
c         dpdt,dzdt - the curve tangent vectors, not necessarily
c             unit vectors
c         zjs - 2 x n array containing surface current, t-hat and
c             theta-hat components, in that order
c         p,theta,z - the point in 3D at which to evaluate the
c             curl of the potential
c
c       output:
c         vals - length 3 array containing p,theta,z components of the
c             curl of the vector potential
c
c       NOTE: the routine only uses the trapezoidal rule since
c       the current is periodic and p,z does NOT lie on ps,zs.
c       the routine will break when p,z is equal to one of the
c       ps,zs.
c
c       NOTE: the output vals IS modulated by exp(ima*m*theta) in order
c       to fully evaluate curl A at a point in 3D.
c
c
        done=1.0d0
        ima=(0,1)
        pi=4*atan(done)
c
c       evaluate each one of the integrals separately, then glue together
c
        zint1=0
        zint2=0
        zint3=0
        zint4=0
        zint5=0
        zint6=0
        zint7=0
        zint8=0
        zint9=0
        zint10=0
        zint11=0
        zint12=0
c
        h=rl/n
        do 2600 i=1,n
c
c       call each of the kernel routines, first single layer
c
        call ghfun2(eps,zk,m,p,z,ps(i),zs(i),ee)
        call ghfun2cc(eps,zk,m,p,z,ps(i),zs(i),cc)
        call ghfun2ss(eps,zk,m,p,z,ps(i),zs(i),ss)
c
c       now rho derivatives
c
        vnp=1
        vnz=0
        call ghfun4n1(eps,zk,m,p,z,ps(i),zs(i),vnp,vnz,eep)
        call ghfun2ccn1(eps,zk,m,p,z,ps(i),zs(i),vnp,vnz,ccp)
        call ghfun2ssn1(eps,zk,m,p,z,ps(i),zs(i),vnp,vnz,ssp)
c
c       and lastly z derivatives
c
        vnp=0
        vnz=1
        call ghfun2ccn1(eps,zk,m,p,z,ps(i),zs(i),vnp,vnz,ccz)
        call ghfun2ssn1(eps,zk,m,p,z,ps(i),zs(i),vnp,vnz,ssz)
c
c       do each of the integrals now, they are calculated in the order
c       they appear in the curl equation my notes
c
        dsdt=dpdt(i)**2+dzdt(i)**2
        dsdt=sqrt(dsdt)
        dpds=dpdt(i)/dsdt
        dzds=dzdt(i)/dsdt
c
        zint1=zint1+h*zjs(1,i)*dzds*ee*dsdt
        zint2=zint2+h*zjs(1,i)*dpds*ssz*dsdt
        zint3=zint3+h*zjs(2,i)*ccz*dsdt
c
        zint4=zint4+h*zjs(1,i)*dpds*ccz*dsdt
        zint5=zint5+h*zjs(2,i)*ssz*dsdt
        zint6=zint6+h*zjs(1,i)*dzds*eep*dsdt
c
        zint7=zint7+h*zjs(1,i)*dpds*ss*dsdt
        zint8=zint8+h*zjs(2,i)*cc*dsdt
        zint9=zint9+h*zjs(1,i)*dpds*ssp*dsdt
        zint10=zint10+h*zjs(2,i)*ccp*dsdt
        zint11=zint11+h*zjs(1,i)*dpds*cc*dsdt
        zint12=zint12+h*zjs(2,i)*ss*dsdt
c
 2600   continue
c
c       finally compute the curl in cylindrical coordinates
c
        dp=ima*m/p*zint1-(ima*zint2+zint3)
        ds=zint4-ima*zint5-zint6
        dz=1/p*(ima*zint7+zint8+p*(ima*zint9+zint10)-
     1      ima*m*(zint11-ima*zint12))
c
        vals(1)=dp*exp(ima*m*theta)
        vals(2)=ds*exp(ima*m*theta)
        vals(3)=dz*exp(ima*m*theta)
c
        return
        end
c
c
c
c
c
        subroutine hdebyeeva(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      dpdt2,dzdt2,norder,ifj,rho,rhom,zjs,zms,alpha,beta,
     2      p,theta,z,hfield)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1),dpdt2(1),dzdt2(1)
        complex *16 ima,zk,rho(1),rhom(1),alpha,beta,hfield(1),
     1      zjs(2,1),zms(2,1),
     2      vals1(100),vals2(100),vals3(100)
c
c       evaluate the h field generated by rho, rhom, alpha, beta
c       on the surface ps,zs at the point p,theta,z which is OFF
c       the surface
c
c       input:
c         eps - the precision to whicih kernels will be evaluated
c         zk - the complex helmholz parameter
c         m - the fourier mode to compute
c         n - number of points in the generator curve discritization
c         rl - length of parameterizing interval
c         ps,zs - points on the generating curve
c         dpdt,dzdt - derivative of ps,zs with respect to parameterizing
c             variable, no necessarily arclength
c         dpdt2,dzdt2 - 2nd derivative of ps,zs with respect to 
c             parameterizing variable, no necessarily arclength
c         ifj - if zjs and zms have been created by a previous
c             call to charge2current, edebyeeva, or hdebyeeva set
c             this to 1 to skip that computation, 0 otherwise
c         rho,rhom - electric and magnetic charges
c         zjs,zms - current constructed from rho, rhom, alpha, beta.
c             only an input variable if ifj=1, otherwise, an output.
c         alpha,beta - coefficients of the harmonic forms
c         p,theta,z - point in cylindrical coordinates at which to
c             evaluate the e field
c
c       output:
c         hfield - the hfield in cylindrical coordinates at the point
c             p,theta,z
c
c
        ima=(0,1)
        done=1
        pi=4*atan(done)
c
c       create the electric and magnetic currents if necessary
c
        if (ifj .eq. 0) then
            stop
            irep=0
            call charge2current(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1          dpdt2,dzdt2,norder,rho,rhom,alpha,beta,irep,zjs,zms)
        endif

c
c       ...calculate the vector potential Q
c
        call vecpoteval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      zms,p,theta,z,vals1)
c
c       ...calculate the gradient of psi
c
        call ghfun2grad(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      rhom,p,theta,z,vals2)
c
c       ...calculate the curl of the vector potential A
c
        call curlaeval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      zjs,p,theta,z,vals3)

c
c       ...and combine them
c
        i1=1
        i2=-1
        i3=1
        hfield(1)=i1*ima*zk*vals1(1)+i2*vals2(1)+i3*vals3(1)
        hfield(2)=i1*ima*zk*vals1(2)+i2*vals2(2)+i3*vals3(2)
        hfield(3)=i1*ima*zk*vals1(3)+i2*vals2(3)+i3*vals3(3)
c
        return
        end
c$$$c
c$$$c
c$$$c
c$$$c
c$$$c
        subroutine edebyeeva(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      dpdt2,dzdt2,norder,ifj,rho,rhom,zjs,zms,alpha,beta,
     2      p,theta,z,efield)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1),dpdt2(1),dzdt2(1)
        complex *16 ima,zk,rho(1),rhom(1),alpha,beta,efield(1),
     1      zjs(2,1),zms(2,1),
     3      vals1(100),vals2(100),vals3(100)
c
c       evaluate the e field generated by rho, rhom, alpha, beta
c       (or zjs, zms) on the surface ps,zs at the point p,theta,z 
c       which is OFF the surface
c
c       input:
c         eps - the precision to whicih kernels will be evaluated
c         zk - the complex helmholz parameter
c         m - the fourier mode to compute
c         n - number of points in the generator curve discritization
c         rl - length of parameterizing interval
c         ps,zs - points on the generating curve
c         dpdt,dzdt - derivative of ps,zs with respect to parameterizing
c             variable, no necessarily arclength
c         dpdt2,dzdt2 - 2nd derivative of ps,zs with respect to 
c             parameterizing variable, no necessarily arclength
c         ifj - if zjs and zms have been created by a previous
c             call to charge2current, edebyeeva, or hdebyeeva set
c             this to 1 to skip that computation, 0 otherwise
c         rho,rhom - electric and magnetic charges
c         zjs,zms - current constructed from rho, rhom, alpha, beta.
c             only an input variable if ifj=1, otherwise, an output.
c         alpha,beta - coefficients of the harmonic forms
c         p,theta,z - point in cylindrical coordinates at which to
c             evaluate the e field
c
c       output:
c         vals - the efield in cylindrical coordinates at the point
c             p,theta,z
c
        ima=(0,1)
        done=1
        pi=4*atan(done)
c
c       create the electric and magnetic currents if necessary
c
        if (ifj .eq. 0) then
        irep=0
        call charge2current(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      dpdt2,dzdt2,norder,rho,rhom,alpha,beta,irep,zjs,zms)
        endif

c
c       ...calculate the vector potential A
c
        call vecpoteval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      zjs,p,theta,z,vals1)
c
c       ...calculate the gradient of phi
c
        call ghfun2grad(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      rho,p,theta,z,vals2)
c
c       ...calculate the curl of the vector potential Q
c
        call curlaeval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      zms,p,theta,z,vals3)

c
c       ...and combine them
c
        i1=1
        i2=-1
        i3=-1
        efield(1)=i1*ima*zk*vals1(1)+i2*vals2(1)+i3*vals3(1)
        efield(2)=i1*ima*zk*vals1(2)+i2*vals2(2)+i3*vals3(2)
        efield(3)=i1*ima*zk*vals1(3)+i2*vals2(3)+i3*vals3(3)
c
        return
        end
c$$$c
c$$$c
c$$$c
c$$$c
c$$$c 
        subroutine charge2current(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      dpdt2,dzdt2,norder,rho,rhom,alpha,beta,irep,zjs,zms)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1),dpdt2(1),dzdt2(1),
     1      dsdt(10000)
        complex *16 ima,zk,rho(1),rhom(1),alpha,beta,zk0,
     1      zjs(2,1),zms(2,1),ch1(2,10000),ch2(2,10000),
     2      zjr(2,10000),zjrm(2,10000),
     3      wmat(2000000),work(1000000),work2(10000),work3(10000)
        external ghfun2
c
c       this subroutine constructs electric current and magnetic
c       current from the electric and magnetic charges, rho, rhom
c       and the coefficients of the harmonic vector fields, alpha,
c       beta
c
c       input:
c         eps - precision with which to calculate kernels
c         zk - the complex helmholtz parameter
c         m - fourier mode to compute
c         n - number of points in curve discretization ps,zs
c         rl - length of parameterization interval
c         ps,zs - parameterization of curve
c         dpdt,dzdt - first derivatives of parameterization
c         dpdt2,dzdt2 - second derivatives of parameterization
c         rho - the electric charge
c         rhom - the magnetic charge
c         alpha - coefficient of first harmonic vector form
c         beta - coefficient of second harmonic vector form
c         irep - 0 or 1, 0 means we invert the surface laplacian,
c             1 means that surflap s_0^2 representation is used.
c
c       output:
c         zjs - tangential electric current
c         zms - tangential magnetic current
c
c
        ima=(0,1)
        done=1
        pi=4*atan(done)
c
        do 1200 i=1,n
        dsdt(i)=sqrt(dpdt(i)**2+dzdt(i)**2)
 1200 continue
c
c       construct the harmonic vector fields if mode=0
c
        if (m .eq. 0) call get2harmvecs(n,ps,zs,ch1,ch2)

c
c       use the rho and rhom representation...
c
        if (irep .eq. 0) then
c
cc        call creainvsurflapmat(ps,zs,dpdt,dzdt,dpdt2,dzdt2,
cc     1      n,m,rl,wmat)
cc        call cmatvec601(n,n,wmat,rho,work)
cc        call surfgradaxi(ps,zs,dpdt,dzdt,n,m,rl,work,zjr)
c
cc        call cmatvec601(n,n,wmat,rhom,work)
cc        call surfgradaxi(ps,zs,dpdt,dzdt,n,m,rl,work,zjrm)
c

cc        call surflapintsolve(ps,zs,dpdt,dzdt,dpdt2,dzdt2,dsdt,
cc     1      n,m,rl,rho,work,work2,work3)
cc        call surfgradaxi(ps,zs,dpdt,dzdt,n,m,rl,work,zjr)


        call surfgradinvlapintmat(ps,zs,dpdt,dzdt,dpdt2,dzdt2,
     1      n,m,rl,wmat)
        call cmatvec601(2*n,n,wmat,rho,zjr)
        call cmatvec601(2*n,n,wmat,rhom,zjrm)


cc        call surflapintsolve(ps,zs,dpdt,dzdt,dpdt2,dzdt2,dsdt,
cc     1      n,m,rl,rhom,work,work2,work3)
cc        call surfgradaxi(ps,zs,dpdt,dzdt,n,m,rl,work,zjrm)

c
        endif

c
c       ...or use rho~ and rhom~ representation
c
        if (irep .eq. 1) then
          call prinf('bombing!! in charge2current irep=*',irep,1)
          stop
        endif

c
c       and assemble
c
        do 1600 i=1,n
c
        zjs(1,i)=ima*zk*(zjr(1,i)-zjrm(2,i))
        if (m .eq. 0) zjs(1,i)=zjs(1,i)+alpha*ch1(1,i)+beta*ch2(1,i)
c
        zjs(2,i)=ima*zk*(zjr(2,i)+zjrm(1,i))
        if (m .eq. 0) zjs(2,i)=zjs(2,i)+alpha*ch1(2,i)+beta*ch2(2,i)
c
        zms(1,i)=zjs(2,i)
        zms(2,i)=-zjs(1,i)
 1600   continue
c
        return
        end
c$$$c
c$$$c
c$$$c
c$$$c
c$$$c
        subroutine surfgradinvlapintmat(ps,zs,dpdt,dzdt,dpdt2,dzdt2,
     1      n,m,rl,wgradinvsl)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1),dpdt2(1),dzdt2(1),
     1      dsdt(10000)
        complex *16 ima,wgradinvsl(2*n,n),alpha(10000),beta(10000),
     1      amat(2000000),cfftf(1000000),cfftb(1000000)
        complex *16, allocatable :: temp(:,:),temp2(:,:),temp3(:,:)
        complex *16, allocatable :: wxx(:,:),gradmuls(:,:)
        complex *16, allocatable :: gtemp(:,:)
        complex *16, allocatable :: a1(:,:),a2(:,:)
c
        allocate( temp(n,n),temp2(n,n),temp3(n,n) )
        allocate( wxx(n,n),gradmuls(2*n,2*n),gtemp(2*n,n) )
        allocate( a1(n,n),a2(n,n) )
c
        done=1
        ima=(0,1)
        pi=4*atan(done)
c
c       construct ode coefficients
c
        do 1600 i = 1,n
        fac2 = (dpdt(i)**2 + dzdt(i)**2)
        alpha(i)=dpdt(i)/ps(i)
     1      -(dpdt(i)*dpdt2(i)+dzdt(i)*dzdt2(i))/fac2
        if (m.ne.0) then
            beta(i) = fac2/(ps(i)**2)
        endif
        dsdt(i)=sqrt(dpdt(i)**2+dzdt(i)**2)
 1600 continue

cccc        call prin2('in mat, alpha=*',alpha,30)
cccc        call prin2('in mat, beta=*',beta,30)

c
c       construct the matrix which will invert the ode
c
        call ode_periodic_sl_mat(n,m,rl,ps,dsdt,
     1      alpha,beta,amat)

c
c       now construct the gradient of the inverse laplacian
c
        call creadftf(n,cfftf)
        call creadftb(n,cfftb)
c
        do 2200 i=1,n
        do 2000 j=1,n
        temp(i,j)=0
        temp2(i,j)=0
        temp3(i,j)=0
 2000 continue
 2200 continue
c

        if (m .eq. 0) goto 5000
c
        do 2600 i=1,n
        temp(i,i)=dsdt(i)**2/n
 2600 continue
c
        call cmatmul601(n,n,cfftf,n,temp,temp2)
        call cmatmul601(n,n,amat,n,temp2,wxx)

c
c       integrate wxx twice
c
        do 3000 i=1,n
        do 2900 j=1,n
        temp(i,j)=0
 2900 continue
 3000 continue
c
        do 3200 i=2,n
        iuse = i-1
        if (iuse .gt. n/2) iuse = iuse-n
        temp(i,i)=-2*pi/rl/(iuse*iuse)
 3200 continue
c
        temp(1,1)=2*pi/rl
c
        call cmatmul601(n,n,temp,n,wxx,temp2)
        call cmatmul601(n,n,cfftb,n,temp2,temp3)
c
        do 3300 i=1,2*n
        do 3250 j=1,2*n
        gradmuls(i,j)=0
 3250 continue
 3300 continue
c
        do 3350 i=1,n
        i1=2*i-1
        i2=i1+1
        gradmuls(i1,i)=1/dsdt(i)
        gradmuls(i2,n+i)=ima*m/ps(i)
 3350 continue
c
        do 3600 i=1,n
        do 3400 j=1,n
        gtemp(n+i,j)=temp3(i,j)
 3400 continue
 3600 continue

c
c       integrate wxx once
c
        do 4000 i=2,n
        iuse = i-1
        if (iuse .gt. n/2) iuse = iuse-n
        temp(i,i)=2*pi/rl/ima/iuse
 4000 continue
c
        temp(1,1)=0
c
        call cmatmul601(n,n,temp,n,wxx,temp2)
        call cmatmul601(n,n,cfftb,n,temp2,temp3)
c
        do 4600 i=1,n
        do 4400 j=1,n
        gtemp(i,j)=temp3(i,j)
 4400 continue
 4600 continue
c
        call cmatmul601(2*n,2*n,gradmuls,n,gtemp,wgradinvsl)
c
        return


c
 5000 continue
c
c       if here, build the matrix for m=0 including the mean condition
c
c       first construct wxx
c
        do 5100 i=1,n
        do 5050 j=1,n
        temp(i,j)=0
        temp2(i,j)=0
 5050 continue
 5100 continue

        do 5200 i=1,n
        temp(i,i)=dsdt(i)*dsdt(i)/n
        temp2(i,i)=1
 5200 continue
c
        temp2(1,1)=0
c
        call cmatmul601(n,n,temp2,n,cfftf,temp3)
        call cmatmul601(n,n,temp3,n,temp,a1)

        do 5300 i=1,n
        temp(i,i)=ps(i)*dsdt(i)/n
        temp2(i,i)=0
 5300 continue
c
        temp2(1,1)=1
c
        call cmatmul601(n,n,temp2,n,cfftf,temp3)
        call cmatmul601(n,n,temp3,n,temp,a2)
c
        do 5600 i=1,n
        do 5500 j=1,n
        temp(i,j)=a2(i,j)+a1(i,j)
 5500 continue
 5600 continue
c
        call cmatmul601(n,n,amat,n,temp,wxx)


c
c       integrate wxx once
c
        do 5700 i=1,n
        do 5750 j=1,n
        temp(i,j)=0
 5750 continue
 5700 continue

        do 5800 i=2,n
        iuse = i-1
        if (iuse .gt. n/2) iuse = iuse-n
        temp(i,i)=2*pi/rl/ima/iuse
 5800 continue
c
        temp(1,1)=0
c
        call cmatmul601(n,n,temp,n,wxx,temp2)
        call cmatmul601(n,n,cfftb,n,temp2,temp3)
c
        do 6600 i=1,n
        do 6400 j=1,n
        gtemp(i,j)=temp3(i,j)
 6400 continue
 6600 continue

c
c       integrate wxx twice
c
        do 7200 i=2,n
        iuse = i-1
        if (iuse .gt. n/2) iuse = iuse-n
        temp(i,i)=-2*pi/rl/(iuse*iuse)
 7200 continue
c
        temp(1,1)=2*pi/rl
c
        call cmatmul601(n,n,temp,n,wxx,temp2)
        call cmatmul601(n,n,cfftb,n,temp2,temp3)
c
        do 7600 i=1,n
        do 7400 j=1,n
        gtemp(n+i,j)=temp3(i,j)
 7400 continue
 7600 continue
c
        do 7800 i=1,2*n
        do 7700 j=1,2*n
        gradmuls(i,j)=0
 7700 continue
 7800 continue

        do 8000 i=1,n
        i1=2*i-1
        i2=i1+1
        gradmuls(i1,i)=1/dsdt(i)
        gradmuls(i2,n+i)=ima*m/ps(i)
cc        gradmuls(i1,i)=1
cc        gradmuls(i2,n+i)=1
 8000 continue
c


        do 9000 i=1,2*n
        do 8800 j=1,n
cccc        wgradinvsl(i,j)=gtemp(i,j)
 8800 continue
 9000 continue

c
        call cmatmul601(2*n,2*n,gradmuls,n,gtemp,wgradinvsl)

c
        return
        end
c
c
c
c
c
        subroutine creadftf(n,a)
        implicit real *8 (a-h,o-z)
        complex *16 a(n,n),ima
c
        ima=(0,1)
        done=1
        pi=4*atan(done)
c
        do 2000 i=1,n
        do 1800 j=1,n
        a(i,j)=exp(-ima*(i-1)*(j-1)*2*pi/n)
 1800 continue
 2000 continue
c
        return
        end
c
c
c
c
c
        subroutine creadftb(n,a)
        implicit real *8 (a-h,o-z)
        complex *16 a(n,n),ima
c
        ima=(0,1)
        done=1
        pi=4*atan(done)
c
        do 2000 i=1,n
        do 1800 j=1,n
        a(i,j)=exp(ima*(i-1)*(j-1)*2*pi/n)
 1800 continue
 2000 continue
c
        return
        end
c
c
c
c
c
      subroutine ode_periodic_sl_mat(npts,mode,period,r,dsdt,
     1    alpha,beta,amat)
      implicit real *8 (a-h,o-z)
cccc      integer npts,mode
      real *8 r(npts),dsdt(npts)
      complex *16 alpha(npts),beta(npts),amat(npts,npts),
     1    work(2000000)
cccc      complex *16 fin(npts)
cccc      complex *16 rhs(npts)
cccc      complex *16 u(npts),ux(npts),uxx(npts)
      complex *16 eye,c0
      integer, allocatable :: ipvt(:)
      complex *16, allocatable :: gamma(:)
      complex *16, allocatable :: gammashift(:)
      complex *16, allocatable :: betat(:)
      complex *16, allocatable :: betatshift(:)
cccc      complex *16, allocatable :: ff(:)
cccc      complex *16, allocatable :: finmean(:)
      complex *16, allocatable :: wfft(:)
C
      allocate(wfft(4*npts+15))
      allocate(ipvt(npts))
      allocate(gamma(npts))
      allocate(betat(npts))
      allocate(gammashift(-npts:npts))
      allocate(betatshift(-npts:npts))
cccc      allocate(finmean(npts))
cccc      allocate(ff(npts))
C
      eye=(0,1)
      pi=4*atan(1.0d0)
      call zffti(npts,wfft)
C
C     Fourier transform alpha, mode*mode*beta (or r*dsdt if mode.eq.0).
C     Fourier transform fin*r*dsdt to get mean of original source
C     density fin, if mode.eq.0 as well.
C
      do i = 1,npts
         gamma(i) = alpha(i)
         if (mode.ne.0) then
            betat(i) = -mode*mode*beta(i)
         else 
            betat(i) = r(i)*dsdt(i)
cccc            finmean(i) = fin(i)*r(i)*dsdt(i)/npts
         endif 
cccc         ff(i) = rhs(i)/npts
      enddo
C
      call zfftf(npts,gamma,wfft)
cccc      call zfftf(npts,ff,wfft)
cccc      call zfftf(npts,finmean,wfft)
      call zfftf(npts,betat,wfft)
C
      do i = -npts,npts
         gammashift(i) = 0.0d0
         betatshift(i) = 0.0d0
      enddo
c
      do i = 0,npts/2
         gammashift(i) = gamma(i+1)/npts
         betatshift(i) = betat(i+1)/npts
      enddo
      if ( mod(npts,2).eq.0) then
         do i = -1,-npts/2+1,-1
            gammashift(i) = gamma(npts+i+1)/npts
            betatshift(i) = betat(npts+i+1)/npts
         enddo
      else
         do i = -1,-npts/2,-1
            gammashift(i) = gamma(npts+i+1)/npts
            betatshift(i) = betat(npts+i+1)/npts
         enddo
      endif
C
      do i = 1,npts
         do j = 1,npts
            amat(i,j) = 0.0d0
         enddo
      enddo
C
      do i = 2,npts
         amat(i,i) = 1.0d0
         do j = 2,npts
            iuse = i-1
            juse = j-1
            if (iuse .gt. npts/2) iuse = iuse-npts
            if (juse .gt. npts/2) juse = juse-npts
            amat(i,j) = amat(i,j) + gammashift(iuse-juse)/(eye*juse)
            if (mode.ne.0) then
               amat(i,j) = amat(i,j)-betatshift(iuse-juse)/(juse**2)
            endif
         enddo
      enddo
c
      if (mode. eq. 0) then
         amat(1,1) = betatshift(0)
cccc         ff(1) = finmean(1)
         do i = 2,npts
            amat(i,1) = 0.0d0
            iuse = i-1
            if (iuse .gt. npts/2) iuse = iuse-npts
            amat(1,i) = -betatshift(-iuse)/(iuse**2)
         enddo
      else
         amat(1,1) = betatshift(0)
         do i = 2,npts
            iuse = i-1
            if (iuse .gt. npts/2) iuse = iuse-npts
            amat(i,1) = betatshift(iuse)
            amat(1,i) = gammashift(-iuse)/(eye*iuse)
            amat(1,i) = amat(1,i)-betatshift(-iuse)/(iuse**2)
         enddo
      endif

cccc      call prin2(' amat is *',amat,30)
cccc      stop

c
c      invert the linear system
c
        call zgeco(amat,npts,npts,ipvt,rcond,gamma)
c
        ijob=01
        call zgedi(amat,npts,npts,ipvt,det,work,ijob)
c
        return
        end
c
c
c
c
c
        subroutine creapiecesa(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      norder,w,wp,wz,wcc,wccp,wccz,wss,wssp,wssz)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1),dsdt(10000),
     1      rnp(10000),rnz(10000)
        complex *16 zk,ima,w(1),wp(1),wz(1),wcc(1),wccp(1),
     1      wccz(1),wss(1),wssp(1),wssz(1)
        external ghfun2,ghfun2cc,ghfun2ss,ghfun4n1,ghfun2ccn1,
     1      ghfun2ssn1
c
c       this routine constructs all possible kernels in matrix form
c       with parameter zk.  the matrices can then be sent onto subroutines
c       which generate more complicated operators.
c
c       input:
c         eps - the precision to which kernels shall be calculated
c         zk - the complex helmholz parameter
c         m - the fourier mode we are computing
c         n - number of points in the discritization
c         rl - length of discritization interval
c         ps,zs - points on the generating curve
c         dpdt,dzdt - derivatives of curve with respect to dicretization
c             parameter, not necessarily arclength
c         norder - order of alpert quadratures to use
c
c       output:
c         w - n by n matrix with axisymmetric kernel
c         wp - n by n matrix with target p derivative kernel
c         wz - n by n matrix with target z derivative kernel
c         wcc - n by n matrix with axisymmetric cos cos kernel
c         wccp - n by n matrix with target p derivative cos cos kernel
c         wccz - n by n matrix with target p derivative cos cos kernel
c         wss - n by n matrix with axisymmetric sin sin kernel
c         wssp - n by n matrix with target p derivative sin sin kernel
c         wssz - n by n matrix with target p derivative sin sin kernel
c       
c
        done=1.0d0
        pi=4*atan(done)
        ima=(0,1)
c
        h=rl/n
        do 1400 i=1,n
        sc=dpdt(i)**2+dzdt(i)**2
        sc=sqrt(sc)
        dsdt(i)=sc
 1400   continue
c
c       next the single layer matrices...
c
        call formslpmatbac(ier,w,norder,ps,zs,dsdt,h,n,
     1      ghfun2,eps,zk,m)
c
        call formslpmatbac(ier,wcc,norder,ps,zs,dsdt,h,n,
     1      ghfun2cc,eps,zk,m)
c
        call formslpmatbac(ier,wss,norder,ps,zs,dsdt,h,n,
     1      ghfun2ss,eps,zk,m)
c
c       ...and the d/dp matrices...
c
        do 1800 i=1,n
        rnp(i)=1
        rnz(i)=0
 1800   continue
c
        call formsprimematbac(ier,wp,norder,ps,zs,rnp,rnz,dsdt,
     1      h,n,ghfun4n1,eps,zk,m)
c
        call formsprimematbac(ier,wccp,norder,ps,zs,rnp,rnz,dsdt,
     1      h,n,ghfun2ccn1,eps,zk,m)
c
        call formsprimematbac(ier,wssp,norder,ps,zs,rnp,rnz,dsdt,
     1      h,n,ghfun2ssn1,eps,zk,m)
c
c       ...and the d/dz matrices
c
        do 2200 i=1,n
        rnp(i)=0
        rnz(i)=1
 2200   continue
c
        call formsprimematbac(ier,wz,norder,ps,zs,rnp,rnz,dsdt,
     1      h,n,ghfun4n1,eps,zk,m)
c
        call formsprimematbac(ier,wccz,norder,ps,zs,rnp,rnz,dsdt,
     1      h,n,ghfun2ccn1,eps,zk,m)
c
        call formsprimematbac(ier,wssz,norder,ps,zs,rnp,rnz,dsdt,
     1      h,n,ghfun2ssn1,eps,zk,m)
c
        return
        end
c
c
c
c
c
        subroutine cmatdiff601(m,n,a,b,c)
        implicit real *8 (a-h,o-z)
        complex *16 a(m,n),b(m,n),c(m,n)
c
c       on exit, c(i,j)=a(i,j)-b(i,j)
c
        do 1600 i=1,m
        do 1400 j=1,n
        c(i,j)=a(i,j)-b(i,j)
 1400   continue
 1600   continue
c
        return
        end
c
c
c
c
c
        subroutine creanxcurlmat2(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      norder,diag,amat,w,wcc,wss,wp,wccp,wssp,wccz,wssz)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1),dsdt(10000),rnp(10000),
     1      rnz(10000)
        complex *16 ima,zk,amat(2*n,2*n),
     1      dp1,dp2,ds1,ds2,dz1,dz2,
     2      apdz1,apdz2,azdp1,azdp2,az1,az2,asdz1,asdz2,as1,as2,
     3      asdp1,asdp2,ap1,ap2,
     4      w(n,n),wcc(n,n),wss(n,n),wp(n,n),wccp(n,n),wssp(n,n),
     5      wccz(n,n),wssz(n,n)
c
c       this subroutine constructs a 2n by 2n matrix which maps
c       a surface current specified in tangential and azimuthal
c       coordinates into the tangential H field, i.e. this matrix
c       computes the quantity (n x curl a) where n is the normal
c       to the surface ps,zs and a is the vector potential generated
c       by the surface current.
c
c       input:
c         eps - the precision with which to evaluate the kernels
c         zk - the complex helmholtz parameter
c         m - the fourier mode to compute
c         n - the number of points in the surface discritization
c         rl - the length of the parameterizing interval for ps,zs
c         ps,zs - the points on the generating curve
c         dpdt,dzdt - tanget vectors to ps,zs
c         norder - order of the Kapur-Rokhlin quadratures to use
c         diag - real number that should be put on the diagonal of the
c             matrix, either +.5d0 or -.5d0, depending on whether
c             constructing the matrix for the interior or exterior
c             scattering problem
c
c       output:
c         amat - the 2n by 2n matrix taking surface currents to
c             surface H fields.
c
c       NOTE:  no assumption on arclength parameterization is made.
c
c       NOTE:  keep in mind that this routine computes the matrix for
c       the operator (n x curl a) always using an outward normal.  depending
c       on your application you may have to multiply the entire matrix
c       by -1, or flip the sign on the diagonal.
c
c
        done=1.0d0
        pi=4*atan(done)
        ima=(0,1)
c
        h=rl/n
        do 1400 i=1,n
        sc=dpdt(i)**2+dzdt(i)**2
        sc=sqrt(sc)
        dsdt(i)=sc
 1400   continue
c
c       now assemble the matrix
c
        do 3600 i=1,n
        i1=2*(i-1)+1
        i2=i1+1
c
        dpdsi=dpdt(i)/dsdt(i)
        dzdsi=dzdt(i)/dsdt(i)
c
        do 3200 j=1,n
        j1=2*(j-1)+1
        j2=j1+1
c
        dpdsj=dpdt(j)/dsdt(j)
        dzdsj=dzdt(j)/dsdt(j)
c
c       compute all the kernel partial derivatives
c
        apdz1=dpdsj*wccz(i,j)
        apdz2=-ima*wssz(i,j)
c
        azdp1=dzdsj*wp(i,j)
        azdp2=0
c
        asdp1=ima*dpdsj*wssp(i,j)
        asdp2=wccp(i,j)
c
        as1=ima*dpdsj*wss(i,j)
        as2=wcc(i,j)
c
        ap1=dpdsj*wcc(i,j)
        ap2=-ima*wss(i,j)
c
        az1=dzdsj*w(i,j)
        az2=0
c
        asdz1=ima*dpdsj*wssz(i,j)
        asdz2=wccz(i,j)
c
c       and compute the vector components, adding in the non-cartesian
c       correction to the theta-hat component
c
        dp1=-dpdsi*(apdz1-azdp1)
        dp2=-dpdsi*(apdz2-azdp2)
c
        ds1=dzdsi/ps(i)*(ps(i)*asdp1+as1-ima*m*ap1)
     1      +dpdsi*(ima*m/ps(i)*az1-asdz1)
        ds2=dzdsi/ps(i)*(ps(i)*asdp2+as2-ima*m*ap2)
     1      +dpdsi*(ima*m/ps(i)*az2-asdz2)
c
        dz1=-dzdsi*(apdz1-azdp1)
        dz2=-dzdsi*(apdz2-azdp2)
c
        amat(i1,j1)=-(dpdsi*dp1+dzdsi*dz1)
        amat(i2,j1)=-ds1
c
        amat(i1,j2)=-(dpdsi*dp2+dzdsi*dz2)
        amat(i2,j2)=-ds2
c
        if (i .eq. j) then
          amat(i1,j1)=amat(i1,j1)+diag
          amat(i2,j2)=amat(i2,j2)+diag
        endif
c

 3200   continue
 3600   continue
c
        return
        end
c
c
c
c
c
        subroutine creanxcurlmat2_st(eps, zk, m, n, rl, ps, zs,
     1      dpdt, dzdt, ps1, zs1, dpdt1, dzdt1, norder, diag, amat,
     2      w, wcc, wss, wp, wccp, wssp, wccz, wssz)
        implicit real *8 (a-h,o-z)
        real *8 ps(1), zs(1), dpdt(1), dzdt(1), dsdt(10000)
        real *8 ps1(1), zs1(1), dpdt1(1), dzdt1(1), dsdt1(10000)
        complex *16 ima,zk,amat(2*n,2*n),
     1      dp1,dp2,ds1,ds2,dz1,dz2,
     2      apdz1,apdz2,azdp1,azdp2,az1,az2,asdz1,asdz2,as1,as2,
     3      asdp1,asdp2,ap1,ap2,
     4      w(n,n),wcc(n,n),wss(n,n),wp(n,n),wccp(n,n),wssp(n,n),
     5      wccz(n,n),wssz(n,n)
c
c       this subroutine constructs a 2n by 2n matrix which maps
c       a surface current specified in tangential and azimuthal
c       coordinates into the tangential H field, i.e. this matrix
c       computes the quantity (n x curl a) where n is the normal
c       to the surface ps,zs and a is the vector potential generated
c       by the surface current.
c
c       input:
c         eps - the precision with which to evaluate the kernels
c         zk - the complex helmholtz parameter
c         m - the fourier mode to compute
c         n - the number of points in the surface discritization
c         rl - the length of the parameterizing interval for ps,zs
c         ps,zs - the points on the generating curve
c         dpdt,dzdt - tanget vectors to ps,zs
c         norder - order of the Kapur-Rokhlin quadratures to use
c         diag - real number that should be put on the diagonal of the
c             matrix, either +.5d0 or -.5d0, depending on whether
c             constructing the matrix for the interior or exterior
c             scattering problem
c
c       output:
c         amat - the 2n by 2n matrix taking surface currents to
c             surface H fields.
c
c       NOTE:  no assumption on arclength parameterization is made.
c
c       NOTE:  keep in mind that this routine computes the matrix for
c       the operator (n x curl a) always using an outward normal.  depending
c       on your application you may have to multiply the entire matrix
c       by -1, or flip the sign on the diagonal.
c
c
        done=1.0d0
        pi=4*atan(done)
        ima=(0,1)
c
        h=rl/n
        do 1400 i=1,n
          sc=dpdt(i)**2+dzdt(i)**2
          sc=sqrt(sc)
          dsdt(i)=sc
 1400   continue
c
        do i=1,n
          sc=dpdt1(i)**2+dzdt1(i)**2
          sc=sqrt(sc)
          dsdt1(i)=sc
        enddo
c
c       now assemble the matrix
c
        do 3600 i=1,n
        i1=2*(i-1)+1
        i2=i1+1
c
        dpdsi=dpdt1(i)/dsdt1(i)
        dzdsi=dzdt1(i)/dsdt1(i)
c
        do 3200 j=1,n
        j1=2*(j-1)+1
        j2=j1+1
c
        dpdsj=dpdt(j)/dsdt(j)
        dzdsj=dzdt(j)/dsdt(j)
c
c       compute all the kernel partial derivatives
c
        apdz1=dpdsj*wccz(i,j)
        apdz2=-ima*wssz(i,j)
c
        azdp1=dzdsj*wp(i,j)
        azdp2=0
c
        asdp1=ima*dpdsj*wssp(i,j)
        asdp2=wccp(i,j)
c
        as1=ima*dpdsj*wss(i,j)
        as2=wcc(i,j)
c
        ap1=dpdsj*wcc(i,j)
        ap2=-ima*wss(i,j)
c
        az1=dzdsj*w(i,j)
        az2=0
c
        asdz1=ima*dpdsj*wssz(i,j)
        asdz2=wccz(i,j)
c
c       and compute the vector components, adding in the non-cartesian
c       correction to the theta-hat component
c
        dp1=-dpdsi*(apdz1-azdp1)
        dp2=-dpdsi*(apdz2-azdp2)
c
        ds1=dzdsi/ps1(i)*(ps1(i)*asdp1+as1-ima*m*ap1)
     1      +dpdsi*(ima*m/ps1(i)*az1-asdz1)
        ds2=dzdsi/ps1(i)*(ps1(i)*asdp2+as2-ima*m*ap2)
     1      +dpdsi*(ima*m/ps1(i)*az2-asdz2)
c
        dz1=-dzdsi*(apdz1-azdp1)
        dz2=-dzdsi*(apdz2-azdp2)
c
        amat(i1,j1)=-(dpdsi*dp1+dzdsi*dz1)
        amat(i2,j1)=-ds1
c
        amat(i1,j2)=-(dpdsi*dp2+dzdsi*dz2)
        amat(i2,j2)=-ds2
c
        if (i .eq. j) then
          amat(i1,j1)=amat(i1,j1)+diag
          amat(i2,j2)=amat(i2,j2)+diag
        endif
c

 3200   continue
 3600   continue
c
        return
        end
c
c
c
c
c
        subroutine creavecpotmat7(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      norder,iftan,amat,wcc,wss,w)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1),dsdt(10000)
        complex *16 zk,amat(3*n,2*n),wcc(n,n),wss(n,n),
     1      w(n,n),ima
c
c       this subroutine creates a matrix which maps a vector of
c       length 2*n representing the t-hat and theta-hat components
c       of a surface current into a vector of length 3*n which
c       represents the vector potential of said current.  the parameter
c       iftan specifies wheather the matrix should map to cylindrical
c       coordinates or relative tangential coordinates using
c       information about the curve stored in in dpdt,dzdt.
c
c       NOTE: it is assumed that current vector of 2*n will be relative
c       to unit vectors, but there is NO assumption in this
c       routine that dpdt**2+dzdt**2=1
c
c       input:
c         eps - the precision to which kernel elements are to be computed
c         zk - the complex helmholtz parameter
c         m - the fourier mode for which to construct the matrix
c         n - the number of points on the curve ps,zs
c         rl - the length of the parameterizing interval NOT LENGTH OF CURVE
c             unless ps,zs,dpdt,dzdt were created using an arclength
c             parameterization
c         ps,zs - the points of the generating curve
c         dpdt,dzdt - derivatives with respect to parameterizing variable
c         norder - 
c         iftan - iftan=1 means return a matrix which maps to tangential
c             coordinates, iftan=0 means return a matrix which maps
c             to cylindrical coordinates
c
c       output:
c         amat - the 3*n x 2*n matrix which maps surface currents to
c             vector potentials
c
c
        done=1.0d0
        pi=4*atan(done)
        ima=(0,1)
c
        h=rl/n
        do 1400 i=1,n
        sc=dpdt(i)**2+dzdt(i)**2
        sc=sqrt(sc)
        dsdt(i)=sc
 1400   continue
c
c       and now combine the w matrices into amat, depending on the
c       parameter iftan
c
        if (iftan .eq. 1) goto 3000
c
        do 2400 i=1,n
        i1=3*(i-1)+1
        i2=i1+1
        i3=i2+1
c
        do 2200 j=1,n
        j1=2*(j-1)+1
        j2=j1+1
c
        dpds=dpdt(j)/dsdt(j)
        dzds=dzdt(j)/dsdt(j)
c
        amat(i1,j1)=dpds*wcc(i,j)
        amat(i2,j1)=ima*dpds*wss(i,j)
        amat(i3,j1)=dzds*w(i,j)
c
        amat(i1,j2)=-ima*wss(i,j)
        amat(i2,j2)=wcc(i,j)
        amat(i3,j2)=0
c
 2200   continue
 2400   continue
c
        return
c

 3000   continue
c
c       if here, construct the matrix in tangent coordinates so
c       that it outputs a vector in t-hat, theta-hat, n-hat coordinates
c
        do 3400 i=1,n
        i1=3*(i-1)+1
        i2=i1+1
        i3=i2+1
c
        do 3200 j=1,n
        j1=2*(j-1)+1
        j2=j1+1
c
        dpdsj=dpdt(j)/dsdt(j)
        dzdsj=dzdt(j)/dsdt(j)
c
        dpdsi=dpdt(i)/dsdt(i)
        dzdsi=dzdt(i)/dsdt(i)
c
        amat(i1,j1)=dpdsi*dpdsj*wcc(i,j)+dzdsi*dzdsj*w(i,j)
        amat(i2,j1)=ima*dpdsj*wss(i,j)
        amat(i3,j1)=-dzdsi*dpdsj*wcc(i,j)+dpdsi*dzdsj*w(i,j)
c
        amat(i1,j2)=dpdsi*(-ima)*wss(i,j)
        amat(i2,j2)=wcc(i,j)
        amat(i3,j2)=-dzdsi*(-ima)*wss(i,j)
c
 3200   continue
 3400   continue
c
        return
        end
c
c
c
c
c
        subroutine jh2bcompute(eps,zk,n,rl,ps,zs,dpdt,dzdt,norder,
     1      ind,cdb)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1),dsdt(10000)
        complex *16 zk,ima,cdb,c1,c2,c3,zk0,cdb0,cdb5,sigma(10000),
     1      cd
        external ghfun2cc,jh2fun1,jh2fun3
c
c       this subroutine carefully computes the integral of the 
c       e field due to j_h2 along a b cycle.  this is a special
c       purpose routine, and has no standalone purpose.
c
c
        done=1.0d0
        pi=4*atan(done)
        ima=(0,1)
        m=0
c
        do 1400 i=1,n
        dd=dpdt(i)**2+dzdt(i)**2
        dsdt(i)=sqrt(dd)
 1400   continue

c
c       evaluate each of the three terms carefully, first the one
c       from i*zk*S_k jh2
c
        do 1600 i=1,n
        sigma(i)=1/ps(i)
 1600   continue
c
        h=rl/n
        call alpertslp1(ier,norder,ps,zs,dsdt,
     1    h,n,ghfun2cc,eps,zk,m,sigma,ind,c2)

c
c       ...and the second term...
c
        do 2000 i=1,n
        sigma(i)=dpdt(i)/dsdt(i)/ps(i)
 2000   continue
c
        call alpertslp1(ier,norder,ps,zs,dsdt,
     1    h,n,jh2fun1,eps,zk,m,sigma,ind,c1)

c
c       ...and now the third
c
        do 2400 i=1,n
        sigma(i)=dzdt(i)/dsdt(i)/ps(i)
 2400   continue
c

        call alpertslp1(ier,norder,ps,zs,dsdt,
     1    h,n,jh2fun3,eps,zk,m,sigma,ind,c3)
c
        cdb=-ima*2*pi*ps(ind)*(c2-c1+c3)
c
        return
        end
c
c
c
c
c
        subroutine jh2fun1(eps,zk,m,p,z,p0,z0,val)
        implicit real *8 (a-h,o-z)
        real *8 p1(10),p2(10)
        complex *16 zk,val,ima,funj1,rint
        external funj1
c
c       this is a special purpose routine for evaluating part of
c       the line integral of e around a b cycle, it has no known
c       standalone purpose in life.
c
c
        ima=(0,1)
        pi=4*atan(1.0d0)
c
        a=(p-p0)**2+(z-z0)**2+4*p*p0
        b=4*p*p0/a
c
        p1(1)=0
        p1(2)=a
        p1(3)=b
        p1(4)=real(zk)
        p1(5)=imag(zk)
c
        p2(1)=m+.1
        if (m .lt. 0) p2(1)=m-.1
        p2(2)=p
        p2(3)=z
        p2(4)=p0
        p2(5)=z0
c
        a4=0
cccc        b4=pi/2
        b4=2*pi
        k4=16
c
        call cadapgau(ier,a4,b4,funj1,p1,p2,k4,eps,
     1      rint,maxrec,numint)
        val=rint*p0/4/pi
c
        return
        end
c
c
c
        function funj1(t,par1,par2)
        implicit real *8 (a-h,o-z)
        complex *16 funj1,ima,zz,z3,zk,zikr
        real *8 par1(1),par2(1)
c        
        ima=(0,1)
        pi=4*atan(1.0d0)
        a=par1(2)
        b=par1(3)
        zk=par1(4)+ima*par1(5)
c
        m=par2(1)
c
        p=par2(2)
        z=par2(3)
        p0=par2(4)
        z0=par2(5)
c
        d=(p**2+p0**2-2*p*p0*cos(t)+(z-z0)**2)
        d=sqrt(d)
        zikr=ima*zk*d
        sc=abs(zikr)
c
        thresh=1.0d-3
        if (sc .ge. thresh) z3=(1-exp(zikr))/zikr
        if (sc .lt. thresh) then
          z3=1+zikr/2+zikr**2/3/2+zikr**3/4/3/2+zikr**4/5/4/3/2
          z3=z3+zikr**5/6/5/4/3/2+zikr**6/7/6/5/4/3/2
          z3=z3+zikr**7/8/7/6/5/4/3/2
          z3=-z3
        endif
c
        zz=(z-z0)*cos(t)*(exp(zikr)+z3)/d/d
        funj1=zz
c
        return
        end
c
c
c
c
c
        subroutine jh2fun3(eps,zk,m,p,z,p0,z0,val)
        implicit real *8 (a-h,o-z)
        real *8 p1(10),p2(10)
        complex *16 zk,val,ima,funj3,rint
        external funj3
c
c       this is a special purpose routine for evaluating part of
c       the line integral of e around a b cycle, it has no known
c       standalone purpose in life.
c
c
        ima=(0,1)
        pi=4*atan(1.0d0)
c
        a=(p-p0)**2+(z-z0)**2+4*p*p0
        b=4*p*p0/a
c
        p1(1)=0
        p1(2)=a
        p1(3)=b
        p1(4)=real(zk)
        p1(5)=imag(zk)
c
        p2(1)=m+.1
        if (m .lt. 0) p2(1)=m-.1
        p2(2)=p
        p2(3)=z
        p2(4)=p0
        p2(5)=z0
c
        a4=0
cccc        b4=pi/2
        b4=2*pi
        k4=16
c
        call cadapgau(ier,a4,b4,funj3,p1,p2,k4,eps,
     1      rint,maxrec,numint)
        val=rint*p0/4/pi
c
        return
        end
c
c
c
        function funj3(t,par1,par2)
        implicit real *8 (a-h,o-z)
        complex *16 funj3,ima,zz,z3,zk,zikr
        real *8 par1(1),par2(1)
c        
        ima=(0,1)
        pi=4*atan(1.0d0)
        a=par1(2)
        b=par1(3)
        zk=par1(4)+ima*par1(5)
c
        m=par2(1)
c
        p=par2(2)
        z=par2(3)
        p0=par2(4)
        z0=par2(5)
c
        d=(p**2+p0**2-2*p*p0*cos(t)+(z-z0)**2)
        d=sqrt(d)
        zikr=ima*zk*d
        sc=abs(zikr)
c
        thresh=1.0d-3
        if (sc .ge. thresh) z3=(1-exp(zikr))/zikr
        if (sc .lt. thresh) then
          z3=1+zikr/2+zikr**2/3/2+zikr**3/4/3/2+zikr**4/5/4/3/2
          z3=z3+zikr**5/6/5/4/3/2+zikr**6/7/6/5/4/3/2
          z3=z3+zikr**7/8/7/6/5/4/3/2
          z3=-z3
        endif
c
        zz=(p-p0*cos(t))*(exp(zikr)+z3)/d/d
        funj3=zz
c
        return
        end
c
c
c
c
c
        subroutine cvecprod601(n,x,y,prod)
        implicit real *8 (a-h,o-z)
        complex *16 x(1),y(1),prod
c
c       calculate the unconjugated inner product of x and y
c
        prod=0
        do 1400 i=1,n
        prod=prod+x(i)*y(i)
 1400   continue
c
        return
        end
c
c
c
c
c
        subroutine creaskdiv(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      norder,amat)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1),rnp(10000),rnz(10000),
     1      dsdt(10000)
        complex *16 ima,zk,amat(n,2*n)
        complex *16, allocatable :: ws(:,:),wps(:,:),wzs(:,:)
        external ghfun4n0,ghfun2
c
c       create the operator s_k surfdiv
c
c
        allocate( ws(n,n),wps(n,n),wzs(n,n) )
c
        ima=(0,1)
        done=1
        pi=4*atan(done)
c
        do 2200 i=1,n
        dd=dpdt(i)**2+dzdt(i)**2
        dsdt(i)=sqrt(dd)
 2200   continue
c
        do 2600 i=1,n
        rnp(i)=1
        rnz(i)=0
 2600   continue
c
        h=rl/n
        call formdlpmatbac(ier,wps,norder,ps,zs,rnp,rnz,dsdt,
     1      h,n,ghfun4n0,eps,zk,m)
c
        do 2800 i=1,n
        rnp(i)=0
        rnz(i)=1
 2800   continue
c
        call formdlpmatbac(ier,wzs,norder,ps,zs,rnp,rnz,dsdt,
     1      h,n,ghfun4n0,eps,zk,m)
c

        call formslpmatbac(ier,ws,norder,ps,zs,dsdt,
     1      h,n,ghfun2,eps,zk,m)


        do 5000 i=1,n
c
        do 4600 j=1,n
        dpds=dpdt(j)/dsdt(j)
        dzds=dzdt(j)/dsdt(j)
c
        j1=2*(j-1)+1
        j2=j1+1
c
        amat(i,j1)=-(dpds*wps(i,j)+dzds*wzs(i,j))
        amat(i,j2)=ima*m/ps(j)*ws(i,j)
 4600   continue
 5000   continue
c
        return
        end
c
c
c
c
c
        subroutine creagradsk(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      norder,amat,w,wp,wz)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1),rnp(10000),rnz(10000),
     1      dsdt(10000)
        complex *16 ima,zk,amat(2*n,n),w(n,n),wp(n,n),wz(n,n)
c
c       create the surface gradient of sk
c
c       input:
c         eps - the precision to which kernel elements are to be computed
c         zk - the complex helmholtz parameter
c         m - the fourier mode for which to construct the matrix
c         n - the number of points on the curve ps,zs
c         rl - the length of the parameterizing interval NOT LENGTH OF CURVE
c             unless ps,zs,dpdt,dzdt were created using an arclength
c             parameterization
c         ps,zs - the points of the generating curve
c         dpdt,dzdt - derivatives with respect to parameterizing variable
c         norder - order of Kapur-Rokhlin quadratures to use when
c             constructing the matrix - can be 2, 6, or 10
c
c       output:
c         amat - the 2n by n matrix mapping a charge density to the target
c             gradient of s_k
c
c
        ima=(0,1)
        done=1
        pi=4*atan(done)
c
        do 2200 i=1,n
        dd=dpdt(i)**2+dzdt(i)**2
        dsdt(i)=sqrt(dd)
 2200   continue
c

        do 5000 i=1,n
        i1=2*(i-1)+1
        i2=i1+1
c
        do 4600 j=1,n
        dpds=dpdt(i)/dsdt(i)
        dzds=dzdt(i)/dsdt(i)
        amat(i1,j)=dpds*wp(i,j)+dzds*wz(i,j)
        amat(i2,j)=ima*m/ps(i)*w(i,j)
 4600   continue
 5000   continue

c
        return
        end
c
c
c
c
c
        subroutine creavecpotmat2x2(eps,zk,m,n,rl,ps,zs,
     1      dpdt,dzdt,norder,wa2,wcc,wss,w)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1)
        complex *16 zk,ima,wa2(2*n,2*n),wcc(n,n),wss(n,n),w(n,n)
c
        complex *16, allocatable :: wa(:,:)
c
c       this subroutine does the same thing as creavecpotmat, but
c       uses the switch iftan=1 and crops off every 3rd row, i.e.
c       the rows which map to normal components
c
c
        allocate(wa(3*n,2*n))
c
        ima=(0,1)
        done=1.d0
        pi=4*atan(done)
c
c       call the regular routine, then chop off every 3rd row
c
        iftan=1
        call creavecpotmat7(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      norder,iftan,wa,wcc,wss,w)
c

        do 2000 i=1,n
        i1=3*(i-1)+1
        i2=i1+1
        i3=i2+1
c
        ii1=2*(i-1)+1
        ii2=ii1+1
c
        do 1800 j=1,n
        j1=2*(j-1)+1
        j2=j1+1
c
        wa2(ii1,j1)=wa(i1,j1)
        wa2(ii2,j1)=wa(i2,j1)
        wa2(ii1,j2)=wa(i1,j2)
        wa2(ii2,j2)=wa(i2,j2)
c
 1800   continue
 2000   continue
c
        return
        end
c
c
c
c
c
        subroutine creanxtmat(n,amat)
        implicit real *8 (a-h,o-z)
        real *8 dpdt(1),dzdt(1)
        complex *16 amat(2*n,2*n)
c
c       this subroutine creates a 2n by 2n matrix which
c       calculates n cross a t-hat, theta-hat vector
c
c
        do 2000 i=1,n
        i1=2*(i-1)+1
        i2=i1+1
c
        do 1600 j=1,n
        j1=2*(j-1)+1
        j2=j1+1
c
        amat(i1,j1)=0
        amat(i2,j1)=0
        amat(i1,j2)=0
        amat(i2,j2)=0
c
        if (i .eq. j) then
        amat(i1,j1)=0
        amat(i1,j2)=1
        amat(i2,j1)=-1
        amat(i2,j2)=0
        endif
c
 1600   continue
 2000   continue
c
        return
        end
c
c
c
c
c
        subroutine creainvsurflapmat(ps,zs,dpdt,dzdt,dpdt2,dzdt2,
     1      n,m,rl,amat)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1),dpdt2(1),dzdt2(1)
        complex *16 amat(n,n),wz(10000),det
        integer *4 ipvt(10000)
c
c       this subroutine returns the inverse surface laplacian matrix,
c       which inverts mean zero functions to mean zero functions
c
c       input:
c         ps,zs - points on the generating curve for the surface of
c             revolution
c         dpdt,dzdt - derivative of curve with respect to
c             parameterization variable, not necessarily arc length
c         dpdt2,dzdt2 - 2nd derivative of curve
c         n - number of points in discritiztion
c         m - the azimuthal fourier mode we are computing
c         rl - interval of parameterization
c
c       output:
c         amat - n by n matrix for the inverse surface laplacian on
c             the surface of revolution
c
c
c       create the forward surface laplacian matrix
c
        ifinv=1
        call surflapaximat(ps,zs,dpdt,dzdt,dpdt2,dzdt2,n,m,rl,
     1      ifinv,amat)

cccc        call prin2('amat=*',amat,30)
c
c       and now invert the thing
c
        call zgeco(amat,n,n,ipvt,rcond,wz)
        ijob=1
        call zgedi(amat,n,n,ipvt,det,wz,ijob)
c
        return
        end
c
c
c
c
c
        subroutine creandotmat(n,dpdt,dzdt,amat)
        implicit real *8 (a-h,o-z)
        real *8 dpdt(1),dzdt(1),dsdt(10000),rnp(10000),
     1      rns(10000),rnz(10000)
        complex *16 ima,amat(n,3*n)
c
c       this subroutine builds a diagonal n x 3n matrix which
c       takes the dot product with the target normal
c
        done=1.0d0
        pi=4*atan(done)
        ima=(0,1)
c
c       fill rn with the normal vectors
c
        do 1400 i=1,n
        dsdt(i)=sqrt(dpdt(i)**2+dzdt(i)**2)
        rnp(i)=dzdt(i)/dsdt(i)
        rns(i)=0
        rnz(i)=-dpdt(i)/dsdt(i)
 1400   continue
c
c       fill diagonal elements with the normal at i, rest with zeros
c
        do 2200 i=1,n
c
        do 2000 j=1,n
        j1=3*(j-1)+1
        j2=j1+1
        j3=j2+1
c
        if  (i .eq. j) goto 1600
c
        amat(i,j1)=0
        amat(i,j2)=0
        amat(i,j3)=0
c
        goto 2000
 1600   continue
c
        amat(i,j1)=rnp(i)
        amat(i,j2)=rns(i)
        amat(i,j3)=rnz(i)
c
 2000   continue
 2200   continue
c
        return
        end
c
c
c
c
c
        subroutine creandotcurlmat_st(eps, zk, m, n, rl, ps, zs,
     1      dpdt, dzdt, ps1, zs1, dpdt1, dzdt1, amat, w, wcc, wss,
     2      wccp, wssp, wccz, wssz)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1),dsdt(10000)
        real *8 ps1(1),zs1(1),dpdt1(1),dzdt1(1),dsdt1(10000)
        complex *16 ima,zk,amat(n,2*n),
     1      dp1,dp2,ds1,ds2,dz1,dz2,
     2      apdz1,apdz2,azdp1,azdp2,az1,az2,asdz1,asdz2,as1,as2,
     3      asdp1,asdp2,ap1,ap2,
     4      w(n,n),wcc(n,n),wss(n,n),wccp(n,n),wssp(n,n),
     5      wccz(n,n),wssz(n,n)
c
c       this routine constructs an n by 2n matrix that calculates
c       the operator n_t \dot curl S(J), where J is a surface vector field
c       this is the source-target verson of creandotcurlmat
c
        done=1.0d0
        pi=4*atan(done)
        ima=(0,1)
c
        h=rl/n
        do i=1,n
          sc=dpdt(i)**2+dzdt(i)**2
          sc=sqrt(sc)
          dsdt(i)=sc
        enddo
c
        do i = 1,n
          sc=dpdt1(i)**2+dzdt1(i)**2
          sc=sqrt(sc)
          dsdt1(i)=sc
        enddo
c
c       now assemble the matrix
c
        do 3600 i=1,n
          i1=2*(i-1)+1
          i2=i1+1
c
cccc        dpdsi=dpdt(i)/dsdt(i)
cccc        dzdsi=dzdt(i)/dsdt(i)
c 
          dpdsi=dpdt1(i)/dsdt1(i)
          dzdsi=dzdt1(i)/dsdt1(i)
c
          do 3200 j=1,n
          j1=2*(j-1)+1
          j2=j1+1
c
          dpdsj=dpdt(j)/dsdt(j)
          dzdsj=dzdt(j)/dsdt(j)
c
c       compute all the kernel partial derivatives
c
          asdp1=ima*dpdsj*wssp(i,j)
          asdp2=wccp(i,j)
c
          as1=ima*dpdsj*wss(i,j)
          as2=wcc(i,j)
c
          ap1=dpdsj*wcc(i,j)
          ap2=-ima*wss(i,j)
c
          az1=dzdsj*w(i,j)
          az2=0
c
          asdz1=ima*dpdsj*wssz(i,j)
          asdz2=wccz(i,j)
c
c       and compute the vector components of the curl
c
          dp1=1/ps1(i)*ima*m*az1-asdz1
          dp2=1/ps1(i)*ima*m*az2-asdz2
c
          dz1=1/ps1(i)*as1+asdp1-1/ps1(i)*ima*m*ap1
          dz2=1/ps1(i)*as2+asdp2-1/ps1(i)*ima*m*ap2
c
c       and fill the matrix with n dot that
c
          amat(i,j1)=dzdsi*dp1-dpdsi*dz1
          amat(i,j2)=dzdsi*dp2-dpdsi*dz2
c
 3200   continue
 3600   continue
c
        return
        end
c
c
c
c
c
        subroutine creandotcurlmat(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      norder,amat,w,wcc,wss,wccp,wssp,wccz,wssz)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1),dsdt(10000)
        complex *16 ima,zk,amat(n,2*n),
     1      dp1,dp2,ds1,ds2,dz1,dz2,
     2      apdz1,apdz2,azdp1,azdp2,az1,az2,asdz1,asdz2,as1,as2,
     3      asdp1,asdp2,ap1,ap2,
     4      w(n,n),wcc(n,n),wss(n,n),wccp(n,n),wssp(n,n),
     5      wccz(n,n),wssz(n,n)
c
c       this routine constructs an n by 2n matrix that calculates
c       the operator n \dot curl F, where F is a vector field depending
c       on p, theta, z
c
c
        done=1.0d0
        pi=4*atan(done)
        ima=(0,1)
c
        h=rl/n
        do 1400 i=1,n
        sc=dpdt(i)**2+dzdt(i)**2
        sc=sqrt(sc)
        dsdt(i)=sc
 1400   continue
c
c       now assemble the matrix
c
        do 3600 i=1,n
        i1=2*(i-1)+1
        i2=i1+1
c
        dpdsi=dpdt(i)/dsdt(i)
        dzdsi=dzdt(i)/dsdt(i)
c
        do 3200 j=1,n
        j1=2*(j-1)+1
        j2=j1+1
c
        dpdsj=dpdt(j)/dsdt(j)
        dzdsj=dzdt(j)/dsdt(j)
c
c       compute all the kernel partial derivatives
c
        asdp1=ima*dpdsj*wssp(i,j)
        asdp2=wccp(i,j)
c
        as1=ima*dpdsj*wss(i,j)
        as2=wcc(i,j)
c
        ap1=dpdsj*wcc(i,j)
        ap2=-ima*wss(i,j)
c
        az1=dzdsj*w(i,j)
        az2=0
c
        asdz1=ima*dpdsj*wssz(i,j)
        asdz2=wccz(i,j)
c
c       and compute the vector components of the curl
c
        dp1=1/ps(i)*ima*m*az1-asdz1
        dp2=1/ps(i)*ima*m*az2-asdz2
c
        dz1=1/ps(i)*as1+asdp1-1/ps(i)*ima*m*ap1
        dz2=1/ps(i)*as2+asdp2-1/ps(i)*ima*m*ap2
c
c       and fill the matrix with n dot that
c
        amat(i,j1)=dzdsi*dp1-dpdsi*dz1
        amat(i,j2)=dzdsi*dp2-dpdsi*dz2
c
 3200   continue
 3600   continue
c
        return
        end
c
c
c
c
c
        subroutine ctranspose601(m,n,a,b)
        implicit real *8 (a-h,o-z)
        complex *16 a(m,n),b(n,m)
c
c       on output, b = a^t
c
        do 1400 i=1,n
        do 1200 j=1,m
        b(i,j)=a(j,i)
 1200   continue
 1400   continue
c
        return
        end
c
c
c
c
c
        subroutine cadjoint601(m,n,a,b)
        implicit real *8 (a-h,o-z)
        complex *16 a(m,n),b(n,m)
c
c       on output, b = a^*
c
        do 1400 i=1,n
        do 1200 j=1,m
        b(i,j)=conjg(a(j,i))
 1200   continue
 1400   continue
c
        return
        end
c
c
c
c
c
        subroutine ccopy601(n,x,y)
        implicit real *8 (a-h,o-z)
        complex *16 x(1),y(1)
c
        do 1400 i=1,n
        y(i)=x(i)
 1400   continue
c
        return
        end
c
c
c
c
c
        subroutine cmatadd601(m, n, x, a, y, b, c)
        implicit real *8 (a-h,o-z)
        complex *16 a(m,n), b(m,n), c(m,n), x, y
c
c       this subroutine performs the matrix-matrix
c       addition x*a(m,n) + y*b(m,n) = c(m,n)
c
        do i = 1, m
        do j = 1, n
            c(i,j) = x*a(i,j) + y*b(i,j)
        enddo
        enddo
c
        return
        end
c
c
c
c
c
        subroutine cmatmul601(m,n,a,n2,b,c)
        implicit real *8 (a-h,o-z)
        complex *16 a(m,n),b(n,n2),c(m,n2),dd
c
c       this subroutine performs the matrix-matrix
c       multiply a(m,n)*b(n,n2)=c(m,n2)
c

c$omp parallel do default(shared)
c$omp$private(i,j,k,dd) 
cccc$omp$schedule(dynamic)
cccc$omp$num_threads(1) 

        do 2000 i=1,m
        do 1800 j=1,n2
c
        dd=0
        do 1400 k=1,n
        dd=dd+a(i,k)*b(k,j)
 1400   continue
c
        c(i,j)=dd
 1800   continue
 2000   continue
c
c$omp end parallel do

        return
        end
c
c
c
c
c
        subroutine cmatvec601(m,n,a,x,b)
        implicit real *8 (a-h,o-z)
        complex *16 a(m,n),x(1),b(1),cd
c
c$omp parallel do default(shared)
c$omp$private(i,j,cd)
cccc$omp$schedule(dynamic)
cccc$omp$num_threads(1) 
c
        do 2000 i=1,m
        cd=0
        do 1600 j=1,n
        cd=cd+a(i,j)*x(j)
 1600   continue
        b(i)=cd
 2000   continue
c
c$omp end parallel do
c
        return
        end
