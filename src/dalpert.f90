




subroutine getalpert(norder, nskip, nextra, extranodes, &
    extraweights)
  implicit real *8 (a-h,o-z)
  real *8 :: extranodes(*), extraweights(*)

  dimension extranodes2(2), extraweights2(2)
  dimension extranodes4(6), extraweights4(6)
  dimension extranodes8(14), extraweights8(14)
  dimension extranodes16(30), extraweights16(30)

  !
  ! this routine returns order 0, 4, 8, and 16 alpert quadratures.
  ! note that order 0 corresponds merely to the punctured trapezoidal
  ! rule.
  !
  ! input:
  !   norder - order of the desired quadrature
  !
  ! output:
  !   nskip - number of trapezoidal points to skip
  !   nextra - number of additional gauss-like quad nodes
  !   xs, whts - the nodes and weights
  
  data extranodes2/-.1591549430918953d0, .1591549430918953d0/
  data extraweights2/ 0.5d0, 0.5d0/

  data extranodes4/-1.023715124251890D+00, &
      -2.935370741501914D-01, &
      -2.379647284118974D-02, &
      2.379647284118974D-02, &
      2.935370741501914D-01, &
      1.023715124251890D+00/ 

  data extraweights4/ 9.131388579526912D-01, &
      4.989017152913699D-01, &
      8.795942675593887D-02, &
      8.795942675593887D-02, &
      4.989017152913699D-01, &
      9.131388579526912D-01/
  
  data extranodes8/ -3.998861349951123D+00, &
      -2.980147933889640D+00, &
      -1.945288592909266D+00, &
      -1.027856640525646D+00, &
      -3.967966533375878D-01, &
      -9.086744584657729D-02, &
      -6.531815708567918D-03, &
      6.531815708567918D-03, &
      9.086744584657729D-02, &
      3.967966533375878D-01, &
      1.027856640525646D+00, &
      1.945288592909266D+00, &
      2.980147933889640D+00, &
      3.998861349951123D+00/

  data extraweights8/1.004787656533285D+00, &
      1.036093649726216D+00, &
      1.008710414337933D+00, &
      7.947291148621894D-01, &
      4.609256358650077D-01, &
      1.701315866854178D-01, &
      2.462194198995203D-02, &
      2.462194198995203D-02, &
      1.701315866854178D-01, &
      4.609256358650077D-01, &
      7.947291148621894D-01, &
      1.008710414337933D+00, &
      1.036093649726216D+00, &
      1.004787656533285D+00/

  data extranodes16/-8.999998754306120d+00, &
      -7.999888757524622d+00, &
      -6.997957704791519d+00, &
      -5.986360113977494d+00, &
      -4.957203086563112d+00, &
      -3.928129252248612d+00, &
      -2.947904939031494d+00, &
      -2.073471660264395d+00, &
      -1.348993882467059d+00, &
      -7.964747731112430d-01, &
      -4.142832599028031d-01, &
      -1.805991249601928d-01, &
      -6.009290785739468d-02, &
      -1.239382725542637d-02, &
      -8.371529832014113d-04, &
      8.371529832014113d-04, &
      1.239382725542637d-02, &
      6.009290785739468d-02, &
      1.805991249601928d-01, &
      4.142832599028031d-01, &
      7.964747731112430d-01, &
      1.348993882467059d+00, &
      2.073471660264395d+00, &
      2.947904939031494d+00, &
      3.928129252248612d+00, &
      4.957203086563112d+00, &
      5.986360113977494d+00, &
      6.997957704791519d+00, &
      7.999888757524622d+00, &
      8.999998754306120d+00/
  
  data extraweights16/1.000007149422537d+00, &
      1.000395017352309d+00, &
      1.004798397441514d+00, &
      1.020308624984610d+00, &
      1.035167721053657d+00, &
      1.014359775369075d+00, &
      9.362411945698647d-01, &
      8.051212946181061d-01, &
      6.401489637096768d-01, &
      4.652220834914617d-01, &
      3.029123478511309d-01, &
      1.704889420286369d-01, &
      7.740135521653088d-02, &
      2.423621380426338d-02, &
      3.190919086626234d-03, &
      3.190919086626234d-03, &
      2.423621380426338d-02, &
      7.740135521653088d-02, &
      1.704889420286369d-01, &
      3.029123478511309d-01, &
      4.652220834914617d-01, &
      6.401489637096768d-01, &
      8.051212946181061d-01, &
      9.362411945698647d-01, &
      1.014359775369075d+00, &
      1.035167721053657d+00, &
      1.020308624984610d+00, &
      1.004798397441514d+00, &
      1.000395017352309d+00, &
      1.000007149422537d+00/

  if (norder.eq. 0) then
    nskip=1
    nextra=0
    return
  endif

  if (norder.eq. 2) then
    nskip=1
    nextra=2
    do i = 1,nextra
      extranodes(i) = extranodes2(i)
      extraweights(i) = extraweights2(i)
    enddo
    return
  endif

  if (norder.eq.4) then
    nskip = 2
    nextra = 6
    do i = 1,nextra
      extranodes(i) = extranodes4(i)
      extraweights(i) = extraweights4(i)
    enddo
    return
  endif

  if (norder.eq.8) then 
    nskip = 5
    nextra = 14
    do i = 1,nextra
      extranodes(i) = extranodes8(i)
      extraweights(i) = extraweights8(i)
    enddo
    return
  endif

  if (norder.eq.16) then 
    nskip = 10
    nextra = 30
    do i = 1,nextra
      extranodes(i) = extranodes16(i)
      extraweights(i) = extraweights16(i)
    enddo
    return
  endif
  
  return
end subroutine getalpert





subroutine dalpertmat_dlp(ier, norder, ns, xys, dxys, &
    h, gfun, par0, par1, par2, diag, ifscale, amat)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,ns), dxys(2,ns), amat(ns,ns)
  real *8 :: par0(*), par1(*), par2(*)

  integer :: its(100),its2(100)
  real *8 :: src(10), targ(10), grad(10), grad0(10)
  real *8 :: tpts(100), xpts(100), ypts(100), spts(100)
  real *8 :: rnxpts(100), rnypts(100), txtra(100), coefs(100)
  real *8 :: extranodes(30), extraweights(30)

  real *8, allocatable :: dnorms(:,:), dsdt(:)

  ! 
  ! this routine builds the matrix which applies the double
  ! layer potential gfun to a vector using alpert quadrature
  ! 
  ! input:
  !     diag - the diagonal to add to the matrix
  !     ifscale - if set to ifscale=1, then scale for surface of
  !           revolution quadrature, i.e. aij = aij*2*pi*xys(1,j)
  ! 

  done=1
  pi=4*atan(done)
  
  call getalpert(norder, nskip, nextra, extranodes, &
      extraweights)

  ier = 1
  if (norder .eq. 0) ier = 0
  if (norder .eq. 2) ier = 0
  if (norder .eq. 4) ier = 0
  if (norder .eq. 8) ier = 0
  if (norder .eq. 16) ier = 0
  if (ier .ne. 0) then
    call prinf('wrong quad order, norder=*', norder, 1)
    stop
  end if
        
  !
  ! carry out "punctured" trapezoidal rule and fill in matrix
  ! entries, skipping entries within nskip of the diagonal
  !
  do j=1,ns
    do i=1,ns
      amat(i,j)=0
    end do
  end do

  n=ns-2*nskip+1

  !
  ! compute norms and dsdt
  !
  allocate(dnorms(2,ns), dsdt(ns))
  do i = 1,ns
    dsdt(i) = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
    dnorms(1,i) = dxys(2,i)/dsdt(i)
    dnorms(2,i) = -dxys(1,i)/dsdt(i)
  end do
  
  do i = 1,ns
    iii = i-1+nskip
    do k=0,n-1
      iii = iii+1
      if (iii .gt. ns) iii = iii-ns
      call gfun(par0, xys(1,iii), xys(1,i), par1, par2, &
          val, grad, grad0)
      u = dnorms(1,iii)*grad0(1) + dnorms(2,iii)*grad0(2)
      amat(i,iii)=u*dsdt(iii)*h
    end do
  end do
  
  if (norder .eq. 0) then

    if (ifscale .eq. 1) then
      do j = 1,ns
        do i = 1,ns
          amat(i,j) = 2*pi*xys(1,j)*amat(i,j)
        end do
      end do
    end if

    do i = 1,ns
      amat(i,i) = amat(i,i) + diag
    end do

    return
  end if

  !
  ! now add in corrections and interpolated stuff for alpert
  ! first determine all the interpolation coefficients
  !
  ninterp=norder+2

  do ipt=1,ns

    do i=1,nextra
      txtra(i)=h*(ipt-1)+h*extranodes(i)
    end do


    do i=1,nextra

      !
      ! find the closest ninterp points to each of the txtra
      !
      
      n1=txtra(i)/h
      if (txtra(i) .lt. 0) n1=n1-1
      n2=n1+1
      nnn=n1-(ninterp-2)/2
      
      do j=1,ninterp
        its(j)=nnn+j-1
        its2(j)=its(j)+1
        if (its2(j) .le. 0) its2(j)=its2(j)+ns
        if (its2(j) .gt. ns) its2(j)=its2(j)-ns
      end do
      
      !
      ! fill interpolation nodes and function values
      !
      do j=1,ninterp
        tpts(j) = its(j)*h
        xpts(j) = xys(1,its2(j))
        ypts(j) = xys(2,its2(j))
        spts(j)=dsdt(its2(j))
        rnxpts(j)=dnorms(1,its2(j))
        rnypts(j)=dnorms(2,its2(j))
      end do
      
      !
      ! now compute the values of xs, ys, dsdt at ttt using barycentric
      ! interpolation
      !
      ttt=txtra(i)
      call bary1_coefs_new(ninterp,tpts,ttt,coefs)

      xxx=0
      yyy=0
      sss=0
      rnxxx=0
      rnyyy=0

      do j=1,ninterp
        xxx = xxx+xpts(j)*coefs(j)
        yyy = yyy+ypts(j)*coefs(j)
        sss = sss+spts(j)*coefs(j)
        rnxxx = rnxxx+rnxpts(j)*coefs(j)
        rnyyy = rnyyy+rnypts(j)*coefs(j)
      end do

      !
      ! evaluate the kernel at the new quadrature point xxx,yyy and
      ! add its contribution to the matrix at its interpolation points
      !
      src(1) = xxx
      src(2) = yyy
      call gfun(par0, src, xys(1,ipt), par1, par2, val, grad, grad0)
      u = rnxxx*grad0(1) + rnyyy*grad0(2)

      do j = 1,ninterp
        jjj = its2(j)
        amat(ipt,jjj) = amat(ipt,jjj) &
            + u*sss*h*extraweights(i)*coefs(j)
      end do

    end do
  end do

  !
  ! and scale if necessary
  !
  if (ifscale .eq. 1) then
    do j = 1,ns
      do i = 1,ns
        amat(i,j) = 2*pi*xys(1,j)*amat(i,j)
      end do
    end do
  end if

  do i = 1,ns
    amat(i,i) = amat(i,i) + diag
  end do

  
  return
end subroutine dalpertmat_dlp





subroutine dalpertmat_slp(ier, norder, ns, xys, dxys, &
    h, gfun, par0, par1, par2, diag, ifscale, amat)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,ns), dxys(2,ns), amat(ns,ns)
  real *8 :: par0(*), par1(*), par2(*)

  integer :: its(100),its2(100)
  real *8 :: src(10), targ(10), grad(10), grad0(10)
  real *8 :: tpts(100), xpts(100), ypts(100), spts(100)
  real *8 :: rnxpts(100), rnypts(100), txtra(100), coefs(100)
  real *8 :: extranodes(30), extraweights(30)

  real *8, allocatable :: dnorms(:,:), dsdt(:)

  ! 
  ! this routine builds the matrix which applies the
  ! single-layer potential gfun to a vector using alpert quadrature
  ! 
  ! input:
  !     diag - the diagonal to add to the matrix
  !     ifscale - if set to ifscale=1, then scale for surface of
  !           revolution quadrature, i.e. aij = aij*2*pi*xys(1,j)
  ! 

  done=1
  pi=4*atan(done)
  
  call getalpert(norder, nskip, nextra, extranodes, &
      extraweights)

  ier = 1
  if (norder .eq. 0) ier = 0
  if (norder .eq. 2) ier = 0
  if (norder .eq. 4) ier = 0
  if (norder .eq. 8) ier = 0
  if (norder .eq. 16) ier = 0
  if (ier .ne. 0) then
    call prinf('wrong quad order, norder=*', norder, 1)
    stop
  end if
        
  !
  ! carry out "punctured" trapezoidal rule and fill in matrix
  ! entries, skipping entries within nskip of the diagonal
  !
  do j=1,ns
    do i=1,ns
      amat(i,j)=0
    end do
  end do

  n=ns-2*nskip+1

  !
  ! compute norms and dsdt
  !
  allocate(dnorms(2,ns), dsdt(ns))
  do i = 1,ns
    dsdt(i) = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
    dnorms(1,i) = dxys(2,i)/dsdt(i)
    dnorms(2,i) = -dxys(1,i)/dsdt(i)
  end do
  
  do i = 1,ns
    iii = i-1+nskip
    do k=0,n-1
      iii = iii+1
      if (iii .gt. ns) iii = iii-ns
      call gfun(par0, xys(1,iii), xys(1,i), par1, par2, &
          val, grad, grad0)
      !!!!u = dnorms(1,iii)*grad0(1) + dnorms(2,iii)*grad0(2)
      u = val
      amat(i,iii)=u*dsdt(iii)*h
    end do
  end do
  
  if (norder .eq. 0) then

    if (ifscale .eq. 1) then
      do j = 1,ns
        do i = 1,ns
          amat(i,j) = 2*pi*xys(1,j)*amat(i,j)
        end do
      end do
    end if

    do i = 1,ns
      amat(i,i) = amat(i,i) + diag
    end do

    return
  end if

  !
  ! now add in corrections and interpolated stuff for alpert
  ! first determine all the interpolation coefficients
  !
  ninterp=norder+2

  do ipt=1,ns

    do i=1,nextra
      txtra(i)=h*(ipt-1)+h*extranodes(i)
    end do


    do i=1,nextra

      !
      ! find the closest ninterp points to each of the txtra
      !
      
      n1=txtra(i)/h
      if (txtra(i) .lt. 0) n1=n1-1
      n2=n1+1
      nnn=n1-(ninterp-2)/2
      
      do j=1,ninterp
        its(j)=nnn+j-1
        its2(j)=its(j)+1
        if (its2(j) .le. 0) its2(j)=its2(j)+ns
        if (its2(j) .gt. ns) its2(j)=its2(j)-ns
      end do
      
      !
      ! fill interpolation nodes and function values
      !
      do j=1,ninterp
        tpts(j) = its(j)*h
        xpts(j) = xys(1,its2(j))
        ypts(j) = xys(2,its2(j))
        spts(j)=dsdt(its2(j))
        rnxpts(j)=dnorms(1,its2(j))
        rnypts(j)=dnorms(2,its2(j))
      end do
      
      !
      ! now compute the values of xs, ys, dsdt at ttt using barycentric
      ! interpolation
      !
      ttt=txtra(i)
      call bary1_coefs_new(ninterp,tpts,ttt,coefs)

      xxx=0
      yyy=0
      sss=0
      rnxxx=0
      rnyyy=0

      do j=1,ninterp
        xxx = xxx+xpts(j)*coefs(j)
        yyy = yyy+ypts(j)*coefs(j)
        sss = sss+spts(j)*coefs(j)
        rnxxx = rnxxx+rnxpts(j)*coefs(j)
        rnyyy = rnyyy+rnypts(j)*coefs(j)
      end do

      !
      ! evaluate the kernel at the new quadrature point xxx,yyy and
      ! add its contribution to the matrix at its interpolation points
      !
      src(1) = xxx
      src(2) = yyy
      call gfun(par0, src, xys(1,ipt), par1, par2, val, grad, grad0)
      !!!!u = rnxxx*grad0(1) + rnyyy*grad0(2)
      u = val

      do j = 1,ninterp
        jjj = its2(j)
        amat(ipt,jjj) = amat(ipt,jjj) &
            + u*sss*h*extraweights(i)*coefs(j)
      end do

    end do
  end do

  !
  ! and scale if necessary
  !
  if (ifscale .eq. 1) then
    do j = 1,ns
      do i = 1,ns
        amat(i,j) = 2*pi*xys(1,j)*amat(i,j)
      end do
    end do
  end if

  do i = 1,ns
    amat(i,i) = amat(i,i) + diag
  end do

  
  return
end subroutine dalpertmat_slp





subroutine bary1_coefs_new(n,ts,ttt,coefs)
  implicit real *8 (a-h,o-z)
  real *8 :: ts(1),ttt,whts(1000),coefs(1)
  !
  ! use barycentric interpolation on ts,xs to evaluate at ttt
  ! first evaluate the barycentric weights (real routine)
  ! 
  ! input:
  !    n - the length of ts,xs
  !    ts - nodes with which to interpolate
  !    ttt - point at which to interpolate
  ! 
  ! output:
  !    coefs - coefficients for the interpolation
  ! 

  do i=1,n
    whts(i)=1
  end do
      

  do  i=1,n
    do  j=1,n
      if (i .ne. j) whts(i)=whts(i)/(ts(i)-ts(j))
    end do
  end do

  !
  ! this uses the '2nd form' of barycentric interpolation, first
  ! form the denominator
  !
  dd=0
  do i=1,n
    dd=dd+whts(i)/(ttt-ts(i))
  end do
  
  !
  ! and next the interpolation coefficients
  !
  do i=1,n
    coefs(i)=whts(i)/(ttt-ts(i))/dd
  end do
  
  return
end subroutine bary1_coefs_new
