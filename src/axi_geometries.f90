subroutine axi_geometry_resolution(n, xys, dxys, d2xys, errs, res)
  implicit real *8 (a-h,o-z)

  real *8 :: xys(2,n), dxys(2,n), d2xys(2,n), errs(6)

  complex *16 :: ima, z1(10000), z2(10000), z3(10000), z4(10000)
  complex *16 :: z5(10000), z6(10000)

  ima = (0,1)
  done = 1
  pi = 4*atan(done)

  !
  ! check resolution of xy, dxy, and d2xy
  !
  do i = 1,n
    z1(i) = 0
    z2(i) = 0
    z3(i) = 0
    z4(i) = 0
    z5(i) = 0
    z6(i) = 0
    do j = 1,n
      z1(i) = z1(i) + xys(1,j)*exp(-2*pi*ima*(i-1)*(j-1)/n)/n      
      z2(i) = z2(i) + xys(2,j)*exp(-2*pi*ima*(i-1)*(j-1)/n)/n      
      z3(i) = z3(i) + dxys(1,j)*exp(-2*pi*ima*(i-1)*(j-1)/n)/n      
      z4(i) = z4(i) + dxys(2,j)*exp(-2*pi*ima*(i-1)*(j-1)/n)/n      
      z5(i) = z5(i) + d2xys(1,j)*exp(-2*pi*ima*(i-1)*(j-1)/n)/n      
      z6(i) = z6(i) + d2xys(2,j)*exp(-2*pi*ima*(i-1)*(j-1)/n)/n      
    end do
    
  end do

  !call prin2('z1 = *', z1, 2*n)

  dnorm1 = 0
  dnorm2 = 0
  dnorm3 = 0
  dnorm4 = 0
  dnorm5 = 0
  dnorm6 = 0

  do i = 1,n
    dnorm1 = dnorm1 + abs(z1(i))**2
    dnorm2 = dnorm2 + abs(z2(i))**2
    dnorm3 = dnorm3 + abs(z3(i))**2
    dnorm4 = dnorm4 + abs(z4(i))**2
    dnorm5 = dnorm5 + abs(z5(i))**2
    dnorm6 = dnorm6 + abs(z6(i))**2
  end do

  dnorm1 = sqrt(dnorm1)
  dnorm2 = sqrt(dnorm2)
  dnorm3 = sqrt(dnorm3)
  dnorm4 = sqrt(dnorm4)
  dnorm5 = sqrt(dnorm5)
  dnorm6 = sqrt(dnorm6)


  dtail1 = 0
  dtail2 = 0
  dtail3 = 0
  dtail4 = 0
  dtail5 = 0
  dtail6 = 0

  do i = 1,4
    dtail1 = dtail1 + abs(z1(n/2-i+1))**2 + abs(z1(n/2+i))**2
    dtail2 = dtail2 + abs(z2(n/2-i+1))**2 + abs(z2(n/2+i))**2
    dtail3 = dtail3 + abs(z3(n/2-i+1))**2 + abs(z3(n/2+i))**2
    dtail4 = dtail4 + abs(z4(n/2-i+1))**2 + abs(z4(n/2+i))**2
    dtail5 = dtail5 + abs(z5(n/2-i+1))**2 + abs(z5(n/2+i))**2
    dtail6 = dtail6 + abs(z6(n/2-i+1))**2 + abs(z6(n/2+i))**2
  end do

  dtail1 = sqrt(dtail1)
  dtail2 = sqrt(dtail2)
  dtail3 = sqrt(dtail3)
  dtail4 = sqrt(dtail4)
  dtail5 = sqrt(dtail5)
  dtail6 = sqrt(dtail6)


  errs(1) = dtail1/dnorm1
  errs(2) = dtail2/dnorm2
  errs(3) = dtail3/dnorm3
  errs(4) = dtail4/dnorm4
  errs(5) = dtail5/dnorm5
  errs(6) = dtail6/dnorm6

  !call prin2('resolution in xs = *', dres1, 1)
  !call prin2('resolution in ys = *', dres2, 1)
  !call prin2('resolution in dxs = *', dres3, 1)
  !call prin2('resolution in dys = *', dres4, 1)
  !call prin2('resolution in d2xs = *', dres5, 1)
  !call prin2('resolution in d2ys = *', dres6, 1)

  res = -1
  do i = 1,4
    if (errs(i) .gt. res) res = errs(i)
  end do
  
  
  return
end subroutine axi_geometry_resolution





subroutine funcurve_resampler(n, rl, ts, h, xys, dxys, d2xys, rltot)
  implicit real *8 (a-h,o-z)
  real *8 :: ts(n), xys(2,n), dxys(2,n), d2xys(2,n)

  real *8 :: dsdt(10000), dtds(10000), dsdt2(10000), dtds2(10000)
  complex *16 :: z1(10000), z2(10000), cd, ima
  external funcurvewrap

  !
  ! this is a wrapper for anaresa and funcurve, arclength sampling,
  ! derivatives, and second derivatives are returned
  !

  done = 1
  ima = (0,1)
  pi = 4*atan(done)
  
  eps = 1.0d-13
  call anaresa(ier, funcurvewrap, rl, n, eps, ts, h, rltot)
  if (ier .ne. 0) then
    call prinf('after anaresa, ier = *', ier, 1)
    stop
  end if
  
  do i=1,n
    call funcurve777(ts(i), par1, par2, xys(1,i), &
        dxys(1,i), d2xys(1,i))
    dsdt(i) = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
    dtds(i) = 1/dsdt(i)
  end do

  !
  ! and scale the derivatives so that the change of variables is
  ! correct, first compute the second derivative of dtds
  !
  do i = 1,n
    cd = 0
    do j = 1,n
      cd = cd + dtds(j)*exp(-2*pi*ima*(i-1)*(j-1)/n)
    end do
    z1(i) = cd/n
  end do
    
  !call prin2('z1 = *', z1, 2*n)
  do i = 1,n/2
    z1(i) = ima*(i-1)*z1(i)
    z1(n-i+1) = ima*(-i)*z1(n-i+1)
  end do

  do i = 1,n
    cd = 0
    do j = 1,n
      cd = cd + z1(j)*exp(2*pi*ima*(i-1)*(j-1)/n)
    end do
    z2(i) = cd
    dtds2(i) = z2(i)
  end do

  !print *
  !call prin2('z2 = *', z2, 2*n)
  !call prin2('dtds2 = *', dtds2, n)
      

  do i = 1,n
    dtds2(i) = dtds2(i)*2*pi/rltot
  end do

  !call prin2('ratios = *', dtds2, n)

    
  do i = 1,n
    d2xys(1,i) = d2xys(1,i)*dtds(i)**2 + dxys(1,i)*dtds2(i)
    d2xys(2,i) = d2xys(2,i)*dtds(i)**2 + dxys(2,i)*dtds2(i)
    dxys(1,i) = dxys(1,i)*dtds(i)
    dxys(2,i) = dxys(2,i)*dtds(i)
    dsdt(i) = 1
  end do

  return


    i = 2
    dxds2 = (xys(1,i+1) - 2*xys(1,i) + xys(1,i-1))/h/h
    call prin2('from finite diff, dxds2 = *', dxds2, 1)
    call prin2('from formula, d2xys = *', d2xys(1,i), 1)
    call prin2('err = *', (dxds2-d2xys(1,i) )/dxds2, 1)
    call prin2('ratio = *', dxds2/d2xys(1,i), 1)

    dyds2 = (xys(2,i+1) - 2*xys(2,i) + xys(2,i-1))/h/h
    print *
    call prin2('from finite diff, dyds2 = *', dyds2, 1)
    call prin2('from formula, d2xys = *', d2xys(2,i), 1)
    call prin2('err = *', (dyds2-d2xys(2,i) )/dyds2, 1)
    call prin2('ratio = *', dyds2/d2xys(2,i), 1)


    !stop
  return
end subroutine funcurve_resampler





subroutine funcurvewrap(t, x, y, dxdt, dydt)
  implicit real *8 (a-h,o-z)

  real *8 :: xy(2), dxy(2), d2xy(2)
  
  call funcurve777(t, p1, p2, xy, dxy, d2xy)

  x = xy(1)
  y = xy(2)
  dxdt = dxy(1)
  dydt = dxy(2)
  
  return
end subroutine funcurvewrap





subroutine funcurve777(t, par1, par2, xy, dxy, d2xy)
  implicit real *8 (a-h,o-z)
  real *8 :: xy(2), dxy(2), d2xy(2)

  
  igeometry = 531
  igeometry = 1
  !igeometry = 3

  if (igeometry .eq. -1) then
    ! a "faster" torus
    x0 = 2
    y0 = 0
    a = 1d0
    b = 1.5d0

    pi = 4*atan(1.0d0)
    f = 2
    xy(1) = x0 + a*cos(f*t)
    xy(2) = y0 + b*sin(f*t)

    dxy(1) = -a*sin(f*t)*f
    dxy(2) = b*cos(f*t)*f

    d2xy(1) = -a*cos(f*t)*f*f
    d2xy(2) = -b*sin(f*t)*f*f
    return
  end if

  if (igeometry .eq. 1) then
    x0 = 2
    y0 = 0
    a = 1d0
    b = 2d0

    xy(1) = x0 + a*cos(t)
    xy(2) = y0 + b*sin(t)

    dxy(1) = -a*sin(t)
    dxy(2) = b*cos(t)

    d2xy(1) = -a*cos(t)
    d2xy(2) = -b*sin(t)
    return
  end if

  if (igeometry .eq. 511) then
    x0 = 2
    y0 = 0
    a = 1d0
    b = 2d0

    xy(1) = x0 + a*cos(t)
    xy(2) = y0 + b*sin(t)

    dxy(1) = -a*sin(t)
    dxy(2) = b*cos(t)

    d2xy(1) = -a*cos(t)
    d2xy(2) = -b*sin(t)
    return
  end if

  if (igeometry .eq. 512) then
    x0 = 3
    y0 = 0
    a = 2.5d0
    b = 1d0

    xy(1) = x0 + a*cos(t)
    xy(2) = y0 + b*sin(t)

    dxy(1) = -a*sin(t)
    dxy(2) = b*cos(t)

    d2xy(1) = -a*cos(t)
    d2xy(2) = -b*sin(t)
    return
  end if

  if (igeometry .eq. 2) then
    !
    ! squiggly thing
    !
    a=.2d0
    b=.3d0
    n=4
    xy(1) = 2+(1+a*cos(n*t))*cos(t)
    xy(2) = 2+(1+b*sin(t))*sin(t)
    dxy(1) = -(1+a*cos(n*t))*sin(t)-a*n*sin(n*t)*cos(t)
    dxy(2) = (1+b*sin(t))*cos(t)+b*cos(t)*sin(t)

    d2xy(1) = -(1+a*cos(n*t))*cos(t)+a*n*sin(n*t)*sin(t) &
        +a*n*sin(n*t)*sin(t)-a*n*n*cos(n*t)*cos(t)
    d2xy(2) = -(1+b*sin(t))*sin(t)+b*cos(t)*cos(t) &
        +b*cos(t)*cos(t)-b*sin(t)*sin(t)

    return
  end if

  if (igeometry .eq. 3) then
    !
    ! super-ellipse, p controls the power
    !
    p = 4
    x0 = .5d0
    y0 = 0
    a = .25d0
    b = .25d0

    c = cos(t)
    s = sin(t)
    u = 1/a**p
    v = 1/b**p

    r = 1/(u*c**p + v*s**p)**(1/p)
    denom = (u*c**p + v*s**p)**(1/p+1)
    dr = -c*s*(-u*c**(p-2) + v*s**(p-2))/denom

    dr2 = -(denom*((-u*c**(p-2)+v*s**(p-2))*(-s**2 + c**2) &
        + (c**2)*(s**2)*(p-2)*(u*c**(p-4) + v*s**(p-4)) ) &
        - (c**2)*(s**2)*(-u*c**(p-2) &
        + v*s**(p-2))*(1+p)*(u*c**p+v*s**p)**(1/p)*(-u*c**(p-2) &
        + v*s**(p-2)) )/denom**2

    xy(1) = x0 + r*c
    xy(2) = y0 + r*s

    dxy(1) = dr*c - r*s
    dxy(2) = dr*s + r*c

    d2xy(1) = dr2*c - dr*s - dr*s - r*c
    d2xy(2) = dr2*s + dr*c + dr*c - r*s

    return
  end if


  if (igeometry .eq. 531) then
    !
    ! super-ellipse, p controls the power
    !
    p = 6
    x0 = .5d0
    y0 = 0
    a = .25d0
    b = .25d0
    b = 4

    c = cos(t)
    s = sin(t)
    u = 1/a**p
    v = 1/b**p

    r = 1/(u*c**p + v*s**p)**(1/p)
    denom = (u*c**p + v*s**p)**(1/p+1)
    dr = -c*s*(-u*c**(p-2) + v*s**(p-2))/denom

    dr2 = -(denom*((-u*c**(p-2)+v*s**(p-2))*(-s**2 + c**2) &
        + (c**2)*(s**2)*(p-2)*(u*c**(p-4) + v*s**(p-4)) ) &
        - (c**2)*(s**2)*(-u*c**(p-2) &
        + v*s**(p-2))*(1+p)*(u*c**p+v*s**p)**(1/p)*(-u*c**(p-2) &
        + v*s**(p-2)) )/denom**2

    xy(1) = x0 + r*c
    xy(2) = y0 + r*s

    dxy(1) = dr*c - r*s
    dxy(2) = dr*s + r*c

    d2xy(1) = dr2*c - dr*s - dr*s - r*c
    d2xy(2) = dr2*s + dr*c + dr*c - r*s

    return
  end if
  

end subroutine funcurve777





subroutine funcurvein(t,x,y,dxdt,dydt,dxdt2,dydt2)
  implicit real *8 (a-h,o-z)
  !
  ! Controls the source surface inside to generate a field in the
  ! exterior. This surface is also the testing surface for the
  ! field generated due to funcurve3 in the exterior.
  !
  done=1
  pi=4*atan(done)

  a=.13d0
  b=.13d0

  x0=2.1d0
  y0=0d0

  x=x0+a*cos(t)
  y=y0+b*sin(t)
  dxdt=-a*sin(t)
  dydt=b*cos(t)
  dxdt2=-a*cos(t)
  dydt2=-b*sin(t)

  return
end subroutine funcurvein





subroutine funcurveout(t,x,y,dxdt,dydt,dxdt2,dydt2)
  implicit real *8 (a-h,o-z)
  !
  ! Controls the source surface outside to generate a field in the
  ! interior. This surface is also the testing surface for the
  ! field generated due to funcurve2 in the interior.
  !
  a=.15d0
  b=.15d0
  x0=6d0
  y0=1d0

  x=x0+a*cos(t)
  y=y0+b*sin(t)
  dxdt=-a*sin(t)
  dydt=b*cos(t)
  dxdt2=-a*cos(t)
  dydt2=-b*sin(t)

  return
end subroutine funcurveout
