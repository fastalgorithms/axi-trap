

!
! this file contains routines needed for solving the MFIE on
! rotationally symmetric genus 1 boundaries
!



subroutine crea_mfie_modes(ier, norder, n, xys, dxys, h, zk, &
    maxm, zdiag, amats)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,n), dxys(2,n)
  complex *16 :: zk, zdiag, amats(2*n,2*n,-maxm:maxm)
  
  complex *16, allocatable :: a(:,:,:)
  complex *16, allocatable :: agrad(:,:,:,:)
  complex *16, allocatable :: agrad0(:,:,:,:)
  complex *16, allocatable :: a_cos(:,:,:)
  complex *16, allocatable :: agrad_cos(:,:,:,:)
  complex *16, allocatable :: agrad0_cos(:,:,:,:)
  complex *16, allocatable :: a_sin(:,:,:)
  complex *16, allocatable :: agrad_sin(:,:,:,:)
  complex *16, allocatable :: agrad0_sin(:,:,:,:)

  !
  ! real work done by crea_mfie_modes0
  !
  
  allocate(a(n,n,-maxm:maxm))
  allocate(a_cos(n,n,-maxm:maxm))
  allocate(a_sin(n,n,-maxm:maxm))
  allocate(agrad(n,n,2,-maxm:maxm))
  allocate(agrad_cos(n,n,2,-maxm:maxm))
  allocate(agrad_sin(n,n,2,-maxm:maxm))
  allocate(agrad0(n,n,2,-maxm:maxm))
  allocate(agrad0_cos(n,n,2,-maxm:maxm))
  allocate(agrad0_sin(n,n,2,-maxm:maxm))

  call crea_gkmall_pieces(ier, norder, n, xys, dxys, h, zk, &
      maxm, a, agrad, agrad0, a_cos, agrad_cos, agrad0_cos, &
      a_sin, agrad_sin, agrad0_sin)

  call crea_mfie_modes0(ier, norder, n, xys, dxys, h, zk, &
      maxm, zdiag, a, agrad, agrad0, a_cos, agrad_cos, agrad0_cos, &
      a_sin, agrad_sin, agrad0_sin, amats)
  
  return
end subroutine crea_mfie_modes


subroutine crea_mfie_modes0(ier, norder, n, xys, dxys, h, zk, &
    maxm, zdiag, a, agrad, agrad0, a_cos, agrad_cos, agrad0_cos, &
    a_sin, agrad_sin, agrad0_sin, amats)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,n), dxys(2,n)
  complex *16 :: zk, zdiag, amats(2*n,2*n,-maxm:maxm)
  complex *16 :: a(n,n,-maxm:maxm)
  complex *16 :: agrad(n,n,2,-maxm:maxm)
  complex *16 :: agrad0(n,n,2,-maxm:maxm)
  complex *16 :: a_cos(n,n,-maxm:maxm)
  complex *16 :: agrad_cos(n,n,2,-maxm:maxm)
  complex *16 :: agrad0_cos(n,n,2,-maxm:maxm)
  complex *16 :: a_sin(n,n,-maxm:maxm)
  complex *16 :: agrad_sin(n,n,2,-maxm:maxm)
  complex *16 :: agrad0_sin(n,n,2,-maxm:maxm)
  
  complex *16 :: ima, dp1,dp2,ds1,ds2,dz1,dz2
  complex *16 :: apdz1,apdz2,azdp1,azdp2,az1,az2,asdz1
  complex *16 :: asdz2,as1,as2, asdp1,asdp2,ap1,ap2

  real *8 :: dsdt(10000), ps(10000), zs(10000), dpdt(10000)
  real *8 :: dzdt(10000)
  
  complex *16, allocatable :: w(:,:),wp(:,:)
  complex *16, allocatable :: wcc(:,:),wccp(:,:),wccz(:,:)
  complex *16, allocatable :: wss(:,:),wssp(:,:),wssz(:,:)
  
  !
  ! constructs all the MFIE matrices, i.e. n \times curl(A)
  !
  
  done=1
  pi=4*atan(done)
  ima=(0,1)
   
  do i = 1,n
    ps(i) = xys(1,i)
    zs(i) = xys(2,i)
    dpdt(i) = dxys(1,i)
    dzdt(i) = dxys(2,i)
    sc=dpdt(i)**2+dzdt(i)**2
    sc=sqrt(sc)
    dsdt(i)=sc
  end do

  !
  ! scale the sin matrices by ima
  !
  !$omp parallel do default(shared)
  do mmm = -maxm,maxm
    do j = 1,n    
      do i = 1,n
        a_sin(i,j,mmm) = a_sin(i,j,mmm)/ima
        agrad_sin(i,j,1,mmm) = agrad_sin(i,j,1,mmm)/ima
        agrad_sin(i,j,2,mmm) = agrad_sin(i,j,2,mmm)/ima
        agrad0_sin(i,j,1,mmm) = agrad0_sin(i,j,1,mmm)/ima
        agrad0_sin(i,j,2,mmm) = agrad0_sin(i,j,2,mmm)/ima
      end do
    end do
  end do
  !$omp end parallel do
  

  !
  ! fill the matrices, one mode at a time
  !

  !$omp parallel do default(shared) &
  !$omp   private(w, wp, wcc, wccp, wccz, wss, wssp, wssz) &
  !$omp   private(i1, i2, dpdsi, dzdsi, j1, j2, dpdsj, dzdsj) &
  !$omp   private(apdz1, apdz2, azdp1, azdp2, asdp1, asdp2) &
  !$omp   private(as1, as2, ap1, ap2, az1, az2, asdz1, asdz2) &
  !$omp   private(dp1, dp2, ds1, ds2, dz1, dz2)
  do m = -maxm,maxm

    allocate(w(n,n),wp(n,n))
    allocate(wcc(n,n),wccp(n,n),wccz(n,n))
    allocate(wss(n,n),wssp(n,n),wssz(n,n))
    
    !
    ! copy the matrices and fill
    !
    call zveccopy(n*n, a(1,1,m), w)
    call zveccopy(n*n, a_cos(1,1,m), wcc)
    call zveccopy(n*n, a_sin(1,1,m), wss)

    call zveccopy(n*n, agrad(1,1,1,m), wp)
    call zveccopy(n*n, agrad_cos(1,1,1,m), wccp)
    call zveccopy(n*n, agrad_sin(1,1,1,m), wssp)

    call zveccopy(n*n, agrad_cos(1,1,2,m), wccz)
    call zveccopy(n*n, agrad_sin(1,1,2,m), wssz)

    do i=1,n
      i1=2*(i-1)+1
      i2=i1+1

      dpdsi=dpdt(i)/dsdt(i)
      dzdsi=dzdt(i)/dsdt(i)

      do j=1,n
        j1=2*(j-1)+1
        j2=j1+1

        dpdsj=dpdt(j)/dsdt(j)
        dzdsj=dzdt(j)/dsdt(j)

        !
        ! compute all the kernel partial derivatives
        !
        apdz1=dpdsj*wccz(i,j)
        apdz2=-ima*wssz(i,j)
        
        azdp1=dzdsj*wp(i,j)
        azdp2=0
        
        asdp1=ima*dpdsj*wssp(i,j)
        asdp2=wccp(i,j)
        
        as1=ima*dpdsj*wss(i,j)
        as2=wcc(i,j)
        
        ap1=dpdsj*wcc(i,j)
        ap2=-ima*wss(i,j)
        
        az1=dzdsj*w(i,j)
        az2=0
        
        asdz1=ima*dpdsj*wssz(i,j)
        asdz2=wccz(i,j)

        !
        ! and compute the vector components, adding in the non-cartesian
        ! correction to the theta-hat component
        !
        dp1=-dpdsi*(apdz1-azdp1)
        dp2=-dpdsi*(apdz2-azdp2)

        ds1=dzdsi/ps(i)*(ps(i)*asdp1+as1-ima*m*ap1) &
            +dpdsi*(ima*m/ps(i)*az1-asdz1)
        ds2=dzdsi/ps(i)*(ps(i)*asdp2+as2-ima*m*ap2) &
            +dpdsi*(ima*m/ps(i)*az2-asdz2)

        dz1=-dzdsi*(apdz1-azdp1)
        dz2=-dzdsi*(apdz2-azdp2)

        amats(i1,j1,m) = -(dpdsi*dp1+dzdsi*dz1)
        amats(i2,j1,m) = -ds1

        amats(i1,j2,m) = -(dpdsi*dp2+dzdsi*dz2)
        amats(i2,j2,m) = -ds2
        
        if (i .eq. j) then
          amats(i1,j1,m) = amats(i1,i1,m) + zdiag
          amats(i2,j2,m) = amats(i2,j2,m) + zdiag
        endif
        
      end do
    end do
    
    deallocate(w,wp)
    deallocate(wcc,wccp,wccz)
    deallocate(wss,wssp,wssz)

  end do
  !$omp end parallel do
  
  return
end subroutine crea_mfie_modes0





subroutine crea_vecpot_modes(ier, norder, n, xys, dxys, h, zk, &
    maxm, iftan, amats)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,n), dxys(2,n)
  complex *16 :: ima, zk, amats(3*n,2*n,-maxm:maxm)
  
  complex *16 :: dp1,dp2,ds1,ds2,dz1,dz2
  complex *16 :: apdz1,apdz2,azdp1,azdp2,az1,az2,asdz1
  complex *16 :: asdz2,as1,as2, asdp1,asdp2,ap1,ap2

  real *8 :: dsdt(10000), ps(10000), zs(10000), dpdt(10000)
  real *8 :: dzdt(10000)
  
  complex *16, allocatable :: w(:,:),wp(:,:)
  complex *16, allocatable :: wcc(:,:),wccp(:,:),wccz(:,:)
  complex *16, allocatable :: wss(:,:),wssp(:,:),wssz(:,:)
  
  complex *16, allocatable :: a(:,:,:)
  complex *16, allocatable :: agrad(:,:,:,:)
  complex *16, allocatable :: agrad0(:,:,:,:)
  complex *16, allocatable :: a_cos(:,:,:)
  complex *16, allocatable :: agrad_cos(:,:,:,:)
  complex *16, allocatable :: agrad0_cos(:,:,:,:)
  complex *16, allocatable :: a_sin(:,:,:)
  complex *16, allocatable :: agrad_sin(:,:,:,:)
  complex *16, allocatable :: agrad0_sin(:,:,:,:)

  !
  ! all the real work is done by crea_vecpot_modes0
  !
  
  allocate(w(n,n),wp(n,n))
  allocate(wcc(n,n),wccp(n,n),wccz(n,n))
  allocate(wss(n,n),wssp(n,n),wssz(n,n))
  
  done=1
  pi=4*atan(done)
  ima=(0,1)
   
  allocate(a(n,n,-maxm:maxm))
  allocate(a_cos(n,n,-maxm:maxm))
  allocate(a_sin(n,n,-maxm:maxm))
  allocate(agrad(n,n,2,-maxm:maxm))
  allocate(agrad_cos(n,n,2,-maxm:maxm))
  allocate(agrad_sin(n,n,2,-maxm:maxm))
  allocate(agrad0(n,n,2,-maxm:maxm))
  allocate(agrad0_cos(n,n,2,-maxm:maxm))
  allocate(agrad0_sin(n,n,2,-maxm:maxm))

  call crea_gkmall_pieces(ier, norder, n, xys, dxys, h, zk, &
      maxm, a, agrad, agrad0, a_cos, agrad_cos, agrad0_cos, &
      a_sin, agrad_sin, agrad0_sin)


  call crea_vecpot_modes0(ier, norder, n, xys, dxys, h, zk, &
    maxm, iftan, a, agrad, agrad0, a_cos, agrad_cos, agrad0_cos, &
    a_sin, agrad_sin, agrad0_sin, amats)

  return
end subroutine crea_vecpot_modes


subroutine crea_vecpot_modes0(ier, norder, n, xys, dxys, h, zk, &
    maxm, iftan, a, agrad, agrad0, a_cos, agrad_cos, agrad0_cos, &
    a_sin, agrad_sin, agrad0_sin, amats)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,n), dxys(2,n)
  complex *16 :: ima, zk, amats(3*n,2*n,-maxm:maxm)
  complex *16 :: a(n,n,-maxm:maxm)
  complex *16 :: agrad(n,n,2,-maxm:maxm)
  complex *16 :: agrad0(n,n,2,-maxm:maxm)
  complex *16 :: a_cos(n,n,-maxm:maxm)
  complex *16 :: agrad_cos(n,n,2,-maxm:maxm)
  complex *16 :: agrad0_cos(n,n,2,-maxm:maxm)
  complex *16 :: a_sin(n,n,-maxm:maxm)
  complex *16 :: agrad_sin(n,n,2,-maxm:maxm)
  complex *16 :: agrad0_sin(n,n,2,-maxm:maxm)
  
  
  complex *16 :: dp1,dp2,ds1,ds2,dz1,dz2
  complex *16 :: apdz1,apdz2,azdp1,azdp2,az1,az2,asdz1
  complex *16 :: asdz2,as1,as2, asdp1,asdp2,ap1,ap2

  real *8 :: dsdt(10000), ps(10000), zs(10000), dpdt(10000)
  real *8 :: dzdt(10000)
  
  complex *16, allocatable :: w(:,:),wp(:,:)
  complex *16, allocatable :: wcc(:,:),wccp(:,:),wccz(:,:)
  complex *16, allocatable :: wss(:,:),wssp(:,:),wssz(:,:)
  
  done=1
  pi=4*atan(done)
  ima=(0,1)
   
  do i = 1,n
    ps(i) = xys(1,i)
    zs(i) = xys(2,i)
    dpdt(i) = dxys(1,i)
    dzdt(i) = dxys(2,i)
    sc=dpdt(i)**2+dzdt(i)**2
    sc=sqrt(sc)
    dsdt(i)=sc
  end do

  !
  ! fill the matrices, one mode at a time
  !
  !$omp parallel do default(shared) &
  !$omp     private(w, wp, wcc, wccp, wccz, wss, wssp, wssz) &
  !$omp     private(i1, i2, i3, j1, j2, dpds, dzds) &
  !$omp     private(dpdsi, dzdsi, dpdsj, dzdsj)

  do m = -maxm,maxm

    allocate(w(n,n),wp(n,n))
    allocate(wcc(n,n),wccp(n,n),wccz(n,n))
    allocate(wss(n,n),wssp(n,n),wssz(n,n))
  
    !
    ! copy the matrices and fill
    !
    call zveccopy(n*n, a(1,1,m), w)
    call zveccopy(n*n, a_cos(1,1,m), wcc)
    call zveccopy(n*n, a_sin(1,1,m), wss)
    
    call zveccopy(n*n, agrad(1,1,1,m), wp)
    call zveccopy(n*n, agrad_cos(1,1,1,m), wccp)
    call zveccopy(n*n, agrad_sin(1,1,1,m), wssp)
    
    call zveccopy(n*n, agrad_cos(1,1,2,m), wccz)
    call zveccopy(n*n, agrad_sin(1,1,2,m), wssz)

    !
    ! iftan=0 means use cylindrical coordinates
    !
    
    if (iftan .eq. 0) then
      do i=1,n
        i1=3*(i-1)+1
        i2=i1+1
        i3=i2+1
        do j=1,n
          j1=2*(j-1)+1
          j2=j1+1
          
          dpds=dpdt(j)/dsdt(j)
          dzds=dzdt(j)/dsdt(j)

          amats(i1,j1,m)=dpds*wcc(i,j)
          amats(i2,j1,m)=dpds*wss(i,j)
          amats(i3,j1,m)=dzds*w(i,j)

          amats(i1,j2,m)=-wss(i,j)
          amats(i2,j2,m)=wcc(i,j)
          amats(i3,j2,m)=0

        end do
      end do
    endif

    !
    ! if here, construct the matrix in tangent coordinates so
    ! that it outputs a vector in t-hat, theta-hat, n-hat coordinates
    !
    if (iftan .eq. 1) then
      do i=1,n
        i1=3*(i-1)+1
        i2=i1+1
        i3=i2+1

        dpdsi=dpdt(i)/dsdt(i)
        dzdsi=dzdt(i)/dsdt(i)

        do j=1,n
          j1=2*(j-1)+1
          j2=j1+1
          
          dpdsj=dpdt(j)/dsdt(j)
          dzdsj=dzdt(j)/dsdt(j)

          amats(i1,j1,m)=dpdsi*dpdsj*wcc(i,j)+dzdsi*dzdsj*w(i,j)
          amats(i2,j1,m)=dpdsj*wss(i,j)
          amats(i3,j1,m)=dzdsi*dpdsj*wcc(i,j)-dpdsi*dzdsj*w(i,j)

          amats(i1,j2,m)=-dpdsi*wss(i,j)
          amats(i2,j2,m)=wcc(i,j)
          amats(i3,j2,m)=-dzdsi*wss(i,j)
        end do
      end do
    end if

    deallocate(w,wp)
    deallocate(wcc,wccp,wccz)
    deallocate(wss,wssp,wssz)
  
    
  end do
  
  return
end subroutine crea_vecpot_modes0






subroutine crea_gkmall_pieces(ier, norder, n, xys, dxys, h, zk, &
    maxm, a, agrad, agrad0, a_cos, agrad_cos, agrad0_cos, &
    a_sin, agrad_sin, agrad0_sin)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,n), dxys(2,n)
  complex *16 :: zk
  complex *16 :: a(n,n,-maxm:maxm), agrad(n,n,2,-maxm:maxm)
  complex *16 :: agrad0(n,n,2,-maxm:maxm)
  complex *16 :: a_cos(n,n,-maxm:maxm), agrad_cos(n,n,2,-maxm:maxm)
  complex *16 :: agrad0_cos(n,n,2,-maxm:maxm)
  complex *16 :: a_sin(n,n,-maxm:maxm), agrad_sin(n,n,2,-maxm:maxm)
  complex *16 :: agrad0_sin(n,n,2,-maxm:maxm)

  complex *16 :: ima, ti

  external gkmall
  
  !
  ! this is the workhorse routine of all the axisymmetric codes. it
  ! constructs all the different matrices corresponding to all the
  ! different kernels and gradients
  !
  ! input:
  !   norder -
  !   n -
  !   xys -
  !   dxys -
  !   h -
  !   zk -
  !   maxm -
  !
  ! output:
  !   a, agrad, agrad0 - 
  !   a_cos, agrad_cos, agrad0_cos - 
  !   a_sin, agrad_sin, agrad0_sin - 
  !

  ima = (0,1)
  done = 1
  pi =4*atan(done)

  ifscale = 1
  call zalpertmodes_grads(ier, norder, n, xys, dxys, &
      h, gkmall, zk, par1, maxm, ifscale, a, agrad, agrad0)

  !
  ! construct the cosine and sine matrices
  !
  ! set the border, +/- maxm values to zero
  !
  m = -maxm
  do j = 1,n
    do i = 1,n
      a_cos(i,j,m) = 0
      agrad_cos(i,j,1,m) = 0
      agrad_cos(i,j,2,m) = 0
      agrad0_cos(i,j,1,m) = 0
      agrad0_cos(i,j,2,m) = 0
      a_sin(i,j,m) = 0
      agrad_sin(i,j,1,m) = 0
      agrad_sin(i,j,2,m) = 0
      agrad0_sin(i,j,1,m) = 0
      agrad0_sin(i,j,2,m) = 0
    end do
  end do

  m = maxm
  do j = 1,n
    do i = 1,n    
      a_cos(i,j,m) = 0
      agrad_cos(i,j,1,m) = 0
      agrad_cos(i,j,2,m) = 0
      agrad0_cos(i,j,1,m) = 0
      agrad0_cos(i,j,2,m) = 0
      a_sin(i,j,m) = 0
      agrad_sin(i,j,1,m) = 0
      agrad_sin(i,j,2,m) = 0
      agrad0_sin(i,j,1,m) = 0
      agrad0_sin(i,j,2,m) = 0
    end do
  end do

  !
  ! and construct the cos and sine matrices
  !

  ti = 2*ima

  !$omp parallel do default(shared) private(m1,m2)
  do m = (-maxm+1),(maxm-1)
    m1 = m-1
    m2 = m+1
    do j = 1,n
      do i = 1,n

        a_cos(i,j,m) = (a(i,j,m2) + a(i,j,m1))/2
        a_sin(i,j,m) = (a(i,j,m2) - a(i,j,m1))/ti

        agrad_cos(i,j,1,m) = (agrad(i,j,1,m2) + agrad(i,j,1,m1))/2
        agrad_cos(i,j,2,m) = (agrad(i,j,2,m2) + agrad(i,j,2,m1))/2
        agrad_sin(i,j,1,m) = (agrad(i,j,1,m2) - agrad(i,j,1,m1))/ti
        agrad_sin(i,j,2,m) = (agrad(i,j,2,m2) - agrad(i,j,2,m1))/ti
        
        agrad0_cos(i,j,1,m) = (agrad0(i,j,1,m2) + agrad0(i,j,1,m1))/2
        agrad0_cos(i,j,2,m) = (agrad0(i,j,2,m2) + agrad0(i,j,2,m1))/2
        agrad0_sin(i,j,1,m) = (agrad0(i,j,1,m2) - agrad0(i,j,1,m1))/ti
        agrad0_sin(i,j,2,m) = (agrad0(i,j,2,m2) - agrad0(i,j,2,m1))/ti
        
      end do
    end do
  end do
  !$omp end parallel do

  
  return
end subroutine crea_gkmall_pieces




subroutine crea_allpots_modes(ier, norder, n, xys, dxys, h, zk, &
    maxm, zdiag, amats, iftan, vecpotmats, zdiag2, sprimemats)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,n), dxys(2,n)
  complex *16 :: zk, zdiag, amats(*), vecpotmats(*), zdiag2
  complex *16 :: sprimemats(n,n,-maxm:maxm)

  real *8 :: dnorms(2,10000)
  
  complex *16 :: ima
  
  complex *16, allocatable :: a(:,:,:)
  complex *16, allocatable :: agrad(:,:,:,:)
  complex *16, allocatable :: agrad0(:,:,:,:)
  complex *16, allocatable :: a_cos(:,:,:)
  complex *16, allocatable :: agrad_cos(:,:,:,:)
  complex *16, allocatable :: agrad0_cos(:,:,:,:)
  complex *16, allocatable :: a_sin(:,:,:)
  complex *16, allocatable :: agrad_sin(:,:,:,:)
  complex *16, allocatable :: agrad0_sin(:,:,:,:)

  !
  ! all the real work is done by crea_vecpot_modes0
  !
    
  done=1
  pi=4*atan(done)
  ima=(0,1)
   
  allocate(a(n,n,-maxm:maxm))
  allocate(a_cos(n,n,-maxm:maxm))
  allocate(a_sin(n,n,-maxm:maxm))
  allocate(agrad(n,n,2,-maxm:maxm))
  allocate(agrad_cos(n,n,2,-maxm:maxm))
  allocate(agrad_sin(n,n,2,-maxm:maxm))
  allocate(agrad0(n,n,2,-maxm:maxm))
  allocate(agrad0_cos(n,n,2,-maxm:maxm))
  allocate(agrad0_sin(n,n,2,-maxm:maxm))

  call crea_gkmall_pieces(ier, norder, n, xys, dxys, h, zk, &
      maxm, a, agrad, agrad0, a_cos, agrad_cos, agrad0_cos, &
      a_sin, agrad_sin, agrad0_sin)

  call crea_vecpot_modes0(ier, norder, n, xys, dxys, h, zk, &
      maxm, iftan, a, agrad, agrad0, a_cos, agrad_cos, agrad0_cos, &
      a_sin, agrad_sin, agrad0_sin, vecpotmats)

  call crea_mfie_modes0(ier, norder, n, xys, dxys, h, zk, &
      maxm, zdiag, a, agrad, agrad0, a_cos, agrad_cos, agrad0_cos, &
      a_sin, agrad_sin, agrad0_sin, amats)

  !
  ! and the sprime matrices manually
  !
  do i = 1,n
    dsdt = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
    dnorms(1,i) = dxys(2,i)/dsdt
    dnorms(2,i) = -dxys(1,i)/dsdt
  end do

  do m = -maxm,maxm
    do j = 1,n
      do i = 1,n
        sprimemats(i,j,m) = dnorms(1,i)*agrad(i,j,1,m) &
            + dnorms(2,i)*agrad(i,j,2,m)
      end do
      sprimemats(j,j,m) = sprimemats(j,j,m) + zdiag2 
    end do
  end do

  return
end subroutine crea_allpots_modes





subroutine axisolve_mfie_new(zk, n, h, xys, &
    dxys, norder, esurf, hsurf, nmodes, ind, cintb, &
    aint, nphi, xyzs, whts, zcurrent, zcharge) 
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,n), dxys(2,n), xyzs(3,*), whts(*)
  complex *16 :: zk, esurf(3,n,nmodes), hsurf(3,n,nmodes)
  complex *16 :: cintb, aint
  complex *16 :: zcurrent(3,*), zcharge(3,*)

  complex *16 :: cd, ima, htan(2,10000), rhs(10000)
  complex *16 :: ctemp1(10000), u(10000), zjrhs(2,10000)
  complex *16 :: zdiag, zdiag2

  real *8, allocatable :: dsdt(:), enorms(:), hnorms(:)

  complex *16, allocatable :: emodes(:,:,:),hmodes(:,:,:)
  complex *16, allocatable :: amodes(:), sprimemats(:,:,:)
  complex *16, allocatable :: amats(:,:,:)
  complex *16, allocatable :: vecpotmats(:,:,:)
  complex *16, allocatable :: zjmodes(:,:,:), rhomodes(:,:)

  external gkmall
  !
  ! this routine solves, mode by mode, a full exterior surface of
  ! revolution scattering problem by first solving the MFIE, and
  ! then solving a secondary equation (n \cdot E = \rho) for the
  ! surface charge. This avoids low-frequency breakdown in the
  ! recovery of \rho. In the zero-mode, an additional stabilizing
  ! condition on the vector potential is added to the system matrix
  ! (line integral of the vector potential). See our IEEE paper...
  ! 
  ! input:
  !
  ! output:
  !

  done=1
  ima=(0,1)
  pi=4*atan(1.0d0)

  allocate( dsdt(n))
  do i=1,n
    dsdt(i) = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
  end do

  !
  ! Compute L2 norm of data
  !
  enorm = 0
  hnorm = 0
  surfarea = 0
  hm = 2*pi/nmodes
  do i = 1,nmodes
    do j = 1,n
      surfarea = surfarea + hm*xys(1,j)*dsdt(j)
      do k = 1,3
        enorm = enorm + hm*xys(1,j)*dsdt(j)*abs(esurf(k,j,i))**2
        hnorm = hnorm + hm*dsdt(j)*xys(1,j)*abs(hsurf(k,j,i))**2
      end do
    end do
  end do

  enorm = sqrt(enorm)
  hnorm = sqrt(hnorm)
  call prin2('e data L2 norm = *', enorm, 1)
  call prin2('h data L2 norm = *', hnorm, 1)

  !
  ! decompose the incoming field esurf, hsurf
  !
  allocate( emodes(3,n,nmodes) )
  allocate( hmodes(3,n,nmodes) )

  call cpu_time(t0)
  !$ t0 = OMP_get_wtime()  
  call axideco(n,nmodes,esurf,hsurf,emodes,hmodes)
  call cpu_time(t1)
  !$ t1 = OMP_get_wtime()
  call prin2('time to fourier decompose data = *', t1-t0, 1)

  
  
  allocate(enorms(nmodes),hnorms(nmodes))
  do i=1,nmodes

    dd1=0
    dd2=0
    do j=1,n
      do k=1,3
        dd1=dd1+abs(emodes(k,j,i))**2
        dd2=dd2+abs(hmodes(k,j,i))**2
      end do
    end do

    enorms(i) = sqrt(dd1)
    hnorms(i) = sqrt(dd2)
  end do

  modemax = -1
  epsfft = 1.0d-15
  do i = 1,nmodes/2-1
    if ((enorms(i)/enorm .lt. epsfft) &
        .and. (hnorms(i)/hnorm .lt. epsfft)) then
      call prinf('modemax = *', i-1, 1)
      call prin2('enorms(maxm) = *', enorms(i), 1)
      call prin2('hnorms(maxm) = *', hnorms(i), 1)
      modemax = i-1
      exit
      !stop
    end if
  end do

  if (modemax .lt. 0) modemax = nmodes/2-1
  maxm = modemax + 5

  allocate(zjmodes(2,n,-modemax:modemax))
  allocate(rhomodes(n,-modemax:modemax))
  call prin2('epsfft = *', epsfft, 1)
  call prinf('maximum mode needed, modemax = *', modemax, 1)
  
  print *
  print *

  !
  ! loop over all modes from -modemax to modemax, solve each
  ! problem, then recombine
  !

  zdiag = -.5d0
  allocate( amats(2*n,2*n,-maxm:maxm) )

  allocate(vecpotmats(3*n,2*n,-maxm:maxm))
  iftan = 1

  allocate(sprimemats(n,n,-maxm:maxm))
  zdiag2 = .5d0
  
  call cpu_time(t0)
  !$ t0 = OMP_get_wtime()
  call crea_allpots_modes(ier, norder, n, xys, dxys, h, zk, &
      maxm, zdiag, amats, iftan, vecpotmats, zdiag2, sprimemats)
  call cpu_time(t1)
  !$ t1 = OMP_get_wtime()
  call prin2('time to build potential mats = *', t1-t0, 1)

  call cpu_time(t0)
  !$ t0 = OMP_get_wtime()

  do mode=-modemax,modemax
    !
    ! construct the mfie matrix to invert, add in extra condition
    ! if in the mode=0 case
    !
    iffix = 0
    if (abs(zk) .lt. 1.0d0) iffix = 1
    
    if ((mode .eq. 0) .and. (iffix .eq. 1)) then
      i3 = 3*ind-1
      do j=1,2*n
        u(j) = 2*pi*xys(1,ind)*ima*vecpotmats(i3,j,mode)
      enddo
      ifrand=0
      call addrowall_new(2*n, 2*n, amats(1,1,mode), ifrand, u)
    endif

    !
    ! construct the rhs for the mfie
    !
    if (mode .eq. 0) modeind = 1
    if (mode .gt. 0) modeind = mode+1
    if (mode .lt. 0) modeind = nmodes+mode+1

    do i=1,n
      dpds = dxys(1,i)/dsdt(i)
      dzds = dxys(2,i)/dsdt(i)
      htan(1,i) = dpds*hmodes(1,i,modeind)+dzds*hmodes(3,i,modeind)
      htan(2,i) = hmodes(2,i,modeind)
    end do
        
    do i=1,n
      zjrhs(1,i) = -htan(2,i)
      zjrhs(2,i) = htan(1,i)
    end do

    if ((mode .eq. 0) .and. (iffix .eq. 1)) then
      do i=1,n
        zjrhs(1,i) = zjrhs(1,i) - cintb
        zjrhs(2,i) = zjrhs(2,i) - cintb
      enddo
    endif

    !
    ! solve the system for the current
    !
    call zgausselim(2*n, amats(1,1,mode), zjrhs, info, &
        zjmodes(1,1,mode), dcond)
  end do

  call cpu_time(t1)
  !$ t1 = OMP_get_wtime()
  call prin2('time to solve all modes for mfie = *', t1-t0, 1)


  !
  ! now solve a series of augmented equations for the charge, rho
  !
  
  call cpu_time(t0)
  !$ t0 = OMP_get_wtime()

  do i = 1,n
    do j = 1,n
      do mmm = -maxm,maxm
        sprimemats(i,j,mmm) = -sprimemats(i,j,mmm)
      end do
    end do
  end do

  allocate( amodes(-modemax:modemax) )
  
  do mode = -modemax,modemax

    if (mode .eq. 0) modeind = 1
    if (mode .gt. 0) modeind = mode+1
    if (mode .lt. 0) modeind = nmodes+mode+1

    !
    ! compute the vector potential on surface ...
    !
    call zmatvec(3*n, 2*n, vecpotmats(1,1,mode), &
        zjmodes(1,1,mode), ctemp1)
    i3 = 3*ind - 1
    amodes(mode) = ctemp1(i3)

    !
    ! ... and n \cdot vector potential
    !
    do i = 1,n
      i3 = 3*i
      rhs(i) = -ima*zk*ctemp1(i3)
    end do

    
    if (mode .eq. 0) then
      call addones_new(n, sprimemats(1,1,mode), xys, h, dsdt)
    end if

    !
    ! calculate E_in \cdot n and adjust the rhs
    !
    call ndotdirect_new(n, dxys, emodes(1,1,modeind), ctemp1)

    do i=1,n
      rhs(i) = rhs(i) - ctemp1(i)
    end do
        
    call zgausselim(n, sprimemats(1,1,mode), rhs, info, &
        rhomodes(1,mode), dcond)

  end do

  call cpu_time(t1)
  !$ t1 = OMP_get_wtime()
  call prin2('time to solve for electric charges for mfie = *', t1-t0, 1)

  !
  ! synthesize the current and charge, evaluate at nphi points
  ! and construct the smooth quadrature weights
  !
  call axisynth1_new(n, xys, dxys, h, modemax, nphi, &
      rhomodes, xyzs, whts, zcharge)

  call axisynth3_new(n, xys, dxys, h, modemax, nphi, &
      zjmodes, xyzs, whts, zcurrent)

  !
  ! compute the integral of the vector potential, \int a \cdot ds
  ! along the xys(1,ind) b-cycle
  !
  hphi = 2*pi/nphi
  aint = 0
  ds = xys(1,ind)*hphi
  do j = 1,nphi
    phi = (j-1)*hphi
    cd = 0
    do mode = -modemax,modemax
      cd = cd + amodes(mode)*exp(ima*mode*phi)
    enddo
    aint = aint + cd*ds    
  enddo

  !!!call prin2('integral of A scattered dot ds is =*',aint,2)
  !!!write(6,*) 'aint = ',aint
  !!!write(13,*) 'aint = ',aint
  
  return
end subroutine axisolve_mfie_new





subroutine ndotdirect_new(n, dxys, field, vals)
  implicit real *8 (a-h,o-z)
  real *8 :: dxys(2,n)
  complex *16 :: field(3,n), vals(n)
  !
  ! apply n \cdot directly to the vector field
  !
  do i=1,n
    dsdt = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
    dpds = dxys(1,i)/dsdt
    dzds = dxys(2,i)/dsdt
    vals(i) = dzds*field(1,i)-dpds*field(3,i)
  end do

  return
end subroutine ndotdirect_new





subroutine addrowall_new(m, n, a, ifrand, u)
  implicit real *8 (a-h,o-z)
  complex *16 :: a(m,n), u(n)
  complex *16, allocatable :: rands(:)
  !
  ! add the vector u to all rows in a
  !
  if (ifrand .eq. 0) then
    do i=1,m
      do j=1,n
        a(i,j)=a(i,j)+u(j)
      end do
    end do
    return
  end if

  !
  ! add random combinations
  !
  allocate(rands(2*m))
  call corrand(2*m,rands)

  do i=1,m
    do j=1,n
      a(i,j)=a(i,j)+rands(i)*u(j)
    end do
  end do
  
  return
end subroutine addrowall_new





subroutine addones_new(n, a, xys, h, dsdt)
  implicit real *8 (a-h,o-z)
  real *8 :: xys(2,n),dsdt(n)
  complex *16 a(n,n), cd

  done=1
  pi=4*atan(done)

  cd=0
  do i=1,n
    cd=cd+2*pi*xys(1,i)*dsdt(i)*h
  end do

  do i=1,n
    do  j=1,n
      a(i,j)=a(i,j)+xys(1,j)*h*dsdt(j)*2*pi/cd/n
    end do
  end do
    
  return
end subroutine addones_new





subroutine zvecdiff_inf(n, x, y, diff)
  implicit real *8 (a-h,o-z)
  complex *16 :: x(n), y(n)

  diff = -1
  do i = 1,n
    d = abs(x(i) - y(i))
    if (d .ge. diff) diff = d
  end do

  return
end subroutine zvecdiff_inf





subroutine zveccopy(n, x, y)
  implicit real *8 (a-h,o-z)
  complex *16 :: x(n), y(n)

  do i = 1,n
    y(i) = x(i)
  end do

  return
end subroutine zveccopy





subroutine curla3d_new(zk, nsrc, src, whts, zjvec, ntarg, &
    targ, hfield)
  implicit real *8 (a-h,o-z)
  real *8 :: src(3,nsrc), whts(nsrc), targ(3,ntarg)
  complex *16 :: zk, zjvec(3,nsrc), hfield(3,ntarg)

  complex *16 :: ima, gkval, gkx, gky, gkz
  !
  ! evaluate the quantity del \times A at the point targ, using
  ! cartesian coordinates
  !
  done=1
  ima=(0,1)
  pi=4*atan(done)

  !
  !       sum over all the src
  !
  
  !$omp parallel do default(shared) &
  !$omp     private(i, x, y, z, j, x0, y0, z0, dx, dy, dz) &
  !$omp     private(r, gkval, gkx, gky, gkz)
  do i = 1,ntarg

    x = targ(1,i)
    y = targ(2,i)
    z = targ(3,i)
    hfield(1,i) = 0
    hfield(2,i) = 0
    hfield(3,i) = 0

    do j = 1,nsrc

      x0 = src(1,j)
      y0=src(2,j)
      z0=src(3,j)
      dx = x-x0
      dy = y-y0
      dz = z-z0

      r=sqrt(dx**2+dy**2+dz**2)
      gkval=exp(ima*zk*r)/4/pi/r
      gkx=gkval*(ima*zk*r-done)*dx/r**2
      gky=gkval*(ima*zk*r-done)*dy/r**2
      gkz=gkval*(ima*zk*r-done)*dz/r**2

      hfield(1,i) = hfield(1,i) &
          + whts(j)*(gky*zjvec(3,j)-gkz*zjvec(2,j))
      hfield(2,i) = hfield(2,i) &
          + whts(j)*(gkz*zjvec(1,j)-gkx*zjvec(3,j))
      hfield(3,i) = hfield(3,i) &
          + whts(j)*(gkx*zjvec(2,j)-gky*zjvec(1,j))

    end do

  end do
  !$omp end parallel do

  return
end subroutine curla3d_new





subroutine efield3d_new(zk, nsrc, src, whts, zjvec, zcharge, &
    ntarg, targ, efield)
  implicit real *8 (a-h,o-z)
  real *8 :: src(3,nsrc), whts(nsrc), targ(3,ntarg)
  complex *16 :: zk, zjvec(3,nsrc), zcharge(nsrc), efield(3,ntarg)

  complex *16 :: ima, gkval, gkx, gky, gkz
  !
  ! evaluate the quantity ima*zk*A - grad(phi)
  !
  done=1
  ima=(0,1)
  pi=4*atan(done)

  !
  !       sum over all the srcs
  !
  !$omp parallel do default(shared) &
  !$omp     private(i, x, y, z, j, x0, y0, z0, dx, dy, dz) &
  !$omp     private(r, gkval, gkx, gky, gkz)
  do i = 1,ntarg

    x = targ(1,i)
    y = targ(2,i)
    z = targ(3,i)
    efield(1,i) = 0
    efield(2,i) = 0
    efield(3,i) = 0

    do j = 1,nsrc

      x0 = src(1,j)
      y0=src(2,j)
      z0=src(3,j)
      dx = x-x0
      dy = y-y0
      dz = z-z0

      r=sqrt(dx**2+dy**2+dz**2)
      gkval=exp(ima*zk*r)/4/pi/r
      gkx=gkval*(ima*zk*r-done)*dx/r**2
      gky=gkval*(ima*zk*r-done)*dy/r**2
      gkz=gkval*(ima*zk*r-done)*dz/r**2

      efield(1,i)  =efield(1,i) + whts(j)*(ima*zk*gkval*zjvec(1,j) &
          - gkx*zcharge(j))
      efield(2,i) = efield(2,i) + whts(j)*(ima*zk*gkval*zjvec(2,j) &
          - gky*zcharge(j))
      efield(3,i) = efield(3,i) + whts(j)*(ima*zk*gkval*zjvec(3,j) &
          - gkz*zcharge(j))


    end do

  end do
  !$omp end parallel do

  return
end subroutine efield3d_new





subroutine emfields3d(zk, nsrc, src, whts, zjvec, zcharge, &
    ntarg, targ, efield, hfield)
  implicit real *8 (a-h,o-z)
  real *8 :: src(3,nsrc), whts(nsrc), targ(3,ntarg)
  complex *16 :: zk, zjvec(3,nsrc), zcharge(nsrc), efield(3,ntarg)
  complex *16 :: hfield(3,ntarg)

  complex *16 :: ima, gkval, gkx, gky, gkz
  !
  ! evaluate the quantity ima*zk*A - grad(phi)
  !
  done=1
  ima=(0,1)
  pi=4*atan(done)

  !
  !       sum over all the srcs
  !
  !$omp parallel do default(shared) &
  !$omp     private(i, x, y, z, j, x0, y0, z0, dx, dy, dz) &
  !$omp     private(r, gkval, gkx, gky, gkz)
  do i = 1,ntarg

    x = targ(1,i)
    y = targ(2,i)
    z = targ(3,i)
    efield(1,i) = 0
    efield(2,i) = 0
    efield(3,i) = 0
    hfield(1,i) = 0
    hfield(2,i) = 0
    hfield(3,i) = 0

    do j = 1,nsrc

      x0 = src(1,j)
      y0 = src(2,j)
      z0 = src(3,j)
      dx = x-x0
      dy = y-y0
      dz = z-z0

      r = sqrt(dx**2+dy**2+dz**2)
      gkval = exp(ima*zk*r)/4/pi/r
      gkx = gkval*(ima*zk*r-done)*dx/r**2
      gky = gkval*(ima*zk*r-done)*dy/r**2
      gkz = gkval*(ima*zk*r-done)*dz/r**2

      efield(1,i)  =efield(1,i) + whts(j)*(ima*zk*gkval*zjvec(1,j) &
          - gkx*zcharge(j))

      efield(2,i) = efield(2,i) + whts(j)*(ima*zk*gkval*zjvec(2,j) &
          - gky*zcharge(j))

      efield(3,i) = efield(3,i) + whts(j)*(ima*zk*gkval*zjvec(3,j) &
          - gkz*zcharge(j))

      hfield(1,i) = hfield(1,i) &
          + whts(j)*(gky*zjvec(3,j)-gkz*zjvec(2,j))
      hfield(2,i) = hfield(2,i) &
          + whts(j)*(gkz*zjvec(1,j)-gkx*zjvec(3,j))
      hfield(3,i) = hfield(3,i) &
          + whts(j)*(gkx*zjvec(2,j)-gky*zjvec(1,j))

    end do

  end do
  !$omp end parallel do

  return
end subroutine emfields3d





subroutine loopfields(zk, center, targ, radius, par1, &
    efield, hfield)
  implicit real *8 (a-h,o-z)
  real *8 :: center(3), targ(3), par1(*)
  complex *16 :: zk, efield(3), hfield(3)

  real *8 :: src2(10), targ2(10)
  complex *16 :: ima, u1, u2, sigma, val, cdp, cdz, grad(10)
  complex *16 :: grad0(10), val2

  !
  ! calculate the e and h fields due to a current loop
  !
  ! input:
  !   zk -
  !   center - the center of the loop in cartesian coordinates
  !   targ - the target location in cartesian coordinates
  !   radius - the radius of the loop
  !   par1 - another paramter for future use
  !
  ! output:
  !   efield, hfield - the maxwell fields in cartesian coordinates
  !

  ima = (0,1)
  done = 1
  pi = 4*atan(done)
  
  dx = targ(1) - center(1)
  dy = targ(2) - center(2)
  dz = targ(3) - center(3)

  p2 = sqrt(dx**2 + dy**2)
  phi2=atan2(dy,dx)
  z2 = targ(3)
  
  z0 = center(3)
        
  mode=0
  src2(1) = radius
  src2(2) = center(3)
  targ2(1) = p2
  targ2(2) = z2
  call gkmone_cos(zk, src2, targ2, par1, mode, val, grad, grad0)

  val = val*pi*2*radius
  cdp = grad(1)*pi*2*radius
  cdz = grad(2)*pi*2*radius
  
  hfield(1)=-cdz/(2*pi*radius)
  hfield(2)=0
  hfield(3)=done/p2/(2*pi*radius)*(val+p2*cdp)

  efield(1)=0
  efield(2)=ima*zk*val/(2*pi*radius)
  efield(3)=0

  !
  ! turn into cartesian coordinates
  !
  u1=hfield(1)
  u2=hfield(2)
  hfield(1)=u1*cos(phi2)-u2*sin(phi2)
  hfield(2)=u1*sin(phi2)+u2*cos(phi2)

  u1=efield(1)
  u2=efield(2)
  efield(1) = u1*cos(phi2)-u2*sin(phi2)
  efield(2) = u1*sin(phi2)+u2*cos(phi2)

  return
end subroutine loopfields





subroutine vecpot_eval(zk, m, n, rl, ps, zs, dpdt, dzdt, &
  zjs, p, theta, z, vals)
  implicit real *8 (a-h,o-z)
  real *8 :: ps(1),zs(1),dpdt(1),dzdt(1), src(10), targ(10)
  complex *16 :: zk,ima,zjs(2,1),vals(1),ee,cc,ss,dp,ds,dz
  complex *16 :: zint1,zint2,zint3,zint4,zint5

  complex *16, allocatable :: zvals(:), grads(:,:), grad0s(:,:)
  
  !   c
  ! c       this subroutine evaluates the vector potential due to a
  ! c       surface current zjs, given in t-hat,theta-hat components
  ! c       at the points which define the surface, ps,zs.
  ! c
  ! c       NOTE: the routine only uses the trapezoidal rule since
  ! c       the current is periodic and p,z does NOT lie on ps,zs.
  ! c       the routine will break when p,z is equal to one of the
  ! c       ps,zs.
  ! c
  ! c       NOTE: the output val IS modulated by exp(ima*m*theta) in order
  ! c       to fully evaluate A at a point in 3D.
  ! c
  ! c       input:
  ! c         eps - the precison with which to evaluate the kernels
  ! c         zk - the complex helmholtz parameter
  ! c         m - the fourier mode we are computing
  ! c         n - the number of points in the discritization of the curve
  ! c         rl - the length of the parameterization interval
  ! c         ps,zs - the points on the generating curve
  ! c         dpdt,dzdt - the tangent vector to the generating curve
  ! c             NOTE: no assumption is made that dpdt**2+dzdt**2=1
  ! c         zjs - a 2 x n array containing tangential and theta components
  ! c             of a surface current
  ! c         p,theta,z - the arbitrary point in cylindrical coordinates
  ! c             at which to evaluate the vector potential
  ! c
  ! c       output:
  ! c         vals - a length 3 array containing the rho,theta,z components
  ! c             of the vector potential
  ! c
  ! c
  done=1.0d0
  pi=4*atan(done)
  ima=(0,1)
  
  zint1=0
  zint2=0
  zint3=0
  zint4=0
  zint5=0
  
  h=rl/n

  maxm = abs(m) + 10
  allocate(zvals(-maxm:maxm))
  allocate(grads(2,-maxm:maxm))
  allocate(grad0s(2,-maxm:maxm))

  targ(1) = p
  targ(2) = z
  
  do i=1,n

    !
    ! call each of the kernel routines
    !
    src(1) = ps(i)
    src(2) = zs(i)
    call gkmall(zk, src, targ, par1, maxm, zvals, grads, grad0s)

    call ghfun2(eps,zk,m,p,z,ps(i),zs(i),ee)

    call prin2('zvals(m) = *', zvals(m), 2)
    call prin2('ee = *', ee, 2)
    stop

    call ghfun2cc(eps,zk,m,p,z,ps(i),zs(i),cc)
    call ghfun2ss(eps,zk,m,p,z,ps(i),zs(i),ss)

    !
    ! and do each of the 5 integrals
    !
    dsdt=dpdt(i)**2+dzdt(i)**2
    dsdt=sqrt(dsdt)
    dpds=dpdt(i)/dsdt
    dzds=dzdt(i)/dsdt

    zint1=zint1+h*zjs(1,i)*dpdt(i)/dsdt*cc*dsdt
    zint2=zint2+h*zjs(2,i)*ss*dsdt
    zint3=zint3+h*zjs(1,i)*dpdt(i)/dsdt*ss*dsdt
    zint4=zint4+h*zjs(2,i)*cc*dsdt
    zint5=zint5+h*zjs(1,i)*dzdt(i)/dsdt*ee*dsdt

  end do

  dp=zint1-ima*zint2
  ds=ima*zint3+zint4
  dz=zint5

  vals(1)=dp*exp(ima*m*theta)
  vals(2)=ds*exp(ima*m*theta)
  vals(3)=dz*exp(ima*m*theta)
  
  return
end subroutine vecpot_eval





subroutine axi_cart2cyl(xyz, avec, rtz, bvec)
  implicit real *8 (a-h,o-z)
  real *8 :: xyz(3), rtz(3)
  complex *16 :: avec(3), bvec(3)

  !
  ! convert the cartesian vector at xyz to the cyldrincal vector at
  ! the same point in cylindrical coords, rtz
  !
  x = xyz(1)
  y = xyz(2)
  z = xyz(3)
  r = sqrt(x**2 + y**2)
  t = atan2(y,x)

  rtz(1) = r
  rtz(2) = t
  rtz(3) = z

  bvec(1) = avec(1)*cos(t) + avec(2)*sin(t)
  bvec(2) = -avec(1)*sin(t) + avec(2)*cos(t)
  bvec(3) = avec(3)
  
  return
end subroutine axi_cart2cyl





subroutine axi_cyl2cart(rtz, avec, xyz, bvec)
  implicit real *8 (a-h,o-z)
  real *8 :: xyz(3), rtz(3)
  complex *16 :: avec(3), bvec(3)

  !
  ! convert the cartesian vector at xyz to the cyldrincal vector at
  ! the same point in cylindrical coords, rtz
  !
  r = rtz(1)
  t = rtz(2)
  z = rtz(3)

  x = cos(t)*r
  y = sin(t)*r

  xyz(1) = x
  xyz(2) = y
  xyz(3) = z

  bvec(1) = avec(1)*cos(t) - avec(2)*sin(t)
  bvec(2) = avec(1)*sin(t) + avec(2)*cos(t)
  bvec(3) = avec(3)
  
  return
end subroutine axi_cyl2cart





subroutine cart2cyl_self(n, pts, field)
  implicit real *8 (a-h,o-z)
  real *8 :: pts(3,1)
  complex *16 :: field(3,n), ima, cdx, cdy
  !
  ! convert a cartesian vector field into a cylindrical one
  !
  done=1
  ima=(0,1)
  pi=4*atan(done)

  do i=1,n
    x=pts(1,i)
    y=pts(2,i)
    r=sqrt(x**2+y**2)
    phi=atan2(y,x)
    cdx=field(1,i)
    cdy=field(2,i)
    field(1,i)=cdx*cos(phi)+cdy*sin(phi)
    field(2,i)=-cdx*sin(phi)+cdy*cos(phi)
  end do
  
  return
end subroutine cart2cyl_self
