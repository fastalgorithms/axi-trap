c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c       this is the end of the debugging code and the beginning
c       of the two routines used for calculating the kernel
c       of the radially symmetric helmholtz problem
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c       This file contains several user-callable subroutines,
c       they are as follows:
c
c       vecpoteval - for a given surface current, evaluates the
c           vector potential at an arbitrary point
c
c       creavecpotmat - creates a matrix that maps surface currents
c           to surface vector potentials which can be specified to
c           be in absolute cylindrical coordinates or relative
c           tangential coordinates
c
c       ghfun2cc - calculates the single layer axisymmetric
c           helmholtz kernel for an angular charge of cos(m*t)*cos(t)
c
c       ghfun2ss -calculates the single layer axisymmetric
c           helmholtz kernel for an angular charge of sin(m*t)*sin(t)
c
c       ghfun2ccgrad - calculates the partial derivatives at the
c           target of the single layer axisymmetric
c           helmholtz kernel for an angular charge of cos(m*t)*cos(t)
c
c       ghfun2ssgrad - calculates the partial derivatives at the
c           target of the single layer axisymmetric
c           helmholtz kernel for an angular charge of sin(m*t)*sin(t)
c
c       ghfun2ccn1 - the same routine as ghfun2ccgrad except the calling
c           sequence is different
c
c       ghfun2ssn1 - the same routine as ghfun2ssgrad except the calling
c           sequence is different
c
c       gh_hess - second derivatives at the target for the single layer
c           axisymmetric helmholtz kernel
c
c       ghcc_hess - second derivatives at the target for the single layer
c           axisymmetric cosine-cosine helmholtz kernel
c
c       ghss_hess - second derivatives at the target for the single layer
c           axisymmetric sine-sine helmholtz kernel
c
c       rotcyl2cart - receives as input a vector in cylindrical
c           coordinates and turns it into the usual cartesian vector
c
c       rotaxi2cart - receives as input a vector in tanget, normal,
c           theta-hat coordinates in axisymmetric coordinates, and
c           turns it into the usual cartesian vector
c
c       ghfun2grad - receives as input a scalar function on a curve,
c           and computes the target gradient in cylindrical coordinates
c           of the single layer potential (for off surface evaluation)
c
c       ghfun2 - calculates the single layer axisymmetric helmholtz
c           kernel with a given fourier mode
c
c       ghfun4n0 - calculates the double layer axisymmetric helmholtz
c           kernel with a given fourier mode
c
c       ghfun4n1 - calculates the target derivative of a single layer
c           axisymmetric helmholtz kernel with a given fourier mode
c
c       laplacepder - calculates the source gradient of a singler layer
c           axisymmetric laplace kernel for fourier mode 0
c
c       Required files:
c           prini.f
c           legeexps.f
c           cadapgau.f
c           elliptic_ke.f
c           chebexps.f
c           alpermat/test16.f
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c
c
        subroutine rqvals(z,eps,vals,nvals)
        implicit real *8 (a-h,o-z)
        real *8 z,cv1,cv2,cv3,vals(0:1),cd,cd2,coef
c
c        This subroutine evaluates half-degree legendre functions
c        of the second kind for real argument z larger than 1.
c
c                      Input parameters:
c
c  z - the point at which the Legendre functions are to be evaluated
c  eps - the relative precision with which to evaluate them
c  
c                      Output parameters:
c
c  vals - the real array of function values (PLEASE NOTE THAT THIS ARRAY
c        MUST CONTAIN ELEMENT NUMBER 0)
c  nvals - the number of elements in array vals that are returned
c
c
c       . . . find how high we should be going
c
        done=1
        cv1=0
        cv2=1
c
cccc        cv1=2.018905820d0
cccc        cv2=0.3931751484d0
c
        dlarge=1/eps**2/10
        do 1200 i=2,10 000 000
c
        nmax=i
        cv3=4*(i-1)*z*cv2/(2*i-1) - (2*i-3)*cv1/(2*i-1)
        dd=abs(cv3)
        if(dd .gt. dlarge) goto 1400  
c
        cv1=cv2
        cv2=cv3
c
 1200 continue
 1400 continue
c
        call prinf('nmax=*',nmax,1)
        call prin2('dd=*',dd,1)
cccc        stop
cccc        call prin2('nmax=*',nmax*done,1)
cccc        stop

c
c        having found the maximum point, construct the 
c        unscaled Legendre functions
c
        vals(nmax)=0
        vals(nmax-1)=1
c
        do 1600 i=nmax,2,-1
        vals(i-2)=4*(i-1)*z*vals(i-1)/(2*i-3) - (2*i-1)*vals(i)/(2*i-3)
 1600 continue

c
c        evaluate the normalization coefficient
c
cccc        call prin2('vals=*',vals,nmax+1)
cccc        stop

        u=sqrt(2/(z+1))
        call elliptic_ke(u,fk,fe)
        rlam=u*fk/vals(0)

c
c       . . . scale them things
c
        do 2400 i=0,nmax
        vals(i)=vals(i)*rlam
 2400 continue


ccc        call prinf('nmax=*',nmax,1)
cccc        call prin2('vals=*',vals,nmax)
cccc        stop

        nvals=nmax
        return

c
c       determine the parameter nvals
c
c
        nvals=0
        eps22=eps**2*rlarge**2/10
c
        do 2600 i=nmax-2,1,-1
c
        dd=vals(i)**2
c
        if(dd .gt. eps22) goto 2800
c
        nvals=i
 2600 continue
 2800 continue
c
        return
        end
c
c 
c 
c 
c 
        subroutine rjbvals7(z,eps,vals,nvals,nmax)
        implicit real *8 (a-h,o-z)
        real *8 z,cv1,cv2,cv3,vals(1),cd,cd2,coef
c
c        This subroutine evaluates Bessel J-functions of the 
c        real argument z. The number nvals of functions returned 
c        is determined (inter alia) by the user-specified precision
c        eps. More specifically, 
c
c        abs(J_{nvals} (z)) \sim eps,                               (1)
c
c        and the value J_{nvals} (z) is itself calculated to the
c        relative precision eps. Thus, this is a fairly gold-plated
c        code; no attempt to optimize it for the CPU time has been
c        made.
c
c                      Input parameters:
c
c  z - the point at which the Bessel functions are to be evaluated
c  eps - the relative precision to which the value J_{nvals} (z) is 
c        to be calculated
c  
c                      Output parameters:
c
c  vals - the array of Bessel functions (PLEASE NOTE THAT THIS ARRAY
c        MUST CONTAIN ELEMENT NUMBER 0)
c  nvals - the number of elements in array vals that are returned with 
c        relative precision eps; also, please note that vals(nvals)
c        will be roughly of (relative) size eps.
c  nmax - the total number of elements in array vals used by this 
c        subroutine; please note that it is somewhat greater than 
c        nvals
c
c
c       . . . find how high we should be going
c
        cv1=0
        cv2=1
c
        dlarge=1/eps**2/10
        do 1200 i=1,10 000 000 
c
        nmax=i
        cv3=2*i/z*cv2-cv1
c
        dd=abs(cv3)
        if(dd .gt. dlarge) goto 1400  
c
        cv1=cv2
        cv2=cv3
c
 1200 continue
 1400 continue
c
        call prinf('nmax=*',nmax,1)
c
c        having found the maximum point, construct the 
c        unscaled Bessel functions
c
        vals(nmax)=0
        vals(nmax-1)=1
        do 1600 i=nmax-1,1,-1
c
        vals(i-1)=2*i/z*vals(i)-vals(i+1)
 1600 continue
c
        call prin2('vals=*',vals(0),nmax)
cccc        stop
c
c        evaluate the normalization coefficient
c
        dd1=abs(cos(z))
        dd2=abs(sin(z))
c
        rlarge=dd1+dd2
cccc        call prin2('z=*',z,1)

c
        if(dd2 .gt. dd1) goto 2000
        i0=0
        cd=vals(i0)/2
        d=-1
        do 1800 i=2,nmax,2
c
        cd=cd+vals(i)*d
c
        d=-d        
 1800 continue
c
        cd=cd*2
c
        coef=cos(z)/cd
        goto 2300
c
 2000 continue
c
        cd2=0
        d=1
        do 2200 i=1,nmax,2
c
        cd2=cd2+vals(i)*d
c
        d=-d        
 2200 continue
c
        cd2=cd2*2
c
        coef=sin(z)/cd2
c
 2300 continue
c
c       . . . scale them things
c
        do 2400 i=0,nmax-2
c
        vals(i)=vals(i)*coef
 2400 continue

        call prin2('after scaling, vals=*',vals(0),nmax)
cccc        stop


c
c       determine the parameter nvals
c
c
        nvals=0
        eps22=eps**2*rlarge**2/10
c
        do 2600 i=nmax-2,1,-1
c
        dd=vals(i)**2
c
        if(dd .gt. eps22) goto 2800
c
        nvals=i
 2600 continue
 2800 continue
c

        call prin2('rlarge=*',rlarge,1)
        call prin2('eps=*',eps,1)
        call prinf('nmax=*',nmax,1)
        call prinf('nvals=*',nvals,1)

        return
        end
c
c 
c 
c 
c 
        subroutine ghfun3(eps,zk,m,p,z,p0,z0,val)
        implicit real *8 (a-h,o-z)
        complex *16 zk,val,ima
c
c       evaluate the single layer kernel elements using special
c       sum formulas
c
c
        ima=(0,1)
        done=1
        pi=4*atan(done)

c
c       determine which formula to use
c




c
        return
        end
c
c
c
c
c
        subroutine ghfun2grad(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      sigma,p,theta,z,vals)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1)
        complex *16 ima,vals(1),zk,sigma(1),zint1,zint2,zint3,
     1      val0,valp,valz
c
c       this subroutine calculates the gradient of a single
c       layer potential for scalar density sigma in
c       cylindrical coordinates which is OFF THE SURFACE i.e.,
c
c           p-hat d/dp + 1/p theta-hat d/dtheta + z-hat d/dz
c
c       and returns it as a length 3 array vals.  the routine only
c       uses the trapezoidal rule and will go bust if p,theta,z lie
c       on the curve ps,zs
c
c       input:
c         eps - the precision with which to evaluate the kernels
c         zk - the complex helmholtz parameter
c         m - the fourier mode we are computing
c         n - the number of points on the curve ps,zs
c         rl - the length of the parameterizing interval
c         ps,zs - the points on the generating curve
c         dpdt,dzdt - tanget vectors to the generating curve
c         sigma - scalar potential we are integrating around the curve
c         p,theta,z - point at which to evaluate the gradient
c
c       output:
c         vals - contains the gradient of the single layer in
c             cylindrical coordinates, p-hat, theta-hat, z-hat
c
c       NOTE: the gradient output IS modulated by exp(ima*m*theta)
c
c
        ima=(0,1)
        done=1.0d0
        pi=4*atan(done)
c
        h=rl/n
        zint1=0
        zint2=0
        zint3=0
c

        do 2400 i=1,n
c
c       call each of the kernels and do each of the 3 integrals
c
        call ghfun2(eps,zk,m,p,z,ps(i),zs(i),val0)
c
        vnp=1
        vnz=0
        call ghfun4n1(eps,zk,m,p,z,ps(i),zs(i),vnp,vnz,valp)
c
        vnp=0
        vnz=1
        call ghfun4n1(eps,zk,m,p,z,ps(i),zs(i),vnp,vnz,valz)
c
        dsdt=sqrt(dpdt(i)**2+dzdt(i)**2)
c
        zint1=zint1+h*sigma(i)*val0*dsdt
        zint2=zint2+h*sigma(i)*valp*dsdt
        zint3=zint3+h*sigma(i)*valz*dsdt
c
 2400   continue
c
        vals(1)=zint2*exp(ima*m*theta)
        vals(2)=ima*m/p*zint1*exp(ima*m*theta)
        vals(3)=zint3*exp(ima*m*theta)
c
        return
        end
c
c
c
c
c
        subroutine ghfun2ccn1(eps,zk,m,p,z,p0,z0,vnp,vnz,val)
        implicit real *8 (a-h,o-z)
        complex *16 zk,val,ima,dp,dz
c
c       returns the directional derivative at the target of ghfun2cc,
c       see ghfun2cc for description
c
        ima=(0,1)
        call ghfun2ccgrad(eps,zk,m,p,z,p0,z0,dp,dz)
        val=vnp*dp+vnz*dz
c
        return
        end
c
c
c
c
c
        subroutine ghfun2ssn1(eps,zk,m,p,z,p0,z0,vnp,vnz,val)
        implicit real *8 (a-h,o-z)
        complex *16 zk,val,ima,dp,dz
c
c       returns the directional derivative at the target of ghfun2ss,
c       see ghfun2ss for description
c
        ima=(0,1)
        call ghfun2ssgrad(eps,zk,m,p,z,p0,z0,dp,dz)
        val=vnp*dp+vnz*dz
c
        return
        end
c
c
c
c
c
        subroutine vecpoteval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      zjs,p,theta,z,vals)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1)
        complex *16 zk,ima,zjs(2,1),vals(1),ee,cc,ss,dp,ds,dz,
     1      zint1,zint2,zint3,zint4,zint5
c
c       this subroutine evaluates the vector potential due to a
c       surface current zjs, given in t-hat,theta-hat components
c       at the points which define the surface, ps,zs.
c
c       NOTE: the routine only uses the trapezoidal rule since
c       the current is periodic and p,z does NOT lie on ps,zs.
c       the routine will break when p,z is equal to one of the
c       ps,zs.
c
c       NOTE: the output val IS modulated by exp(ima*m*theta) in order
c       to fully evaluate A at a point in 3D.
c
c       input:
c         eps - the precison with which to evaluate the kernels
c         zk - the complex helmholtz parameter
c         m - the fourier mode we are computing
c         n - the number of points in the discritization of the curve
c         rl - the length of the parameterization interval
c         ps,zs - the points on the generating curve
c         dpdt,dzdt - the tangent vector to the generating curve
c             NOTE: no assumption is made that dpdt**2+dzdt**2=1
c         zjs - a 2 x n array containing tangential and theta components
c             of a surface current
c         p,theta,z - the arbitrary point in cylindrical coordinates
c             at which to evaluate the vector potential
c
c       output:
c         vals - a length 3 array containing the rho,theta,z components
c             of the vector potential
c
c
        done=1.0d0
        pi=4*atan(done)
        ima=(0,1)
c
        zint1=0
        zint2=0
        zint3=0
        zint4=0
        zint5=0
c
        h=rl/n


        do 2000 i=1,n
c
c       call each of the kernel routines
c
        call ghfun2(eps,zk,m,p,z,ps(i),zs(i),ee)
        call ghfun2cc(eps,zk,m,p,z,ps(i),zs(i),cc)
        call ghfun2ss(eps,zk,m,p,z,ps(i),zs(i),ss)
c
c       and do each of the 5 integrals
c
        dsdt=dpdt(i)**2+dzdt(i)**2
        dsdt=sqrt(dsdt)
        dpds=dpdt(i)/dsdt
        dzds=dzdt(i)/dsdt
c
        zint1=zint1+h*zjs(1,i)*dpdt(i)/dsdt*cc*dsdt
        zint2=zint2+h*zjs(2,i)*ss*dsdt
        zint3=zint3+h*zjs(1,i)*dpdt(i)/dsdt*ss*dsdt
        zint4=zint4+h*zjs(2,i)*cc*dsdt
        zint5=zint5+h*zjs(1,i)*dzdt(i)/dsdt*ee*dsdt
c
 2000   continue
c

        dp=zint1-ima*zint2
        ds=ima*zint3+zint4
        dz=zint5
c
        vals(1)=dp*exp(ima*m*theta)
        vals(2)=ds*exp(ima*m*theta)
        vals(3)=dz*exp(ima*m*theta)
c
        return
        end
c
c
c
c
c
        subroutine creavecpotmat(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      norder,iftan,amat,wcc,wss,w)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1),dsdt(10000)
        complex *16 zk,amat(3*n,2*n),wcc(n,n),wss(n,n),
     1      w(n,n),ima
        external ghfun2,ghfun2cc,ghfun2ss
c
c       this subroutine creates a matrix which maps a vector of
c       length 2*n representing the t-hat and theta-hat components
c       of a surface current into a vector of length 3*n which
c       represents the vector potential of said current.  the parameter
c       iftan specifies wheather the matrix should map to cylindrical
c       coordinates or relative tangential coordinates using
c       information about the curve stored in in dpdt,dzdt.
c
c       NOTE: it is assumed that current vector of 2*n will be relative
c       to unit vectors, but there is NO assumption in this
c       routine that dpdt**2+dzdt**2=1
c
c       input:
c         eps - the precision to which kernel elements are to be computed
c         zk - the complex helmholtz parameter
c         m - the fourier mode for which to construct the matrix
c         n - the number of points on the curve ps,zs
c         rl - the length of the parameterizing interval NOT LENGTH OF CURVE
c             unless ps,zs,dpdt,dzdt were created using an arclength
c             parameterization
c         ps,zs - the points of the generating curve
c         dpdt,dzdt - derivatives with respect to parameterizing variable
c         norder - order of Alpert quadratures to use when
c             constructing the matrix - can be 2, 6, or 10
c         iftan - iftan=1 means return a matrix which maps to tangential
c             coordinates, iftan=0 means return a matrix which maps
c             to cylindrical coordinates
c
c       output:
c         amat - the 3*n x 2*n matrix which maps surface currents to
c             vector potentials
c
c       work:
c         wcc - needs to be of length n**2 complex *16 words
c         wss - needs to be of length n**2 complex *16 words
c         w - needs to be of length n**2 complex *16 words
c
c
        done=1.0d0
        pi=4*atan(done)
        ima=(0,1)
c
        h=rl/n
        do 1400 i=1,n
        sc=dpdt(i)**2+dzdt(i)**2
        sc=sqrt(sc)
        dsdt(i)=sc
 1400   continue
c
c
cccc        call prin2('dsdt=*',dsdt,n)
c
c       first form the three n x n matrices that will be needed 
c       to build the 3*n x 2*n large one
c
        call formslpmatbac(ier,wcc,norder,ps,zs,dsdt,h,n,
     1      ghfun2cc,eps,zk,m)
c
        call formslpmatbac(ier,wss,norder,ps,zs,dsdt,h,n,
     1      ghfun2ss,eps,zk,m)
c
        call formslpmatbac(ier,w,norder,ps,zs,dsdt,h,n,
     1      ghfun2,eps,zk,m)
c
c       and now combine those guys into amat, depending on the
c       parameter iftan
c
cccc        call prinf('iftan=*',iftan,1)
        if (iftan .eq. 1) goto 3000
c
        do 2400 i=1,n
        i1=3*(i-1)+1
        i2=i1+1
        i3=i2+1
c
        do 2200 j=1,n
        j1=2*(j-1)+1
        j2=j1+1
c
        dpds=dpdt(j)/dsdt(j)
        dzds=dzdt(j)/dsdt(j)
c
        amat(i1,j1)=dpds*wcc(i,j)
        amat(i2,j1)=ima*dpds*wss(i,j)
        amat(i3,j1)=dzds*w(i,j)
c
        amat(i1,j2)=-ima*wss(i,j)
        amat(i2,j2)=wcc(i,j)
        amat(i3,j2)=0
c
 2200   continue
 2400   continue
c
        return
c

 3000   continue
c
c       if here, construct the matrix in tangent coordinates so
c       that it outputs a vector in t-hat, theta-hat, n-hat coordinates
c
        do 3400 i=1,n
        i1=3*(i-1)+1
        i2=i1+1
        i3=i2+1
c
        do 3200 j=1,n
        j1=2*(j-1)+1
        j2=j1+1
c
        dpdsj=dpdt(j)/dsdt(j)
        dzdsj=dzdt(j)/dsdt(j)
c
        dpdsi=dpdt(i)/dsdt(i)
        dzdsi=dzdt(i)/dsdt(i)
c
        amat(i1,j1)=dpdsi*dpdsj*wcc(i,j)+dzdsi*dzdsj*w(i,j)
        amat(i2,j1)=ima*dpdsj*wss(i,j)
        amat(i3,j1)=dzdsi*dpdsj*wcc(i,j)-dpdsi*dzdsj*w(i,j)
c
        amat(i1,j2)=dpdsi*(-ima)*wss(i,j)
        amat(i2,j2)=wcc(i,j)
        amat(i3,j2)=dzdsi*(-ima)*wss(i,j)
c
 3200   continue
 3400   continue
c
        return
        end
c
c
c
c
c
        subroutine ghfun2cc(eps,zk,m,p,z,p0,z0,val)
        implicit real *8 (a-h,o-z)
        complex *16 ima,val,rint,funcc,zk
        real *8 p1(10),p2(10)
        external funcc
c
c       this subroutine evaluates the integral
c
c       val = p0/(4pi) \int_0^2pi exp(i*zk*r)/r * cos(m*t) * cos(t) dt
c
c       the integral arises when calculating the vector potential of
c       a vector density pointing in the theta-hat or rho-hat direction
c       when in cyclindrical coordinates
c
c       input:
c           eps - the precision to pass to the adaptive gaussian
c                 quadrature routine
c           zk - the complex helmholtz parameter
c           m - the fourier mode to evaluate
c           p,z - cylindrical coordinates for target point
c           p0,z0 - cylindrical coordinates for source point
c
c       output:
c           val - the value of the integral
c
c
        ima=(0,1)
        pi=4*atan(1.0d0)
c
        a=(p-p0)**2+(z-z0)**2+4*p*p0
        b=4*p*p0/a
c
        p1(1)=0
        p1(2)=a
        p1(3)=b
        p1(4)=dble(zk)
        p1(5)=imag(zk)
c
        p2(1)=m+.1
        if (m .lt. 0) p2(1)=m-.1
        p2(2)=p
        p2(3)=z
        p2(4)=p0
        p2(5)=z0
c
        a4=0
        b4=pi/2
        k4=16
c
        call cadapgau(ier,a4,b4,funcc,p1,p2,k4,eps,
     1      rint,maxrec,numint)
c
        if (ier .ne. 0) then
          call prinf('in cadapgau, ier=*',ier,1)
          stop
        endif
c
cccc        call prinf('in ghfun2cc, after cadapgau, ier=*',ier,1)
cccc        call prinf('in ghfun2cc, after cadapgau, maxrec=*',maxrec,1)
cccc        call prinf('in ghfun2cc, after cadapgau, numint=*',numint,1)
c

        rk=sqrt(b)

        d=((p-p0)**2+(z-z0)**2)/a
        rkp=sqrt(d)

        call ke_eva(rk,rkp,fk,fe)
cccc        call elliptic_ke(rk,fk,fe)


cccc        call prin2('zk=*',zk,2)
cccc        call prin2('rint=*',rint,2)


        val=rint
        val=val+fk*4/sqrt(a)
        val=val/4/pi
        val=val*p0
c
        return
        end
c
c
c
        function funcc(t,par1,par2)
        implicit real *8 (a-h,o-z)
        complex *16 funcc,val,ima,zz,zz2,z3,zk
        real *8 par1(1),par2(1)
c        
        ima=(0,1)
        pi=4*atan(1.0d0)
c
        a=par1(2)
        b=par1(3)
        zk=par1(4)+ima*par1(5)
c
        m=par2(1)
c
        p=par2(2)
        z=par2(3)
        p0=par2(4)
        z0=par2(5)
c
cccc        zz=(1-2*sin(m*t)**2)*(1-2*sin(t)**2)
        zz=cos(2*m*t)*cos(2*t)

        zz=zz*exp(ima*zk*sqrt(a)*sqrt(1-b*cos(t)**2))-1
        zz=zz/sqrt(1-b*cos(t)**2)*4/sqrt(a)
        funcc=zz
c
        return
        end
c
c
c
c
c
        subroutine ghfun2ss(eps,zk,m,p,z,p0,z0,val)
        implicit real *8 (a-h,o-z)
        complex *16 ima,val,rint,funss,zk
        real *8 p1(10),p2(10)
        external funss
c
c       this subroutine evaluates the integral
c
c       val = p0/(4pi) \int_0^2pi exp(i*zk*r)/r * sin(m*t) * sin(t) dt
c
c       the integral arises when calculating the vector potential of
c       a vector density pointing in the theta-hat or rho-hat direction
c       when using cyclindrical coordinates
c
c       input:
c           eps - the precision to pass to the adaptive gaussian
c                 quadrature routine
c           zk - the complex helmholtz parameter
c           m - the fourier mode to evaluate
c           p,z - cylindrical coordinates for target point
c           p0,z0 - cylindrical coordinates for source point
c
c       output:
c           val - the value of the integral
c
c
        ima=(0,1)
        pi=4*atan(1.0d0)
c
        a=(p-p0)**2+(z-z0)**2+4*p*p0
        b=4*p*p0/a
c
        p1(1)=0
        p1(2)=a
        p1(3)=b
        p1(4)=dble(zk)
        p1(5)=imag(zk)
c
        p2(1)=m+.1
        if (m .lt. 0) p2(1)=m-.1
        p2(2)=p
        p2(3)=z
        p2(4)=p0
        p2(5)=z0
c
        a4=0
        b4=pi/2
        k4=16
c
        call cadapgau(ier,a4,b4,funss,p1,p2,k4,eps,
     1      rint,maxrec,numint)
c
        if (ier .ne. 0) then
          call prinf('in cadapgau, ier=*',ier,1)
          stop
        endif
c
cccc        call prinf('in ghfun2ss, after cadapgau, ier=*',ier,1)
cccc        call prinf('in ghfun2ss, after cadapgau, maxrec=*',maxrec,1)
cccc        call prinf('in ghfun2ss, after cadapgau, numint=*',numint,1)

        val=rint

        rk=sqrt(b)

        d=((p-p0)**2+(z-z0)**2)/a
        rkp=sqrt(d)

cccc        call ke_eva(rk,rkp,fk,fe)

cc        call elliptic_ke(sqrt(b),fk,fe)
cc        val=val+fk*4/sqrt(a)

        val=val/4/pi
        val=val*p0
c
        return
        end
c
c
c
        function funss(t,par1,par2)
        implicit real *8 (a-h,o-z)
        complex *16 funss,val,ima,zz,zz2,z3,zk
        real *8 par1(1),par2(1)
c        
        ima=(0,1)
        pi=4*atan(1.0d0)
        a=par1(2)
        b=par1(3)
        zk=par1(4)+ima*par1(5)
c
        m=par2(1)
c
        p=par2(2)
        z=par2(3)
        p0=par2(4)
        z0=par2(5)
c
        d=p**2+p0**2-2*p*p0*(1-2*sin(t)**2)+(z-z0)**2
        d=sqrt(d)
c
        t2=t-pi/4
        tm2=m*t-pi/4

        zz=(1-2*sin(tm2)**2)*(1-2*sin(t2)**2)
        zz=zz*exp(ima*zk*sqrt(a)*sqrt(1-b*cos(t)**2))
cccc        zz=zz*exp(ima*zk*sqrt(a)*sqrt(1-b*cos(t)**2))-1
        zz=zz/sqrt(1-b*cos(t)**2)*4/sqrt(a)
        funss=zz
c
        return
        end
c
c
c
c
c
        subroutine ghfun2ccgrad(eps,zk,m,p,z,p0,z0,dp,dz)
        implicit real *8 (a-h,o-z)
        real *8 p1(10),p2(10)
        complex *16 zk,val,ima,funccp,funccz,rint,rint2,dp,dz
        external funccp,funccz
c
c       this subroutine calculates the partial derivatives of
c       ghfun2cc with respect to p and z
c
c       input:
c           eps - the precision to pass to the adaptive gaussian
c                 quadrature routine
c           zk - the complex helmholtz parameter
c           m - the fourier mode to evaluate
c           p,z - cylindrical coordinates for target point
c           p0,z0 - cylindrical coordinates for source point
c
c       output:
c           dp - the p partial derivative of ghfun2cc
c           dz - the z partial derivative of ghfun2cc
c
c
        ima=(0,1)
        pi=4*atan(1.0d0)
c
        a=(p-p0)**2+(z-z0)**2+4*p*p0
        b=4*p*p0/a
c
        p1(1)=0
        p1(2)=a
        p1(3)=b
        p1(4)=dble(zk)
        p1(5)=imag(zk)
c
        p2(1)=m+.1
        if (m .lt. 0) p2(1)=m-.1
        p2(2)=p
        p2(3)=z
        p2(4)=p0
        p2(5)=z0
c
        a4=0
        b4=pi/2
        k4=16
c
        call cadapgau(ier,a4,b4,funccp,p1,p2,k4,eps,
     1      dp,maxrec,numint)
        dp=dp*p0/4/pi
c
        if (ier .ne. 0) then
          call prinf('in cadapgau, ier=*',ier,1)
          stop
        endif
c
cccc        call prinf('in ghfun2ccgrad, after cadapgau, ier=*',ier,1)
cccc        call prinf('in ghfun2ccgrad, after cadapgau, maxrec=*',maxrec,1)
cccc        call prinf('in ghfun2ccgrad, after cadapgau, numint=*',numint,1)
c

        call cadapgau(ier,a4,b4,funccz,p1,p2,k4,eps,
     1      dz,maxrec,numint)
        dz=dz*p0/4/pi
c
        if (ier .ne. 0) then
          call prinf('in cadapgau, ier=*',ier,1)
          stop
        endif
c
cccc        call prinf('in ghfun2ccgrad, after cadapgau, ier=*',ier,1)
cccc        call prinf('in ghfun2ccgrad, after cadapgau, maxrec=*',maxrec,1)
cccc        call prinf('in ghfun2ccgrad, after cadapgau, numint=*',numint,1)
c
        return
        end
c
c
c
        function funccp(t,par1,par2)
        implicit real *8 (a-h,o-z)
        complex *16 funccp,val,ima,zz,zz2,z3,zk
        real *8 par1(1),par2(1)
c        
        ima=(0,1)
        pi=4*atan(1.0d0)
        a=par1(2)
        b=par1(3)
        zk=par1(4)+ima*par1(5)
c
        m=par2(1)
c
        p=par2(2)
        z=par2(3)
        p0=par2(4)
        z0=par2(5)
c
        d=(p-p0)**2+4*p*p0*sin(t)**2+(z-z0)**2
        d=sqrt(d)
c
        zz=(1-2*sin(m*t)**2)*(1-2*sin(t)**2)
        zz=zz*4*exp(ima*zk*d)*(p-p0+2*p0*sin(t)**2)
        zz=zz*(ima*zk-1/d)/d/d
c
        funccp=zz
c
        return
        end
c
c
c
        function funccz(t,par1,par2)
        implicit real *8 (a-h,o-z)
        complex *16 funccz,val,ima,zz,zz2,z3,zk
        real *8 par1(1),par2(1)
c        
        ima=(0,1)
        pi=4*atan(1.0d0)
        a=par1(2)
        b=par1(3)
        zk=par1(4)+ima*par1(5)
c
        m=par2(1)
c
        p=par2(2)
        z=par2(3)
        p0=par2(4)
        z0=par2(5)
c
        d=(p-p0)**2+4*p*p0*sin(t)**2+(z-z0)**2
        d=sqrt(d)
c
        zz=(1-2*sin(m*t)**2)*(1-2*sin(t)**2)
        zz=zz*4*exp(ima*zk*d)*(z-z0)
        zz=zz*(ima*zk-1/d)/d/d
c
        funccz=zz
c
        return
        end
c
c
c
c
c
        subroutine ghfun2ssgrad(eps,zk,m,p,z,p0,z0,dp,dz)
        implicit real *8 (a-h,o-z)
        real *8 p1(10),p2(10)
        complex *16 zk,val,ima,funssp,funssz,rint,dp,dz
        external funssp,funssz
c
c       this subroutine calculates the partial derivatives of
c       ghfun2ss with respect to p and z
c
c       input:
c           eps - the precision to pass to the adaptive gaussian
c                 quadrature routine
c           zk - the complex helmholtz parameter
c           m - the fourier mode to evaluate
c           p,z - cylindrical coordinates for target point
c           p0,z0 - cylindrical coordinates for source point
c
c       output:
c           dp - the p partial derivative of ghfun2ss
c           dz - the z partial derivative of ghfun2ss
c
c
        ima=(0,1)
        pi=4*atan(1.0d0)
c
        a=(p-p0)**2+(z-z0)**2+4*p*p0
        b=4*p*p0/a
c
        p1(1)=0
        p1(2)=a
        p1(3)=b
        p1(4)=dble(zk)
        p1(5)=imag(zk)
c
        p2(1)=m+.1
        if (m .lt. 0) p2(1)=m-.1
        p2(2)=p
        p2(3)=z
        p2(4)=p0
        p2(5)=z0
c
        a4=0
        b4=pi/2
        k4=16
c
        call cadapgau(ier,a4,b4,funssp,p1,p2,k4,eps,
     1      dp,maxrec,numint)
        dp=dp*p0/4/pi
c
        if (ier .ne. 0) then
          call prinf('in cadapgau, ier=*',ier,1)
          stop
        endif
c
cccc        call prinf('in ghfun2ssgrad, after cadapgau, ier=*',ier,1)
cccc        call prinf('in ghfun2ssgrad, after cadapgau, maxrec=*',maxrec,1)
cccc        call prinf('in ghfun2ssgrad, after cadapgau, numint=*',numint,1)
c

        call cadapgau(ier,a4,b4,funssz,p1,p2,k4,eps,
     1      dz,maxrec,numint)
        dz=dz*p0/4/pi
c
        if (ier .ne. 0) then
          call prinf('in cadapgau, ier=*',ier,1)
          stop
        endif
c

cccc        call prinf('in ghfun2ssgrad, after cadapgau, ier=*',ier,1)
cccc        call prinf('in ghfun2ssgrad, after cadapgau, maxrec=*',maxrec,1)
cccc        call prinf('in ghfun2ssgrad, after cadapgau, numint=*',numint,1)
c
c
        return
        end
c
c
c
        function funssp(t,par1,par2)
        implicit real *8 (a-h,o-z)
        complex *16 funssp,val,ima,zz,zz2,z3,zk
        real *8 par1(1),par2(1)
c        
        ima=(0,1)
        pi=4*atan(1.0d0)
        a=par1(2)
        b=par1(3)
        zk=par1(4)+ima*par1(5)
c
        m=par2(1)
c
        p=par2(2)
        z=par2(3)
        p0=par2(4)
        z0=par2(5)
c
        d=(p-p0)**2+4*p*p0*sin(t)**2+(z-z0)**2
        d=sqrt(d)
 
        t2=t-pi/4
        tm2=m*t-pi/4

        zz=(1-2*sin(tm2)**2)*(1-2*sin(t2)**2)
        zz=zz*4*exp(ima*zk*d)*(p-p0+2*p0*sin(t)**2)
        zz=zz*(ima*zk-1/d)/d/d
        funssp=zz
c
        return
        end
c
c
c
        function funssz(t,par1,par2)
        implicit real *8 (a-h,o-z)
        complex *16 funssz,val,ima,zz,zz2,z3,zk
        real *8 par1(1),par2(1)
c        
        ima=(0,1)
        pi=4*atan(1.0d0)
        a=par1(2)
        b=par1(3)
        zk=par1(4)+ima*par1(5)
c
        m=par2(1)
c
        p=par2(2)
        z=par2(3)
        p0=par2(4)
        z0=par2(5)
c
        d=(p-p0)**2+4*p*p0*sin(t)**2+(z-z0)**2
        d=sqrt(d)

        t2=t-pi/4
        tm2=m*t-pi/4

        zz=(1-2*sin(tm2)**2)*(1-2*sin(t2)**2)
        zz=zz*4*exp(ima*zk*d)*(z-z0)
        zz=zz*(ima*zk-1/d)/d/d
        funssz=zz
c
        return
        end
c
c
c
c
c
        subroutine rotcyl2cart(apsz,theta,axyz)
        implicit real *8 (a-h,o-z)
        complex *16 apsz(1),axyz(1)
c
c       this routine takes a vector in cylindrical coordinates from a point
c       with angle theta, and expresses it in terms of cartesian
c       coordinates
c
c       input:
c           apsz - length 3 array with cylindrical components
c               in the dirction of rho-hat, theta-hat, z-hat
c           theta - the angle necessary for rho-hat and theta-hat evals
c
c       output:
c           axyz - length 3 vector of cartesian components of above vector
c
c
        axyz(1)=apsz(1)*cos(theta)+apsz(2)*(-sin(theta))
        axyz(2)=apsz(1)*sin(theta)+apsz(2)*(cos(theta))
        axyz(3)=apsz(3)
c
        return
        end
c
c
c
c
c
        subroutine rotaxi2cart(atsn,vt,vn,theta,axyz)
        implicit real *8 (a-h,o-z)
        real *8 vt(1),vn(1)
        complex *16 atsn(1),axyz(1)
c
c       this routine takes a vector defined in LOCAL axisymmetric
c       coordinates (tangent, theta-hat, normal) and expresses it
c       in cartesian coordinates
c
c       input:
c
c         atsn - length 3 array with the LOCAL axisymmetric coordinates
c             in the direction of tangent, theta-hat, normal
c         theta - the theta coordinate of the base of the vector
c         vt - the length 2 vector of p,z coordinates for tangent vector
c         vn - the length 2 vector of p,z coordinates for normal vector
c
c       output:
c
c         axyz - length 3 array with cartesian coordinates of the vector
c
c
        pi=4*atan(1.0d0)
c
        axyz(1)=atsn(1)*vt(1)*cos(theta)-atsn(2)*sin(theta)+
     1       atsn(3)*vn(1)*cos(theta)
c
        axyz(2)=atsn(1)*vt(1)*sin(theta)+atsn(2)*cos(theta)+
     1       atsn(3)*vn(1)*sin(theta)
c
        axyz(3)=atsn(1)*vt(2)+atsn(3)*vn(2)
c
        return
        end
c
c
c
c
c
        subroutine ghfun2(eps,zk,m,p,z,p0,z0,val)
        implicit real *8 (a-h,o-z)
        complex *16 dz,ima,zz,val,rint,funuser2,zk
        real *8 p1(10),p2(10)
        external funuser2
c
c       calculate the radial helmholtz greens function by
c       integrating an alternative formula on [0,pi/2] as well as
c       subracting off the laplace kernel and then adding it back in -
c       uses adaptive gaussian quadrature
c
c       input:
c           eps - the precision to pass to the adaptive gaussian
c                 quadrature routine
c           zk - the complex helmholtz parameter
c           m - the fourier mode to evaluate
c           p,z - cylindrical coordinates for target point
c           p0,z0 - cylindrical coordinates for source point
c
c       output:
c           val - the value of the single layer kernel
c
c
c
        ima=(0,1)
        pi=4*atan(1.0d0)
c
        a=(p-p0)**2+(z-z0)**2+4*p*p0
        b=4*p*p0/a
cccc        call prin2('in ghfun2, b=*',b,1)
c
        p1(1)=0
        p1(2)=a
        p1(3)=b
        p1(4)=dble(zk)
        p1(5)=imag(zk)
c
        p2(1)=m+.1
        if (m .lt. 0) p2(1)=m-.1
        p2(2)=p
        p2(3)=z
        p2(4)=p0
        p2(5)=z0
c
        a4=0
        b4=pi/2
        k4=16
c

        call cadapgau(ier,a4,b4,funuser2,p1,p2,k4,eps,
     1      rint,maxrec,numint)
c
        if (ier .ne. 0) then
          call prinf('in cadapgau, ier=*',ier,1)
          stop
        endif
c
cccc        call prinf('in ghfun2, after cadapgau, ier=*',ier,1)
cccc        call prinf('in ghfun2, after cadapgau, maxrec=*',maxrec,1)
cccc        call prinf('in ghfun2, after cadapgau, numint=*',numint,1)
c
        rk=sqrt(b)

        d=((p-p0)**2+(z-z0)**2)/a
        rkp=sqrt(d)

        call ke_eva(rk,rkp,fk,fe)
cccc        call elliptic_ke(rk,fk,fe)

cc        call prin2('zk=*',zk,2)
cc        call prin2('rint=*',rint,2)

        val=rint
        val=val+fk*4/sqrt(a)
        val=val/4/pi
        val=val*p0
c
        return
        end
c
c
c
        function funuser2(t,par1,par2)
        implicit real *8 (a-h,o-z)
        complex *16 funuser2,ima,zz,zk,zikr,z3
        real *8 par1(1),par2(1)
c
        ima=(0,1)
        a=par1(2)
        b=par1(3)
        zk=par1(4)+ima*par1(5)
c
        m=par2(1)
c
        p=par2(2)
        z=par2(3)
        p0=par2(4)
        z0=par2(5)

c
c       evaluate this integrand correctly by taylor series if kr is small
c
        d=(p-p0)**2+(z-z0)**2+4*p*p0*sin(t)**2

        zikr=ima*zk*sqrt(d)
        sc=abs(zikr)
c
        thresh=1.0d-3
        if ((sc .lt. thresh) .and. (m .eq. 0)) then
c
          z3=1+zikr/2+zikr**2/6+zikr**3/24+zikr**4/120
          z3=z3+zikr**5/720+zikr**6/5040
          z3=z3+zikr**7/40320
          z3=z3*ima*zk
c
          zz=z3*4
          funuser2=zz
          return
c
        endif


        zz=(1-2*sin(m*t)**2)
        zz=zz*exp(ima*zk*sqrt(a)*sqrt(1-b*cos(t)**2))-1
        zz=zz/sqrt(1-b*cos(t)**2)*4/sqrt(a)
c
        funuser2=zz
c
        return
        end
c
c
c
c
c
        subroutine gh_hess(eps, zk, mode, r, z, r0, z0, ddr,
     1      ddz, ddrz)
        implicit real *8 (a-h,o-z)
        complex *16 ima, zk, ddr, ddz, ddrz
        complex *16 cdr, cdz, cdrz, cd, zikr
        complex *16 term1, term2, term3, zfacr, zfacz, ze
c        
c       compute the second derivatives of the kernel
c
        ima = (0,1)
        done = 1
        pi = 4*atan(done)
c
        n = 2*(mode + zk) + 200
        h = 2*pi/n

        cdr = 0
        cdz = 0
        cdrz = 0
        
        do i = 1,n

          t = (i-1)*h
          d = sqrt(r**2 + r0**2 + (z-z0)**2 - 2*r*r0*cos(t))
          cd = exp(ima*zk*d)
          zikr = ima*zk*d
          ze = exp(-ima*mode*t)
          
          zfacr = r-r0*cos(t)
          term1 = (d*ima*zk - (ima*zk*d-1)*3d0)*zfacr**2/d**5
          term2 = (ima*zk*d - 1)/d**3
          term3 = ima*zk*(ima*zk*d - 1)*zfacr**2/d**4

          cdr = cdr + h*(term1 + term2 + term3)*cd*ze

          zfacz = z-z0
          term1 = (d*ima*zk - (ima*zk*d-1)*3d0)*zfacz**2/d**5
          term2 = (ima*zk*d - 1)/d**3
          term3 = ima*zk*(ima*zk*d - 1)*zfacz**2/d**4

          cdz = cdz + h*(term1 + term2 + term3)*cd*ze

          term1 = (d*ima*zk - (ima*zk*d-1)*3d0)/d**5
          term3 = ima*zk*(ima*zk*d - 1)/d**4

          cdrz = cdrz + h*(term1 + term3)*cd*zfacr*zfacz*ze
          
        end do
        
        ddr = cdr/4/2/pi
        ddz = cdz/4/2/pi
        ddrz = cdrz/4/2/pi
        
c
        return
        end
c
c
c
c
c
        subroutine ghcc_trap(eps, zk, mode, r, z, r0, z0, val)
        implicit real *8 (a-h,o-z)
        complex *16 ima, zk, ddr, ddz, ddrz, val
        complex *16 cdr, cdz, cdrz, cd, zikr
        complex *16 term1, term2, term3, zfacr, zfacz, ze
c        
c       compute the second derivatives of the cosine kernel
c
        ima = (0,1)
        done = 1
        pi = 4*atan(done)
c
        n = 2*(mode + zk) + 200
        h = 2*pi/n

        cdr = 0
        cdz = 0
        cdrz = 0

        val = 0
        
        do i = 1,n

          t = (i-1)*h
          d = sqrt(r**2 + r0**2 + (z-z0)**2 - 2*r*r0*cos(t))
          cd = exp(ima*zk*d)
          zikr = ima*zk*d

          val = val + h*cd/d*cos(t)*cos(mode*t)/4/pi*r0

          
        end do
        
        
c
        return
        end
c
c
c
c
c
        subroutine ghcc_hess(eps, zk, mode, r, z, r0, z0, ddr,
     1      ddz, ddrz)
        implicit real *8 (a-h,o-z)
        complex *16 ima, zk, ddr, ddz, ddrz
        complex *16 cdr, cdz, cdrz, cd, zikr
        complex *16 term1, term2, term3, zfacr, zfacz, ze
c        
c       compute the second derivatives of the cosine kernel
c
c       NOTE: the output is scaled by r0 so as to agree with the other
c       cosine kernels 
c
c       
        ima = (0,1)
        done = 1
        pi = 4*atan(done)
c
        n = 2*(mode + zk) + 200
        h = 2*pi/n

        cdr = 0
        cdz = 0
        cdrz = 0
        
        do i = 1,n

          t = (i-1)*h
          d = sqrt(r**2 + r0**2 + (z-z0)**2 - 2*r*r0*cos(t))
          cd = exp(ima*zk*d)
          zikr = ima*zk*d
          ze = exp(-ima*mode*t)
          
          zfacr = r-r0*cos(t)
          term1 = (d*ima*zk - (ima*zk*d-1)*3d0)*zfacr**2/d**5
          term2 = (ima*zk*d - 1)/d**3
          term3 = ima*zk*(ima*zk*d - 1)*zfacr**2/d**4

          cdr = cdr + h*(term1 + term2 + term3)*cd*cos(t)*ze

          zfacz = z-z0
          term1 = (d*ima*zk - (ima*zk*d-1)*3d0)*zfacz**2/d**5
          term2 = (ima*zk*d - 1)/d**3
          term3 = ima*zk*(ima*zk*d - 1)*zfacz**2/d**4

          cdz = cdz + h*(term1 + term2 + term3)*cd*cos(t)*ze

          term1 = (d*ima*zk - (ima*zk*d-1)*3d0)/d**5
          term3 = ima*zk*(ima*zk*d - 1)/d**4

          cdrz = cdrz + h*(term1 + term3)*cd*zfacr*zfacz*cos(t)*ze
          
        end do
        
        ddr = cdr*r0/4/pi
        ddz = cdz*r0/4/pi
        ddrz = cdrz*r0/4/pi
        
c
        return
        end
c
c
c
c
c
        subroutine ghss_hess(eps, zk, mode, r, z, r0, z0, ddr,
     1      ddz, ddrz)
        implicit real *8 (a-h,o-z)
        complex *16 ima, zk, ddr, ddz, ddrz
        complex *16 cdr, cdz, cdrz, cd, zikr
        complex *16 term1, term2, term3, zfacr, zfacz, ze
c        
c       compute the second derivatives of the cosine kernel
c
c       NOTE: the output is scaled by r0 so as to agree with the other
c       sine kernels 
c
c
        ima = (0,1)
        done = 1
        pi = 4*atan(done)
c
        n = 2*(mode + zk) + 200
        h = 2*pi/n

        cdr = 0
        cdz = 0
        cdrz = 0
        
        do i = 1,n

          t = (i-1)*h
          d = sqrt(r**2 + r0**2 + (z-z0)**2 - 2*r*r0*cos(t))
          cd = exp(ima*zk*d)
          zikr = ima*zk*d
          ze = ima*exp(-ima*mode*t)
          
          zfacr = r-r0*cos(t)
          term1 = (d*ima*zk - (ima*zk*d-1)*3d0)*zfacr**2/d**5
          term2 = (ima*zk*d - 1)/d**3
          term3 = ima*zk*(ima*zk*d - 1)*zfacr**2/d**4

          cdr = cdr + h*(term1 + term2 + term3)*cd*sin(t)*ze

          zfacz = z-z0
          term1 = (d*ima*zk - (ima*zk*d-1)*3d0)*zfacz**2/d**5
          term2 = (ima*zk*d - 1)/d**3
          term3 = ima*zk*(ima*zk*d - 1)*zfacz**2/d**4

          cdz = cdz + h*(term1 + term2 + term3)*cd*sin(t)*ze

          term1 = (d*ima*zk - (ima*zk*d-1)*3d0)/d**5
          term3 = ima*zk*(ima*zk*d - 1)/d**4

          cdrz = cdrz + h*(term1 + term3)*cd*zfacr*zfacz*sin(t)*ze
          
        end do
        
        ddr = r0*cdr/4/pi
        ddz = r0*cdz/4/pi
        ddrz = r0*cdrz/4/pi
        
c
        return
        end
c
c
c
c
c
        subroutine gh_grad(eps, zk, m, p, z, p0, z0, dp, dz)
        implicit real *8 (a-h,o-z)
        complex *16 ima, zk, dp, dz
c
c       calculate the gradient of the greens function using gnfun4n1
c
        vnp = 1
        vnz = 0
        call ghfun4n1(eps, zk, m, p, z, p0, z0, vnp, vnz, dp)
        
        vnp = 0
        vnz = 1
        call ghfun4n1(eps, zk, m, p, z, p0, z0, vnp, vnz, dz)
        
        return
        end
c
c
c
c
c
        subroutine ghfun4n1(eps,zk,m,p,z,p0,z0,vnp,vnz,val)
        implicit real *8 (a-h,o-z)
        complex *16 ima,val,zk
c
c       calculate the TARGET directional derivative of the radial helmholtz 
c       greens function by integrating a modified formula on [0,pi/2]
c       using an adaptive gaussian quadrature.  the routine is even
c       fancier in that it does the integral by subtracting off 
c       the laplace kernel, and then adding it back in afterwards.
c       this code has been tested and seems to be faster than calculating
c       the individual partials and constructing the derivative that way.
c       this routines is very efficient when vn points into
c       quadrants 2 or 3 since some cancellation occurs in the integrand.
c
c       input:
c           eps - the precision to pass to the adaptive gaussian
c                 quadrature routine
c           zk - the complex helmholtz parameter
c           m - the fourier mode to evaluate
c           p,z - cylindrical coordinates for target point
c           p0,z0 - cylindrical coordinates for source point
c           vnp,vnz - the direction in which to calculate the
c                 target directional derivative
c
c       output:
c           val - the target directional derivative of the kernel in the
c                 direction vnp,vnz
c
c
        ima=(0,1)
        pi=4*atan(1.0d0)
c
c       call the source gradient function with the parameters
c       reversed and rescale afterwards by p and p0
c
        call ghfun4n0(eps,zk,m,p0,z0,p,z,vnp,vnz,val)
        val=val/p*p0
c
        return
        end
c
c
c
c
c
        subroutine ghfun4n0(eps,zk,m,p,z,p0,z0,vnp0,vnz0,val)
        implicit real *8 (a-h,o-z)
        complex *16 ima,val,rint,funuser4n,zk
        real *8 p1(10),p2(10)
        external funuser4n
c
c       calculate the source directional derivative of the radial helmholtz 
c       greens function by integrating a modified formula on [0,pi/2]
c       using an adaptive gaussian quadrature.  the routine is even
c       fancier in that it does the integral by subtracting off 
c       the laplace kernel, and then adding it back in afterwards.
c       this code has been tested and seems to be faster than calculating
c       the individual partials and constructing the derivative that way.
c       this routines is very efficient when vn points into
c       quadrants 2 or 3 since some cancellation occurs in the integrand.
c
c       input:
c           eps - the precision to pass to the adaptive gaussian
c                 quadrature routine
c           zk - the complex helmholtz parameter
c           m - the fourier mode to evaluate
c           p,z - cylindrical coordinates for target point
c           p0,z0 - cylindrical coordinates for source point
c           vnp0,vnz0 - the direction in which to calculate the
c                 source directional derivative
c
c       output:
c           val - the source directional derivative of the kernel in the
c                 direction vnp0,vnz0
c
c
        ima=(0,1)
        pi=4*atan(1.0d0)
c
        a=(p-p0)**2+(z-z0)**2+4*p*p0
        b=4*p*p0/a
cccc        call prin2('in ghfun0, b=*',b,1)
c
        p1(1)=0
        p1(2)=a
        p1(3)=b
        p1(4)=dble(zk)
        p1(5)=imag(zk)
c
        p2(1)=m+.1
        if (m .lt. 0) p2(1)=m-.1
        p2(2)=p
        p2(3)=z
        p2(4)=p0
        p2(5)=z0
        p2(6)=vnp0
        p2(7)=vnz0
c
        a4=0
        b4=pi/2
        k4=16
c
        call cadapgau(ier,a4,b4,funuser4n,p1,p2,k4,eps,
     1      rint,maxrec,numint)
c
        if (ier .ne. 0) then
          call prinf('in cadapgau, ier=*',ier,1)
          stop
        endif
c
cccc        call prinf('in ghfun4n0, after cadapgau, ier=*',ier,1)
c        if (numint .gt. 1) then
c        call prinf('in ghfun4n0, after cadapgau, maxrec=*',maxrec,1)
c        call prinf('in ghfun4n0, after cadapgau, numint=*',numint,1)
c        endif
c
        call laplacepder(p,z,p0,z0,dp0,dz0)
        val=rint+vnp0*dp0+vnz0*dz0
        val=val/4/pi
        val=val*p0
c
        return
        end
c
c
c
        function funuser4n(t,par1,par2)
        implicit real *8 (a-h,o-z)
        complex *16 funuser4n,val,ima,zz,zz2,z3,zk,zikr
        real *8 par1(1),par2(1)
c        
        ima=(0,1)
        done=1.0d0

        a=par1(2)
        b=par1(3)
        zk=par1(4)+ima*par1(5)
c
        m=par2(1)
c
        p=par2(2)
        z=par2(3)
        p0=par2(4)
        z0=par2(5)
c
        vnp0=par2(6)
        vnz0=par2(7)
c
cccc        d=(p-p0)**2+(z-z0)**2+4*p*p0-4*p*p0*cos(t)**2
        d=(p-p0)**2+(z-z0)**2+4*p*p0*sin(t)**2

        dd=(1-2*sin(m*t)**2)
        zz2=exp(ima*zk*sqrt(d))

c
c       the p0 part of the integrand
c
ccc        zz=(p+p0-2*p*cos(t)**2)

        zz=1
        zz=zz*4*(ima*zk*zz2*dd*sqrt(d)-(zz2*dd-1))/d**1.5d0
cccc        z3=vnp0*zz




c
c       do integrad by power series if ikr is too small
c

        zikr=ima*zk*sqrt(d)
        sc=abs(zikr)
c
        thresh=1.0d-3
        if ((sc .lt. thresh) .and. (m .eq. 0)) then
c
        z3=1/zikr/2
        z3=z3+done/3
        z3=z3+zikr/8
        z3=z3+zikr**2/30
        z3=z3+zikr**3/144
        z3=z3+zikr**4/840
        z3=z3+zikr**5/5760
        z3=z3+zikr**6/45360d0
        z3=z3+zikr**7/403200d0
        z3=z3+zikr**8/3991680d0
        z3=z3+zikr**9/43545600d0
        z3=z3+zikr**10/518918400d0

cccc        z3=z3*vnp0*4*(p+p0-2*p*cos(t)**2)*(ima*zk)**3
c
       

        zz=z3*4*(ima*zk)**3
c
        endif





c
c       the z0 part of the integrand
c
cccc        zz=(z0-z)
cccc        zz=zz*4*(ima*zk*zz2*dd*sqrt(d)-(zz2*dd-1))/d**1.5d0
c

cccc        funuser4n=z3+vnz0*zz



        funuser4n=vnp0*(p+p0-2*p*cos(t)**2)*zz+vnz0*(z0-z)*zz
c
        return
        end
c
c
c
c
c
        subroutine laplacepder(pp,zz,p,z,dgdp,dgdz)
        implicit real *8 (a-h,o-z)
c
c       this routine calculates the partial source derivatives
c       of the axisymmetric laplace kernel using full
c       double precision elliptic integral routines
c
c       input:
c            pp,zz - the cylindrical coordinates of the target
c            p,z - the cylindrical coordinates of the source
c
c       output:
c            dgdp - the partial derivative of the kernel with
c                respect to the source p
c            dgdz - the partial derivative of the kernel with
c                respect to the source z
c
c
        done=1
        pi=4*datan(done)
c
        a=(pp-p)**2+(zz-z)**2+4*pp*p
        rm=4*pp*p/a
        rk=sqrt(rm)
c
        rkp2=((pp-p)**2+(zz-z)**2)/a
        rkp=sqrt(rkp2)

cccc        call elliptic_ke(rk,fk,fe)
        call ke_eva(rk,rkp,fk,fe)


c
c       if rm is too small, use the limiting value of derivative
c       of K
c
        thresh=1.0d-8
        if (rm .lt. thresh) then
          rrr=pi/8
          goto 2000
        endif

c


c
c       if rm is too close to 1, use series expansion to calculate
c       the value fE
c

        
        if (1 .eq. 0) then
c
        thresh=1.0d-2
        if (rkp .lt. thresh) then

cc          call prin2('in here, rkp2=*',rkp2,1)
cc          stop

          fkold=fk

          done=1

          rlog=log(4/rkp)

          fk=rlog+done/2/2*(rlog-1)*rkp**2
     1      +(done*3/8)**2*(rlog-1-done/6)*rkp**4
     2      +(done*15/48)**2*(rlog-1-done/6-done/15)*rkp**6
     3      +(done*105/384)**2*(rlog-1-done/6-done/15-done/28)*rkp**8

cc          call prin2('fk=*',fk,1)
cc          call prin2('fe=*',fe,1)
cc          d=(fk-fkold)/fk

cc          call prin2('relative error between k vals=*',d,1)
cc          stop
        endif

        endif

c
c       if here, use normal calculation
c
cccc        rrr=(fe/2/rm/(1-rm)-fk/2/rm)
cccc        rrr=(fe-(1-rm)*fk)/2/rm/(1-rm)

        rmminus=rkp2
        rrr=(fe-rkp2*fk)/2/rm/rkp2


 2000 continue

c
c       calculate the p derivative
c
        dgdp=fk*(-4*(pp+p)/(a**(1.5d0)))
        rr=4/sqrt(a)
        rr=rr*rrr*(-8*pp*p*(pp+p)/(a**2)+4*pp/a)
        dgdp=dgdp+rr

c
c       ... and the z derivative
c
        dgdz=fk*4*(zz-z)/(a**(1.5d0))
        rr=4/sqrt(a)*rrr
        rr=rr*8*pp*p*(zz-z)/(a**2)
        dgdz=dgdz+rr
c
        return
        end
c
c
c
c
c
        subroutine ke_eva(rk,rkp,valf,vale)
        implicit real *8 (a-h,o-z)
        real *8 cpe(6)
        data cpe /0.5000000000000000d+00,
     2            0.1083333333333333d+01,
     3            0.1200000000000000d+01,
     4            0.1251190476190476D+01,
     5            0.1280158730158730D+01,
     6            0.1298845598845599D+01/
c
c       computes the complete elliptic integrals E and K
c       to full double precision for any value of rk in [0,1)
c
c       F=int_0^pi/2 {1/(1-rk^2 sin(t)^2)}
c
c       E=int_0^pi/2 {1-rk^2 sin(t)^2}
c
c       The code uses Chebyshev interpolation for values of rk
c       in the interval [0 , 1023/1024] and a power series expansion
c       for values of rk in the interval (1023/1024 , 1)
c
c       input:
c         rk - the elliptic integral parameter above
c         rkp - the user is instructed to supply this number CORRECTLY
c           if rk is anywhere near 1, rkp = sqrt(1-rk**2)
c
c
        done=1.0d0
        b=1023*done/1024
c

        call elliptic_ke(rk,valf,vale)
        if (rk .lt. b) return

c
c       if here, then use rkp which the user should have supplied
c       CORRECTLY as the value sqrt(1-rk**2)
c

        rlog=log(4/rkp)

        coef2=done/2/2*(rlog-1)
        coef4=(done*3/8)**2*(rlog-1-done/6)
        coef6=(done*15/48)**2*(rlog-1-done/6-done/15)
        coef8=(done*105/384)**2*(rlog-1-done/6-done/15-done/28)
        coef10=(done*945/3840)**2*(rlog-1-done/6-done/15-done/28
     1      -done/45)
        coef12=(done*10395/46080)**2*(rlog-1-done/6-done/15-done/28
     1      -done/45-done/66)
        coef14=(done*135135/645120)**2*(rlog-1-done/6-done/15
     1      -done/28-done/45-done/66-done/91)
c
        valf=rlog+coef2*rkp**2
     1      +coef4*rkp**4
     2      +coef6*rkp**6
     3      +coef8*rkp**8
     3      +coef10*rkp**10
     3      +coef12*rkp**12
     3      +coef14*rkp**14

c
c       now calculate E
c

        xp=rkp
        xplog=-log(xp/4)
c
        fe=1
        fe=fe+(xplog-cpe(1))/2*xp**2
        fe=fe+3*(xplog-cpe(2))/16*xp**4
c
        fe=fe+45*(xplog-cpe(3))/384*xp**6
        fe=fe+1575*(xplog-cpe(4))/18432*xp**8
c
        fe=fe+99225*(xplog-cpe(5))/1474560*xp**10
        fe=fe+9823275*(xplog-cpe(6))/176947200*xp**12
c
        vale=fe
c

cccc        valf=valf+1.12341234d-10
cccc        vale=vale+1.345234523d-10

c
        return
        end
