



subroutine creabeltrami_shell(eps, zk, m, n, rl, ps_in, zs_in, &
  dpdt_in, dzdt_in, dpdt2_in, dzdt2_in, ps_out, zs_out, &
  dpdt_out, dzdt_out, dpdt2_out, dzdt2_out,  &
  norder, ind, cmat)
  implicit real *8 (a-h, o-z)

  real *8 :: ps_in(1), zs_in(1), dpdt_in(1), dzdt_in(1)
  real *8 :: dpdt2_in(1), dzdt2_in(1), dsdt_in(10000)
  real *8 :: ps_out(1), zs_out(1), dpdt_out(1)
  real *8 :: dzdt_out(1)
  real *8 :: dpdt2_out(1), dzdt2_out(1), dsdt_out(10000)

  complex *16 :: ima, zk, cmat(2*n+2, 2*n+2), zk0, val
  complex *16 :: ch1_in(2,10000), ch2_in(2,10000), x, y
  complex *16 :: ch1_out(2,10000), ch2_out(2,10000)
  complex *16 :: cd, cd0, cd1, cd2, cda, cdb, cda2, cdb2, cdb0
  complex *16 :: jh_in(2,10000), mh_in(2,10000)
  complex *16 :: jh_out(2,10000), mh_out(2,10000)

  external ghfun2, ghfun4n1

  real *8 :: wsave(1000000)
  complex *16 ctemp(1000000), ctemp1(1000000),& 
      ctemp3(1000000), ctemp4(1000000),  &
      work(10000000), work2(10000000), ctemp2(1000000),&
      work3(10000000), &
      uz(10000000),vz(10000000),sz(1000000)
  complex *16, allocatable :: wsprime(:,:), wgradrep(:,:)
  complex *16, allocatable :: wa2(:,:)
  complex *16, allocatable :: wa12(:,:), wa21(:,:)
  complex *16, allocatable :: wa2_11(:,:), wa2_12(:,:)
  complex *16, allocatable :: wa2_21(:,:), wa2_22(:,:)
  complex *16, allocatable :: w(:,:),wcc(:,:),wss(:,:)
  complex *16, allocatable :: wp(:,:),wccp(:,:),wssp(:,:)
  complex *16, allocatable :: wz(:,:),wccz(:,:),wssz(:,:)
  complex *16, allocatable :: wnxcurl(:,:)
  complex *16, allocatable :: wnxcurl11(:,:), wnxcurl22(:,:)
  complex *16, allocatable :: wnxcurl12(:,:), wnxcurl21(:,:)
  complex *16, allocatable :: wndotcurl12(:,:),wndotcurl21(:,:)
  complex *16, allocatable :: wndot(:,:),wnxt(:,:)
  complex *16, allocatable :: a1(:,:),a2(:,:),a3(:,:)
  complex *16, allocatable :: e1(:,:),e2(:,:),e3(:,:)
  complex *16, allocatable :: etop(:,:),ebottom(:,:)
  complex *16, allocatable :: wbeltrami_ext(:,:)
  complex *16, allocatable :: wbeltrami_int(:,:)
  complex *16, allocatable :: cmat_in(:,:), cmat_ext(:,:)
  complex *16, allocatable :: wsprime11(:,:), wsprime12(:,:)
  complex *16, allocatable :: wsprime21(:,:), wsprime22(:,:)
  !c
  !c       this subroutine constructs the genus 2 beltrami matrix
  !c
  allocate(wsprime(n,n))
  allocate(wgradrep(2*n,n))
  allocate(wbeltrami_ext(2*n,n))
  allocate(wbeltrami_int(2*n,n))
  
  allocate(wa2(2*n,2*n))
  
  allocate(wa12(3*n,2*n))
  allocate(wa21(3*n,2*n))
  
  allocate(wa2_11(2*n,2*n))
  allocate(wa2_12(2*n,2*n))
  allocate(wa2_21(2*n,2*n))
  allocate(wa2_22(2*n,2*n))
  
  allocate(w(n,n))
  allocate(wcc(n,n))
  allocate(wss(n,n))
  
  allocate(wp(n,n))
  allocate(wccp(n,n))
  allocate(wssp(n,n))
  
  allocate(wz(n,n))
  allocate(wccz(n,n))
  allocate(wssz(n,n))
  
  allocate(wnxcurl(2*n,2*n))
  allocate(wnxcurl22(2*n,2*n))
  allocate(wnxcurl11(2*n,2*n))
  allocate(wnxcurl12(2*n,2*n))
  allocate(wnxcurl21(2*n,2*n))
  allocate(wndotcurl21(n,2*n))
  allocate(wndotcurl12(n,2*n))
  allocate(wndot(n,3*n))
  allocate(wnxt(2*n,2*n))
  
  allocate(a1(n,n), a2(n,n), a3(n,n))
  allocate(e1(2*n,n),e2(2*n,n),e3(2*n,n))
  allocate(etop(2*n,n),ebottom(2*n,n))
  
  allocate(wsprime11(n,n))
  allocate(wsprime12(n,n))
  allocate(wsprime21(n,n))
  allocate(wsprime22(n,n))
  
  ima=(0,1)
  done=1.0d0
  pi=4*atan(done)

  !
  ! begin building the matrix
  ! compute dsdt around the two curves
  !
  do i=1,n
    dd = sqrt(dpdt_in(i)**2 + dzdt_in(i)**2)
    dsdt_in(i) = dd
    dd = sqrt(dpdt_out(i)**2 + dzdt_out(i)**2)
    dsdt_out(i) = dd
  enddo
  

  ! if (1 .eq. 0) then

  !   call prin2('ps_in = *', ps_in, n)
  !   call prin2('zs_in = *', zs_in, n)
  !   call prin2('dpdt_in = *', dpdt_in, n)
  !   call prin2('dzdt_in = *', dzdt_in, n)
  !   call prin2('dpdt2_in = *', dpdt2_in, n)
  !   call prin2('dzdt2_in = *', dzdt2_in, n)
  !   call prin2('dsdt_in = *', dsdt_in, n)

  !   print *
  !   print *
  !   print *
    
  !   !c
  !   !c       print out the resolution of the geometry
  !   !c
  !   call dffti(n, wsave)
    
  !   do i = 1,n
  !     !ps_in(i) = ps_in(i)*dsdt_in(i)
  !     !zs_in(i) = zs_in(i)*dsdt_in(i)
  !     !dpdt_in(i) = dpdt_in(i)*dsdt_in(i)
  !     !dzdt_in(i) = dzdt_in(i)*dsdt_in(i)
  !     !dpdt2_in(i) = dpdt2_in(i)*dsdt_in(i)
  !     !dzdt2_in(i) = dzdt2_in(i)*dsdt_in(i)
  !   enddo

  !   call dfftf(n, ps_in, wsave)
  !   call prin2('fft of ps_in = *', ps_in, n)
    
  !   call dfftf(n, zs_in, wsave)
  !   call prin2('fft of zs_in = *', zs_in, n)
    
  !   call dfftf(n, dpdt_in, wsave)
  !   call prin2('fft of dpdt_in = *', dpdt_in, n)
    
  !   call dfftf(n, dzdt_in, wsave)
  !   call prin2('fft of dzdt_in = *', dzdt_in, n)

  !   call dfftf(n, dpdt2_in, wsave)
  !   call prin2('fft of dpdt2_in = *', dpdt2_in, n)

  !   call dfftf(n, dzdt2_in, wsave)
  !   call prin2('fft of dzdt2_in = *', dzdt2_in, n)
    
  !   call dfftf(n, dsdt_in, wsave)
  !   call prin2('fft of dsdt_in = *', dsdt_in, n)
    
  !   print *
  !   print *
  !   print *
    
  !   call dfftf(n, ps_out, wsave)
  !   call prin2('fft of ps_out = *', ps_out, n)
 
  !   call dfftf(n, zs_out, wsave)
  !   call prin2('fft of zs_out = *', zs_out, n)
 
  !   call dfftf(n, dpdt_out, wsave)
  !   call prin2('fft of dpdt_out = *', dpdt_out, n)
    
  !   call dfftf(n, dzdt_out, wsave)
  !   call prin2('fft of dzdt_out = *', dzdt_out, n)
    
  !   call dfftf(n, dpdt2_out, wsave)
  !   call prin2('fft of dpdt2_out = *', dpdt2_out, n)
    
  !   call dfftf(n, dzdt2_out, wsave)
  !   call prin2('fft of dzdt2_out = *', dzdt2_out, n)
    
  !   call dfftf(n, dsdt_out, wsave)
  !   call prin2('fft of dsdt_out = *', dsdt_out, n)
    
  ! endif

  
  do i = 1,2*n+2
    do j = 1,2*n+2
      cmat(j,i) = 0
    end do
  end do
  

  
  !
  ! . . . first the (1,1) block, interior to interior
  !
  allocate( cmat_in(n+1,n+1) )
  
  tau = 1
  call creabeltrami_exterior(eps, zk, m, n, rl, ps_in, zs_in, &
      dpdt_in, dzdt_in, dpdt2_in, dzdt2_in, norder, &
      ind, tau, cmat_in)
  
  do i = 1,n
    do j = 1,n
      cmat(i,j) = cmat_in(i,j)
    enddo
  enddo

  !c
  !c       . . . next the (2,1) block, interior sources to exterior
  !c
  call surfgradinvlapintmat(ps_in, zs_in, dpdt_in, &
      dzdt_in, dpdt2_in, dzdt2_in, n, m, rl, wgradrep)

  h=rl/n
  call creapieces_st(eps, zk, m, n, rl, ps_in, zs_in, &
      dpdt_in, dzdt_in, ps_out, zs_out, dpdt_out,  &
      dzdt_out, w, wp, wz, wcc, wccp, wccz, wss, wssp, &
      wssz)

  do i=1,n
    dpds = dpdt_out(i)/dsdt_out(i)
    dzds = dzdt_out(i)/dsdt_out(i)
    do j=1,n
      wsprime(i,j) = dzds*wp(i,j) - dpds*wz(i,j)
    enddo
  enddo

  iftan = 0
  call creavecpotmat7(eps, zk, m, n, rl, ps_in, zs_in, &
      dpdt_in, dzdt_in, norder, iftan, wa21, wcc, wss, w)
  
  !call creavecpotmat2x2(eps, zk, m, n, rl, ps_in, zs_in,  &
  !    dpdt_in, dzdt_in, norder, wa2_21, wcc, wss, w)

  do i = 1,n
    i1 = 2*(i-1)+1
    i2 = 2*i

    iii1 = 3*(i-1)+1
    iii2 = iii1 + 1
    iii3 = iii2 + 1

    dpds = dpdt_out(i)/dsdt_out(i)
    dzds = dzdt_out(i)/dsdt_out(i)
    do j = 1,2*n
      wa2_21(i1,j) = dpds*wa21(iii1,j) + dzds*wa21(iii3,j)
      wa2_21(i2,j) = wa21(iii2,j)
    end do
  end do


  
  diag = 0
  call creanxcurlmat2_st(eps, zk, m, n, rl, ps_in, zs_in, &
      dpdt_in, dzdt_in, ps_out, zs_out, dpdt_out, dzdt_out, &
      norder, diag, wnxcurl21, w, wcc, wss, wp, wccp, wssp,  &
      wccz, wssz)
  
  call creandotcurlmat_st(eps, zk, m, n, rl, ps_in, zs_in, &
      dpdt_in, dzdt_in, ps_out, zs_out, dpdt_out, dzdt_out, &
      wndotcurl21, w, wcc, wss, wccp, wssp, wccz, wssz)
  
  call creandotmat(n, dpdt_out, dzdt_out, wndot)
  call creanxtmat(n, wnxt)

  !
  ! and assemble
  !
  x = 1
  y = ima
  call cmatmul601(2*n, 2*n, wnxt, n, wgradrep, work)
  call cmatadd601(2*n, n, x, wgradrep, y, work, wbeltrami_int)
  call cmatmul601(n, 2*n, wndotcurl21, n, wbeltrami_int, a3)
  
  call cmatmul601(n, 3*n, wndot, 2*n, wa21, work)
  call cmatmul601(n, 2*n, work, n, wbeltrami_int, a1)

  do i = 1, n
    do j = 1, n
      cmat(n+i,j) = -zk*zk*a1(i,j) - wsprime(i,j) - zk*a3(i,j)
    enddo
  enddo

  !
  ! . . . next the (1,2) block, exterior sources to interior
  !  first construct the source-target matrices
  !
  call surfgradinvlapintmat(ps_out, zs_out, dpdt_out, &
      dzdt_out, dpdt2_out, dzdt2_out, n, m, rl, wgradrep)

  h=rl/n
  call creapieces_st(eps, zk, m, n, rl, ps_out, zs_out, &
      dpdt_out, dzdt_out, ps_in, zs_in, dpdt_in,  &
      dzdt_in, w, wp, wz, wcc, wccp, wccz, wss, wssp, &
      wssz)

  do i=1,n
    dpds = dpdt_in(i)/dsdt_in(i)
    dzds = dzdt_in(i)/dsdt_in(i)
    do j=1,n
      wsprime(i,j) = dzds*wp(i,j) - dpds*wz(i,j)
    enddo
  enddo

  iftan = 0
  call creavecpotmat7(eps, zk, m, n, rl, ps_out, zs_out, &
      dpdt_out, dzdt_out, norder, iftan, wa12, wcc, wss, w)

  do i = 1,n
    i1 = 2*(i-1)+1
    i2 = 2*i

    iii1 = 3*(i-1)+1
    iii2 = iii1 + 1
    iii3 = iii2 + 1

    dpds = dpdt_in(i)/dsdt_in(i)
    dzds = dzdt_in(i)/dsdt_in(i)
    do j = 1,2*n
      wa2_12(i1,j) = dpds*wa12(iii1,j) + dzds*wa12(iii3,j)
      wa2_12(i2,j) = wa12(iii2,j)
    end do
  end do

  !call creavecpotmat2x2(eps, zk, m, n, rl, ps_out, zs_out, &
  !    dpdt_out, dzdt_out, norder, wa2_12, wcc, wss, w)
  
  diag = 0
  call creanxcurlmat2_st(eps, zk, m, n, rl, ps_out, zs_out, &
      dpdt_out, dzdt_out, ps_in, zs_in, dpdt_in, dzdt_in, &
      norder, diag, wnxcurl12, w, wcc, wss, wp, wccp, wssp, & 
      wccz, wssz)

  call creandotcurlmat_st(eps, zk, m, n, rl, ps_out, zs_out, &
      dpdt_out, dzdt_out, ps_in, zs_in, dpdt_in, dzdt_in, &
      wndotcurl12, w, wcc, wss, wccp, wssp, wccz, wssz)
  
  call creandotmat(n, dpdt_in, dzdt_in, wndot)
  call creanxtmat(n, wnxt)

  !c
  !c       and assemble
  !c
  x = 1
  y = -ima
  call cmatmul601(2*n, 2*n, wnxt, n, wgradrep, work)
  call cmatadd601(2*n, n, x, wgradrep, y, work, wbeltrami_ext)

  call cmatmul601(n, 3*n, wndot, 2*n, wa12, work)
  call cmatmul601(n, 2*n, work, n, wbeltrami_ext, a1)
  
  call cmatmul601(n, 2*n, wndotcurl12, n, wbeltrami_ext, a3)
  
  do i = 1, n
    do j = 1, n
      cmat(i,n+j) = -zk*zk*a1(i,j) - wsprime(i,j) - zk*a3(i,j)
    enddo
  enddo

  !c
  !c       . . . next the (2,2) block, exterior to exterior, but the
  !c       interior of the domain
  !c
  allocate( cmat_ext(n+1, n+1) )

  call creabeltrami_interior(eps, zk, m, n, rl, ps_out, zs_out, &
      dpdt_out, dzdt_out, dpdt2_out, dzdt2_out, norder, &
      ind, tau, cmat_ext)

  do i = 1,n
    do j = 1,n
      cmat(n+i,n+j) = cmat_ext(i,j)
    enddo
  enddo



!  do i = 1,n
!    do j = 1,n
!      cmat(n+i,j) = 0
!      cmat(n+i,n+j) = 0
!      cmat(i,n+j) = 0
!    end do
!    cmat(n+i,n+i) = 1  
!  end do
  

  
  
  do i = 1,2*n+2
    cmat(2*n+1,i) = 0
    cmat(2*n+2,i) = 0
    cmat(i,2*n+1) = 0
    cmat(i,2*n+2) = 0
  enddo
  
  !
  ! return if non-azimuthal mode
  !
  if (m .ne. 0) then
    cmat(2*n+1,2*n+1) = 1
    cmat(2*n+2,2*n+2) = 1
    return
  endif

  !
  ! m=0, now for the augmented conditions
  !
  call get2harmvecs(n, ps_in, zs_in, ch1_in, ch2_in)
  call get2harmvecs(n, ps_out, zs_out, ch1_out, ch2_out)

  !
  !. . . make inner and outer harmonic vector fields
  !
  do i = 1, n
    jh_out(1,i) = ch1_out(1,i) - ima*ch2_out(1,i)
    jh_out(2,i) = ch1_out(2,i) - ima*ch2_out(2,i)
    mh_out(1,i) = -ima*jh_out(1,i)
    mh_out(2,i) = -ima*jh_out(2,i)
  enddo

  do i = 1, n
    mh_in(1,i) = ch1_in(1,i) + ima*ch2_in(1,i)
    mh_in(2,i) = ch1_in(2,i) + ima*ch2_in(2,i)
    jh_in(1,i) = ima*mh_in(1,i)
    jh_in(2,i) = ima*mh_in(2,i)
  enddo

  !
  ! add in the contribution of the harmonic vector fields to
  ! the normal components of the field
  !
  ! interior-to-interior
  !
  do i = 1,n
    cmat(i,2*n+1) = cmat_in(i,n+1)
  enddo

  !
  ! interior sources, exterior targets
  !
  call creandotmat(n, dpdt_out, dzdt_out, wndot)
  call cmatmul601(n, 3*n, wndot, 2*n, wa21, work2)
  call cmatvec601(n, 2*n, work2, mh_in, work)
  
  call cmatvec601(n, 2*n, wndotcurl21, mh_in, work2)

  do i = 1, n
    cmat(n+i,2*n+1) = ima*zk*work(i) + ima*work2(i)
  enddo

  !
  ! exterior sources, interior targets
  !
  call creandotmat(n, dpdt_in, dzdt_in, wndot)
  call cmatmul601(n, 3*n, wndot, 2*n, wa12, work2)
  call cmatvec601(n, 2*n, work2, mh_out, work)
  
  !!!call cmatvec601(n, 2*n, wndotcurl12, jh_out, work2)
  call cmatvec601(n, 2*n, wndotcurl12, mh_out, work2)

  do i = 1, n
    cmat(i,2*n+2) = ima*zk*work(i) + ima*work2(i)
  enddo
  
  !
  ! exterior-exterior
  !
  do i = 1,n
    cmat(n+i,2*n+2) = cmat_ext(i,n+1)
  enddo

  !call prin2('inside shell, 2n+1 col of cmat = *', &
  !    cmat(1,2*n+1), 2*n)
  !call prin2('inside shell, n+1 col of cmat_in = *', &
  !    cmat_in(1,n+1), 2*n)
  
  !
  ! initialize the system matrix for line integrals
  !
  !do i = 1,2*n+2
  !  cmat(2*n+1,i) = 0
  !  cmat(2*n+2,i) = 0
  !enddo

  !
  !  . . . and now the flux conditions, first the contribution
  !  of the charge densities
  !
  ! \int_(A_out) - \int_(A_in)
  ! \int_(B_out) - \int_(B_in)
  !
  !  interior charge, interior fluxes
  !
  call creapiecesa(eps, zk, m, n, rl, ps_in, zs_in, &
      dpdt_in, dzdt_in, norder, w, wp, wz, wcc, wccp, wccz, &
      wss, wssp, wssz)

  call creavecpotmat2x2(eps, zk, m, n, rl, ps_in, zs_in, &
      dpdt_in, dzdt_in, norder, wa2_11, wcc, wss, w)
  
  diag = .5d0
  call creanxcurlmat2(eps, zk, m, n, rl, ps_in, zs_in, &
      dpdt_in, dzdt_in, norder, diag, wnxcurl11, w, wcc, wss, &
      wp, wccp, wssp, wccz, wssz)
  
  call cmatmul601(2*n, 2*n, wa2_11, n, wbeltrami_int, e1)
  
  call cmatmul601(2*n, 2*n, wnxt, 2*n, wnxcurl11, work)
  call cmatmul601(2*n, 2*n, work, n, wbeltrami_int, e2)
  
  do i = 1, 2*n
    do j = 1, n
      etop(i,j) = -zk*zk*e1(i,j) + zk*e2(i,j)
    enddo
  enddo

  do i = 1, n
    ctemp1(i) = h*dsdt_in(i)
    i1 = 2*(i-1)+1
    do j = 1, n
      a1(i,j) = etop(i1,j)
    enddo
  enddo

  ione = 1
  call cmatmul601(ione, n, ctemp1, n, a1, work)

  iii = 2*ind
  scl = 2*pi*ps_in(ind)
  do j = 1, n
    !cmat(2*n+1,j) = -work(j)
    cmat(2*n+2,j) = -scl*etop(iii,j)
  enddo

  do j = 1,n
    cmat(2*n+1,j) = -cmat_in(n+1,j)
  end do
  
  
  !
  ! interior charge, exterior fluxes
  !
  call cmatmul601(2*n, 2*n, wa2_21, n, wbeltrami_int, e1)

  call cmatmul601(2*n, 2*n, wnxt, 2*n, wnxcurl21, work)
  call cmatmul601(2*n, 2*n, work, n, wbeltrami_int, e2)

  do i = 1, 2*n
    do j = 1, n
      etop(i,j) = -zk*zk*e1(i,j) + zk*e2(i,j)
    enddo
  enddo

  do i = 1, n
    ctemp1(i) = h*dsdt_out(i)
    i1 = 2*(i-1)+1
    do j = 1, n
      a1(i,j) = etop(i1,j)
    enddo
  enddo
  
  ione = 1
  call cmatmul601(ione, n, ctemp1, n, a1, work)

  iii = 2*ind
  scl = 2*pi*ps_out(ind)
  do j = 1, n
    cmat(2*n+1,j) = cmat(2*n+1,j) + work(j)
    cmat(2*n+2,j) = cmat(2*n+2,j) + scl*etop(iii,j)
  enddo

  !
  ! exterior charge, exterior fluxes
  !
  call creapiecesa(eps, zk, m, n, rl, ps_out, zs_out, &
      dpdt_out, dzdt_out, norder, w, wp, wz, wcc, wccp, wccz, &
      wss, wssp, wssz)
  
  call creavecpotmat2x2(eps, zk, m, n, rl, ps_out, zs_out, &
      dpdt_out, dzdt_out, norder, wa2_22, wcc, wss, w)
  
  diag = -.5d0
  call creanxcurlmat2(eps, zk, m, n, rl, ps_out, zs_out, &
      dpdt_out, dzdt_out, norder, diag, wnxcurl22, w, wcc, wss, &
      wp, wccp, wssp, wccz, wssz)
  
  call cmatmul601(2*n, 2*n, wa2_22, n, wbeltrami_ext, e1)

  call cmatmul601(2*n, 2*n, wnxt, 2*n, wnxcurl22, work)
  call cmatmul601(2*n, 2*n, work, n, wbeltrami_ext, e2)

  do i = 1, 2*n
    do j = 1, n
      etop(i,j) = -zk*zk*e1(i,j) + zk*e2(i,j)
    enddo
  enddo
  
  do i = 1, n
    ctemp1(i) = h*dsdt_out(i)
    i1 = 2*(i-1)+1
    do j = 1,n
      a1(i,j) = etop(i1,j)
    enddo
  enddo
  
  call cmatmul601(ione, n, ctemp1, n, a1, work)
  
  iii = 2*ind
  scl = 2*pi*ps_out(ind)
  do j = 1, n
    !!!cmat(2*n+1,n+j) = work(j)
    cmat(2*n+2,n+j) = scl*etop(iii,j)
  enddo

  do j = 1,n
    cmat(2*n+1,n+j) = cmat_ext(n+1,j)
  end do
  

  
  !
  ! exterior charge, interior fluxes
  !
  call cmatmul601(2*n, 2*n, wa2_12, n, wbeltrami_ext, e1)

  call cmatmul601(2*n, 2*n, wnxt, 2*n, wnxcurl12, work)
  call cmatmul601(2*n, 2*n, work, n, wbeltrami_ext, e2)
  
  do i = 1, 2*n
    do j = 1, n
      etop(i,j) = -zk*zk*e1(i,j) + zk*e2(i,j)
    enddo
  enddo
  
  do i = 1, n
    ctemp1(i) = h*dsdt_in(i)
    i1 = 2*(i-1)+1
    do j = 1, n
      a1(i,j) = etop(i1,j)
    enddo
  enddo
  
  call cmatmul601(ione, n, ctemp1, n, a1, work)

  iii = 2*ind
  scl = 2*pi*ps_in(ind)
  do j = 1, n
    cmat(2*n+1,n+j) = cmat(2*n+1,n+j) - work(j)
    cmat(2*n+2,n+j) = cmat(2*n+2,n+j) - scl*etop(iii,j)
  enddo


  
  !
  ! . . . and now the contribution of harmonic vector fields
  ! to the fluxes
  ! interior vecs, interior fluxes
  !
  call cmatvec601(2*n, 2*n, wa2_11, mh_in, work)

  call cmatmul601(2*n, 2*n, wnxt, 2*n, wnxcurl11, work3)
  !!!!call cmatvec601(2*n, 2*n, work3, jh_in, work2)
  call cmatvec601(2*n, 2*n, work3, mh_in, work2)

  cda = 0
  do i = 1,n
    i2 = 2*(i-1)+1
    !!!!cd = ima*zk*work(i2) - work2(i2)
    cd = ima*zk*work(i2) - ima*work2(i2)
    cda = cda + h*dsdt_in(i)*cd
  enddo

  iii = 2*ind 
  cdb = ima*zk*work(iii) - ima*work2(iii)

  !cmat(2*n+1,2*n+1) = -cda
  !!!!cmat(2*n+1,2*n+1) = 0
  cmat(2*n+1,2*n+1) = -cmat_in(n+1,n+1)
  cmat(2*n+2,2*n+1) = -2*pi*ps_in(ind)*cdb

  !
  ! interior vecs, exterior fluxes
  !
  
  
  call cmatvec601(2*n, 2*n, wa2_21, mh_in, work)

  call cmatmul601(2*n, 2*n, wnxt, 2*n, wnxcurl21, work3)
  !!!!call cmatvec601(2*n, 2*n, work3, jh_in, work2)
  call cmatvec601(2*n, 2*n, work3, mh_in, work2)
  
  cda = 0
  do i = 1,n
    i2 = 2*(i-1) + 1
    cd = ima*zk*work(i2) - ima*work2(i2)
    !!!!cd = ima*zk*work(i2)
    cda = cda + h*dsdt_out(i)*cd
    !!!!cda = cda + h*dsdt_out(i)*(ima*zk*work(i2))
  enddo

  iii = 2*ind 
  !!!!cdb = 2*pi*ps_out(ind)*(ima*zk*work(iii) - work2(iii))
  cdb = 2*pi*ps_out(ind)*(ima*zk*work(iii) - ima*work2(iii))

  cmat(2*n+1,2*n+1) = cmat(2*n+1,2*n+1) + cda
  !cmat(2*n+1,2*n+1) = cda

  cmat(2*n+2,2*n+1) = cmat(2*n+2,2*n+1) + cdb

  !
  ! exterior vecs, exterior fluxes
  !
  call cmatvec601(2*n, 2*n, wa2_22, mh_out, work)
  
  call cmatmul601(2*n, 2*n, wnxt, 2*n, wnxcurl22, work3)
  call cmatvec601(2*n, 2*n, work3, jh_out, work2)
  
  cda = 0
  do i = 1,n
    i2 = 2*i-1
    cda = cda + h*dsdt_out(i)*(ima*zk*work(i2) - work2(i2))
  enddo
  
  iii = 2*ind 
  cdb = ima*zk*work(iii) - work2(iii)

  !!!!cmat(2*n+1,2*n+2) = cda
  cmat(2*n+1,2*n+2) = cmat_ext(n+1,n+1)
  cmat(2*n+2,2*n+2) = 2*pi*ps_out(ind)*cdb

  !
  ! exterior vecs, interior fluxes
  !
  call cmatvec601(2*n, 2*n, wa2_12, mh_out, work)

  call cmatmul601(2*n, 2*n, wnxt, 2*n, wnxcurl12, work3)
  call cmatvec601(2*n, 2*n, work3, jh_out, work2)

  cda = 0
  do i = 1,n
    i2 = 2*i-1
    cd = ima*zk*work(i2) - work2(i2)
    cda = cda + h*dsdt_in(i)*cd
  enddo

  iii = 2*ind 
  cdb = 2*pi*ps_in(ind)*(ima*zk*work(iii) - work2(iii))

  cmat(2*n+1,2*n+2) = cmat(2*n+1,2*n+2) - cda
  cmat(2*n+2,2*n+2) = cmat(2*n+2,2*n+2) - cdb
  
  return
end subroutine creabeltrami_shell





subroutine beltrami_eval(zk, m, n, rl, ps, zs, dpdt, dzdt, &
    dpdt2, dzdt2, sigma, zms, p, theta, z, field)
  implicit real *8 (a-h,o-z)
  real *8 :: ps(n), zs(n), dpdt(n), dzdt(n), dpdt2(n), dzdt2(n)
  complex *16 :: ima, zk, sigma(n), zms(2,n), field(3)
  complex *16 ::  vals1(10), vals2(10), vals3(10)
  ! !c
  ! !c       evaluate the beltrami field generated by sigma and current zms
  ! !c       on the surface ps,zs at the point p,theta,z which is OFF
  ! !c       the surface
  ! !c
  ! !c       input:
  ! !c         eps - the precision to whicih kernels will be evaluated
  ! !c         zk - the complex helmholz parameter
  ! !c         m - the fourier mode to compute
  ! !c         n - number of points in the generator curve discritization
  ! !c         rl - length of parameterizing interval
  ! !c         ps,zs - points on the generating curve
  ! !c         dpdt,dzdt - derivative of ps,zs with respect to parameterizing
  ! !c             variable, no necessarily arclength
  ! !c         dpdt2,dzdt2 - 2nd derivative of ps,zs with respect to 
  ! c             parameterizing variable, no necessarily arclength
  ! c         sigma - beltrami charge
  ! c         zms - beltrami current
  ! c         p,theta,z - point in cylindrical coordinates at which to
  ! c             evaluate the e field
  ! c
  ! c       output:
  ! c         hfield - the beltrami field in cylindrical coordinates 
  ! c             at the point p,theta,z
  ! c

  ima=(0,1)
  done=1
  pi=4*atan(done)
  eps = 1.0d-12
  
  !c
  !c       ...calculate the vector potential Q
  !c
  call vecpoteval(eps, zk, m, n, rl, ps, zs, dpdt, dzdt, &
      zms, p, theta, z, vals1)
  !c
  !c       ...calculate the gradient of psi
  !c
  call ghfun2grad(eps,zk,m,n,rl,ps,zs,dpdt,dzdt, &
      sigma,p,theta,z,vals2)
  !c
  !c       ...calculate the curl of the vector potential Q
  !c
  call curlaeval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt, &
      zms,p,theta,z,vals3)

  !c
  !c       ...and combine them
  !c
  i1 = 1
  i2 = -1
  i3 = 1

  field(1) = ima*zk*vals1(1) - vals2(1) + ima*vals3(1)
  field(2) = ima*zk*vals1(2) - vals2(2) + ima*vals3(2)
  field(3) = ima*zk*vals1(3) - vals2(3) + ima*vals3(3)

  !field(1) = ima*zk*vals1(1)
  !field(2) = ima*zk*vals1(2)
  !field(3) = ima*zk*vals1(3)

  return
end subroutine beltrami_eval





subroutine beltrami_line_integrals(n, ps, zs, h, dsdt, btan, ind, &
    cda, cdb)
  implicit real *8 (a-h,o-z)
  real *8 :: ps(n), zs(n), dsdt(n)
  complex *16 :: btan(2,n), cda, cdb
  !
  ! compute the line integrals around an A cycle and a B cycle
  !
  done = 1
  pi = 4*atan(done)

  cda = 0
  do i = 1, n
    cda = cda + h*dsdt(i)*btan(1,i)
  enddo

  
  cdb = 2*pi*ps(ind)*btan(2,ind)

  return
end subroutine beltrami_line_integrals





subroutine beltrami_current_outgoing(zk, m, n, rl, ps, zs, dpdt, &
    dzdt, dpdt2, dzdt2, sigma, alpha, zms)
  implicit real *8 (a-h,o-z)
  real *8 :: ps(1), zs(1), dpdt(1), dzdt(1), dpdt2(1), dzdt2(1)
  complex *16 :: ima, zk, sigma(1), alpha, zms(2,1)
  complex *16 :: ch1(2,10000), ch2(2,10000), zvtemp(2,10000)
  complex *16 :: zvtemp2(2,10000), wmat(2000000), work(1000000)
  !
  !       this subroutine constructs a beltrami current on
  !       the surface ps,zs for evaluation in the EXTERIOR
  ! 
  !        input:
  !          zk - the complex helmholtz parameter
  !          m - fourier mode to compute
  !          n - number of points in curve discretization ps,zs
  !          rl - length of parameterization interval
  !          ps,zs - parameterization of curve
  !          dpdt,dzdt - first derivatives of parameterization
  !          dpdt2,dzdt2 - second derivatives of parameterization
  !          sigma - the beltrami charge
  !          alpha - coefficient of tangential harmonic vector field
  ! 
  !        output:
  !          zms - tangential beltrami current
  ! 
  ! 
  ima=(0,1)
  done=1
  pi=4*atan(done)
  
  !
  !     build the current
  !
  call surfgradinvlapintmat(ps,zs,dpdt,dzdt,dpdt2,dzdt2, &
      n,m,rl,wmat)

  call cmatvec601(2*n, n, wmat, sigma, zvtemp)
  call nxdirect(n, zvtemp, zvtemp2)

  !
  ! and assemble
  !
  do i = 1, n
    zms(1,i)=ima*zk*(zvtemp(1,i) + ima*zvtemp2(1,i))
    zms(2,i)=ima*zk*(zvtemp(2,i) + ima*zvtemp2(2,i))
  enddo

  if (m .ne. 0) return

  !
  ! construct the harmonic vector fields if mode=0
  !
  call get2harmvecs(n,ps,zs,ch1,ch2)
  do i = 1, n
    ch1(1,i) = ch1(1,i) + ima*ch2(1,i)
    ch1(2,i) = ch1(2,i) + ima*ch2(2,i)
  enddo

  do i = 1, n
    zms(1,i) = zms(1,i) + alpha*ch1(1,i)
    zms(2,i) = zms(2,i) + alpha*ch1(2,i)
  enddo

  return
end subroutine beltrami_current_outgoing





subroutine beltrami_current_incoming(zk, m, n, rl, ps, zs, dpdt, &
    dzdt, dpdt2, dzdt2, sigma, alpha, zms)
  implicit real *8 (a-h,o-z)
  real *8 :: ps(1), zs(1), dpdt(1), dzdt(1), dpdt2(1), dzdt2(1)
  complex *16 :: ima, zk, sigma(1), alpha, zms(2,1)
  complex *16 :: ch1(2,10000), ch2(2,10000), zvtemp(2,10000)
  complex *16 :: zvtemp2(2,10000), wmat(2000000), work(1000000)
  !
  !       this subroutine constructs a beltrami current on
  !       the surface ps,zs for evaluation in the INTERIOR
  ! 
  !        input:
  !          zk - the complex helmholtz parameter
  !          m - fourier mode to compute
  !          n - number of points in curve discretization ps,zs
  !          rl - length of parameterization interval
  !          ps,zs - parameterization of curve
  !          dpdt,dzdt - first derivatives of parameterization
  !          dpdt2,dzdt2 - second derivatives of parameterization
  !          sigma - the beltrami charge
  !          alpha - coefficient of tangential harmonic vector field
  ! 
  !        output:
  !          zms - tangential beltrami current
  ! 
  ! 
  ima=(0,1)
  done=1
  pi=4*atan(done)
  
  !
  !     build the current
  !
  call surfgradinvlapintmat(ps,zs,dpdt,dzdt,dpdt2,dzdt2, &
      n,m,rl,wmat)

  call cmatvec601(2*n, n, wmat, sigma, zvtemp)
  call nxdirect(n, zvtemp, zvtemp2)

  !
  ! and assemble
  !
  do i = 1, n
    zms(1,i)=ima*zk*(zvtemp(1,i) - ima*zvtemp2(1,i))
    zms(2,i)=ima*zk*(zvtemp(2,i) - ima*zvtemp2(2,i))
  enddo

  if (m .ne. 0) return

  !
  ! construct the harmonic vector fields if mode=0
  !
  call get2harmvecs(n,ps,zs,ch1,ch2)

  do i = 1, n
    ch1(1,i) = -ima*ch1(1,i) - ch2(1,i)
    ch1(2,i) = -ima*ch1(2,i) - ch2(2,i)
  enddo

  do i = 1, n
    zms(1,i) = zms(1,i) + alpha*ch1(1,i)
    zms(2,i) = zms(2,i) + alpha*ch1(2,i)
  enddo

  return
end subroutine beltrami_current_incoming
