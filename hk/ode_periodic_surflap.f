C***********************************************************************
      subroutine ode_periodic_sl(npts,mode,period,fin,r,dsdt,
     1           alpha,beta,rhs,u,ux,uxx)
C***********************************************************************
C
C     This subroutine solves the ODE
C
C     u_xx + alpha u_x - (mode*mode)*beta u = rhs
C
C     on [0,period] with periodic boundary conditions: 
C 
C     u(period) = u(0)
C     u_x(period) = u_x(0)
C
C     for complex-valued functions.
C
C     If (mode.eq.0) then the ODE is rank-one deficient and we
C     compute the solution thar preserves the mean of the right-hand side 
C     as a function on the surface of revolution:
C
C     int_0^L  u(t) * r(t) *dsdt(t) * dt = 
C     int_0^L  fin(t) * r(t) *dsdt(t) * dt = 
C
C     and we return a solution which is also of mean zero.
C
C     The method is a Fourier-based second kind integral equation.
C 
C     We expand u_xx as a Fourier series and solve the 
C     corresponding (dense) convolution equation (see Notes).
C
C     INPUT:
C
C     npts     = number of grid points
C     mode     = azimuthal mode number
C     period   = length of interval
C     fin      = original right-hand side 
C     r        = r function on surface of revolution,
c                needed to compute mean value integral
C     dsdt     = arclength function on surface of revolution,
c                needed to compute mean value integral
C     alpha    = coefficient of u_x
C     beta     = coefficient of u
C     rhs      = scaled right-hand side - right-hand side for ODE
C
C     Solution of the system returns alpha''(t) 
C
C     OUTPUT:
C
C     u         = solution
C     ux        = first derivative
C     uxx       = second derivative
C
C-----------------------------------------------------------------------
      implicit real *8 (a-h,o-z)
      integer npts,mode
      real *8 r(npts),dsdt(npts)
      complex *16 alpha(npts),beta(npts)
      complex *16 fin(npts)
      complex *16 rhs(npts)
      complex *16 u(npts),ux(npts),uxx(npts)
      complex *16 eye,c0
      integer, allocatable :: ipvt(:)
      complex *16, allocatable :: amat(:,:)
      complex *16, allocatable :: gamma(:)
      complex *16, allocatable :: gammashift(:)
      complex *16, allocatable :: betat(:)
      complex *16, allocatable :: betatshift(:)
      complex *16, allocatable :: ff(:)
      complex *16, allocatable :: finmean(:)
      complex *16, allocatable :: wfft(:)
      complex *16, allocatable :: work(:)
C
      allocate(wfft(4*npts+15))
      allocate(ipvt(npts))
      allocate(work(npts))
      allocate(amat(npts,npts))
      allocate(gamma(npts))
      allocate(betat(npts))
      allocate(gammashift(-npts:npts))
      allocate(betatshift(-npts:npts))
      allocate(finmean(npts))
      allocate(ff(npts))
C
      eye=dcmplx(0.0d0,1.0d0)
      pi=4*atan(1.0d0)
      call zffti(npts,wfft)
C
C     Fourier transform alpha, mode*mode*beta (or r*dsdt if mode.eq.0).
C     Fourier transform fin*r*dsdt to get mean of original source
C     density fin, if mode.eq.0 as well.
C
      do i = 1,npts
         gamma(i) = alpha(i)
         if (mode.ne.0) then
            betat(i) = -mode*mode*beta(i)
         else 
            betat(i) = r(i)*dsdt(i)
            finmean(i) = fin(i)*r(i)*dsdt(i)/npts
         endif 
         ff(i) = rhs(i)/npts
      enddo
C
      call zfftf(npts,gamma,wfft)
      call zfftf(npts,ff,wfft)
      call zfftf(npts,finmean,wfft)
      call zfftf(npts,betat,wfft)
C
      do i = -npts,npts
         gammashift(i) = 0.0d0
         betatshift(i) = 0.0d0
      enddo
c
      do i = 0,npts/2
         gammashift(i) = gamma(i+1)/npts
         betatshift(i) = betat(i+1)/npts
      enddo
      if ( mod(npts,2).eq.0) then
         do i = -1,-npts/2+1,-1
            gammashift(i) = gamma(npts+i+1)/npts
            betatshift(i) = betat(npts+i+1)/npts
         enddo
      else
         do i = -1,-npts/2,-1
            gammashift(i) = gamma(npts+i+1)/npts
            betatshift(i) = betat(npts+i+1)/npts
         enddo
      endif
C
      do i = 1,npts
         do j = 1,npts
            amat(i,j) = 0.0d0
         enddo
      enddo
C
      do i = 2,npts
         amat(i,i) = 1.0d0
         do j = 2,npts
            iuse = i-1
            juse = j-1
            if (iuse .gt. npts/2) iuse = iuse-npts
            if (juse .gt. npts/2) juse = juse-npts
            amat(i,j) = amat(i,j) + gammashift(iuse-juse)/(eye*juse)
            if (mode.ne.0) then
               amat(i,j) = amat(i,j)-betatshift(iuse-juse)/(juse**2)
            endif
         enddo
      enddo
      if (mode. eq. 0) then
         amat(1,1) = betatshift(0)
         ff(1) = finmean(1)
         do i = 2,npts
            amat(i,1) = 0.0d0
            iuse = i-1
            if (iuse .gt. npts/2) iuse = iuse-npts
            amat(1,i) = -betatshift(-iuse)/(iuse**2)
         enddo
      else
         amat(1,1) = betatshift(0)
         do i = 2,npts
            iuse = i-1
            if (iuse .gt. npts/2) iuse = iuse-npts
            amat(i,1) = betatshift(iuse)
            amat(1,i) = gammashift(-iuse)/(eye*iuse)
            amat(1,i) = amat(1,i)-betatshift(-iuse)/(iuse**2)
         enddo
      endif
ccc      call prin2(' amat is *',amat,2*npts*npts)
c
c      solve linear system
c
      call zgeco(amat,npts,npts,ipvt,rcond,gamma)
      job = 0
      call zgesl(amat,npts,npts,ipvt,ff,job)
      write(6,*)' rcond is ',rcond
ccc      call prin2(' after solve ff is *',ff,2*npts)
c
c     ff is now Fourier series for uxx
c     transform back to get uxx.
c
      c0 = ff(1)
      sc = 2*pi/period
      do i = 2,npts
         work(i) = ff(i)
      enddo
      work(1) = 0.0d0
      call zfftb(npts,work,wfft)
      do i = 1,npts
         uxx(i) = work(i)*sc
      enddo
c
c     now Fourier integrate to recover ux,
c     setting zero mode to 0.
c
      do i = 2,npts
         iuse = i-1
         if (iuse .gt. npts/2) iuse = iuse-npts
         work(i) = ff(i)/(eye*iuse)
      enddo
      work(1) = 0.0d0
      call zfftb(npts,work,wfft)
      do i = 1,npts
         ux(i) = work(i)*sc
      enddo
c
c     now Fourier integrate twice to recover u,
c     setting zero mode to zero for mode.eq.0 and
c     to c0 otherwise.
c
      do i = 2,npts
         iuse = i-1
         if (iuse .gt. npts/2) iuse = iuse-npts
         work(i) = -ff(i)/(iuse*iuse)
      enddo
      work(1) = c0
      call zfftb(npts,work,wfft)
      do i = 1,npts
         u(i) = work(i)*sc
      enddo
c
      return
      end
