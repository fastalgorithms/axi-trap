

X = load('xmesh.dat');
Y = load('ymesh.dat');
Z = load('zmesh.dat');
Cr = load('rcolors.dat');
Ci = load('icolors.dat');
Vr = load('real_vectors.dat');
Vi = load('imag_vectors.dat');

edgealpha = .05;
facealpha = 1;

%ax = subplot(1,2,1);
hr = surf(X,Y,Z,Cr);
shading interp;
hold on;
%hvr = quiver3(Vr(:,1), Vr(:,2), Vr(:,3), Vr(:,4), Vr(:,5), Vr(:,6),...
%              'AutoScaleFactor', 1);
set(hr, 'edgealpha', edgealpha);
set(hr, 'facealpha', facealpha);
set(hr, 'clipping', 'off');
%set(hvr, 'color', 'yellow')
%set(hvr, 'clipping', 'off');
axis equal;
title 'Magnitude of real part of J current'
colorbar
xlabel 'x-axis'
ylabel 'y-axis'
zlabel 'z-axis'

view(160,35);

print('jcurrent.jpg', '-djpeg', '-r300')



%figure

% %ax = subplot(1,2,2);
% hi = surf(X,Y,Z,Ci);
% shading interp;
% hold on;
% hvi = quiver3(Vi(:,1), Vi(:,2), Vi(:,3), Vi(:,4), Vi(:,5), Vi(:,6),...
%     'AutoScaleFactor', 1.5);
% set(hi, 'edgealpha', edgealpha);
% set(hi, 'facealpha', facealpha);
% set(hr, 'clipping', 'off');
% set(hvr, 'color', 'red')
% set(hvr, 'clipping', 'off');
% axis equal;
% title 'imaginary part';
