clear all

n = 200
phi = linspace(0,3*pi/2,n);
theta = linspace(0,2*pi,n);

e = .85
delt = .3
alpha = asin(delt)
rkap = 2.0

x0 = 2.0d0
z0 = 0

[t, p] = meshgrid(phi, theta);

x = (x0 + e*cos(p+alpha*sin(p))).*cos(t);
y = (x0 + e*cos(p+alpha*sin(p))).*sin(t);
z = z0 + e*rkap*sin(p);

c = cos(t).*cos(p);

shading interp
surf(x, y, z, c, 'EdgeColor','none')
axis equal
hold on

az = 45;
el = 30;
view(az, el);


n = 50
t = 0
rads = linspace(0,1,n)
theta = linspace(0,2*pi,n)
[r,p] = meshgrid(rads,theta)

torang = 0
x = (x0 + e*r.*cos(p+alpha*sin(p)))*cos(torang)
y = (x0 + e*r.*cos(p+alpha*sin(p)))*sin(torang)
z = z0 + e*rkap*r.*sin(p)

c = cos(p).*r

%x = (x0 + r.*cos(p))*cos(torang)
%y = (x0 + r.*cos(p))*sin(torang)
%z = z0 + r.*cos(p)


%x = (x0 + r.*e*cos(p+alpha*sin(p))).*cos(t);
%y = (x0 + r.*e*cos(p+alpha*sin(p))).*sin(t);
%z = z0 + r.*e*rkap*sin(p);
%shading interp
surf(x, y, z, c, 'EdgeColor','none')

%hold off