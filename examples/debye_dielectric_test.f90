program debye_dielectric_test
  implicit real *8 (a-h,o-z)
  real *8 :: ps(10000),zs(10000),dpdt(10000)
  real *8 :: dzdt(10000),dpdt2(10000),dzdt2(10000),dsdt(10000)
  real *8 :: center(10),xyz(10)
  real *8 :: targ(10)
  real *8 :: xys(2,10000), dxys(2,10000), d2xys(2,10000)

  real *8, allocatable :: rnodes(:,:,:), xyzs(:,:,:)
  real *8, allocatable :: testpts(:,:), whts(:,:), vals(:)
  real *8, allocatable :: triaskel(:,:,:), tempvecs(:,:,:)
  real *8, allocatable :: dein(:,:,:), dhin(:,:,:)

  complex *16 :: zk, ima, cinta, cinta2, cintb2
  complex *16 :: omega, e1, e0, u1, u0, zk1, zk0
  complex *16 :: aint, cintb, efield(3,10000), hfield(3,10000)
  complex *16 :: etest(3,10000), htest(3,10000), ediff(3,10000)

  complex *16, allocatable :: esurf(:,:,:),hsurf(:,:,:)
  complex *16, allocatable :: ein(:,:,:), hin(:,:,:)
  complex *16, allocatable :: rho1(:,:), sigma1(:,:)
  complex *16, allocatable :: jcurr1(:,:,:), kcurr1(:,:,:)
  complex *16, allocatable :: rho0(:,:), sigma0(:,:)
  complex *16, allocatable :: jcurr0(:,:,:), kcurr0(:,:,:)
  complex *16, allocatable :: zvals(:,:)

  done=1
  pi=4*atan(done)
  ima=(0,1)

  call prini(6,13)

  !
  ! set some subroutine parameters
  !
  n = 301
  n = 201
  n = 65
  n = 129
  n = 257
  n = 513
  call prinf('n = *', n, 1)
  
  nphi = 64
  nphi = 128
  nphi = 256
  nphi = 512
  call prinf('number of azimuthal points, nphi=*',nphi,1)

  norder=4
  norder=8
  ! norder=16
  call prinf('alpert order=*',norder,1)

  !
  ! generate the points on the scatterer
  !
  dmax = -1
  h = 2*pi/n
  do i=1,n
    t=h*(i-1)
    call funcurve(t, par1, par2, xys(1,i), dxys(1,i), d2xys(1,i))
    ps(i) = xys(1,i)
    zs(i) = xys(2,i)
    if (ps(i) .gt. dmax) dmax = ps(i)
    dpdt(i) = dxys(1,i)
    dzdt(i) = dxys(2,i)
    dsdt(i) = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
  end do



  !
  ! set permittivity, permeability, frequency, and mode
  !
  
  omega = 1

  !
  ! set inside parameters
  !
  !e0=.9d0
  !u0=1.1d0
  dlam0 = 4.0d0
  u0 = 1
  e0 = 1/u0*(2*pi/dlam0/omega)**2
  zk0=omega*sqrt(e0*u0)

  !
  ! set outside parameters
  !
  !e1=1.3d0
  !u1=.83d0
  dlam1 = 1.0d0
  u1 = 1
  e1 = 1/u1*(2*pi/dlam1/omega)**2
  zk1=omega*sqrt(e1*u1)

  print *
  call prin2('omega=*',omega,2)

  print *
  call prin2('e0=*',e0,2)
  call prin2('u0=*',u0,2)
  call prin2('zk0=*',zk0,2)
  call prin2('dlam0 = *', dlam0, 1)

  print *
  call prin2('e1=*',e1,2)
  call prin2('u1=*',u1,2)
  call prin2('zk1=*',zk1,2)
  call prin2('dlam1 = *', dlam1, 1)
  


  !
  ! evaluate an incoming field on the surface of the scatterer
  !

  !
  ! drive the thing with a current loop
  ! change the value of zk depending on whether inside or outside
  !
  ! interior loop
  ! al = .34d0
  ! center(1) = 3d0*cos(al)
  ! center(2) = 3d0*sin(al)
  ! center(3) = .1d0
  ! zk = zk1

  ! exterior loop
  al = .34d0 + pi
  center(1) = 3d0*cos(al)
  center(2) = 3d0*sin(al)
  center(3) = 2.55d0
  zk = zk0

  radloop = 0.2d0


  call prin2('center of current loop =  *', center, 3)
  !stop

  !
  ! now evaluate the e/h fields at each point on the scatterer
  !
  print *, '. . . evaluating the data on the scatterer'

  allocate(xyzs(3,nphi,n))
  allocate( dein(3,nphi,n) )
  allocate( dhin(3,nphi,n) )
  allocate( ein(3,nphi,n) )
  allocate( hin(3,nphi,n) )

  !$omp parallel do default(shared) &
  !$omp     private(phi, targ)
  do j=1,nphi
    phi = (j-1)*2*pi/nphi
    do i=1,n
      xyzs(1,j,i) = xys(1,i)*cos(phi)
      xyzs(2,j,i) = xys(1,i)*sin(phi)
      xyzs(3,j,i) = xys(2,i)
      !call axidebye_dielectric_loopfields(omega, u1, e1, &
      !    center, xyzs(1,j,i), radloop, par1, ein(1,j,i), &
      !    hin(1,j,i))
      call axidebye_dielectric_loopfields(omega, u0, e0, &
          center, xyzs(1,j,i), radloop, par1, ein(1,j,i), &
          hin(1,j,i))
      do k = 1,3
        ein(k,j,i) = -ein(k,j,i)
        hin(k,j,i) = -hin(k,j,i)
      end do

      
      dein(1,j,i) = ein(1,j,i)
      dhin(1,j,i) = hin(1,j,i)
    enddo
  enddo
  !$omp end parallel do

  

  
  iw = 15
  call axi_vtk_surface(iw, n, xys, nphi, 'The body of revolution')

  
  iw = 31
  call axi_vtk_vector(iw, n, xys, nphi, dein, 'incoming e field')

  iw = 32
  call axi_vtk_vector(iw, n, xys, nphi, dhin, 'incoming h field')

  
  !
  ! call the solver
  !
  allocate(whts(nphi,n))
  allocate(rho1(nphi,n), sigma1(nphi,n))
  allocate(rho0(nphi,n), sigma0(nphi,n))
  allocate(jcurr1(3,nphi,n), kcurr1(3,nphi,n))
  allocate(jcurr0(3,nphi,n), kcurr0(3,nphi,n))

  call cpu_time(tstart)
  !$ tstart = OMP_get_wtime()

  call axidebye_dielectric_solver(e1, u1, e0, u0, omega, &
      n, h, xys, dxys, d2xys, norder, nphi, xyzs, ein, hin, &
      whts, rho1, sigma1, jcurr1, kcurr1, rho0, sigma0, &
      jcurr0, kcurr0, modemax, esterr)

  call cpu_time(tend)
  !$ tend = OMP_get_wtime()

  tsolve = tend-tstart


  call prinf('after axidebye_dielectric solver, modemax = *', &
      modemax, 1)
  !call prin2('after axidebye_dielectric solver, esterr = *', &
  !    esterr, 1)

  iw = 41
  mmm = 2
  call axi_vtk_scalar(iw, n, xys, nphi, mmm, rho1, 'rho1')

  iw = 42
  call axi_vtk_scalar(iw, n, xys, nphi, mmm, sigma1, 'sigma1')

  iw = 43
  call axi_vtk_scalar(iw, n, xys, nphi, mmm, rho0, 'rho0')

  iw = 44
  call axi_vtk_scalar(iw, n, xys, nphi, mmm, sigma0, 'sigma0')

  !
  ! generate some points to test the field 
  !
  ntheta1=5
  nphi1=10

  x0 = 0
  y0 = 3
  z0 = 0
  rad = .15d0

  htheta=pi/(ntheta1+1)
  hphi=2*pi/nphi1

  ntest = 0
  allocate(testpts(3,nphi1*ntheta1))
  do i=1,ntheta1
    theta=htheta*i
    do j=1,nphi1
      phi=hphi*(j-1)
      x=x0+rad*sin(theta)*cos(phi)
      y=y0+rad*sin(theta)*sin(phi)
      z=z0+rad*cos(theta)
      ntest = ntest+1
      testpts(1,ntest)=x
      testpts(2,ntest)=y
      testpts(3,ntest)=z
    enddo
  enddo

  !call prin2('testpts = *', testpts, 3*ntest)
  !stop
  

  !ntest = 1
  !testpts(1,ntest) = 2
  !testpts(2,ntest) = 0
  !testpts(3,ntest) = 0

  !
  ! now evaluate the resulting field at the test points
  !
  nsrc = n*nphi
  !call axidebye_eval3d(zk1, nsrc, xyzs, whts, rho1, sigma1, &
  !    jcurr1, kcurr1, ntest, testpts, efield, hfield)
  call axidebye_eval3d(zk0, nsrc, xyzs, whts, rho0, sigma0, &
      jcurr0, kcurr0, ntest, testpts, efield, hfield)

  do i = 1,ntest
    do j = 1,3
      !efield(j,i) = sqrt(u1)*efield(j,i)
      !hfield(j,i) = sqrt(e1)*hfield(j,i)
      efield(j,i) = sqrt(u0)*efield(j,i)
      hfield(j,i) = sqrt(e0)*hfield(j,i)
    end do
  end do
  
  
  !
  ! . . . and the known fields on the test surface
  !
  do i = 1,ntest
    !call axidebye_dielectric_loopfields(omega, u1, e1, &
    !    center, testpts(1,i), radloop, par1, &
    !    etest(1,i), htest(1,i))
    call axidebye_dielectric_loopfields(omega, u0, e0, &
        center, testpts(1,i), radloop, par1, &
        etest(1,i), htest(1,i))
  end do



  !do i = 1,ntest
  !  do j = 1,3
  !    ediff(j,i) = etest(j,i)/efield(j,i)
  !  end do
  !end do

  !call prin2('ediff = *', ediff, 6*ntest)
  !stop


  !
  ! calculate the errors
  !
  print *
  call prin2('scattered efield = *', efield, 30)
  call prin2('known efield=*',etest, 30)

  print *
  call prin2('scattered hfield = *', hfield, 30)
  call prin2('known hfield = *', htest, 30)

  call crelfielderr(ntest,htest,hfield,herr)
  call cfielderr(ntest,htest,hfield,herr2)
  call crelfielderr(ntest, etest, efield, eerr)
  call cfielderr(ntest, etest, efield, eerr2)

  print *
  call prinf('n = *', n, 1)
  call prinf('nphi = *', nphi, 1)
  call prinf('ntot = *', n*nphi, 1)
  call prin2('time for solve = *', tsolve, 1)
  
  print *
  call prin2('on test surface, relative e error=*',eerr,1)
  call prin2('on test surface, relative h error=*',herr,1)

  print *
  call prin2('on test surface, rmse e error=*',eerr2,1)
  call prin2('on test surface, rmse h error=*',herr2,1)



end program debye_dielectric_test






subroutine crelfielderr(n,x,y,err)
  implicit real *8 (a-h,o-z)
  complex *16 x(3,1),y(3,1),cd1,cd2,cd3
  !c
  !c       calculates the relative error between the two fields
  !c       x and y, i.e. calculates |x - y|/|y| where |y|**2 is the
  !c       sum of squares of the field vector at each of the n points
  !c
  dd1=0
  dd2=0

  do i=1,n
    cd1=x(1,i)-y(1,i)
    cd2=x(2,i)-y(2,i)
    cd3=x(3,i)-y(3,i)
    dd1=dd1+abs(cd1)**2+abs(cd2)**2+abs(cd3)**2
    dd2=dd2+abs(y(1,i))**2+abs(y(2,i))**2+abs(y(3,i))**2
  end do

  err=sqrt(dd1/dd2)

  return
end subroutine crelfielderr





subroutine cfielderr(n,x,y,err)
  implicit real *8 (a-h,o-z)
  complex *16 x(3,1),y(3,1),cd1,cd2,cd3
  !c
  !c       calculates the rmse between the two fields
  !c       x and y, i.e. calculates |x - y|/sqrt(n) where |y|**2 is the
  !c       sum of squares of the field vector at each of the n points
  !c
  dd1=0
  do i=1,n
    cd1=x(1,i)-y(1,i)
    cd2=x(2,i)-y(2,i)
    cd3=x(3,i)-y(3,i)
    dd1=dd1+abs(cd1)**2+abs(cd2)**2+abs(cd3)**2
  end do

  err=sqrt(dd1/n)

  return
end subroutine cfielderr
