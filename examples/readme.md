There are a lot of files in this directory. The following is a rough
guide. See `makefile` for executing the following examples.


* **Laplace codes**
  * `lap_kernels`: Test evaluation of the Laplace modal kernels.


* **Helmholtz codes**
  * `helm_kernels`: Test evaluation of the Helmholtz modal kernels.
  * `helm_greens`: Test Green's identity using the Helmholtz modal kernels
  * `helm_g1_int_dir`: Solve an interior Dirichlet problem in an axisymmetrix geometry.


* **Maxwell codes (PEC)**
  * debye_pec_test1.mk : Test a single mode of axisymmetric Debye.
  * debye_pec_test.mk :  Tests several modes of axisymmetric Debye.


* **Maxwell codes (dieletric)**
  * 

* **Beltrami codes**
  * beltrami_g1_
  * beltrami_g2.mk            tests the toroidal shell Beltrami code
  * taylor_g2.mk : solves an actual toroidal shell Taylor-state problem, only
    fluxes are specified


