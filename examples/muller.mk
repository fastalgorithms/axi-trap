
rm int2
rm test16.o

gfortran -w -c test16.f
gfortran -w -o int2 test16.o \
            ../bin/prini.o \
            ../bin/quaplot.o \
            ../bin/legeexps.o \
            ../bin/cadapgau.o \
            ../bin/elliptic_ke.o \
            ../bin/chebexps.o \
            ../bin/cqrsolve.o \
            ../bin/csvdpiv.o \
            ../bin/corrand.o \
            ../helmrot/test65.f \
            ../aximfie/test51.f \
            ../alpertmat/test15.f \
            ../axidebye/test112.f \
            ../hellskitchen/Greengard/Trapquads2d/formsysmatkrc2.f \
            ../hellskitchen/Greengard/Trapquads2d/slpgen_hybrid.f \
            ../hellskitchen/Greengard/Trapquads2d/slpgenc_hybrid.f \
            ../hellskitchen/Greengard/Trapquads2d/slpgenc2.f \
            ../hellskitchen/Greengard/Trapquads2d/dlpgenc2.f \
            ../hellskitchen/Greengard/Trapquads2d/fdiffc.f \
            ../hellskitchen/Greengard/Trapquads2d/surfdivaxi.f \
            ../hellskitchen/Greengard/Trapquads2d/surflapaxi.f \
            ../hellskitchen/Greengard/Trapquads2d/surfgradaxi.f \
            ../hellskitchen/Greengard/Trapquads2d/surfcurlaxi.f \
            ../hellskitchen/Greengard/Trapquads2d/edebye.f \
            ../hellskitchen/Common/dfft.f \
            ../hellskitchen/Common/zgecoall.f

int2
