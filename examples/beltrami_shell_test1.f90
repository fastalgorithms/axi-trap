program beltrami_shell_test1

  implicit real *8 (a-h,o-z)
  real *8 ps(10000),zs(10000),dpdt(10000),&
      dzdt(10000),dpdt2(10000),dzdt2(10000),dsdt(10000),&
      psg(10000),zsg(10000),dpdtg(10000),dsdtg(10000),&
      dzdtg(40000),dpdtg2(10000),dzdtg2(10000)
  
  real *8  ps_in(10000), zs_in(10000)
  real *8  dpdt_in(10000), dzdt_in(10000)
  real *8  dpdt2_in(10000), dzdt2_in(10000)
  real *8  dsdt_in(10000)
  
  real *8  ps_out(10000), zs_out(10000)
  real *8  dpdt_out(10000), dzdt_out(10000)
  real *8  dpdt2_out(10000), dzdt2_out(10000)
  real *8  dsdt_out(10000)
  
  real *8  psg_out(10000), zsg_out(10000)
  real *8  dpdtg_out(10000), dzdtg_out(10000)
  real *8  dpdtg2_out(10000), dzdtg2_out(10000)
  real *8  dsdtg_out(10000), tsout(10000), tsin(10000)
  
  complex *16 hfield_in(3,10000), htan_in(2,10000)
  complex *16 hfield_out(3,10000), htan_out(2,10000)
  complex *16 flux_in, flux_out, flux_tor, flux_pol
  complex *16 rho_in(10000), rho_out(10000)
  complex *16 rhom_in(10000), rhom_out(10000)
  complex *16 zjs_in(2,10000), zjs_out(2,10000)
  complex *16 zms_in(2,10000), zms_out(2,10000)
  complex *16 alpha_in, alpha_out
  complex *16 bfield_in(10), bfield_out(10), bfield(10)
  complex *16 field(10000), field2(10000)
  
  complex *16 rhom2(10000), alpha2, beta2
  complex *16 rho2(10000), zwind1, zwind2
  complex *16 zjs2(2,10000), zms2(2,10000)
  complex *16 cda_in, cda_out, cdb_in, cdb_out
  complex *16 :: sigma(10000), sigma2(10000)
  complex *16 :: y1(10000), y2(10000)
  
  complex *16, allocatable :: cmat(:,:)
  complex *16, allocatable :: cmat_out(:,:), cmat_in(:,:)
  complex *16, allocatable :: amat(:,:), bmat(:,:)
  
  integer  ipvt(10000)
  complex *16 zk,zk0,cd,ima,cdh,cdb,zk2,cdt,cds,cd2,&
      zkinit,cda, flux, hdotn,&
      wz(1000000),wz2(1000000),sz(100000),&
      work(2000000), work2(2000000),&
      rho1(10000),rhom1(10000),alpha1,beta1,&
      rho(10000),rhom(10000),alpha,beta,&
      efield(3,10000),hfield(3,10000),&
      etest(3,10000),htest(3,10000),&
      etan(2,10000),htan(2,10000), zroot,&
      wndot(2000000),rhs(100000),sol(100000),&
      vals1(100),vals2(100),vals3(100),&
      hin(2,10000),zjs(2,10000),zms(2,10000),&
      zjs1(2,10000),zms1(2,10000),wsave(2000000),&
      ctemp1(10000),ctemp2(10000), u1, u2, u3, uu,&
      f1, f2, f3, ff, w, f12, f13, f23, f123,&
      vec1(10),vec2(10),db1dp, db2dp, db3dp,&
      evals1(3,10000),evals2(3,10000),evals3(3,10000),&
      hvals1(3,10000),hvals2(3,10000),hvals3(3,10000),&
      u(10000),ux(10000),uxx(10000), det(10),&
      bfield1(10), bfield2(10), db1dz, db2dz, db3dz,&
      r1(10000), r2(10000), denom1, denom2,&
      db1da, db2da, db3da, diver
  
  external fcurvein, fcurveout
  
  done = 1
  ima = (0,1)
  pi = 4*atan(1.0d0)
  
  call prini(6,13)
  print *, 'enter n:'
  read *, n
  call prinf('n=*',n,1)
  
  !c
  !c       create the interior and exterior scattering surfaces
  !c
  rl=2*pi
  h=rl/n
  
  do i=1,n
    
    t=h*(i-1)
    call fcurvein(t, ps_in(i), zs_in(i), dpdt_in(i), &
        dzdt_in(i), dpdt2_in(i),dzdt2_in(i))
    dd=dpdt_in(i)**2+dzdt_in(i)**2
    dsdt_in(i)=sqrt(dd)
    
    call fcurveout(t, ps_out(i), zs_out(i), dpdt_out(i), &
        dzdt_out(i), dpdt2_out(i),dzdt2_out(i))
    dd=dpdt_out(i)**2 + dzdt_out(i)**2
    dsdt_out(i)=sqrt(dd)
    
  end do

  
  
  iw=11
  itype = 1
  call pyplot2(iw, ps_in, zs_in, n, itype, &
      ps_out, zs_out, n, itype, 'scattering curve*')
  
  !c
  !c     build source surface, try placing inside as well as outside
  !c
  scin = 0
  do i=1,n
    t=h*(i-1)
    call fsource_in(t,psg(i),zsg(i),dpdtg(i),dzdtg(i), &
        dpdtg2(i),dzdtg2(i))
    dd=dpdtg(i)**2+dzdtg(i)**2
    dsdtg(i) = sqrt(dd)
    scin = scin + h*dsdtg(i)*2*pi*psg(i)
  end do
  
  scout = 0
  do i = 1,n
    t=h*(i-1)
    call fsource_out(t, psg_out(i), zsg_out(i), dpdtg_out(i), &
        dzdtg_out(i), dpdtg2_out(i), dzdtg2_out(i))
    dd=dpdtg_out(i)**2+dzdtg_out(i)**2
    dsdtg_out(i)=sqrt(dd)
    scout = scout + h*dsdtg_out(i)*2*pi*psg_out(i)
  enddo
  
  iw=12
  itype=1
  call pyplot3(iw, ps_in, zs_in, n, itype, &
      ps_out, zs_out, n, itype, psg, zsg, n, itype, &
      'scattering and inside source surface*')
  
  iw=13
  itype=1
  call pyplot3(iw, ps_in, zs_in, n, itype, &
      ps_out, zs_out, n, itype, psg_out, zsg_out, n, itype, &
      'scattering and outside source surfaces*')
  
  !c
  !c       set some subroutine parameters
  !c
  zk = 2.3d0 + ima*1.0d-15
  call prin2(' *',pi,0)
  call prin2(' *',pi,0)
  call prin2('zk=*',zk,2)
  
  mode = 0
  m = mode
  call prinf('mode=*',mode,1)
  
  eps=1.0d-12
  call prin2('kernel eval precision=*',eps,1)
  
  norder=4
  norder=8
  norder=16
  call prinf('alpert order=*',norder,1)
  
  !c
  !c       construct a rho, rhom, alpha, and beta on the source curve
  !c
  do i = 1, n
    t=2*pi*(i-1)/n
    rhom1(i)=(pi-ima)*sin(2*t)/psg(i)/dsdtg(i)/scin
    !rhom1(i) = 0
    rhom2(i)=(pi-ima)*sin(2*t)/psg_out(i)/dsdtg_out(i)/scout*100
    !rhom2(i) = 0
  enddo
  
  if (m .eq. 0) then
    alpha1 = (3.2d0 + ima*5.2d0)/scin
    !alpha1 = 0
    alpha2 = (3.2d0 + ima*5.2d0)/scout
    !alpha2 = 0
  else
    alpha1 = 0
    alpha2 = 0
  endif
  
  
  !c
  !c       calculate the incoming data, n \cdot H
  !c
  print *
  print *
  print *, '... calculating incoming data ...'
  
  
  
  
  call beltrami_current_outgoing(zk, m, n, rl, psg,&
      zsg, dpdtg, dzdtg, dpdtg2, dzdtg2,&
      rhom1,  alpha1, zms1)
  
  call prin2('rhom2 = *', rhom2, 30)
  
  call beltrami_current_outgoing(zk, m, n, rl, psg_out,&
      zsg_out, dpdtg_out, dzdtg_out, dpdtg2_out, dzdtg2_out,&
      rhom2,  alpha2, zms2)
  
  
  call prin2('source beltrami current = *', zms2, 30)
  
  
  do i = 1, n
    
    theta = 0
    call beltramieva(eps, zk, m, n, rl, psg, zsg, &
        dpdtg, dzdtg, dpdtg2, dzdtg2, rhom1, alpha1, zms1,&
        ps_in(i), theta, zs_in(i), field)
    
    call beltrami_eval(zk, m, n, rl, psg_out, zsg_out,&
        dpdtg_out, dzdtg_out, dpdtg2_out, dzdtg2_out, rhom2,&
        zms2, ps_in(i), theta, zs_in(i), field2)

    hfield_in(1,i) = field(1) + field2(1)
    hfield_in(2,i) = field(2) + field2(2)
    hfield_in(3,i) = field(3) + field2(3)
    
    dpds = dpdt_in(i)/dsdt_in(i)
    dzds = dzdt_in(i)/dsdt_in(i)
    htan_in(1,i) = dpds*hfield_in(1,i) + dzds*hfield_in(3,i)
    htan_in(2,i) = hfield_in(2,i)
    
    call beltramieva(eps, zk, m, n, rl, psg, zsg, &
        dpdtg, dzdtg, dpdtg2, dzdtg2, rhom1, alpha1, zms1,&
        ps_out(i), theta, zs_out(i), field)
    call beltrami_eval(zk, m, n, rl, psg_out, zsg_out,&
        dpdtg_out, dzdtg_out, dpdtg2_out, dzdtg2_out, rhom2,&
        zms2, ps_out(i), theta, zs_out(i), field2)
    
    hfield_out(1,i) = field(1) + field2(1)
    hfield_out(2,i) = field(2) + field2(2)
    hfield_out(3,i) = field(3) + field2(3)
    
    dpds = dpdt_out(i)/dsdt_out(i)
    dzds = dzdt_out(i)/dsdt_out(i)
    htan_out(1,i) = dpds*hfield_out(1,i) + dzds*hfield_out(3,i)
    htan_out(2,i) = hfield_out(2,i)
    
  enddo
  
  
  
  
  call ndotdirect(n, dpdt_in, dzdt_in, hfield_in, rhs(1))
  call ndotdirect(n, dpdt_out, dzdt_out, hfield_out, rhs(n+1))
  
  call prin2('hfield_in =*', hfield_in, 30)
  
  call prin2('rhs = *', rhs, 30)
  
  !c
  !c       construct the line integrals
  !c
  if (m .eq. 0) then
    cdb_out = 0
    cdb_in = 0
    cda_out = 0
    cda_in = 0

    do i = 1,n
      cda_in  = cda_in  + h*dsdt_in(i)*htan_in(1,i)
      cda_out = cda_out + h*dsdt_out(i)*htan_out(1,i)
    end do

    ind = 1
    cdb_in = 2*pi*ps_in(ind)*htan_in(2,ind)
    cdb_out = 2*pi*ps_out(ind)*htan_out(2,ind)

    cda = cda_out - cda_in
    cdb = cdb_out - cdb_in

    !call prin2('inside a cycle integral = *', cda_in, 2)

    rhs(2*n+1) = cda
    rhs(2*n+2) = cdb
  else

    rhs(2*n+1) = 0
    rhs(2*n+2) = 0
  endif

  !call prin2('rhs = *', rhs, 2*(2*n+2))
  
  lda = 2*n+2
  allocate( cmat(lda,lda) )
  call creabeltrami_shell(eps, zk, m, n, rl, ps_in, zs_in,&
      dpdt_in, dzdt_in, dpdt2_in, dzdt2_in, ps_out, zs_out,&
      dpdt_out, dzdt_out, dpdt2_out, dzdt2_out, &
      norder, ind, cmat)


  iftest = 0
  if (iftest .eq. 1) then

    nnn = n+1
  allocate(amat(nnn,nnn))
  allocate(bmat(nnn,nnn))




  ! call prin2('after shell, 2n+1 col of cmat = *', cmat(1,2*n+1), 2*n)

  
  nnn = n+1
  tau = 1
  !
  ! build the matrix for just the interior boundary
  !
  call creabeltrami_exterior(eps, zk, m, n, rl, ps_in, zs_in,&
      dpdt_in, dzdt_in, dpdt2_in, dzdt2_in, &
      norder, ind, tau, amat)
  
  do i = 1,2*n+2
    sigma(i) = 0
    sigma2(i) = 0
  end do

  sigma(n+1) = alpha2
  sigma2(2*n+1) = alpha2

  !call prin2('sigma = *', sigma, 2*nnn)
  !call prin2('sigma2 = *', sigma2, 2*lda)
  
  call zmatvec(nnn, nnn, amat, sigma, y1)
  call zmatvec(lda, lda, cmat, sigma2, y2)

  call prin2('interior, y1 = *', y1, 2*n)

  !call prin2('after y1, n+1 col of amat = *', amat(1,n+1), 2*n)

  
  call prin2('interior, y2 = *', y2, 2*n)


  do i = 1,n
    rhom(i) = 0
  end do
  alpha = alpha2

  call beltrami_current_outgoing(zk, m, n, rl, ps_in,& 
      zs_in, dpdt_in, dzdt_in, dpdt2_in, dzdt2_in,&
      rhom, alpha, zms)

  do i = 1,n
    theta = 0
    call beltrami_eval(zk, m, n, rl, ps_in, zs_in,&
        dpdt_in, dzdt_in, dpdt2_in, dzdt2_in, rhom,&
        zms, ps_out(i), theta, zs_out(i), bfield)

    ds = sqrt(dpdt_out(i)**2 + dzdt_out(i)**2)
    dpds = dpdt_out(i)/ds
    dzds = dzdt_out(i)/ds
    field(i) = dzds*bfield(1) - dpds*bfield(3)
  end do

  call prin2('manually, ndor on outside = *', field, 2*n)
  call prin2('from matvec, y2 = *', y2(n+1), 2*n)
  do i = 1,n
    field(i) = y2(n+i) - field(i)
  end do
  call prin2('diffs = *', field, 2*n)
  

  !
  ! now test the exterior analogue
  !
  print *
  print *
  print *
  print *
  nnn = n+1
  tau = 1
  !
  ! build the matrix for just the interior boundary
  !
  call creabeltrami_interior(eps, zk, m, n, rl, ps_out, zs_out,&
      dpdt_out, dzdt_out, dpdt2_out, dzdt2_out, &
      norder, ind, tau, bmat)
  
  do i = 1,2*n+2
    sigma(i) = 0
    sigma2(i) = 0
  end do

  sigma(n+1) = alpha2
  sigma2(2*n+2) = alpha2

  !call prin2('sigma = *', sigma, 2*nnn)
  !call prin2('sigma2 = *', sigma2, 2*lda)
  
  call zmatvec(nnn, nnn, bmat, sigma, y1)
  call zmatvec(lda, lda, cmat, sigma2, y2)

  call prin2('exterior, y1 = *', y1, 2*n)

  !call prin2('after y1, n+1 col of amat = *', amat(1,n+1), 2*n)

  
  call prin2('exterior, y2 = *', y2(n+1), 2*n)


  do i = 1,n
    rhom(i) = 0
  end do
  alpha = alpha2

  call beltrami_current_incoming(zk, m, n, rl, ps_out,& 
      zs_out, dpdt_out, dzdt_out, dpdt2_out, dzdt2_out,&
      rhom, alpha, zms)

  do i = 1,n
    theta = 0
    call beltrami_eval(zk, m, n, rl, ps_out, zs_out,&
        dpdt_out, dzdt_out, dpdt2_out, dzdt2_out, rhom,&
        zms, ps_in(i), theta, zs_in(i), bfield)

    ds = sqrt(dpdt_in(i)**2 + dzdt_in(i)**2)
    dpds = dpdt_in(i)/ds
    dzds = dzdt_in(i)/ds
    field(i) = dzds*bfield(1) - dpds*bfield(3)
  end do

  call prin2('manually, ndot on inside = *', field, 2*n)
  call prin2('from matvec, y2 = *', y2, 2*n)
  do i = 1,n
    field(i) = y2(i) - field(i)
  end do
  call prin2('diffs = *', field, 2*n)

  ! stop

  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  !
  ! test a cycle integrals manually
  !
  tau = 1
  call creabeltrami_exterior(eps, zk, m, n, rl, ps_in, zs_in,&
      dpdt_in, dzdt_in, dpdt2_in, dzdt2_in, &
      norder, ind, tau, amat)

  !
  ! test rho inside contribution
  !
  do i = 1,1000
    sigma(i) = 0
    sigma2(i) = 0
  end do

  do i = 1,n
    t = (i-1)*2*pi/n
    sigma(i) = cos(t)
    sigma2(i) = sigma(i)
  end do

  nnn = n+1
  call zmatvec(nnn, nnn, amat, sigma, y1)
  cda_in = y1(n+1)

  alpha = 0
  call beltrami_current_outgoing(zk, m, n, rl, ps_in,& 
      zs_in, dpdt_in, dzdt_in, dpdt2_in, dzdt2_in,&
      sigma, alpha, zms)

  do i = 1,n
    theta = 0
    call beltrami_eval(zk, m, n, rl, ps_in, zs_in,&
        dpdt_in, dzdt_in, dpdt2_in, dzdt2_in, sigma,&
        zms, ps_out(i), theta, zs_out(i), hfield(1,i))
    dpds = dpdt_out(i)/dsdt_out(i)
    dzds = dzdt_out(i)/dsdt_out(i)
    htan(1,i) = dpds*hfield(1,i) + dzds*hfield(3,i)
    htan(2,i) = hfield(2,i)
  end do

  call beltrami_line_integrals(n, ps_out, zs_out, h, &
      dsdt_out, htan, ind, cda_out, cdb)

  cda = cda_out - cda_in
  print *
  print *
  call prin2('manually, inside rhom cda = *', cda, 2)

  call zmatvec(lda, lda, cmat, sigma2, y2)
  call prin2('from cmat, a integ = *', y2(2*n+1), 2)
  call prin2('diff = *', cda-y2(2*n+1), 2)


  !
  ! test outside rhom manually
  !
  call creabeltrami_interior(eps, zk, m, n, rl, ps_out, zs_out,&
      dpdt_out, dzdt_out, dpdt2_out, dzdt2_out, &
      norder, ind, tau, bmat)

  do i = 1,1000
    sigma(i) = 0
    sigma2(i) = 0
  end do

  do i = 1,n
    t = (i-1)*2*pi/n
    sigma(i) = cos(t)
    sigma2(n+i) = sigma(i)
  end do

  nnn = n+1
  call zmatvec(nnn, nnn, bmat, sigma, y1)
  cda_out = y1(n+1)

  alpha = 0
  call beltrami_current_incoming(zk, m, n, rl, ps_out,& 
      zs_out, dpdt_out, dzdt_out, dpdt2_out, dzdt2_out,&
      sigma, alpha, zms)

  do i = 1,n
    theta = 0
    call beltrami_eval(zk, m, n, rl, ps_out, zs_out,&
        dpdt_out, dzdt_out, dpdt2_out, dzdt2_out, sigma,&
        zms, ps_in(i), theta, zs_in(i), hfield(1,i))
    dpds = dpdt_in(i)/dsdt_in(i)
    dzds = dzdt_in(i)/dsdt_in(i)
    htan(1,i) = dpds*hfield(1,i) + dzds*hfield(3,i)
    htan(2,i) = hfield(2,i)
  end do

  call beltrami_line_integrals(n, ps_in, zs_in, h, &
      dsdt_in, htan, ind, cda_in, cdb)

  cda = cda_out - cda_in
  print *
  print *
  call prin2('manually, outside rhom cda = *', cda, 2)

  call zmatvec(lda, lda, cmat, sigma2, y2)
  call prin2('from cmat, a integ = *', y2(2*n+1), 2)
  call prin2('diff = *', cda-y2(2*n+1), 2)



  !
  ! test harm vec contributions now, first inside
  !
  do i = 1,1000
    sigma(i) = 0
    sigma2(i) = 0
  end do

  alpha = 3.2d0 + ima
  sigma(n+1) = alpha
  nnn = n+1
  call zmatvec(nnn, nnn, amat, sigma, y1)
  cda_in = y1(n+1)

  call beltrami_current_outgoing(zk, m, n, rl, ps_in,& 
      zs_in, dpdt_in, dzdt_in, dpdt2_in, dzdt2_in,&
      sigma, alpha, zms)

  do i = 1,n
    theta = 0
    call beltrami_eval(zk, m, n, rl, ps_in, zs_in,&
        dpdt_in, dzdt_in, dpdt2_in, dzdt2_in, sigma, &
        zms, ps_out(i), theta, zs_out(i), hfield(1,i))
    dpds = dpdt_out(i)/dsdt_out(i)
    dzds = dzdt_out(i)/dsdt_out(i)
    htan(1,i) = dpds*hfield(1,i) + dzds*hfield(3,i)
    htan(2,i) = hfield(2,i)
  end do

  call beltrami_line_integrals(n, ps_out, zs_out, h, &
      dsdt_out, htan, ind, cda_out, cdb)

  !!!!call prin2('from line_integrals, cda_out = *', cda_out, 2)
  
  !!!!cda_in = 0
  cda = cda_out - cda_in
  print *
  print *
  call prin2('manually, inside alpha cda = *', cda, 2)

  sigma2(2*n+1) = alpha
  call zmatvec(lda, lda, cmat, sigma2, y2)
  call prin2('from cmat, a integ = *', y2(2*n+1), 2)
  call prin2('diff = *', cda-y2(2*n+1), 2)



  !
  ! test harm vec contributions now, now outside
  !
  do i = 1,1000
    sigma(i) = 0
    sigma2(i) = 0
  end do

  alpha = 3.2d0 + ima
  sigma(n+1) = alpha
  nnn = n+1
  call zmatvec(nnn, nnn, bmat, sigma, y1)
  cda_out = y1(n+1)

  call beltrami_current_incoming(zk, m, n, rl, ps_out,& 
      zs_out, dpdt_out, dzdt_out, dpdt2_out, dzdt2_out,&
      sigma, alpha, zms)

  do i = 1,n
    theta = 0
    call beltrami_eval(zk, m, n, rl, ps_out, zs_out,&
        dpdt_out, dzdt_out, dpdt2_out, dzdt2_out, sigma, &
        zms, ps_in(i), theta, zs_in(i), hfield(1,i))
    dpds = dpdt_in(i)/dsdt_in(i)
    dzds = dzdt_in(i)/dsdt_in(i)
    htan(1,i) = dpds*hfield(1,i) + dzds*hfield(3,i)
    htan(2,i) = hfield(2,i)
  end do

  call beltrami_line_integrals(n, ps_in, zs_in, h, &
      dsdt_in, htan, ind, cda_in, cdb)

  !!!!call prin2('from line_integrals, cda_out = *', cda_out, 2)
  
  !!!!cda_in = 0
  cda = cda_out - cda_in
  print *
  print *
  call prin2('manually, inside alpha cda = *', cda, 2)

  sigma2(2*n+2) = alpha
  call zmatvec(lda, lda, cmat, sigma2, y2)
  call prin2('from cmat, a integ = *', y2(2*n+1), 2)
  call prin2('diff = *', cda-y2(2*n+1), 2)

  stop
  
  end if








  
  
  
  call prin2('the system matrix = *', cmat, 30)


  !call zgausselim(lda, cmat, rhs, info, sol, dcond)

  call ccopy601(lda**2, cmat, work)
  call ccopy601(lda, rhs, sol)

  
  
  ijob=0
  call zgeco(work, lda, lda, ipvt, rcond, wz)
  call zgesl(work, lda, lda, ipvt, sol, ijob)
  call prin2('from zgeco, rcond=*', 1/rcond, 1)
  !call prin2('from zgausselim, dcond = *', dcond, 1)
  
  call ccopy601(n, sol(1), rhom_in)
  call ccopy601(n, sol(n+1), rhom_out)

  call prin2('rhom_in = *', rhom_in, 30)
  call prin2('rhom_out = *', rhom_out, 30)

  

  alpha_in = sol(2*n+1)
  alpha_out = sol(2*n+2)
  call prin2('alpha_in = *', alpha_in, 2)
  call prin2('alpha_out = *', alpha_out, 2)
  !c
  !c       test the field at a point
  !c
  print *
  print *
  print *, '... testing solution ...'

  p1 = .7d0
  theta1 = 0
  z1 = .5d0

  call prin2('testing point, p1 = *', p1, 1)
  call prin2('testing point, theta1 = *', theta1, 1)
  call prin2('testing point, z1 = *', z1, 1)
  
  call beltramieva(eps, zk, m, n, rl, psg, zsg, &
      dpdtg, dzdtg, dpdtg2, dzdtg2, rhom1, alpha1, zms1,&
      p1, theta1, z1, field)

  call beltramieva(eps, zk, m, n, rl, psg_out, zsg_out,& 
      dpdtg_out, dzdtg_out, dpdtg2_out, dzdtg2_out, &
      rhom2, alpha2, zms2, p1, theta1, z1, field2)

  bfield1(1) = field(1) + field2(1)
  bfield1(2) = field(2) + field2(2)
  bfield1(3) = field(3) + field2(3)


  !c
  !c       form the current on the inner  and outer surfaces
  !c
  !call prin2('rhom_in = *', rhom_in, 2*n)
  !call prin2('alpha_in = *', alpha_in, 2)
  call beltrami_current_outgoing(zk, m, n, rl, ps_in,& 
      zs_in, dpdt_in, dzdt_in, dpdt2_in, dzdt2_in,&
      rhom_in, alpha_in, zms_in)
  !call prin2('zms_in = *', zms_in, 2*2*n)

  !call prin2('rhom_out = *', rhom_out, 2*n)
  !call prin2('alpha_out = *', alpha_out, 2)
  call beltrami_current_incoming(zk, m, n, rl, ps_out, &
      zs_out, dpdt_out, dzdt_out, dpdt2_out, dzdt2_out,&
      rhom_out, alpha_out, zms_out)
  !call prin2('zms_out = *', zms_out, 2*2*n)

  call beltrami_eval(zk, m, n, rl, ps_in, zs_in,&
      dpdt_in, dzdt_in, dpdt2_in, dzdt2_in, rhom_in,&
      zms_in, p1, theta1, z1, bfield_in)


  call beltrami_eval(zk, m, n, rl, ps_out, zs_out,&
      dpdt_out, dzdt_out, dpdt2_out, dzdt2_out, rhom_out,&
      zms_out, p1, theta1, z1, bfield_out)

  bfield(1) = bfield_in(1) + bfield_out(1)
  bfield(2) = bfield_in(2) + bfield_out(2)
  bfield(3) = bfield_in(3) + bfield_out(3)
  call prin2('known beltrami field = *', bfield1, 6)
  call prin2('from integral equation, field = *', bfield, 6)

  call crelfielderr(1, bfield1, bfield, err)
  call prin2('relative error = *', err, 1)
  
  stop

end program beltrami_shell_test1

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


subroutine fcurveout(t,x,y,dxdt,dydt,dxdt2,dydt2)
  implicit real *8 (a-h,o-z)
  !c
  !c       controls the surface of the outer boundary 
  !c
  !c       an antoine-jeff like geometry
  !c
  done=1
  pi=4*atan(done)
  !c
  !c       an antoine-jeff like geometry
  !c
  e = .32d0*2
  alpha = 0.336d0
  rkap = 1.7d0

  x0 = 1.0d0
  y0 = 0

  x = x0 + e*cos(t+alpha*sin(t))
  y = y0 + e*rkap*sin(t)

  dxdt = -e*sin(t+alpha*sin(t))*(1+alpha*cos(t))
  dydt = e*rkap*cos(t)

  dxdt2 = -e*cos(t+alpha*sin(t))*(1+alpha*cos(t))**2 &
      +e*sin(t+alpha*sin(t))*alpha*sin(t)
  dydt2 = -e*rkap*sin(t)

  !return

  !c       an ellipse
  !c
  x0 = 1.0d0
  y0 = 0d0

  a = .95d0
  b = 1.95d0
  b = 1.25d0

  x=x0+a*cos(t)
  y=y0+b*sin(t)
  dxdt=-a*sin(t)
  dydt=b*cos(t)
  dxdt2=-a*cos(t)
  dydt2=-b*sin(t)

  return
end subroutine fcurveout





subroutine fcurvein(t,x,y,dxdt,dydt,dxdt2,dydt2)
  implicit real *8 (a-h,o-z)
  !c
  !c       controls the surface of the outer boundary 
  !
  e = .32d0/2.5d0*2
  alpha = 0.336d0
  rkap = 1.7d0

  x0 = 1.0d0
  y0 = 0

  x = x0 + e*cos(t+alpha*sin(t))
  y = y0 + e*rkap*sin(t)

  dxdt = -e*sin(t+alpha*sin(t))*(1+alpha*cos(t))
  dydt = e*rkap*cos(t)

  dxdt2 = -e*cos(t+alpha*sin(t))*(1+alpha*cos(t))**2 &
      +e*sin(t+alpha*sin(t))*alpha*sin(t)
  dydt2 = -e*rkap*sin(t)
  !c
  !cccc        return
  !c
  !c       an ellipse
  !c
  x0 = 1.2d0
  y0 = 0d0

  a = .25d0
  b = .35d0

  x=x0+a*cos(t)
  y=y0+b*sin(t)
  dxdt=-a*sin(t)
  dydt=b*cos(t)
  dxdt2=-a*cos(t)
  dydt2=-b*sin(t)

  return
end subroutine fcurvein





subroutine crelfielderr(n,x,y,err)
  implicit real *8 (a-h,o-z)
  complex *16 x(3,1),y(3,1),cd1,cd2,cd3
  !c
  !c       calculates the relative error between the two fields
  !c       x and y, i.e. calculates |x - y|/|y| where |y|**2 is the
  !c       sum of squares of the field vector at each of the n points
  !c
  dd1=0
  dd2=0

  do i=1,n

    cd1=x(1,i)-y(1,i)
    cd2=x(2,i)-y(2,i)
    cd3=x(3,i)-y(3,i)
    
    dd1=dd1+abs(cd1)**2+abs(cd2)**2+abs(cd3)**2
    dd2=dd2+abs(y(1,i))**2+abs(y(2,i))**2+abs(y(3,i))**2
    
  end do
  

  err=sqrt(dd1/dd2)

  return
end subroutine crelfielderr






subroutine fsource_in(t,x,y,dxdt,dydt,dxdt2,dydt2)
  implicit real *8 (a-h,o-z)
  !c
  !c       controls the surface of source field 
  !c
  !c       a small circle
  !c
  !c        x0 = 6.01d0
  !c        y0 = .02d0
  !c
  !c        a = .15d0
  !c        b = .15d0
  !c
  x0 = 1.2d0
  y0 = 0d0
  a=.1d0
  b=.1d0
  
  x=x0+a*cos(t)
  y=y0+b*sin(t)
  dxdt=-a*sin(t)
  dydt=b*cos(t)
  dxdt2=-a*cos(t)
  dydt2=-b*sin(t)
  
  return
end subroutine fsource_in





subroutine fsource_out(t,x,y,dxdt,dydt,dxdt2,dydt2)
  implicit real *8 (a-h,o-z)
  !c
  !c       controls the surface of the source field
  !c
  !c       a small circle
  !c
  x0 = 2.5d0
  y0 = 2.5d0
  
  a=.2d0
  b=.2d0
  
  x=x0+a*cos(t)
  y=y0+b*sin(t)
  dxdt=-a*sin(t)
  dydt=b*cos(t)
  dxdt2=-a*cos(t)
  dydt2=-b*sin(t)
  
  return
end subroutine fsource_out
