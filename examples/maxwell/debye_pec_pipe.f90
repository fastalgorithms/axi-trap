program debye_pec_pipe
  implicit real *8 (a-h,o-z)
  real *8 :: ps(10000),zs(10000),dpdt(10000)
  real *8 :: dzdt(10000),dpdt2(10000),dzdt2(10000),dsdt(10000)
  real *8 :: center(10),xyz(10)
  real *8 :: targ(10), ts(10000)
  real *8 :: xys(2,10000), dxys(2,10000), d2xys(2,10000)
  real *8 :: dtds(10000), dtds2(10000), errs(10)

  real *8, allocatable :: rnodes(:,:,:), pts(:,:,:), xyzs(:,:,:)
  real *8, allocatable :: testpts(:,:), whts(:,:), vals(:)
  real *8, allocatable :: triaskel(:,:,:), tempvecs(:,:,:)
  real *8, allocatable :: dein(:,:,:), dhin(:,:,:)

  complex *16 :: zk, ima, cinta, cinta2, cintb2, surfa
  complex *16 :: aint, cintb, efield(3,10000), hfield(3,10000)
  complex *16 :: etest(3,10000), htest(3,10000), etau
  complex *16 :: cd

  complex *16, allocatable :: esurf(:,:,:),hsurf(:,:,:)
  complex *16, allocatable :: ein(:,:,:), hin(:,:,:)
  complex *16, allocatable :: rho(:,:), sigma(:,:)
  complex *16, allocatable :: jcurr(:,:,:), kcurr(:,:,:)
  complex *16, allocatable :: zvals(:,:)

  external funcurvewrap
  
  done=1
  pi=4*atan(done)
  ima=(0,1)

  call prini(6,13)


  !
  ! calculate the length of the generating curve
  !
  n = 5000
  rl = 2*pi
  call funcurve_resampler(n, rl, ts, h, xys, dxys, d2xys, rltot)
  call prin2('length of generating curve = *', rltot, 1)

  !
  ! set some subroutine parameters
  !
  dlam = 6.0d0/50
  dlam = 6.0d0/20
  dlam = 0.5d0
  dlam = dlam/2/2/2/2
  zk = 2*pi/dlam
  !zk = 2*pi/dlam + ima*1.0d-10
  !zk = 2.2d0+ima*1.242359537d-10
  !zk = 20.0d0 +ima*1.242359537d-10
  !zk = 40.0d0 +ima*1.242359537d-10
  !!!!zk = 3.43d0+ima/100000


  ptsper = 10
  !n = 2049
  n = (int(rltot/dlam)+1)*ptsper
  nover = 1
  n = n*nover

  !if (n .lt. 100) n = 101
  if (mod(n,2) .eq. 0) n = n+1
  call prinf('n = *', n, 1)

  call prin2(' *',pi,0)
  call prinf('n = *', n, 1)
  call prin2('zk=*',zk,2)
  call prin2('wavelength = *', dlam, 1)

  norder = 2
  norder = 4
  norder = 8
  !norder=16
  call prinf('alpert order=*',norder,1)

  !
  ! generate the points on the scatterer
  !
  ifresamp = 1
  if (ifresamp .eq. 1) then

    print *
    print *, '- - - resampling the geometry - - - '
    
    call funcurve_resampler(n, rl, ts, h, xys, dxys, d2xys, rltot)

    do i = 1,n
      dsdt(i) = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
    end do

    duds = 2*pi/rltot
    dsdu = 1/duds
    do i = 1,n
      dxys(1,i) = dxys(1,i)*dsdu
      dxys(2,i) = dxys(2,i)*dsdu
      d2xys(1,i) = d2xys(1,i)*dsdu*dsdu
      d2xys(2,i) = d2xys(2,i)*dsdu*dsdu
      dpdt(i) = dxys(1,i)
      dzdt(i) = dxys(2,i)
      dsdt(i) = sqrt(dxys(1,i)**2 + dxys(2,i)**2)
    end do
    h = h*duds
    
    print *
    print *, '- - - - from arclength resampler - - - -'
    !call prin2('dxys = *', dxys, 2*n)
    !call prin2('dsdt = *', dsdt, n)
    !call prin2('d2xys = *', dxys, 2*n)
    call prin2('h = *', h, 1)
    call prin2('rltot = *', rltot, 1)
    print *, '- - - - - - - - - - - - - - - - - '
    print *
    
  end if


  call axi_geometry_resolution(n, xys, dxys, d2xys, errs, res)
  call prin2('errs = *', errs, 6)
  call prin2('geometry resolution = *', res, 1)
  

  print *, '- - - - - - - - - - -- - - - - '
  call prin2('length of gen curve in wavelengths = *', rltot/dlam, 1)
  call prin2('points per wavelength = *', n/(rltot/dlam), 1)

  
  !
  ! find the minimum ps to construct spanning surface for
  ! the m=0 mode
  !
  ind=1
  dmax = -1
  top = -10000
  bot = 10000
  diam = -1
  do i=1,n
    if (xys(1,i) .lt. xys(1,ind)) ind=i
    if (xys(1,i) .gt. dmax) dmax=xys(1,i)
    if (xys(2,i) .lt. bot) bot = xys(2,i)
    if (xys(2,i) .gt. top) top = xys(2,i)
    ddd = sqrt(xys(1,i)**2 + xys(2,i)**2)
    if (ddd .gt. diam) diam=ddd
  enddo
  diam = 2*diam
  
  call prin2('horizontal size of scatterer in wavelengths = *', &
      2*dmax/dlam, 1)
  call prin2('vertical size of scatterer in wavelengths = *', &
      (top-bot)/dlam, 1)

  dltot = 2*pi*dmax
  nphi = (int(dltot/dlam)+1)*ptsper
  nphi = nover*nphi
  !if (nphi .lt. 128) nphi=128
  call prin2('azimuthal circum in wavelengths = *', dltot/dlam, 1)
  call prinf('setting nphi = *', nphi, 1)
  call prin2('azimuthal points per wavelength = *', &
      nphi/(dltot/dlam), 1)

  !stop

  !
  ! evaluate an incoming field on the surface of the scatterer
  !
  ifplot = 1
  if (ifplot .eq. 1) then
    iw = 11
    print *, '. . . plotting the surface'
    call axi_vtk_surface(iw, n, xys, nphi, 'The body of revolution')
  end if
  

  allocate( esurf(3,n,nphi) )
  allocate( hsurf(3,n,nphi) )
  allocate( ein(3,nphi,n) )
  allocate( hin(3,nphi,n) )

  !
  ! drive the thing with a current loop
  !
  al = .34d0
  !al = 0
  !center(1) = 2d0*cos(al)
  !center(2) = 2d0*sin(al)
  !center(3) = .1d0

  center(1) = 0.5d0*cos(al)
  center(2) = 0.5d0*sin(al)
  center(3) = .1d0
  radloop = .1d0

  call prin2('center = *', center, 3)
  
  !
  ! now evaluate the e/h fields at each point on the scatterer
  !
  print *, '. . . evaluating the data on the scatterer'

  allocate(xyzs(3,nphi,n))

  !$omp parallel do default(shared) &
  !$omp     private(phi, targ)
  do j=1,nphi
    phi = (j-1)*2*pi/nphi
    do i=1,n
      targ(1) = xys(1,i)*cos(phi)
      targ(2) = xys(1,i)*sin(phi)
      targ(3) = xys(2,i)
      xyzs(1,j,i) = targ(1)
      xyzs(2,j,i) = targ(2)
      xyzs(3,j,i) = targ(3)
      call loopfields(zk, center, xyzs(1,j,i), radloop, par1, &
          ein(1,j,i), hin(1,j,i))
    enddo
  enddo
  !$omp end parallel do



  allocate(dein(3,nphi,n))
  allocate(dhin(3,nphi,n))
  do j = 1,n
    do i = 1,nphi
      do k = 1,3
        dein(k,i,j) = ein(k,i,j)
        dhin(k,i,j) = hin(k,i,j)
      end do
    end do
  end do
  
  if (ifplot .eq. 1) then
    iw = 31
    print *, '. . . plotting the incoming field'
    call axi_vtk_vector(iw, n, xys, nphi, dein, 'e field data')
    iw = 32
    call axi_vtk_vector(iw, n, xys, nphi, dhin, 'h field data')
  end if
  

  surfa = 0
  dph = 2*pi/nphi
  do i = 1,nphi
    ph = 2*pi*(i-1)/nphi
    do j = 1,n
      dx = dxys(1,j)/dsdt(j)*cos(ph)
      dy = dxys(1,j)/dsdt(j)*sin(ph)
      dz = dxys(2,j)/dsdt(j)
      etau = ein(1,i,j)*dx + ein(2,i,j)*dy + ein(3,i,j)*dz
      da = dph*xys(1,j)*h*dsdt(j)
      surfa = surfa + etau*da
    end do
  end do

  call prin2('from cartesian surface integral, surfa = *', surfa, 2)
  
  
  cintb = 0
  hhh = 2*pi*xys(1,ind)/nphi
  do i = 1,nphi
    phi = 2*pi/nphi*(i-1)
    dx = -sin(phi)
    dy = cos(phi)
    dz = 0
    cintb = cintb + hhh*(dx*ein(1,i,ind) + &
        dy*ein(2,i,ind) + dz*ein(3,i,ind))
  end do

  cintb = cintb/zk

  !
  ! finally done setting up the problem, now solve the Gen Debye PEC
  ! problem with data esurf,hsurf
  !
  allocate(whts(nphi,n))
  allocate(rho(nphi,n), sigma(nphi,n))
  allocate(jcurr(3,nphi,n), kcurr(3,nphi,n))

  
  call cpu_time(tstart)
  !$ tstart = OMP_get_wtime()

  call axidebye_pec_solver(zk, n, h, xys, &
      dxys, d2xys, norder, nphi, xyzs, ein, hin, ind, &
      surfa, cintb, whts, rho, sigma, jcurr, &
      kcurr, modemax, esterr)

  call cpu_time(tend)
  !$ tend = OMP_get_wtime()

  call prin2('time for axidebye_pec_solver = *', tend-tstart, 1)
  
  !!!!call prin2('after axidebye_pec_solver, esterr = *', esterr, 1)

  if (ifplot .eq. 1) then
    iw = 41
    m = 2
    call axi_vtk_scalar(iw, n, xys, nphi, m, rho, 'rho')
    iw = 42
    call axi_vtk_scalar(iw, n, xys, nphi, m, sigma, 'sigma')
  end if
  
  !
  ! generate some points to test the field in the outside of the
  ! scatterer
  !
  ntheta1=5
  nphi1=10

  x0 = 0
  y0 = 0
  z0 = 0
  rad = 8

  htheta=pi/(ntheta1+1)
  hphi=2*pi/nphi1

  ntest = 0
  allocate(testpts(3,nphi1*ntheta1))
  do i=1,ntheta1
    theta=htheta*i
    do j=1,nphi1
      phi=hphi*(j-1)
      x=x0+rad*sin(theta)*cos(phi)
      y=y0+rad*sin(theta)*sin(phi)
      z=z0+rad*cos(theta)
      ntest = ntest+1
      testpts(1,ntest)=x
      testpts(2,ntest)=y
      testpts(3,ntest)=z
    enddo
  enddo


  !
  ! now evaluate the resulting field at the test points
  !
  nsrc = n*nphi
  call axidebye_eval3d(zk, nsrc, xyzs, whts, rho, sigma, jcurr,&
      kcurr, ntest, testpts, efield, hfield)

  !
  ! . . . and the known fields on the test surface
  !
  do i=1,ntest
    call loopfields(zk, center, testpts(1,i), radloop, par1, &
        etest(1,i), htest(1,i))
  enddo

  !
  ! calculate the errors
  !
  print *
  call prin2('scattered efield = *', efield, 30)
  call prin2('known efield=*',etest,30)

  print *
  call prin2('scattered hfield = *', hfield, 30)
  call prin2('known hfield = *', htest, 30)

  call crelfielderr(ntest,htest,hfield,herr)
  call crelfielderr(ntest, etest, efield, eerr)

  print *
  print *, '- - - - - - output - - - - - -'
  call prin2('dlam = *', dlam, 1)
  call prinf('n = *', n, 1)
  call prin2('n over lambda = *', n/(rltot/dlam), 1)
  call prinf('nphi = *', nphi, 1)
  call prin2('l over lambda = *', nphi/(dltot/dlam), 1)
  call prinf('ntot = *', n*nphi, 1)
  call prin2('diam over lambda = *', diam/dlam, 1)
  call prin2('geom res = *', res, 1)
  call prinf('norder = *', norder, 1)
  call prin2('time for axidebye_pec_solver = *', tend-tstart, 1)

  print *
  call prin2('on test surface, relative e error=*',eerr,1)
  call prin2('on test surface, relative h error=*',herr,1)
  print *, ' - - - - - - - - - - - - - - - -'

end program debye_pec_pipe






subroutine crelfielderr(n,x,y,err)
  implicit real *8 (a-h,o-z)
  complex *16 x(3,1),y(3,1),cd1,cd2,cd3
  !c
  !c       calculates the relative error between the two fields
  !c       x and y, i.e. calculates |x - y|/|y| where |y|**2 is the
  !c       sum of squares of the field vector at each of the n points
  !c
  dd1=0
  dd2=0

  do i=1,n
    cd1=x(1,i)-y(1,i)
    cd2=x(2,i)-y(2,i)
    cd3=x(3,i)-y(3,i)
    dd1=dd1+abs(cd1)**2+abs(cd2)**2+abs(cd3)**2
    dd2=dd2+abs(y(1,i))**2+abs(y(2,i))**2+abs(y(3,i))**2
  end do

  err=sqrt(dd1/dd2)

  return
end subroutine crelfielderr





subroutine cfielderr(n,x,y,err)
  implicit real *8 (a-h,o-z)
  complex *16 x(3,1),y(3,1),cd1,cd2,cd3
  !c
  !c       calculates the rmse between the two fields
  !c       x and y, i.e. calculates |x - y|/sqrt(n) where |y|**2 is the
  !c       sum of squares of the field vector at each of the n points
  !c
  dd1=0
  do i=1,n
    cd1=x(1,i)-y(1,i)
    cd2=x(2,i)-y(2,i)
    cd3=x(3,i)-y(3,i)
    dd1=dd1+abs(cd1)**2+abs(cd2)**2+abs(cd3)**2
  end do

  err=sqrt(dd1/n)

  return
end subroutine cfielderr
