        implicit real *8 (a-h,o-z)
        real *8 ps(10000),zs(10000),dpdt(10000),
     1      dzdt(10000),dpdt2(10000),dzdt2(10000),dsdt(10000),
     4      errs(10000),xs(10000),whts(10000),
     5      tks(10000),yes1(10000),yhs1(10000),
     5      yes0(10000),yhs0(10000),rcs(10000),
     6      ptest(10000),ztest(10000),
     7      psin(10000),zsin(10000),dpdtin(10000),dzdtin(10000),
     8      dpdt2in(10000),dzdt2in(10000),dsdtin(10000),
     7      psout(10000),zsout(10000),dpdtout(10000),
     8      dzdtout(10000),dpdt2out(10000),dzdt2out(10000),
     9      dsdtout(10000)
        complex *16 zk1,zk0,zk000,cd,ima,cdh,cdb,zk2,zk,
     1      u1,u0,e1,e0,omega,omega0,
     1      cdt,cds,cd2,amat(4000000),
     1      u(1000000),v(1000000),sz(100000),work(10000000),
     2      rhoin(10000),rhomin(10000),
     3      zjsin(2,10000),zksin(2,10000),
     4      rhoout(10000),rhomout(10000),
     3      zjsout(2,10000),zksout(2,10000),
     4      ain,bin,aout,bout,
     2      ein(3,10000),hin(3,10000),
     2      eout(3,10000),hout(3,10000),
     3      etanin(2,10000),htanin(2,10000),
     3      etanout(2,10000),htanout(2,10000),
     4      wndot(1000000),rhs(10000),sol(10000),
     5      vals1(100),vals2(100),vals3(100),
     6      zjs1(2,10000),zks1(2,10000),
     7      zjs0(2,10000),zks0(2,10000),
     8      rhsin(10000),rhsout(10000),
     9      etest(3,10000),htest(3,10000)
        external ghfun2
c
        done=1
        ima=(0,1)
        pi=4*atan(1.0d0)
c
        call prini(6,13)
c
        call prin2('enter n:*',pi,0)
        read *, n
        call prinf('n=*',n,1)

c
c       create the scattering surface
c
        rl=2*pi
        h=rl/n
        do 1400 i=1,n
        t=h*(i-1)
        call funcurve(t,ps(i),zs(i),dpdt(i),dzdt(i),
     1      dpdt2(i),dzdt2(i))
        dd=dpdt(i)**2+dzdt(i)**2
        dsdt(i)=sqrt(dd)
 1400   continue


c
c       build 2 source surfaces, first the one inside the surface...
c
        do 1800 i=1,n
        t=h*(i-1)
        call funcurve2(t,psin(i),zsin(i),dpdtin(i),dzdtin(i),
     1      dpdt2in(i),dzdt2in(i))
        dd=dpdtin(i)**2+dzdtin(i)**2
        dsdtin(i)=sqrt(dd)
 1800   continue

c
c       ...and then the one outside
c
        do 2000 i=1,n
        t=h*(i-1)
        call funcurve3(t,psout(i),zsout(i),dpdtout(i),dzdtout(i),
     1      dpdt2out(i),dzdt2out(i))
        dd=dpdtout(i)**2+dzdtout(i)**2
        dsdtout(i)=sqrt(dd)
 2000   continue
c

        iw=12
        itype=2
        call quaplot3(iw,ps,zs,n,itype,psin,zsin,n,itype,
     1      psout,zsout,n,itype,
     1      'scattering, inside source, and outside source*')


c
c       set permittivity, permeability, frequency, and mode
c
        omega0=1
        num=0

        do 6600 ijk=0,105

        sc=10.0d0**(done/7)
cccc        omega=1.0d-1

        omega=omega0/(sc**ijk)
        num=num+1
        tks(num)=omega


c
        e1=1.3d0
        e0=.9d0
        u1=.83d0
        u0=1.1d0
c
        zk0=omega*sqrt(e0*u0)
        zk1=omega*sqrt(e1*u1)


        call prin2(' *',pi,0)
        call prin2('omega=*',omega,2)
        call prin2('e1=*',e1,2)
        call prin2('e0=*',e0,2)
        call prin2('u1=*',u1,2)
        call prin2('u0=*',u0,2)
        call prin2('zk0=*',zk0,2)
        call prin2('zk1=*',zk1,2)
c
        mode=0
        m=mode
        call prinf('mode=*',mode,1)
        eps=1.0d-10
        call prin2('kernel precision=*',eps,1)
c
        norder=16
        norder=8
        call prinf('alpert quadrature order=*',norder,1)



        if (1 .eq. 0) then
c
c       test fields are maxwellian...check divergences
c
        do 2200 i=1,n
        zjsin(1,i)=1
        zjsin(2,i)=1
        zksin(1,i)=-1
        zksin(2,i)=-2
 2200 continue


        d=1.0d-4

        p=6
        theta=pi/4.32423
        z=4
c
        call prin2(' *',pi,0)
        call prin2('TESTING FIELDS...*',pi,0)

        call divefield5(eps,omega,e1,u1,m,n,rl,psin,zsin,
     1      dpdtin,dzdtin,zjsin,zksin,p,theta,z,d)

        call divhfield5(eps,omega,e1,u1,m,n,rl,psin,zsin,
     1      dpdtin,dzdtin,zjsin,zksin,p,theta,z,d)

c
c       check curls
c
        call hmullereva(eps,omega,e1,u1,m,n,rl,psin,zsin,
     1      dpdtin,dzdtin,zjsin,zksin,p,theta,z,vals1)

        ifefield=1
        call curlfield5(eps,omega,e1,u1,m,n,rl,psin,zsin,
     1      dpdtin,dzdtin,ifefield,zjsin,zksin,
     2      p,theta,z,d,vals2)
c
        vals1(1)=ima*omega*u1*vals1(1)
        vals1(2)=ima*omega*u1*vals1(2)
        vals1(3)=ima*omega*u1*vals1(3)
c
        call prin2('i omega u H=*',vals1,6)
        call prin2('curl e=*',vals2,6)

        vals1(1)=abs(vals1(1)-vals2(1))/abs(vals1(1))
        vals1(2)=abs(vals1(2)-vals2(2))/abs(vals1(2))
        vals1(3)=abs(vals1(3)-vals2(3))/abs(vals1(3))
        call prin2('and relative difference is=*',vals1,6)

c
        call emullereva(eps,omega,e1,u1,m,n,rl,psin,zsin,
     1      dpdtin,dzdtin,zjsin,zksin,p,theta,z,vals1)

        ifefield=0
        call curlfield5(eps,omega,e1,u1,m,n,rl,psin,zsin,
     1      dpdtin,dzdtin,ifefield,zjsin,zksin,
     2      p,theta,z,d,vals2)

c
        vals1(1)=-ima*omega*e1*vals1(1)
        vals1(2)=-ima*omega*e1*vals1(2)
        vals1(3)=-ima*omega*e1*vals1(3)
c
        call prin2('-i omega e E=*',vals1,6)
        call prin2('curl h=*',vals2,6)
c
        vals1(1)=abs(vals1(1)-vals2(1))/abs(vals1(1))
        vals1(2)=abs(vals1(2)-vals2(2))/abs(vals1(2))
        vals1(3)=abs(vals1(3)-vals2(3))/abs(vals1(3))
        call prin2('and relative difference is=*',vals1,6)

cccc        stop
        endif




c
c       construct the muller dielectric matrix to invert
c
        nnn=4*n
        call creamuller(eps,m,n,rl,ps,zs,
     1      dpdt,dzdt,norder,e1,e0,u1,u0,omega,amat)


        eps14=1.0d-14
        lw=10000000
        call csvdpiv(ier,amat,nnn,nnn,u,v,sz,ncols,eps14,
     1      work,lw,ltot)
        call prinf('muller mat after csvdpivot, ncols=*',ncols,1)
cccc        call prin2('muller mat singular values=*',sz,2*ncols)

        rcond=abs(sz(1))/abs(sz(ncols))
        call prin2('after csvdpivot, rcond=*',rcond,1)
        rcs(num)=rcond




c
c       now slap some charges on the two test surfaces and create
c       incoming fields
c
        do 2400 i=1,n
        t=2*pi*(i-1)/n
        rhoin(i)=exp(ima*t)/psin(i)/dsdtin(i)
        rhomin(i)=ima*exp(2*ima*t)/psin(i)/dsdtin(i)
        rhoout(i)=ima*exp(2*ima*t)/psout(i)/dsdtout(i)
        rhomout(i)=exp(1*ima*t)/psout(i)/dsdtout(i)
 2400   continue
c
        if (m .eq. 0) then
        ain=1+ima
        bin=1-ima
        aout=1-ima
        bout=1+ima
        endif
c
        if (m .ne. 0) then
        ain=0
        bin=0
        aout=0
        bout=0
        endif


c
c       create j and k on each surface, evaluate the fields on ps,zs
c

        irep=0
        call charge2current(eps,zk1,m,n,rl,psin,zsin,
     1      dpdtin,dzdtin,dpdt2in,dzdt2in,norder,
     1      rhoin,rhomin,ain,bin,irep,zjsin,zksin)
c
        call charge2current(eps,zk0,m,n,rl,psout,zsout,
     1      dpdtout,dzdtout,dpdt2out,dzdt2out,norder,
     1      rhoout,rhomout,aout,bout,irep,zjsout,zksout)


cc        call prin2('zjsin=*',zjsin,2*n*2)
cc        call prin2('zksin=*',zksin,2*n*2)
cc        call prin2('zjsout=*',zjsout,2*n*2)
cc        call prin2('zksout=*',zksout,2*n*2)

c
        theta=0
        do 2800 i=1,n
c
        call emullereva(eps,omega,e1,u1,m,n,rl,psin,zsin,
     1      dpdtin,dzdtin,zjsin,zksin,ps(i),theta,zs(i),ein(1,i))
c
        call hmullereva(eps,omega,e1,u1,m,n,rl,psin,zsin,
     1      dpdtin,dzdtin,zjsin,zksin,ps(i),theta,zs(i),hin(1,i))


        call emullereva(eps,omega,e0,u0,m,n,rl,psout,zsout,
     1      dpdtout,dzdtout,zjsout,zksout,ps(i),theta,zs(i),eout(1,i))
c
        call hmullereva(eps,omega,e0,u0,m,n,rl,psout,zsout,
     1      dpdtout,dzdtout,zjsout,zksout,ps(i),theta,zs(i),hout(1,i))
 2800   continue


c
c       convert to tangential fields
c
        do 3400 i=1,n
c
        dpds=dpdt(i)/dsdt(i)
        dzds=dzdt(i)/dsdt(i)
c
        etanin(1,i)=dpds*ein(1,i)+dzds*ein(3,i)
        etanin(2,i)=ein(2,i)
c
        htanin(1,i)=dpds*hin(1,i)+dzds*hin(3,i)
        htanin(2,i)=hin(2,i)
c
        etanout(1,i)=dpds*eout(1,i)+dzds*eout(3,i)
        etanout(2,i)=eout(2,i)
c
        htanout(1,i)=dpds*hout(1,i)+dzds*hout(3,i)
        htanout(2,i)=hout(2,i)
 3400   continue




c
c       assemble rhs vector now
c
        do 3800 i=1,n
        i1=2*(i-1)+1
        i2=i1+1
        i3=2*n+i1
        i4=i3+1
c
        rhs(i1)=etanin(1,i)-etanout(1,i)
        rhs(i2)=etanin(2,i)-etanout(2,i)
        rhs(i3)=htanin(1,i)-htanout(1,i)
        rhs(i4)=htanin(2,i)-htanout(2,i)
 3800   continue


c
c       solve the system
c
        call ccopy601(nnn**2,amat,work)
        call ccopy601(nnn,rhs,sol)
        call cqrsolv(work,nnn,sol,rcond)

        call prin2('after cqrsolv, rcond=*',rcond,1)

        call ccopy601(2*n,sol(1),zjs1)
        call ccopy601(2*n,sol(2*n+1),zks1)

c
c       we have exterior currents, construct interior currents
c
        do 4000 i=1,n
        zjs0(1,i)=e0/e1*zjs1(1,i)
        zjs0(2,i)=e0/e1*zjs1(2,i)
        zks0(1,i)=u0/u1*zks1(1,i)
        zks0(2,i)=u0/u1*zks1(2,i)
 4000 continue


cccc        call prin2('after solving, zjs1=*',zjs1,2*n*2)
cccc        call prin2('after solving, zks1=*',zks1,2*n*2)

cccc        call prin2('after solving, scaled zjs0=*',zjs0,2*n*2)
cccc        call prin2('after solving, scaled zks0=*',zks0,2*n*2)



c
c       check the solution, first in the exterior
c 

        do 4200 i=1,n
c
        p=psout(i)
        theta=0
        z=zsout(i)
c
c       evaluate the scattered field
c
        call emullereva(eps,omega,e1,u1,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs1,zks1,p,theta,z,etest(1,i))
c
        call hmullereva(eps,omega,e1,u1,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs1,zks1,p,theta,z,htest(1,i))
c

c
c       evaluate the known data field
c
        call emullereva(eps,omega,e1,u1,m,n,rl,psin,zsin,
     1      dpdtin,dzdtin,zjsin,zksin,p,theta,z,ein(1,i))
c
        call hmullereva(eps,omega,e1,u1,m,n,rl,psin,zsin,
     1      dpdtin,dzdtin,zjsin,zksin,p,theta,z,hin(1,i))
 4200 continue


cc        call prin2(' *',pi,0)
cc        call prin2('exterior scattered e field=*',etest,50)
cc        call prin2('exterior known e field=*',ein,50)

cc        call prin2(' *',pi,0)
cc        call prin2('exterior scattered h field=*',htest,50)
cc        call prin2('exterior known h field=*',hin,50)


c
c       check all exterior errors
c

        call crelfielderr(n,etest,ein,eerr)
        call crelfielderr(n,htest,hin,herr)
c
        call prin2('exterior e field relative error=*',eerr,1)
        call prin2('exterior h field relative error=*',herr,1)

        yes1(num)=eerr
        yhs1(num)=herr

c
c       check the solution in the interior now
c

        do 4600 i=1,n
c
        p=psin(i)
        theta=0
        z=zsin(i)
c
c       evaluate the scattered field
c
        call emullereva(eps,omega,e0,u0,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs0,zks0,p,theta,z,etest(1,i))
c
        call hmullereva(eps,omega,e0,u0,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs0,zks0,p,theta,z,htest(1,i))
c

c
c       evaluate the known data field
c
        call emullereva(eps,omega,e0,u0,m,n,rl,psout,zsout,
     1      dpdtout,dzdtout,zjsout,zksout,p,theta,z,eout(1,i))
c
        call hmullereva(eps,omega,e0,u0,m,n,rl,psout,zsout,
     1      dpdtout,dzdtout,zjsout,zksout,p,theta,z,hout(1,i))
 4600 continue


cc        call prin2(' *',pi,0)
cc        call prin2('interior scattered e field=*',etest,50)
cc        call prin2('interior known e field=*',eout,50)

cc        call prin2(' *',pi,0)
cc        call prin2('interior scattered h field=*',htest,50)
cc        call prin2('interior known h field=*',hout,50)


        call crelfielderr(n,etest,eout,eerr)
        call crelfielderr(n,htest,hout,herr)
c
        call prin2('interior e field relative error=*',eerr,1)
        call prin2('interior h field relative error=*',herr,1)

        yes0(num)=eerr
        yhs0(num)=herr


 6600   continue


c
c       print out all of the errors now
c
        iw=31
        itype=3
        call quagraph(iw,tks,yes1,num,itype,'exterior e error*')
c
        iw=32
        itype=3
        call quagraph(iw,tks,yhs1,num,itype,'exterior h error*')
c
        iw=33
        itype=3
        call quagraph(iw,tks,yes0,num,itype,'interior e error*')
c
        iw=34
        itype=3
        call quagraph(iw,tks,yhs0,num,itype,'interior h error*')
c
        iw=35
        itype=3
        call quagraph(iw,tks,rcs,num,itype,'condition number*')
c
        call prin2('tks=*',tks,num)
        call prin2('rcs=*',rcs,num)


        stop
        end
c
c
c
c
c
        subroutine crelfielderr(n,x,y,err)
        implicit real *8 (a-h,o-z)
        complex *16 x(3,1),y(3,1),cd1,cd2,cd3
c
c       calculates the relative error between the two fields
c       x and y, i.e. calculates |x - y|/|y| where |y|**2 is the
c       sum of squares of the field vector at each of the n points
c
        dd1=0
        dd2=0
c
        do 1600 i=1,n
c
        cd1=x(1,i)-y(1,i)
        cd2=x(2,i)-y(2,i)
        cd3=x(3,i)-y(3,i)
c
        dd1=dd1+abs(cd1)**2+abs(cd2)**2+abs(cd3)**2
        dd2=dd2+abs(y(1,i))**2+abs(y(2,i))**2+abs(y(3,i))**2
c
 1600 continue
c
        err=sqrt(dd1/dd2)
c
        return
        end
c
c
c
c
c
        subroutine funcurve3(t,x,y,dxdt,dydt,dxdt2,dydt2)
        implicit real *8 (a-h,o-z)
c
c       controls the surface of the field testing torus
c
c       a small circle
c
        a=.15d0
        b=.1d0
        x0=5d0
        y0=1d0
c
        x=x0+a*cos(t)
        y=y0+b*sin(t)
        dxdt=-a*sin(t)
        dydt=b*cos(t)
        dxdt2=-a*cos(t)
        dydt2=-b*sin(t)
c
        return
        end
c
c
c
c
c
        subroutine funcurve(t,x,y,dxdt,dydt,dxdt2,dydt2)
        implicit real *8 (a-h,o-z)
c
c       controls the scattering surface
c
        done=1
        pi=4*atan(done)
c
c       ellipse
c
        a=2.4d0
        b=2.1d0
        x0=4
        y0=2
c
        x=x0+a*cos(t)
        y=y0+b*sin(t)
        dxdt=-a*sin(t)
        dydt=b*cos(t)
        dxdt2=-a*cos(t)
        dydt2=-b*sin(t)
cccc        return

c
c       squiggly thing
c
        a=.2d0
        b=.3d0
c
        n=4
c
        x=2+(1+.2d0*cos(n*t))*cos(t)
        y=2+(1+.3d0*sin(t))*sin(t)

        dxdt=-(1+a*cos(n*t))*sin(t)-a*n*sin(n*t)*cos(t)
        dydt=(1+b*sin(t))*cos(t)+b*cos(t)*sin(t)
c
        dxdt2=-(1+a*cos(n*t))*cos(t)+a*n*sin(n*t)*sin(t)
     1      +a*n*sin(n*t)*sin(t)-a*n*n*cos(n*t)*cos(t)
c
        dydt2=-(1+b*sin(t))*sin(t)+b*cos(t)*cos(t)
     1      +b*cos(t)*cos(t)-b*sin(t)*sin(t)

c
        return
        end
c
c
c
c
c
        subroutine funcurve2(t,x,y,dxdt,dydt,dxdt2,dydt2)
        implicit real *8 (a-h,o-z)
c
c       controls the source surface
c
        done=1
        pi=4*atan(done)
c
c       ellipse
c
        a=.1d0
        b=.15d0
c
        x0=2.2d0
        y0=2.3d0
c
        x=x0+a*cos(t)
        y=y0+b*sin(t)
        dxdt=-a*sin(t)
        dydt=b*cos(t)
        dxdt2=-a*cos(t)
        dydt2=-b*sin(t)
c
        return
        end
c
c
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c       this is the end of the debugging code, and the
c       beginning of the routines for solving the generalised
c       debye dielectric problem
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
c       contained below are several user-callable subroutines.
c       they are:
c
c       creamuller - builds the 4n x 4n matrix solving the
c           dielectric problem on a surface of revolution using
c           muller's representation of inner and outer currents
c
c       emullereva - evaluates the e field generated using
c           electric and magnetic currents in muller's representation
c
c       hmullereva - evaluates the h field generated using
c           electric and magnetic currents in muller's representation
c
c
c       some other routines are contained below as well, which
c       are not known to have any use to the user, they are:
c
c       curlfield5 - using finite differences, calculates the curl
c           of either a muller e field or a muller h field
c
c       divefield5 - using finite differences, calculates the
c           divergence of a muller e field
c
c       divhfield5 - using finite differences, calculates the
c           divergence of a muller h field
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
        subroutine creamuller(eps,m,n,rl,ps,zs,
     1      dpdt,dzdt,norder,e1,e0,u1,u0,omega,amat)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1),dsdt(10000)
        complex *16 e1,e0,u1,u0,omega,ima,zk0,zk1,zk000,
     1      amat(4*n,4*n),work(2000000),cd
        complex *16, allocatable :: a1(:,:),b1(:,:)
        complex *16, allocatable :: a0(:,:),b0(:,:)
        complex *16, allocatable :: wsurfdiv(:,:),wsurfgrad(:,:)
        complex *16, allocatable :: wa1(:,:),wa0(:,:)
        complex *16, allocatable :: wnxcurl1(:,:),wnxcurl0(:,:)
        complex *16, allocatable :: w1(:,:),wp1(:,:),wz1(:,:)
        complex *16, allocatable :: wcc1(:,:),wccp1(:,:),wccz1(:,:)
        complex *16, allocatable :: wss1(:,:),wssp1(:,:),wssz1(:,:)
        complex *16, allocatable :: w0(:,:),wp0(:,:),wz0(:,:)
        complex *16, allocatable :: wcc0(:,:),wccp0(:,:),wccz0(:,:)
        complex *16, allocatable :: wss0(:,:),wssp0(:,:),wssz0(:,:)
        complex *16, allocatable :: wnxt(:,:)
c
c       input:
c         eps - the precision to which kernels should be evaluated
c         m - the fourier mode to compute
c         n - number of points in the curve discretization
c         rl - parameterization interval, 0 to rl
c         ps,zs - points on the generating curve
c         dpdt,dzdt - derivatives of generating curve with respect to
c             the parameterization variable
c         norder - order of quadrature to use when constructing
c             matrix operators
c         e1,e0,u1,u0 - electric permitivitties and magnetic permeabilities
c         omega - frequency
c
c       output:
c         amat - the 4n by 4n matrix which when inverted solves 
c             muller's dielectric formulation
c
c
        allocate( a1(2*n,2*n), b1(2*n,2*n) )
        allocate( a0(2*n,2*n), b0(2*n,2*n) )
c
        allocate( wsurfdiv(n,2*n), wsurfgrad(2*n,n) )        
        allocate( wa1(2*n,2*n), wa0(2*n,2*n) )
c
        allocate( wnxcurl1(2*n,2*n), wnxcurl0(2*n,2*n) )
c
        allocate( w1(n,n),wp1(n,n),wz1(n,n) )
        allocate( wcc1(n,n),wccp1(n,n),wccz1(n,n) )
        allocate( wss1(n,n),wssp1(n,n),wssz1(n,n) )
        allocate( w0(n,n),wp0(n,n),wz0(n,n) )
        allocate( wcc0(n,n),wccp0(n,n),wccz0(n,n) )
        allocate( wss0(n,n),wssp0(n,n),wssz0(n,n) )
c
        allocate( wnxt(2*n,2*n) )
c
        done=1.0d0
        ima=(0,1)
        pi=4*atan(done)
c
c       compute dsdt around the generating curve
c
        do 1200 i=1,n
        dd=dpdt(i)**2+dzdt(i)**2
        dd=sqrt(dd)
        dsdt(i)=dd
 1200   continue
c

        zk0=omega*sqrt(e0*u0)
        zk1=omega*sqrt(e1*u1)

c
c       first build some matrices that we'll need
c
        call surfdivaximat(ps,zs,dpdt,dzdt,n,m,rl,wsurfdiv)
        call surfgradaximat(ps,zs,dpdt,dzdt,n,m,rl,wsurfgrad)

c
        h=rl/n
        call creapiecesa(eps,zk1,m,n,rl,ps,zs,dpdt,dzdt,
     1      norder,w1,wp1,wz1,wcc1,wccp1,wccz1,wss1,wssp1,wssz1)
c
        call creapiecesa(eps,zk0,m,n,rl,ps,zs,dpdt,dzdt,
     1      norder,w0,wp0,wz0,wcc0,wccp0,wccz0,wss0,wssp0,wssz0)


        call creavecpotmat2x2(eps,zk1,m,n,rl,ps,zs,dpdt,dzdt,
     1      norder,wa1,wcc1,wss1,w1)
c
        call creavecpotmat2x2(eps,zk0,m,n,rl,ps,zs,dpdt,dzdt,
     1      norder,wa0,wcc0,wss0,w0)



        diag=-.5d0
        call creanxcurlmat2(eps,zk1,m,n,rl,ps,zs,dpdt,dzdt,
     1      norder,diag,wnxcurl1,w1,wcc1,wss1,wp1,
     2      wccp1,wssp1,wccz1,wssz1)
c
        diag=.5d0
        call creanxcurlmat2(eps,zk0,m,n,rl,ps,zs,dpdt,dzdt,
     1      norder,diag,wnxcurl0,w0,wcc0,wss0,wp0,
     2      wccp0,wssp0,wccz0,wssz0)
c

        call creanxtmat(n,wnxt)

c
c       there are 4 blocks in two unknowns j1 and k1, build them
c       one at a time. first, the block corresponding to the 
c       contribution of j1 to the jump in E
c
        call cmatmul601(2*n,n,wsurfgrad,n,w1,work)
        call cmatmul601(2*n,n,work,2*n,wsurfdiv,a1)

        call cmatmul601(2*n,n,wsurfgrad,n,w0,work)
        call cmatmul601(2*n,n,work,2*n,wsurfdiv,a0)

c
        do 2000 i=1,2*n
        do 1800 j=1,2*n
        amat(i,j)=ima*omega*(u1*wa1(i,j)-u0*e0/e1*wa0(i,j))
     1      -done/ima/omega/e1*(a1(i,j)-a0(i,j))
 1800   continue
 2000   continue

c
c       ... the contribution of k1 to the jump in E
c
        call cmatmul601(2*n,2*n,wnxt,2*n,wnxcurl1,b1)
        call cmatmul601(2*n,2*n,wnxt,2*n,wnxcurl0,b0)
c
        do 2800 i=1,2*n
        do 2600 j=1,2*n
        amat(i,2*n+j)=-b1(i,j)+u0/u1*b0(i,j)
 2600   continue
 2800   continue

c
c       ... the contribution of j1 to the jump in H
c
        do 3600 i=1,2*n
        do 3400 j=1,2*n
        amat(2*n+i,j)=b1(i,j)-e0/e1*b0(i,j)
 3400 continue
 3600 continue

c
c       ... the contribution of k1 to the jump in H
c
        do 4000 i=1,2*n
        do 3800 j=1,2*n
        amat(2*n+i,2*n+j)=ima*omega*(e1*wa1(i,j)-e0*u0/u1*wa0(i,j))
     1      -done/ima/omega/u1*(a1(i,j)-a0(i,j))
 3800 continue
 4000 continue

c
        return
        end
c
c
c
c
c
        subroutine emullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p,theta,z,efield)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1)
        complex *16 omega,e,u,ima,zk,zjs(2,1),zks(2,1),
     1      efield(1),vals1(10),vals2(10),vals3(10),
     2      work(10000)
c
c       given muller currents zjs and zks on the surface ps,zs,
c       evaluate the electric field at the point p,theta,z
c
c       input:
c         eps - 
c         omega -
c         e -
c         u -
c         m -
c         n -
c         rl -
c         ps,zs -
c         dpdt,dzdt -
c         zjs,zks -
c         p,theta,z -
c
c       output:
c         efield - the electric field in cylindrical coordinates at
c             at the point p,theta,z
c
c
        done=1.0d0
        ima=(0,1)
        pi=4*atan(done)
c
        zk=omega*sqrt(e*u)
c
c       ...calculate the vector potential A
c
        call vecpoteval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      zjs,p,theta,z,vals1)
c
c       ...calculate the gradient of phi
c
        call surfdivaxi(ps,zs,dpdt,dzdt,n,m,rl,zjs,work)
        call ghfun2grad(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      work,p,theta,z,vals2)

c
c       ...calculate the curl of the vector potential Q
c
        call curlaeval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      zks,p,theta,z,vals3)

c
c       ...and combine them
c
        efield(1)=ima*omega*u*vals1(1)-vals2(1)/ima/omega/e-vals3(1)
        efield(2)=ima*omega*u*vals1(2)-vals2(2)/ima/omega/e-vals3(2)
        efield(3)=ima*omega*u*vals1(3)-vals2(3)/ima/omega/e-vals3(3)
c
        return
        end
c
c
c
c
c
        subroutine hmullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p,theta,z,hfield)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1)
        complex *16 ima,zk,zjs(2,1),zks(2,1),hfield(1),
     1      vals1(10),vals2(10),vals3(10),work(10000)
c
c       given muller currents zjs and zks on the surface ps,zs,
c       evaluate the magnetc field at the point p,theta,z
c
c       input:
c         eps - 
c         omega -
c         e -
c         u -
c         m -
c         n -
c         rl -
c         ps,zs -
c         dpdt,dzdt -
c         zjs,zks -
c         p,theta,z -
c
c       output:
c         hfield - the magnetic field in cylindrical coordinates at
c             at the point p,theta,z
c
c
        done=1.0d0
        ima=(0,1)
        pi=4*atan(done)
c
        zk=omega*sqrt(e*u)
c
c       ...calculate the vector potential Q
c
        call vecpoteval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      zks,p,theta,z,vals1)
c
c       ...calculate the gradient of psi
c
        call surfdivaxi(ps,zs,dpdt,dzdt,n,m,rl,zks,work)
        call ghfun2grad(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      work,p,theta,z,vals2)
c
c       ...calculate the curl of the vector potential A
c
        call curlaeval(eps,zk,m,n,rl,ps,zs,dpdt,dzdt,
     1      zjs,p,theta,z,vals3)

c
c       ...and combine them
c
        hfield(1)=ima*omega*e*vals1(1)-vals2(1)/ima/omega/u+vals3(1)
        hfield(2)=ima*omega*e*vals1(2)-vals2(2)/ima/omega/u+vals3(2)
        hfield(3)=ima*omega*e*vals1(3)-vals2(3)/ima/omega/u+vals3(3)
c
        return
        end
c
c
c
c
c
        subroutine curlfield5(eps,omega,e,u,m,n,rl,ps,zs,dpdt,
     1      dzdt,ifefield,zjs,zks,p,theta,z,h,curl)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1)
        complex *16 omega,e,u,zk,zjs(2,1),zks(2,1),
     1      curl(1),ima,vp1(10),vp2(10),
     1      vs1(10),vs2(10),vz1(10),vz2(10),dp(10),ds(10),
     2      dz(10),vals(10)
c
c       compute the curl of an H field or an E field using
c       finite differences in cylindrical coordinates - this is
c       most likely only a routine that would be used in debugging
c
        done=1.0d0
        pi=4*atan(done)
        ima=(0,1)
c
        zk=omega*sqrt(e*u)
c
        if (ifefield .eq. 1) goto 2000
c
c       if here, construct the curl of an h field
c
        call hmullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p-h,theta,z,vp1)
        call hmullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p+h,theta,z,vp2)

        call hmullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p,theta-h,z,vs1)
        call hmullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p,theta+h,z,vs2)

        call hmullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p,theta,z-h,vz1)
        call hmullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p,theta,z+h,vz2)

        call hmullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p,theta,z,vals)

c
        goto 2400
c
 2000   continue
c
c       if here, construct the curl of an e field
c
        call emullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p-h,theta,z,vp1)
        call emullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p+h,theta,z,vp2)

        call emullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p,theta-h,z,vs1)
        call emullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p,theta+h,z,vs2)

        call emullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p,theta,z-h,vz1)
        call emullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p,theta,z+h,vz2)

        call emullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p,theta,z,vals)

c
 2400   continue
c
c       compute the individual derivatives
c
        dp(1)=(vp2(1)-vp1(1))/2/h
        dp(2)=(vp2(2)-vp1(2))/2/h
        dp(3)=(vp2(3)-vp1(3))/2/h
c
        ds(1)=(vs2(1)-vs1(1))/2/h
        ds(2)=(vs2(2)-vs1(2))/2/h
        ds(3)=(vs2(3)-vs1(3))/2/h
c
        dz(1)=(vz2(1)-vz1(1))/2/h
        dz(2)=(vz2(2)-vz1(2))/2/h
        dz(3)=(vz2(3)-vz1(3))/2/h
c
c       and assemble the curl
c
        curl(1)=1/p*ds(3)-dz(2)
        curl(2)=dz(1)-dp(3)
        curl(3)=1/p*(vals(2)+p*dp(2)-ds(1))
c
        return
        end
c
c
c
c
c
        subroutine divefield5(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p,theta,z,h)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1)
        complex *16 omega,e,u,zjs(2,1),zks(2,1),
     1      ima,zk,vp1(10),vp2(10),vs1(10),vs2(10),
     1      vz1(10),vz2(10),dp,ds,dz,dd
c
c       take the divergence of the muller e field to make sure it is zero
c
c
        ima=(0,1)
        done=1.0d0
        pi=4*atan(1.0d0)
c
        zk=omega*sqrt(e*u)
c

        call emullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p-h,theta,z,vp1)
        call emullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p+h,theta,z,vp2)

        call emullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p,theta-h,z,vs1)
        call emullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p,theta+h,z,vs2)

        call emullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p,theta,z-h,vz1)
        call emullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p,theta,z+h,vz2)


c
c       and now compute the divergence
c
        dp=1/p*((p+h)*vp2(1)-(p-h)*vp1(1))/2/h
        ds=1/p*(vs2(2)-vs1(2))/2/h
        dz=(vz2(3)-vz1(3))/2/h
c
        dd=dp+ds+dz
        call prin2('divergence of the e field=*',dd,2)
c
        return
        end
c
c
c
c
c
        subroutine divhfield5(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p,theta,z,h)
        implicit real *8 (a-h,o-z)
        real *8 ps(1),zs(1),dpdt(1),dzdt(1)
        complex *16 omega,e,u,zjs(2,1),zks(2,1),
     1      ima,zk,vp1(10),vp2(10),vs1(10),vs2(10),
     1      vz1(10),vz2(10),dp,ds,dz,dd
c
c       take the divergence of the muller h field to make sure it is zero
c
        ima=(0,1)
        done=1.0d0
        pi=4*atan(1.0d0)
c
        zk=omega*sqrt(e*u)
c

        call hmullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p-h,theta,z,vp1)
        call hmullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p+h,theta,z,vp2)

        call hmullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p,theta-h,z,vs1)
        call hmullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p,theta+h,z,vs2)

        call hmullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p,theta,z-h,vz1)
        call hmullereva(eps,omega,e,u,m,n,rl,ps,zs,
     1      dpdt,dzdt,zjs,zks,p,theta,z+h,vz2)

c
c       and now compute the divergence
c
        dp=1/p*((p+h)*vp2(1)-(p-h)*vp1(1))/2/h
        ds=1/p*(vs2(2)-vs1(2))/2/h
        dz=(vz2(3)-vz1(3))/2/h
c
        dd=dp+ds+dz
        call prin2('divergence of the h field=*',dd,2)
c
        return
        end
