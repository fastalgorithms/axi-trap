"""
Simple demo with multiple subplots.
"""
import numpy as np
import matplotlib.pyplot as plt

from matplotlib import rc

from matplotlib.path import Path
from matplotlib.patches import PathPatch
import matplotlib as mpl

plt.rc("font", size=12)
#a = np.mean(x) - 2.2*np.std(x)
#b = np.mean(x) + 2.2*np.std(x)


#cb=pt.colorbar(shrink=.9)
#pt.clim([a,b])
#pt.title("total t component")
#pt.savefig("plot41.pdf")
#pt.show()




x1 = np.linspace(0.0, 5.0)
x2 = np.linspace(0.0, 2.0)

y1 = np.cos(2 * np.pi * x1) * np.exp(-x1)
y2 = np.cos(2 * np.pi * x2)

files = iter(['plot31', 'plot41', 'plot51',
              'plot32', 'plot42', 'plot52',
              'plot33', 'plot43', 'plot53',
              'plot34', 'plot44', 'plot54',
              'plot35', 'plot45', 'plot55',
              'plot36', 'plot46', 'plot56',
              'plot37', 'plot47', 'plot57',
              'plot38', 'plot48', 'plot58',
              'plot39', 'plot49', 'plot59'])



rc('text', usetex=True)



nr = 3
nc = 5
fig, axes = plt.subplots(nrows=nr, ncols=nc, sharex=True, sharey=True)
#fig, axes = plt.subplots(nrows=3, ncols=5)


for col in range(nc):
    for row in range(nr):
        s = files.next()
        #print 'file = ', s

        sfile1 = s + '.dat1'
        sfile2 = s + '.dat2'

        ax = axes[row,col]
        #im = ax.imshow(np.random.random((10,10)), vmin=0, vmax=1,
        #               extent=[0,10,0,10])


        x = np.loadtxt(sfile1)
        x = x.reshape(   51,   51, order="F")
        y = np.loadtxt(sfile2)
        path = Path(y)
        patch = PathPatch(path, facecolor="none")
        ax.add_patch(patch)
        im = ax.imshow(x, extent=[ 1.00, 3.00,-2.00, 2.00],
                       clip_path=patch, clip_on=True)

        im.set_clim([-.75,.75])
        
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.get_xaxis().tick_bottom()
        ax.get_yaxis().tick_left()
        
        #ax.plot(x1, y1, 'ko-')

        ax.set_aspect('equal')
        #ax.set_xbound(1, 3)
        #ax.set_ybound(-2, 2)


fig.subplots_adjust(right=0.8)
cbar_ax = fig.add_axes([0.85, 0.15, 0.025, 0.7])
fig.colorbar(im, cax=cbar_ax)
        
#cax, kw = mpl.colorbar.make_axes([ax for ax in axes.flat])
#plt.colorbar(im, cax=cax, **kw)

        
        
for col in range(nc):
    ax = axes[nr-1,col]
    ax.set_xticks([1.0, 2.0, 3.0])


ax = axes[2,0]
ax.set_xlabel(r'$\lambda = 5.52819$' '\n' r'$\Phi=0.09837$',
              fontsize=10)

ax = axes[2,1]
ax.set_xlabel(r'$\lambda = 5.56546$' '\n' r'$\Phi=0.11103$',
              fontsize=10)

ax = axes[2,2]
ax.set_xlabel(r'$\lambda = 6.13551$' '\n' r'$\Phi=0.34113$',
              fontsize=10)

ax = axes[2,3]
ax.set_xlabel(r'$\lambda = 6.34490$' '\n' r'$\Phi=0.19677$',
              fontsize=10)

ax = axes[2,4]
ax.set_xlabel(r'$\lambda = 6.55792$' '\n' r'$\Phi=0.01727$',
              fontsize=10)

for row in range(nr):
    ax = axes[row,0]
    ax.set_yticks((-2.0, 0.0, 2.0))

ax = axes[0,0]
ax.set_ylabel(r'$r$-component')

ax = axes[1,0]
ax.set_ylabel(r'$\varphi$-component')

ax = axes[2,0]
ax.set_ylabel(r'$z$-component')

        

#ax = axes[nr-1,0]
#ax.set_xlim(1.0,3.0)

#fig.add_subplot(111, frameon=False)
# hide tick and tick label of the big axes
#plt.tick_params(labelcolor='none', top='off', bottom='off',
#                left='off', right='off')
#plt.xlabel("common X")
#plt.ylabel("common Y")

#plt.xlim(1.0, 3.0)
#plt.ylim(-2.0, 2.0)


plt.subplots_adjust(hspace=.2, wspace=.1)
plt.savefig('alleigs.pdf', transparent=True)
plt.show()
exit()


#for ax in axes.flat:
#    im = ax.imshow(np.random.random((10,10)), vmin=0, vmax=1)



# isub = 0
# for s in files:
#     sfile1 = s + '.dat1'
#     sfile2 = s + '.dat2'
#     isub = isub + 1
#     ax = fig.subplot(5, 3, isub)
#     im = ax.imshow(np.random.random((10,10)), vmin=0, vmax=1)

#     #x = np.loadtxt(sfile1)
#     #x = x.reshape(   51,   51, order="F")
#     #y = np.loadtxt(sfile2)
#     #path = Path(y)
#     #patch = PathPatch(path, facecolor="none")
#     #ax.add_patch(patch)
#     #im = ax.imshow(x, extent=[ 1.00, 3.00,-2.00, 2.00],
#     #            clip_path=patch, clip_on=True)
#     #ax.set_aspect('equal')

# cax, kw = mpl.colorbar.make_axes([ax for ax in axes.flat])
# plt.colorbar(im, cax=cax, **kw)
# plt.show()





# isub = 0
# for s in files:

#     s = 'plot31'
#     sfile1 = s + '.dat1'
#     sfile2 = s + '.dat2'

#     #isub = isub + 1
#     #plt.subplot(5, 3, isub)

#     x = np.loadtxt(sfile1)
#     x = x.reshape(   51,   51, order="F")
#     y = np.loadtxt(sfile2)
#     path = Path(y)
#     patch = PathPatch(path, facecolor="none")
#     plt.gca().add_patch(patch)
#     plt.imshow(x, extent=[ 1.00, 3.00,-2.00, 2.00],
#                clip_path=patch, clip_on=True)
#     #plt.axes().set_aspect('equal')
#     plt.clim([-2,2])
#     plt.axis('off')
#     plt.savefig(s + '.pdf')
#     plt.clf()    





# #plt.plot(x1, y1, 'ko-')

# plt.subplot(5, 3, 2)
# plt.plot(x2, y2, 'r.-')

# plt.subplot(5, 3, 3)
# plt.plot(x2, y2, 'r.-')

# #
# # second eigenvalue
# #
# plt.subplot(5, 3, 4)
# plt.plot(x1, y1, 'ko-')

# plt.subplot(5, 3, 5)
# plt.plot(x2, y2, 'r.-')

# plt.subplot(5, 3, 6)
# plt.plot(x2, y2, 'r.-')



#
# construct the titles and labels
#
# plt.subplot(5, 3, 1)
# plt.title(r'$r$-component')
# plt.ylabel(r'$\lambda = 5.23423423$' '\n' r'$z$-axis')

# plt.subplot(5, 3, 2)
# plt.title(r'$\varphi$-component')

# plt.subplot(5, 3, 3)
# plt.title(r'$z$-component')

# plt.subplot(5, 3, 4)
# plt.ylabel(r'$\lambda = 5.23423423$' '\n' r'$z$-axis')

# plt.subplot(5, 3, 7)
# plt.ylabel(r'$\lambda = 5.23423423$' '\n' r'$z$-axis')

# plt.subplot(5, 3, 10)
# plt.ylabel(r'$\lambda = 5.23423423$' '\n' r'$z$-axis')

# plt.subplot(5, 3, 13)
# plt.ylabel(r'$\lambda = 5.23423423$' '\n' r'$z$-axis')






#plt.savefig('alleigs.pdf')
#plt.show()
