program beltrami_example2
  implicit real *8 (a-h,o-z)
  real *8 ps(10000),zs(10000),dpdt(10000), &
      dzdt(10000),dpdt2(10000),dzdt2(10000),dsdt(10000), &
      psg(10000),zsg(10000),dpdtg(10000),dsdtg(10000), &
      dzdtg(40000),dpdtg2(10000),dzdtg2(10000), &
      errs(10000),xs(10000),whts(10000), & 
      tks(10000),yes(10000),yhs(10000), &
      ptest(10000),ztest(10000), &
      err1(10000),err2(10000),err3(10000),err4(10000), &
      ts(10000),ts2(10000),ts3(10000),dtds(10000), &
      dtds2(10000), &
      ps1(10000),zs1(10000),&
      dpds1(10000),dzds1(10000),dpds2(10000),dzds2(10000),&
      dpds21(10000),dzds21(10000),&
      surfrhom(4, 1000000),&
      volh(6, 100000), surfm(6, 1000000), &
      rvals(100000), rks(100000), slice(4,100000)

  real *8 :: ps_in(10000), zs_in(10000)
  real *8 :: dpdt_in(10000), dzdt_in(10000)
  real *8 :: dpdt2_in(10000), dzdt2_in(10000)
  real *8 :: dsdt_in(10000)
  real *8 :: ps_out(10000), zs_out(10000)
  real *8 :: dpdt_out(10000), dzdt_out(10000)
  real *8 :: dpdt2_out(10000), dzdt2_out(10000)
  real *8 :: dsdt_out(10000)
  real *8 :: psg_out(10000), zsg_out(10000)
  real *8 :: dpdtg_out(10000), dzdtg_out(10000)
  real *8 :: dpdtg2_out(10000), dzdtg2_out(10000)
  real *8 :: dsdtg_out(10000)

  real *8, allocatable :: pzgrid(:,:,:), pvals(:,:)
  real *8, allocatable :: tvals(:,:), zvals(:,:)
  
  complex *16 :: hfield_in(3,10000), htan_in(2,10000)
  complex *16 :: hfield_out(3,10000), htan_out(2,10000)
  complex *16 :: flux_in, flux_out, flux_tor, flux_pol
  complex *16 :: rho_in(10000), rho_out(10000)
  complex *16 :: rhom_in(10000), rhom_out(10000)
  complex *16 :: zjs_in(2,10000), zjs_out(2,10000)
  complex *16 :: zms_in(2,10000), zms_out(2,10000)
  complex *16 :: alpha_in, alpha_out
  complex *16 :: bfield_in(10), bfield_out(10), bfield(10)
  complex *16 :: sigma(10000), field(10000)
  complex *16 :: field2(10000), sigma2(10000)

  complex *16 :: rhom2(10000), alpha2, beta2
  complex *16 :: rho2(10000), zwind1, zwind2
  complex *16 :: zjs2(2,10000), zms2(2,10000)

  complex *16, allocatable :: cmat(:,:), fieldimage(:,:,:)
  complex *16, allocatable :: cmat_out(:,:), cmat_in(:,:)

  complex *16 zk,zk0,cd,ima,cdh,cdb,zk2,cdt,cds,cd2, &
      zkinit,cda, flux, hdotn,&
      wz(1000000),wz2(1000000),sz(100000),&
      work(2000000), work2(2000000),&
      rho1(10000),rhom1(10000),alpha1,beta1,&
      rho(10000),rhom(10000),alpha,beta,&
      efield(3,10000),hfield(3,10000),&
      etest(3,10000),htest(3,10000),&
      etan(2,10000),htan(2,10000), zroot,&
      wndot(2000000),rhs(100000),sol(100000),&
      vals1(100),vals2(100),vals3(100),&
      hin(2,10000),zjs(2,10000),zms(2,10000),&
      zjs1(2,10000),zms1(2,10000),wsave(2000000),&
      ctemp1(10000),ctemp2(10000), u1, u2, u3, uu,&
      f1, f2, f3, ff, w, f12, f13, f23, f123,&
      vec1(10),vec2(10),db1dp, db2dp, db3dp,&
      evals1(3,10000),evals2(3,10000),evals3(3,10000),&
      hvals1(3,10000),hvals2(3,10000),hvals3(3,10000),&
      u(10000),ux(10000),uxx(10000), det(10),&
      bfield1(10), bfield2(10), db1dz, db2dz, db3dz,&
      r1(10000), r2(10000), denom1, denom2,&
      db1da, db2da, db3da, diver

  done=1
  ima=(0,1)
  pi=4*atan(done)
  
  call prini(6,13)

  !
  ! create the interior and exterior scattering surfaces
  !
  !
  ! load  the boundary curve
  !
  n = 26
  n = 50
  n = 100
  !n = 200
  !n = 400
  call prinf('n=*', n, 1)

  if (n .eq. 26) then
    call load_ex1_n26(ps_out, zs_out, dpdt_out, dzdt_out, &
        dpdt2_out, dzdt2_out)
    call load_interior_n26(ps_in, zs_in, dpdt_in, dzdt_in, &
        dpdt2_in, dzdt2_in)
    call load_interior_phi05_n26(ps_in, zs_in, dpdt_in, dzdt_in, &
        dpdt2_in, dzdt2_in)
  elseif (n .eq. 50) then
    call load_ex1_n50(ps_out, zs_out, dpdt_out, dzdt_out, &
        dpdt2_out, dzdt2_out)
    call load_interior_n50(ps_in, zs_in, dpdt_in, dzdt_in, &
        dpdt2_in, dzdt2_in)
    call load_interior_phi05_n50(ps_in, zs_in, dpdt_in, dzdt_in, &
        dpdt2_in, dzdt2_in)
  elseif (n .eq. 100) then
    call load_ex1_n100(ps_out, zs_out, dpdt_out, dzdt_out, &
        dpdt2_out, dzdt2_out)
    call load_interior_n100(ps_in, zs_in, dpdt_in, dzdt_in, &
        dpdt2_in, dzdt2_in)
    call load_interior_phi05_n100(ps_in, zs_in, dpdt_in, dzdt_in, &
        dpdt2_in, dzdt2_in)
  elseif (n .eq. 200) then
    call load_ex1_n200(ps_out, zs_out, dpdt_out, dzdt_out, &
        dpdt2_out, dzdt2_out)
    call load_interior_n200(ps_in, zs_in, dpdt_in, dzdt_in, &
        dpdt2_in, dzdt2_in)
    call load_interior_phi05_n200(ps_in, zs_in, dpdt_in, dzdt_in, &
        dpdt2_in, dzdt2_in)
  elseif (n .eq. 400) then
    call load_exterior_n400(ps_out, zs_out, dpdt_out, dzdt_out, &
        dpdt2_out, dzdt2_out)
    call load_interior_n400(ps_in, zs_in, dpdt_in, dzdt_in, &
        dpdt2_in, dzdt2_in)
    call load_interior_phi05_n400(ps_in, zs_in, dpdt_in, dzdt_in, &
        dpdt2_in, dzdt2_in)
  endif


  rl = 2*pi
  h = rl/n

  !
  ! load other geometry, to test
  !
  do i = 1,n
    t = (i-1)*h
    !call fcurveout(t, ps_out(i), zs_out(i), dpdt_out(i), &
    !    dzdt_out(i), dpdt2_out(i), dzdt2_out(i))
    dsdt_out(i) = sqrt(dpdt_out(i)**2 + dzdt_out(i)**2)

    !call fcurvein(t, ps_in(i), zs_in(i), dpdt_in(i), &
    !    dzdt_in(i), dpdt2_in(i), dzdt2_in(i))
    dsdt_in(i) = sqrt(dpdt_in(i)**2 + dzdt_in(i)**2)
  end do
  

  ifres = 0
  if (ifres .eq. 1) then
    call dffti(n, wsave)

    do i = 1,n 
      ps_in(i) = ps_in(i)/n
      zs_in(i) = zs_in(i)/n
      dpdt_in(i) = dpdt_in(i)/n
      dzdt_in(i) = dzdt_in(i)/n
      dpdt2_in(i) = dpdt2_in(i)/n
      dzdt2_in(i) = dzdt2_in(i)/n
      dsdt_in(i) = dsdt_in(i)/n
      ps_out(i) = ps_out(i)/n
      zs_out(i) = zs_out(i)/n
      dpdt_out(i) = dpdt_out(i)/n
      dzdt_out(i) = dzdt_out(i)/n
      dpdt2_out(i) = dpdt2_out(i)/n
      dzdt2_out(i) = dzdt2_out(i)/n
      dsdt_out(i) = dsdt_out(i)/n
    end do
    
    call dfftf(n,ps_in,wsave)
    call dfftf(n,zs_in,wsave)
    call dfftf(n,dpdt_in,wsave)
    call dfftf(n,dzdt_in,wsave)
    call dfftf(n,dpdt2_in,wsave)
    call dfftf(n,dzdt2_in,wsave)
    call dfftf(n,dsdt_in,wsave)
    call dfftf(n,ps_out,wsave)
    call dfftf(n,zs_out,wsave)
    call dfftf(n,dpdt_out,wsave)
    call dfftf(n,dzdt_out,wsave)
    call dfftf(n,dpdt2_out,wsave)
    call dfftf(n,dzdt2_out,wsave)
    call dfftf(n,dsdt_out,wsave)

    call prin2('fft of ps_in = *', ps_in, n)
    call prin2('fft of zs_in = *', zs_in, n)
    call prin2('fft of dpdt_in = *', dpdt_in, n)
    call prin2('fft of dzdt_in = *', dzdt_in, n)
    call prin2('fft of dpdt2_in = *', dpdt2_in, n)
    call prin2('fft of dzdt2_in = *', dzdt2_in, n)
    call prin2('fft of dsdt_in = *', dsdt_in, n)
    call prin2('fft of ps_out = *', ps_out, n)
    call prin2('fft of zs_out = *', zs_out, n)
    call prin2('fft of dpdt_out = *', dpdt_out, n)
    call prin2('fft of dzdt_out = *', dzdt_out, n)
    call prin2('fft of dpdt2_out = *', dpdt2_out, n)
    call prin2('fft of dzdt2_out = *', dzdt2_out, n)
    call prin2('fft of dsdt_out = *', dsdt_out, n)

    stop

  end if

  
  

  
  iw=11
  itype=1
  call pyplot2(iw, ps_in, zs_in, n, itype, &
      ps_out, zs_out, n, itype, 'boundary of toroidal shell*')

  !
  !    set some subroutine parameters
  !
  zk = 2.281569790667874d0
  call prin2(' *',pi,0)
  call prin2(' *',pi,0)
  call prin2('zk=*',zk,2)
  
  mode = 0
  m = mode
  call prinf('mode=*',mode,1)

  eps=1.0d-12
  call prin2('kernel eval precision=*',eps,1)

  norder=4
  norder=8
  norder=16
  call prinf('alpert order=*',norder,1)

  !
  ! specify rhs
  !
  do i = 1,2*n
    rhs(i) = 0
  end do


  iflux = 5
  if (iflux .eq. 5) then
    ! inner phi = 0.5
    flux_tor = 2.156652670699272d0
    flux_tor = 2.157082003669861d0
    flux_tor = 2.157081806131285d0
    flux_tor = 2.157081806131163d0
    flux_tor = 2.157081806131173d0
    flux_pol = -pi
  elseif (iflux .eq. 1) then
    ! inner phi = 1
    flux_tor = 4.685620754234608d0
    flux_pol = -2*pi
  elseif (iflux .eq. 15) then
    ! inner phi = 1.5
    flux_tor = 7.303155731965825d0
    flux_pol = -2*pi*1.5d0
  end if

  

  
  if (mode .eq. 0) then
    rhs(2*n+1) = -flux_tor*zk
    rhs(2*n+2) = flux_pol*zk
  else
    rhs(2*n+1) = 0
    rhs(2*n+2) = 0
  end if
  
  print *
  print *
  call prin2('toroidal flux = *', flux_tor, 2)
  call prin2('poloidal flux = *', flux_pol, 2)
  
  !
  ! finally build the matrix and solve
  !
  lda = 2*n+2
  ind = 1
  allocate( cmat(lda,lda) )
  call creabeltrami_shell(eps, zk, m, n, rl, ps_in, zs_in, &
      dpdt_in, dzdt_in, dpdt2_in, dzdt2_in, ps_out, zs_out, &
      dpdt_out, dzdt_out, dpdt2_out, dzdt2_out, &
      norder, ind, cmat)

  call zgausselim(lda, cmat, rhs, info, sol, dcond)
  call prin2('after zgausslim, dcond = *', dcond, 1)

  call ccopy601(n, sol, rhom_in)
  call ccopy601(n, sol(n+1), rhom_out)

  alpha_in = sol(2*n+1)
  alpha_out = sol(2*n+2)
  call prin2('rhom_in = *', rhom_in, 30)
  call prin2('rhom_out = *', rhom_out, 30)
  call prin2('alpha_in = *', alpha_in, 2)
  call prin2('alpha_out = *', alpha_out, 2)

  
  
  
  !
  ! test the field at a point
  !
  print *
  print *
  print *, '... evaluating beltrami field  ...'

  !
  ! form the current on the inner surface
  !
  !call formbcurr1(eps, zk, m, n, rl, ps_in, zs_in, &
  !    dpdt_in, dzdt_in, dpdt2_in, dzdt2_in, rhom_in, &
  !    alpha_in, zms_in)
  call beltrami_current_outgoing(zk, m, n, rl, ps_in, zs_in, &
      dpdt_in, dzdt_in, dpdt2_in, dzdt2_in, rhom_in, &
      alpha_in, zms_in)

  !
  ! form the current on the outer surface
  !
  !call beltramicurrent(eps, zk, m, n, rl, ps_out, zs_out, &
  !    dpdt_out, dzdt_out, dpdt2_out, dzdt2_out, norder, &
  !    rhom_out, alpha_out, rho_out, zjs_out, zms_out)

  call beltrami_current_incoming(zk, m, n, rl, ps_out, zs_out, &
      dpdt_out, dzdt_out, dpdt2_out, dzdt2_out, &
      rhom_out, alpha_out, zms_out)

  !call prin2('zms_in = *', zms_in, 30)
  !call prin2('zms_out = *', zms_out, 30)



  
  !
  ! and evaluate the separate fields
  !
  p1 = .5d0
  z1 = -1.5d0
  theta1 = 0
  call prin2('test point, p1 = *', p1, 1)
  call prin2('test point, theta1 = *', theta1, 1)
  call prin2('test point, z1 = *', z1, 1)
  call beltramieva(eps, zk, m, n, rl, ps_in, zs_in,  &
      dpdt_in, dzdt_in, dpdt2_in, dzdt2_in, rhom_in, &
      alpha_in, zms_in, p1, theta1, z1, bfield_in)

  call beltramieva(eps, zk, m, n, rl, ps_out, zs_out,  &
      dpdt_out, dzdt_out, dpdt2_out, dzdt2_out, rhom_out, &
      alpha_out, zms_out, p1, theta1, z1, bfield_out)

  bfield(1) = bfield_in(1) + bfield_out(1)
  bfield(2) = bfield_in(2) + bfield_out(2)
  bfield(3) = bfield_in(3) + bfield_out(3)
  call prin2('from integral equation, field = *', bfield, 6)

  write(6,*) 'Br = ', bfield(1)
  write(6,*) 'Bphi = ', bfield(2)
  write(6,*) 'Bz = ', bfield(3)


  iferr = 0
  if (iferr .eq. 1) then
    call zffti(n, wsave)
    do i = 1,n
      !rhom_in(i) = rhom_in(i)*dsdt_in(i)/n
      !rhom_out(i) = rhom_out(i)*dsdt_out(i)/n
    end do
    call zfftf(n, rhom_in, wsave)
    call zfftf(n, rhom_out, wsave)

    call prin2('fft of rhom_in dsdt_in = *', rhom_in, 2*n)
    call prin2('fft of rhom_out dsdt_out = *', rhom_out, 2*n)
    call zfftb(n, rhom_in, wsave)
    call zfftb(n, rhom_out, wsave)
  endif

  
  !
  ! make a heat plot of the thing
  !
  a = 0
  b = 2
  nr = 101

  c = -2
  d = 2
  ni = 101

  allocate(pzgrid(2,ni,nr))
  do i = 1,nr
    do j = 1,ni
      p1 = a + (b-a)*i/(nr+1)
      z1 = d - (d-c)*j/(ni+1)
      pzgrid(1,j,i) = p1
      pzgrid(2,j,i) = z1
    end do
  end do
  
  print *
  print *, '---making plot of the beltrami field---'

  dtot = nr*ni*done
  ntot = 0

  allocate(pvals(ni,nr), tvals(ni,nr), zvals(ni,nr))

  do i = 1, nr
    do j = 1, ni

      ntot = ntot + 1
      if (mod(ntot,250) .eq. 1) then
        print *, 'percent done = ', ntot/dtot
      endif
          
      p1 = pzgrid(1,j,i)
      z1 = pzgrid(2,j,i)
      theta1 = 0

      call beltramieva(eps, zk, m, n, rl, ps_in, zs_in, &
          dpdt_in, dzdt_in, dpdt2_in, dzdt2_in, rhom_in,& 
          alpha_in, zms_in, p1, theta1, z1, bfield_in)

      call beltramieva(eps, zk, m, n, rl, ps_out, zs_out,  &
          dpdt_out, dzdt_out, dpdt2_out, dzdt2_out, rhom_out, &
          alpha_out, zms_out, p1, theta1, z1, bfield_out)

      bfield(1) = bfield_in(1) + bfield_out(1)
      bfield(2) = bfield_in(2) + bfield_out(2)
      bfield(3) = bfield_in(3) + bfield_out(3)
      
      pvals(j,i) = bfield(1)
      tvals(j,i) = bfield(2)
      zvals(j,i) = bfield(3)
          
    enddo
  enddo

  iw = 65
  call pyimage2_shell(iw, ni, nr, pvals, ps_out, zs_out, n, &
      ps_in, zs_in, n, a, b, &
      c, d, 'r component*')

  iw = 66
  call pyimage2_shell(iw, ni, nr, tvals, ps_out, zs_out, n,  &
      ps_in, zs_in, n, a, b, &
      c, d, 'theta component*')

  iw = 67
  call pyimage2_shell(iw, ni, nr, zvals, ps_out, zs_out, n,  &
      ps_in, zs_in, n, a, b, &
      c, d, 'z component*')

  stop
end program beltrami_example2







!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc



subroutine fcurveout(t,x,y,dxdt,dydt,dxdt2,dydt2)
  implicit real *8 (a-h,o-z)
  !
  ! controls the surface of the outer boundary 
  !
  done=1
  pi=4*atan(done)


  x0 = 1.0d0
  y0 = 0
  a = .95d0
  b = 1.95d0

  x = x0 + a*cos(t)
  y = y0 + b*sin(t)
  dxdt = -a*sin(t)
  dydt = b*cos(t)
  dxdt2 = -a*cos(t)
  dydt2 = -b*sin(t)
  return

  !
  ! an antoine geometry
  !  
  e = .95d0
  delt = .3d0
  alpha = asin(delt)
  rkap = 2.0d0

  x0 = 1.0d0
  y0 = 0
     
  x = x0 + e*cos(t+alpha*sin(t))
  y = y0 + e*rkap*sin(t)
  
  dxdt = -e*sin(t+alpha*sin(t))*(1+alpha*cos(t))
  dydt = e*rkap*cos(t)
  
  dxdt2 = -e*cos(t+alpha*sin(t))*(1+alpha*cos(t))**2 &
      +e*sin(t+alpha*sin(t))*alpha*sin(t)
  dydt2 = -e*rkap*sin(t)
  
  return
end subroutine fcurveout





subroutine fcurvein(t,x,y,dxdt,dydt,dxdt2,dydt2)
  implicit real *8 (a-h,o-z)
  !
  !       controls the surface of the inner boundary
  !
  x0 = 1.2d0
  y0 = 0
  a = .25d0
  b = .45d0

  x = x0 + a*cos(t)
  y = y0 + b*sin(t)
  dxdt = -a*sin(t)
  dydt = b*cos(t)
  dxdt2 = -a*cos(t)
  dydt2 = -b*sin(t)
  return

  e = .95d0
  e = e/2
  delt = .3d0
  alpha = asin(delt)
  rkap = 2.0d0

  x0 = 1.0d0
  y0 = 0
     
  x = x0 + e*cos(t+alpha*sin(t))
  y = y0 + e*rkap*sin(t)

  dxdt = -e*sin(t+alpha*sin(t))*(1+alpha*cos(t))
  dydt = e*rkap*cos(t)

  dxdt2 = -e*cos(t+alpha*sin(t))*(1+alpha*cos(t))**2 &
      +e*sin(t+alpha*sin(t))*alpha*sin(t)
  dydt2 = -e*rkap*sin(t)
  
  return
end subroutine fcurvein
